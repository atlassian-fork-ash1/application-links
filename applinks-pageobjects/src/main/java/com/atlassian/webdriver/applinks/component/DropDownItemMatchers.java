package com.atlassian.webdriver.applinks.component;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.is;

/**
 * Matchers for {@link com.atlassian.webdriver.applinks.component.DropDownItem}
 *
 * @since 4.3
 */
public final class DropDownItemMatchers {
    private DropDownItemMatchers() {
        throw new AssertionError("Don't instantiate me");
    }

    @Nonnull
    public static Matcher<DropDownItem> hasName(String name) {
        return hasNameThat(is(name));
    }

    @Nonnull
    public static Matcher<DropDownItem> hasNameThat(final Matcher<String> nameMatcher) {
        return new FeatureMatcher<DropDownItem, String>(nameMatcher, "name that matches", "name") {
            @Override
            protected String featureValueOf(final DropDownItem actual) {
                return actual.getName();
            }
        };
    }

    /**
     * Matcher for the data-action attribute of items
     */
    @Nonnull
    public static Matcher<DropDownItem> hasAction(final String actionId) {
        return hasActionThat(is(actionId));
    }

    @Nonnull
    public static Matcher<DropDownItem> hasActionThat(final Matcher<String> actionMatcher) {
        return new FeatureMatcher<DropDownItem, String>(actionMatcher, "data-action attribute that matches", "data-action") {
            @Override
            protected String featureValueOf(final DropDownItem actual) {
                return actual.getAction();
            }
        };
    }

    @Nonnull
    public static Matcher<DropDownItem> hasAttribute(final String name, final String value) {
        return hasAttributeThat(name, is(value));
    }

    @Nonnull
    public static Matcher<DropDownItem> hasAttributeThat(final String name, final Matcher<String> actionMatcher) {
        return new FeatureMatcher<DropDownItem, String>(actionMatcher, name + " attribute that matches", name) {
            @Override
            protected String featureValueOf(final DropDownItem actual) {
                return actual.getAttribute(name);
            }
        };
    }
}

package com.atlassian.webdriver.applinks.grid;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Row in a grid finder
 */
public class GridFinderRow {
    private final GridFinder<?> gridFinder;
    private final PageElement row;

    public GridFinderRow(GridFinder gridFinder, PageElement row) {
        this.gridFinder = gridFinder;
        this.row = row;
    }

    public PageElement find(By by) {
        return row.find(by);
    }

    public String findAttribute(String attributeName) {
        return row.getAttribute(attributeName);
    }

    public PageElement findCell(int columnIndex) {
        List<PageElement> cells = allCells();
        if (columnIndex >= cells.size()) {
            return null;
        }
        return cells.get(columnIndex);
    }

    public PageElement findCell(String columnName) {
        Integer columnIndex = gridFinder.getColumnIndices().get(columnName);
        if (null == columnIndex) {
            return null;
        }
        return findCell(columnIndex);
    }

    public List<PageElement> allCells() {
        return row.findAll(gridFinder.getCellLocator());
    }
}

package com.atlassian.webdriver.applinks.page.v1;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.component.OrphanedTrustRelationshipsDialog;
import com.atlassian.webdriver.applinks.component.v1.AddApplicationLinkDialogStep1;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Representation of a pre v4.0.0 Application Links admin page.
 *
 * @since 4.0.0
 */
public class ListApplicationLinkPage extends AbstractApplicationLinkPage {

    @ElementBy(id = "add-first-application-link")
    protected PageElement addFirstApplicationLink;

    @ElementBy(id = "add-application-link")
    protected PageElement addApplicationLinkButton;

    @ElementBy(id = "show-orphaned-trust")
    protected PageElement showOrphanedTrustRelationshipsDialog;

    @ElementBy(className = "links-loading-message")
    protected PageElement linksLoadingMessage;

    @ElementBy(className = "orphaned-trust-warning")
    protected PageElement orphanedTrustWarning;

    @WaitUntil
    protected void waitForPageFullyLoaded() {
        // have to wait until the loading finish before we can extract information.
        waitUntilTrue(isApplinksListFullyLoaded());
    }

    public AddApplicationLinkDialogStep1 addApplicationLink() {
        addApplicationLinkButton.click();
        return pageBinder.bind(AddApplicationLinkDialogStep1.class);
    }

    public ListApplicationLinkPage togglePrimaryLink(String linkUrl) {
        PageElement row = elementFinder.find(By.id("ual-row-" + linkUrl));

        if (!row.isPresent())
            throw new IllegalStateException("cannot toggle primary app that doesn't exist:[" + linkUrl + "]");

        if (getApplicationLinks().size() < 2)
            throw new IllegalStateException("cannot toggle primary when there's only one app link");

        if (!row.find(By.className("app-toggleprimary-link")).isPresent())
            throw new IllegalStateException("cannot toggle primary app when the app is already primary");

        row.find(By.className("app-toggleprimary-link")).click();
        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    public boolean isOrphanedTrustWarningVisible() {
        return orphanedTrustWarning.isPresent() && orphanedTrustWarning.isVisible();
    }

    public OrphanedTrustRelationshipsDialog showOrphanTrustRelationshipsDialog() {
        if (!showOrphanedTrustRelationshipsDialog.isVisible()) {
            throw new IllegalStateException("link to show the orphaned trust relationships is not visible to the user");
        }

        showOrphanedTrustRelationshipsDialog.click();
        return pageBinder.bind(OrphanedTrustRelationshipsDialog.class, this);
    }

    public String getAddFirstApplicationLinkText() {
        return addFirstApplicationLink.getText();
    }

    public String getPageInfo() {
        waitForPageFullyLoaded();

        waitUntilTrue(pageInfoDiv.timed().isVisible());
        return pageInfoDiv.getText();
    }
}

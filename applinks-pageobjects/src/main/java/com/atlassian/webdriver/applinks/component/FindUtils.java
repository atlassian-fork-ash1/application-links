package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;


public class FindUtils {
    public static PageElement findVisibleBy(By by, PageElementFinder elementFinder) {
        Iterable<PageElement> elements = elementFinder.findAll(by);

        elements = Iterables.filter(elements, new Predicate<PageElement>() {
            public boolean apply(PageElement input) {
                return input.isVisible();
            }
        });

        if (Iterables.size(elements) == 0) {
            throw new IllegalStateException("can't find element by:" + by.toString());
        }

        if (Iterables.size(elements) > 1) {
            throw new IllegalStateException("there are more than one visible elements identified by:" + by.toString());
        }

        return Iterables.getOnlyElement(elements);
    }

    public static TimedCondition isAtLeastOneElementVisibleCondition(By by, PageElementFinder elementFinder) {
        return isAtLeastOneElementVisibleCondition(by, elementFinder, TimeoutType.DEFAULT);
    }

    public static TimedCondition isAtLeastOneElementVisibleCondition(By by, PageElementFinder elementFinder, TimeoutType timeout) {
        final TimeoutType finalTimeout = timeout;
        return Conditions.or(Lists.transform(elementFinder.findAll(by, timeout), new Function<PageElement, TimedQuery<Boolean>>() {
            public TimedQuery<Boolean> apply(PageElement from) {
                return from.withTimeout(finalTimeout).timed().isVisible();
            }
        }));
    }
}
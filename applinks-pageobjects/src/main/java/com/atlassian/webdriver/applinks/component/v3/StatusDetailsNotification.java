package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.search.PageElementQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.element.ClickableElement;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.PageElements.getText;
import static com.atlassian.pageobjects.elements.PageElements.hasClass;
import static com.atlassian.webdriver.applinks.component.v3.TroubleshootingPageUtils.clickAndCloseNewWindow;
import static com.atlassian.webdriver.applinks.element.ClickableElement.checkTagName;
import static com.google.common.base.Predicates.not;

/**
 * @since 5.1
 */
public class StatusDetailsNotification {
    protected static final String CLASS_CONTAINER = "applink-status-notification";
    protected static final String CLASS_TITLE = "applink-status-notification-title";
    protected static final String CLASS_ACTIONS = "applink-status-notification-actions";

    private static final Predicate<PageElement> CLICKABLE_ELEMENT = input -> checkTagName(input.getTagName());

    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected WebDriver driver;
    @Inject
    protected Timeouts timeouts;

    protected final PageElement container;

    public StatusDetailsNotification(@Nonnull PageElement container) {
        this.container = container;
    }

    @Nonnull
    public TimedCondition isPresent() {
        return getDetailsContainer().timed().isPresent();
    }

    @Nullable
    public String getTitle() {
        return getDetailsContainer().find(By.className(CLASS_TITLE)).getText();
    }

    @Nonnull
    public Iterable<String> getContents() {
        return getContentsQuery().map(getText()).now();
    }

    /**
     * @return all links in the status details contents
     */
    @Nonnull
    public Iterable<ClickableElement> getContentLinks() {
        return getContentsQuery()
                .by(By.tagName("a"))
                .bindTo(ClickableElement.class)
                .now();
    }

    @Nullable
    public ClickableElement findContentLink(@Nonnull By by) {
        return getContentsQuery()
                .by(by)
                .filter(CLICKABLE_ELEMENT)
                .bindTo(ClickableElement.class)
                .first();
    }

    @Nonnull
    public Iterable<ClickableElement> findContentLinks(@Nonnull By by) {
        return getContentsQuery()
                .by(by)
                .filter(CLICKABLE_ELEMENT)
                .bindTo(ClickableElement.class)
                .now();
    }

    @Nullable
    public String getTroubleshootingUrl() {
        return getTroubleshootingLink().getAttribute("href");
    }

    @Nonnull
    public Iterable<ClickableElement> getActions() {
        return getDetailsContainer().search()
                .by(By.className(CLASS_ACTIONS))
                .by(By.tagName("a"))
                .bindTo(ClickableElement.class)
                .now();

    }

    public void openTroubleshootingPage() {
        clickAndCloseNewWindow(() -> getTroubleshootingLink().javascript().mouse().click(), driver, timeouts);
    }

    @Nonnull
    protected PageElement getDetailsContainer() {
        return container.find(By.className(CLASS_CONTAINER));
    }

    @Nonnull
    protected PageElement getTroubleshootingLink() {
        return getDetailsContainer().find(By.className("troubleshoot-link"));
    }

    @Nonnull
    protected PageElementQuery<PageElement> getContentsQuery() {
        return getDetailsContainer().search()
                .by(By.tagName("p"))
                .filter(not(hasClass(CLASS_TITLE)))
                .filter(not(hasClass(CLASS_ACTIONS)));
    }
}

package com.atlassian.webdriver.applinks.util;

/**
 * Stuff that should be in {@link com.atlassian.webdriver.Elements}
 *
 * @since 5.0
 */
public final class ApplinksElements {
    private ApplinksElements() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    public static final String TAG_ANCHOR = "a";
}

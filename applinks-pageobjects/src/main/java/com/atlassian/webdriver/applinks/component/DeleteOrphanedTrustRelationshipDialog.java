package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class DeleteOrphanedTrustRelationshipDialog {

    @Inject
    protected PageElementFinder elementFinder;

    @Inject
    protected PageBinder pageBinder;

    @ElementBy(id = "#orphaned-trust-certificates-delete-dialog")
    protected PageElement orphanedTrustDialog;

    public void confirm() {
        PageElement deleteButton = orphanedTrustDialog.find(By.cssSelector("button.wizard-submit"));
        if (!deleteButton.isVisible()) {
            throw new IllegalStateException("delete button in the delete orphaned trust relationship dialog is not visible to the user");
        }
        deleteButton.click();
    }
}

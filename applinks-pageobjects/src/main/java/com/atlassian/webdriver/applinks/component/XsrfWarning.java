package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class XsrfWarning {
    @ElementBy(id = "xsrf-token-missing-header")
    private PageElement header;
    @ElementBy(id = "xsrf-retry")
    private PageElement retryButton;

    public boolean isVisible() {
        Poller.waitUntilTrue(header.timed().isVisible());

        //applinks.properties: applinks.xsrf.token.missing
        return "XSRF Security Token Missing".equals(header.getText());
    }

    public void retryOperation() {
        retryButton.click();
    }
}

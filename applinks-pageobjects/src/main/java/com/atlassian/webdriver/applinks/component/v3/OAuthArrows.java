package com.atlassian.webdriver.applinks.component.v3;

/**
 * @since v5.2
 */
public enum OAuthArrows {
    RIGHT_MATCH("arrow-right-match.svg"),
    LEFT_MATCH("arrow-left-match.svg"),
    RIGHT_MISMATCH("arrow-right-mismatch.svg"),
    LEFT_MISMATCH("arrow-left-mismatch.svg"),
    RIGHT_DISABLED("arrow-right-disabled.svg"),
    LEFT_DISABLED("arrow-left-disabled.svg");

    private String image;

    OAuthArrows(String image) {
        this.image = image;
    }

    public String image() {
        return image;
    }
}

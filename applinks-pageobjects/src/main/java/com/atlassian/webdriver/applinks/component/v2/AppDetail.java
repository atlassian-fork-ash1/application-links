package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * Represents a UI block describing an Application
 *
 * @since v4.0
 */
public class AppDetail {
    private PageElement appDetail;

    public AppDetail(final PageElement appDetail) {
        super();
        this.appDetail = appDetail;
    }

    public String getApplicationUrl() {
        Poller.waitUntilTrue(this.appDetail.find(By.className("detail")).timed().isVisible());
        PageElement applicationUrl = this.appDetail.find(By.className("detail")).findAll(By.tagName("p")).get(0);
        PageElement applicationUrlTitle = applicationUrl.find(By.tagName("strong"));
        return applicationUrl.getText().replace(applicationUrlTitle.getText(), "").trim();
    }
}

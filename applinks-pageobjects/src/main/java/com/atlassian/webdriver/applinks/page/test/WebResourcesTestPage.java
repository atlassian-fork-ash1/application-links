package com.atlassian.webdriver.applinks.page.test;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.PageElementJavascript;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the web resource test page. Allows to load selected web resources onto a blank page for testing.
 *
 * @since 5.0
 */
public class WebResourcesTestPage extends ApplinkAbstractPage {
    private static final String RESOURCE_KEY_PARAM = "resourceKeys";

    private static final Function<String, String> ADD_PARAM_NAME = new Function<String, String>() {
        @Override
        public String apply(String value) {
            return RESOURCE_KEY_PARAM + "=" + value;
        }
    };

    @Inject
    protected PageElementFinder finder;

    @ElementBy(id = "applinks-web-resources-test")
    protected PageElement idElement;

    private final Set<String> webResourceKeys;

    public WebResourcesTestPage(Iterable<String> webResourceKeys) {
        this.webResourceKeys = ImmutableSet.copyOf(Iterables.transform(webResourceKeys, ADD_PARAM_NAME));
    }

    @Override
    public String getUrl() {
        String queryParams = webResourceKeys.isEmpty() ? "" : "?" + Joiner.on("&").join(webResourceKeys);
        return "/plugins/servlet/applinks-tests/web-resources" + queryParams;
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(isAt());
    }

    public TimedCondition isAt() {
        return idElement.timed().isPresent();
    }

    public PageElementJavascript javascript() {
        return finder.find(By.tagName("body")).javascript();
    }
}

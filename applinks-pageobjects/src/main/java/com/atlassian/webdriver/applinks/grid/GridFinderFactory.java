package com.atlassian.webdriver.applinks.grid;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Factory for creating grid finders
 */
public class GridFinderFactory {
    private static final By BY_TH = By.cssSelector("thead th");
    private static final By BY_TR = By.cssSelector("tbody tr");
    private static final By BY_TD = By.tagName("td");

    @Inject
    private Timeouts timeouts;

    public GridFinder<GridFinderRow> create(PageElement root, By columnNameLocator, By rowLocator, By cellLocator) {
        return new GridFinder<GridFinderRow>(timeouts, root, columnNameLocator, rowLocator, cellLocator, defaultRowCreator);
    }


    private static final GridFinderRowCreator<GridFinderRow> defaultRowCreator = new GridFinderRowCreator<GridFinderRow>() {
        public GridFinderRow create(GridFinder<GridFinderRow> gridFinder, PageElement pageElement) {
            return new GridFinderRow(gridFinder, pageElement);
        }
    };

    /**
     * Returns a GridFinder that finds elements based on default HTML tags (TH for header cell, TR for body row, TD for body cell)
     */
    public GridFinder<GridFinderRow> createTable(PageElement root) {
        return new GridFinder<GridFinderRow>(timeouts, root, BY_TH, BY_TR, BY_TD, defaultRowCreator);
    }

    /**
     * Returns a GridFinder that finds elements based on default HTML tags (TH for header cell, TR for body row, TD for body cell)
     */
    public <R extends GridFinderRow> GridFinder<R> createTable(PageElement root, GridFinderRowCreator<R> rowCreator) {
        return new GridFinder<R>(timeouts, root, BY_TH, BY_TR, BY_TD, rowCreator);
    }
}

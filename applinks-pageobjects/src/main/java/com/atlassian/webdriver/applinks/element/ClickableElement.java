package com.atlassian.webdriver.applinks.element;

import com.atlassian.pageobjects.elements.PageElement;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Represents a link anchor or a button, with link text and URL.
 *
 * @since 5.2
 */
public class ClickableElement {
    public static final String ATTRIBUTE_HREF = "href";

    protected final PageElement element;

    public ClickableElement(@Nonnull PageElement element) {
        requireNonNull(element, "element");
        checkState(checkTagName(element.getTagName()));
        this.element = element;
    }

    public static boolean checkTagName(@Nullable String tagName) {
        return "a".equals(tagName) || "button".equals(tagName);
    }

    @Nullable
    public String getText() {
        return element.getText();
    }

    @Nullable
    public String getUrl() {
        return element.getAttribute(ATTRIBUTE_HREF);
    }

    public void follow() {
        element.click();
    }
}

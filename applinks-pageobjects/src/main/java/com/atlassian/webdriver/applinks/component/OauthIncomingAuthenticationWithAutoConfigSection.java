package com.atlassian.webdriver.applinks.component;

import com.atlassian.aui.auipageobjects.AuiCheckbox;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

public class OauthIncomingAuthenticationWithAutoConfigSection extends ConfigureApplicationSection {
    @ElementBy(id = "auth-oauth-action-disable")
    private PageElement disableButton;
    @ElementBy(id = "auth-oauth-action-enable")
    private PageElement enableButton;
    @ElementBy(id = "two-lo-enabled")
    private AuiCheckbox twoLOEnabledCheckbox;
    @ElementBy(id = "two-lo-execute-as")
    private PageElement twoLOExecuteAsTextBox;
    @ElementBy(id = "two-lo-impersonation-enabled")
    private AuiCheckbox twoLOImpersonationEnabledCheckbox;
    @ElementBy(id = "auth-oauth-action-update")
    private PageElement updateButton;
    @ElementBy(id = "2lo-section-footer")
    private PageElement twoLoSectionFooterDiv;
    @ElementBy(id = "2lo-section")
    private PageElement twoLoSection;
    @ElementBy(id = "2lo-impersonation-section")
    private PageElement twoLoImpersonationSection;
    @ElementBy(cssSelector = ".status-configured")
    private PageElement configuredStatus;
    @ElementBy(cssSelector = ".status-not-configured")
    private PageElement notConfiguredStatus;

    public OauthIncomingAuthenticationWithAutoConfigSection disable() {
        disableButton.click();

        return pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    public OauthIncomingAuthenticationWithAutoConfigSection enable() {
        enableButton.click();

        return pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    public boolean enableButtonIsVisible() {
        Poller.waitUntilTrue(enableButton.timed().isPresent());
        return enableButton.isVisible();
    }

    public boolean disableButtonIsVisible() {
        Poller.waitUntilTrue(disableButton.timed().isPresent());
        return disableButton.isVisible();
    }

    public boolean isConfigured() {
        return "Configured".equals(configuredStatus.getText());
    }

    public boolean isNotConfigured() {
        return "Not Configured".equals(notConfiguredStatus.getText());
    }

    public boolean is2LOSectionPresent() {
        return twoLoSection.isPresent();
    }

    public TimedCondition is2LOSectionPresentTimed() {
        return twoLoSection.timed().isPresent();
    }

    public boolean is2LOWithImpersonationSectionPresent() {
        return twoLoImpersonationSection.isPresent();
    }

    public TimedCondition is2LOWithImpersonationSectionPresentTimed() {
        return twoLoImpersonationSection.timed().isPresent();
    }

    public boolean is2LOEnabled() {
        return twoLOEnabledCheckbox.isSelected();
    }

    public TimedCondition is2LOEnabledTimed() {
        return twoLOEnabledCheckbox.timed().isSelected();
    }

    public String get2LOExecuteAs() {
        return twoLOExecuteAsTextBox.getValue();
    }

    public OauthIncomingAuthenticationWithAutoConfigSection set2LOExecuteAs(String username) {
        twoLOExecuteAsTextBox.clear();
        twoLOExecuteAsTextBox.type(username);

        return pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    public boolean is2LOImpersonationEnabled() {
        return twoLOImpersonationEnabledCheckbox.isSelected();
    }

    public TimedCondition is2LOImpersonationEnabledTimed() {
        return twoLOImpersonationEnabledCheckbox.timed().isSelected();
    }

    public OauthIncomingAuthenticationWithAutoConfigSection clickUpdate2LOConfig() {
        updateButton.click();
        OauthIncomingAuthenticationWithAutoConfigSection nextPage = pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);

        // Clicking the update button will cause the sub-iframe to be redirected first to the remote application, and
        // back to itself. One way to ensure this dance has completed is to wait for the update button to loaded again.
        Poller.waitUntilTrue(updateButton.withTimeout(TimeoutType.PAGE_LOAD).timed().isPresent());
        return nextPage;
    }

    public OauthIncomingAuthenticationWithAutoConfigSection check2LO() {
        twoLOEnabledCheckbox.check();
        return this;
    }

    public OauthIncomingAuthenticationWithAutoConfigSection uncheck2LO() {
        twoLOEnabledCheckbox.uncheck();
        return this;
    }

    public OauthIncomingAuthenticationWithAutoConfigSection check2LOImpersonation() {
        twoLOImpersonationEnabledCheckbox.check();
        return this;
    }

    public OauthIncomingAuthenticationWithAutoConfigSection uncheck2LOImpersonation() {
        twoLOImpersonationEnabledCheckbox.uncheck();
        return this;
    }

    /**
     * @return error message or null if no error present
     */
    public String get2LOErrorMessage() {
        return getMessageById("two-lo-update-error");
    }

    /**
     * @return success message or null if no success present
     */
    public String get2LOSuccessMessage() {
        return getMessageById("two-lo-update-success");
    }

    private String getMessageById(String id) {
        Poller.waitUntilTrue(twoLoSectionFooterDiv.timed().isPresent());
        PageElement message = elementFinder.find(By.id(id));
        if (message.isPresent()) {
            return message.getText();
        } else {
            return null;
        }
    }
}
package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.DataAttributeFinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge;
import com.atlassian.webdriver.applinks.component.v3.IncomingLocalAuthDropdown;
import com.atlassian.webdriver.applinks.component.v3.OutgoingLocalAuthDropdown;
import com.atlassian.webdriver.applinks.component.v3.RemoteAuthenticationStatus;
import com.atlassian.webdriver.applinks.component.v3.StatusDetailsNotification;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Objects.requireNonNull;

/**
 * Represents the V3 "Status UI" application links config page.
 *
 * @since 5.2
 */
public class V3EditApplinkPage extends ApplinkAbstractPage {
    @ElementBy(id = "applink-edit-header")
    protected PageElement header;

    @ElementBy(id = "applink-edit-form")
    protected PageElement editForm;

    @ElementBy(id = "applink-status-details-container")
    protected PageElement statusDetailsContainer;

    @ElementBy(className = "auth-table", within = "editForm")
    protected PageElement authTable;

    @ElementBy(id = "edit-submit")
    protected PageElement submitButton;

    @ElementBy(id = "edit-cancel")
    protected PageElement cancelButton;

    protected OutgoingLocalAuthDropdown outgoingAuthDropdown;

    protected IncomingLocalAuthDropdown incomingAuthDropdown;

    private final String applinkId;

    public V3EditApplinkPage(@Nonnull String applinkId) {
        this.applinkId = requireNonNull(applinkId, "applinkId");
    }

    public V3EditApplinkPage(@Nonnull ApplicationId applinkId) {
        this(requireNonNull(applinkId, "applinkId").get());
    }

    public V3EditApplinkPage(@Nonnull TestApplink.Side side) {
        this(requireNonNull(side, "side").id());
    }

    @Nonnull
    public static String getUrl(@Nonnull String applinkId) {
        return "/plugins/servlet/applinks/edit/" + applinkId;
    }

    @Override
    public String getUrl() {
        return getUrl(applinkId);
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(editForm.timed().isPresent());
        waitUntilTrue(submitButton.timed().isPresent());
        DataAttributeFinder headerData = DataAttributeFinder.query(header);
        // make sure both applink and status are loaded
        waitUntilTrue(headerData.timed().hasDataAttribute("status-loaded", Boolean.TRUE.toString()));
        waitUntilTrue(headerData.timed().hasDataAttribute("applink-loaded", Boolean.TRUE.toString()));

        outgoingAuthDropdown = pageBinder.bind(OutgoingLocalAuthDropdown.class);
        incomingAuthDropdown = pageBinder.bind(IncomingLocalAuthDropdown.class);

        waitUntilTrue(outgoingAuthDropdown.isDropdownVisible());
        waitUntilTrue(incomingAuthDropdown.isDropdownVisible());
    }

    @Nullable
    public String getHeader() {
        return header.find(By.className("applink-name")).getText();
    }

    @Nonnull
    public TimedQuery<String> getApplicationName() {
        return getApplicationNameInput().timed().getValue();
    }

    @Nonnull
    public TimedQuery<String> getApplicationNameError() {
        return getFormErrorFor(getApplicationNameInput());
    }

    @Nonnull
    public TimedQuery<String> getApplicationUrl() {
        return getApplicationUrlInput().timed().getValue();
    }

    @Nonnull
    public TimedQuery<String> getApplicationUrlError() {
        return getFormErrorFor(getApplicationUrlInput());
    }

    @Nonnull
    public TimedQuery<String> getDisplayUrl() {
        return getDisplayUrlInput().timed().getValue();
    }

    @Nonnull
    public TimedQuery<String> getDisplayUrlError() {
        return getFormErrorFor(getDisplayUrlInput());
    }

    @Nonnull
    public ApplinkStatusLozenge getStatus() {
        return pageBinder.bind(ApplinkStatusLozenge.class, header);
    }

    @Nonnull
    public StatusDetailsNotification getStatusDetails() {
        return pageBinder.bind(StatusDetailsNotification.class, statusDetailsContainer);
    }

    @Nonnull
    public OutgoingLocalAuthDropdown getOutgoingAuthDropdown() {
        return outgoingAuthDropdown;
    }

    @Nonnull
    public IncomingLocalAuthDropdown getIncomingAuthDropdown() {
        return incomingAuthDropdown;
    }

    /**
     * @return incoming remote auth status, NOTE: this matches the local <i>outgoing</i> status dropdown
     */
    @Nonnull
    public RemoteAuthenticationStatus getIncomingRemoteAuthStatus() {
        return pageBinder.bind(RemoteAuthenticationStatus.class,
                authTable.find(By.id("applink-incoming-remote-auth-section")), applinkId);
    }

    /**
     * @return outgoing remote auth status, NOTE: this matches the local <i>incoming</i> status dropdown
     */
    @Nonnull
    public RemoteAuthenticationStatus getOutgoingRemoteAuthStatus() {
        return pageBinder.bind(RemoteAuthenticationStatus.class,
                authTable.find(By.id("applink-outgoing-remote-auth-section")), applinkId);
    }

    @Nonnull
    public TimedQuery<String> getOutgoingStatusImageSrc() {
        return editForm.find(By.cssSelector("#applink-outgoing-auth-arrow .v3-connectivity-arrow")).timed()
                .getAttribute("src");
    }

    @Nonnull
    public TimedQuery<String> getIncomingStatusImageSrc() {
        return editForm.find(By.cssSelector("#applink-incoming-auth-arrow .v3-connectivity-arrow")).timed()
                .getAttribute("src");
    }

    @Nonnull
    public OutgoingConnectivityDialog triggerOutgoingConnectivityDialog() {
        final OutgoingConnectivityDialog outgoingConnectivityDialog = getOutgoingConnectivityDialog();
        if (!getOutgoingConnectivityDialog().isVisible().now()) {
            PageElement outgoingArrowTrigger = editForm.find(By.cssSelector(".arrow-right-trigger"));
            waitUntilTrue(outgoingArrowTrigger.timed().isVisible());
            outgoingArrowTrigger.click();
        }
        return outgoingConnectivityDialog;
    }

    @Nonnull
    public IncomingConnectivityDialog triggerIncomingConnectivityDialog() {
        final IncomingConnectivityDialog incomingConnectivityDialog = getIncomingConnectivityDialog();
        if (!incomingConnectivityDialog.isVisible().now()) {
            PageElement outgoingArrowTrigger = editForm.find(By.cssSelector(".arrow-left-trigger"));
            waitUntilTrue(outgoingArrowTrigger.timed().isVisible());
            outgoingArrowTrigger.click();
        }
        return incomingConnectivityDialog;
    }

    @Nonnull
    public IncomingConnectivityDialog getIncomingConnectivityDialog() {
        return pageBinder.bind(IncomingConnectivityDialog.class);
    }

    @Nonnull
    public OutgoingConnectivityDialog getOutgoingConnectivityDialog() {
        return pageBinder.bind(OutgoingConnectivityDialog.class);
    }

    @Nonnull
    public V3EditApplinkPage setApplicationName(@Nonnull String name) {
        typeInto(getApplicationNameInput(), name);
        return this;
    }

    @Nonnull
    public V3EditApplinkPage setApplicationUrl(@Nonnull String url) {
        typeInto(getApplicationUrlInput(), url);
        return this;
    }

    @Nonnull
    public V3EditApplinkPage setDisplayUrl(@Nonnull String url) {
        typeInto(getDisplayUrlInput(), url);
        return this;
    }

    @Nonnull
    public V3ListApplicationLinksPage submit() {
        submitButton.click();
        return pageBinder.bind(V3ListApplicationLinksPage.class);
    }

    @Nonnull
    public V3EditApplinkPage submitExpectingError() {
        submitButton.click();
        // should land back on this page
        return this;
    }

    @Nonnull
    public V3ListApplicationLinksPage cancel() {
        cancelButton.click();
        return pageBinder.bind(V3ListApplicationLinksPage.class);
    }

    protected static TimedQuery<String> getFormErrorFor(PageElement element) {
        return DataAttributeFinder.query(element).timed().getDataAttribute("aui-notification-error");
    }

    protected PageElement getApplicationNameInput() {
        return editForm.find(By.id("application-name"));
    }

    protected PageElement getApplicationUrlInput() {
        return editForm.find(By.id("application-url"));
    }

    protected PageElement getDisplayUrlInput() {
        return editForm.find(By.id("display-url"));
    }

    private static void typeInto(PageElement inputField, String value) {
        // getting rid of all race conditions
        inputField.click();
        inputField.clear();
        waitUntilEquals("", inputField.timed().getValue());
        inputField.type(value);
        waitUntilEquals(value, inputField.timed().getValue());
    }
}

package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.search.AnyQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.Elements;
import com.atlassian.webdriver.applinks.component.DropDownItem;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.DropDownItemMatchers.hasAction;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.hasItem;

/**
 * Represents the drop-down menu in the actions section of an applink in the applinks configuration screen
 *
 * @since 4.3
 */
public class V3ApplinkActionDropDown {
    public static final String DELETE_ACTION_ID = "delete";
    public static final String GO_TO_REMOTE_ACTION_ID = "remote";
    public static final String LEGACY_EDIT_ACTION_ID = "legacy-edit";
    public static final String MAKE_PRIMARY_ACTION_ID = "primary";

    @Inject
    protected PageElementFinder pageElementFinder;
    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected Timeouts timeouts;

    private final PageElement container;

    public V3ApplinkActionDropDown(@Nonnull final PageElement container) {
        this.container = requireNonNull(container);
    }

    @Nonnull
    public TimedQuery<Iterable<DropDownItem>> getActions() {
        return dropDownItemsQuery().timed();
    }

    @Nonnull
    public TimedQuery<Iterable<String>> getActionNames() {
        return dropDownItemsQuery().map(DropDownItem::getName).timed();
    }

    @Nonnull
    public TimedQuery<Iterable<String>> getActionIds() {
        return dropDownItemsQuery().map(DropDownItem::getAction).timed();
    }

    @Nonnull
    public TimedCondition isOpen() {
        return container.timed().isVisible();
    }

    public void close() {
        container.type(Keys.ESCAPE);
        waitUntilFalse(isOpen());
    }

    /**
     * Sets the application link on this row to be the primary link
     */
    public void makePrimary() {
        getAction(MAKE_PRIMARY_ACTION_ID).click();
    }

    /**
     * Deletes the application link.
     */
    public void delete() {
        getAction(DELETE_ACTION_ID).click();
        PageElement deleteButton = pageElementFinder.find(By.className("delete-confirm-button"));
        waitUntilTrue(deleteButton.timed().isVisible());
        deleteButton.click();
    }

    public void goToRemote() {
        getAction(GO_TO_REMOTE_ACTION_ID).click();
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    private DropDownItem getAction(String action) {
        waitUntil(action + " action not present", getActions(), hasItem(hasAction(action)));
        return toStream(getActions().now()).filter(DropDownItem.hasAction(action)).findFirst().get();
    }

    private AnyQuery<DropDownItem> dropDownItemsQuery() {
        return container.search()
                .by(By.tagName(Elements.TAG_LI))
                .bindTo(DropDownItem.class);
    }
}

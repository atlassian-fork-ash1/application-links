package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;

import javax.inject.Inject;

public class AddEntityLinkSectionAuthorize {
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "authorize-link")
    private PageElement authorizeLink;

    public OAuthConfirmPage<AddEntityLinkSection> authorize() {
        Poller.waitUntilTrue(authorizeLink.timed().isVisible());
        authorizeLink.click();
        return pageBinder.bind(OAuthConfirmPage.class, pageBinder.bind(AddEntityLinkSection.class));
    }
}
package com.atlassian.webdriver.applinks.externalcomponent;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.WebDriver;

import java.util.function.Supplier;
import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.applinks.internal.common.lang.FunctionalInterfaces.supplierOf;

/**
 * Page Object for OAuth confirmation page
 */
public class OAuthConfirmPage<T> {
    private static final String OAUTH_WINDOW_NAME = "com_atlassian_applinks_authentication";

    @Inject
    private PageBinder pageBinder;
    @Inject
    private WebDriver driver;

    @ElementBy(name = "approve")
    private PageElement allowField;
    @ElementBy(name = "deny")
    private PageElement denyField;
    @ElementBy(cssSelector = "#oauth-authorize #container p")
    private PageElement textElement;

    private final Supplier<T> nextPage;

    public OAuthConfirmPage(@Nonnull T nextPage) {
        this.nextPage = nextPage instanceof Supplier ? (Supplier) nextPage : supplierOf(nextPage);
    }

    public T denyHandlingWebLoginIfRequired(String username, String password) {
        return handleWebLoginIfRequiredThenClick(username, password, denyField);
    }

    public T confirmHandlingWebLoginIfRequired(String username, String password) {
        return handleWebLoginIfRequiredThenClick(username, password, allowField);
    }

    public T confirmHandlingWebLoginIfRequired(TestAuthentication authentication) {
        return confirmHandlingWebLoginIfRequired(authentication.getUsername(), authentication.getPassword());
    }

    /**
     * Enters username and password into the login form if one is present.
     *
     * @param username username
     * @param password password
     * @return the current page after login
     */
    public OAuthConfirmPage<T> handleWebLoginIfRequired(String username, String password) {
        handleWebLoginIfRequiredThenClick(username, password, null);
        return this;
    }

    /***
     * Enters username and password into the login form if one is present and then click the element inside
     * the OAuth confirmation frame.
     *
     * @param username     username
     * @param password     password
     * @param fieldToClick the element to click, null means do not click anything.
     * @return the expected page
     */
    private T handleWebLoginIfRequiredThenClick(String username, String password, PageElement fieldToClick) {
        String initialWindowName = driver.getWindowHandle();
        driver.switchTo().window(OAUTH_WINDOW_NAME);
        WebLoginPage<OAuthConfirmPage<T>> webLoginPage = pageBinder.bind(WebLoginPage.class, this);
        webLoginPage.handleWebLoginIfRequired(username, password);
        if (fieldToClick != null) {
            fieldToClick.click();
        }
        driver.switchTo().window(initialWindowName);
        return nextPage.get();
    }

    /**
     * @return the confirmation message displayed to the user
     */
    public String getConfirmationText() {
        String initialWindowName = driver.getWindowHandle();
        driver.switchTo().window(OAUTH_WINDOW_NAME);
        try {
            // We return this text in order to have a nice error message when the test fails
            return textElement.getText();
        } finally {
            driver.switchTo().window(initialWindowName);
        }
    }
}
package com.atlassian.webdriver.applinks.component.v2;

import java.util.List;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * Abstract representation of the main dialogs used in the creation process.
 * They include details sections, showing info about the local and remote applications,
 * plus options depending on the type of link being created.
 *
 * @since v4.0.0
 */
public class AbstractCreationDialog extends AbstractDialog {
    public List<PageElement> getApplicationDetails() {
        // wait until at least one is present.
        Poller.waitUntilTrue(this.elementFinder.find(By.className("detail")).timed().isPresent());

        return this.elementFinder.findAll(By.className("detail"));
    }

    public PageElement getLocalApplicationDetails() {
        return getApplicationDetails().get(0);
    }

    public String getLocalApplicationUrl() {
        Poller.waitUntilTrue(this.getLocalApplicationDetails().timed().isPresent());
        return this.getLocalApplicationDetails().findAll(By.tagName("p")).get(0).getText();
    }

    public String getLocalApplicationName() {
        Poller.waitUntilTrue(this.getLocalApplicationDetails().timed().isPresent());
        return this.getLocalApplicationDetails().findAll(By.tagName("p")).get(1).getText();
    }

    public String getLocalApplicationApplication() {
        Poller.waitUntilTrue(this.getLocalApplicationDetails().timed().isPresent());
        return this.getLocalApplicationDetails().findAll(By.tagName("p")).get(2).getText();
    }

    public PageElement getRemoteApplicationDetails() {
        return getApplicationDetails().get(0);
    }

    public String getRemoteApplicationUrl() {
        Poller.waitUntilTrue(this.getRemoteApplicationDetails().timed().isPresent());
        return this.getRemoteApplicationDetails().findAll(By.tagName("p")).get(0).getText();
    }

    public CreateNonUalIncomingDialog getCreateNonUalIncomingDialog() {
        return pageBinder.bind(CreateNonUalIncomingDialog.class);
    }
}

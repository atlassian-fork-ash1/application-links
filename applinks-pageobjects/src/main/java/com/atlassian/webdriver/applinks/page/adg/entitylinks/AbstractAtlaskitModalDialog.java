package com.atlassian.webdriver.applinks.page.adg.entitylinks;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;

import static org.hamcrest.Matchers.is;

public abstract class AbstractAtlaskitModalDialog {
    @ElementBy(className = "atlaskit-portal-container")
    protected PageElement portalContainer;

    public void waitUntilDialogClosed() {
        Poller.waitUntil(portalContainer.timed().isPresent(), is(false), Poller.by(new DefaultTimeouts().timeoutFor(TimeoutType.PAGE_LOAD)));
    }
}

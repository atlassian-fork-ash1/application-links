package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * @since 3.7
 */
public class CorsAuthenticationSection extends AbstractAuthenticationSection {
    @ElementBy(id = "auth-cors-disable")
    private PageElement disableButton;
    @ElementBy(id = "auth-cors-enable")
    private PageElement enableButton;
    @Inject
    private PageElementFinder finder;

    public void disable() {
        Poller.waitUntilTrue(disableButton.timed().isVisible());
        disableButton.click();
    }

    public void enable() {
        Poller.waitUntilTrue(enableButton.timed().isVisible());
        enableButton.click();
    }

    public boolean isConfigured() {
        PageElement status = finder.find(By.cssSelector(".status-configured"));
        Poller.waitUntilTrue(status.timed().isVisible());

        return "Configured".equals(status.getText());
    }

    public boolean isNotConfigured() {
        PageElement status = finder.find(By.cssSelector(".status-not-configured"));
        Poller.waitUntilTrue(status.timed().isVisible());

        return "Not Configured".equals(status.getText());
    }
}

package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;

/**
 * Represents the dialog shown to a user when the entered URL doesn't match that returned in the remote manifest
 *
 * @since v4.0.0
 */
public class ConfirmUrlDialog extends AbstractDialog {
    @ElementBy(id = "application-url-rpc")
    protected PageElement applicationUrlRpcTextBox;

    @ElementBy(id = "application-url-display")
    protected PageElement applicationUrlDisplayTextBox;

    @Override
    public TimedQuery<Boolean> isAt() {
        return applicationUrlDisplayTextBox.timed().isVisible();
    }

    public CreateUalDialog clickContinueAndOpenCreateUalDialog() {
        this.clickContinue();
        return this.getCreateUalDialog();
    }

    public ConfirmUrlDialog setApplicationUrlRpc(String applicationUrlRpc) {
        Poller.waitUntilTrue(isAt());
        applicationUrlRpcTextBox.clear();
        applicationUrlRpcTextBox.type(applicationUrlRpc);
        return this;
    }

    public CreateUalDialog getCreateUalDialog() {
        return pageBinder.bind(CreateUalDialog.class);
    }

    public TimedElement getApplicationUrlRpcTextBoxTimed() {
        return this.applicationUrlRpcTextBox.timed();
    }

    public TimedElement getApplicationUrlDisplayTextBoxTimed() {
        return this.applicationUrlDisplayTextBox.timed();
    }
}

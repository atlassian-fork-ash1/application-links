package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import com.atlassian.webdriver.applinks.util.GetTextFunction;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import java.util.List;

public class AuthTestPage extends ApplinkAbstractPage {
    @ElementBy(id = "disableRefresh")
    private PageElement disableRefreshCheckbox;

    @ElementBy(cssSelector = ".created-from-java .applink-authenticate")
    private PageElement authenticateLink;

    @ElementBy(cssSelector = ".created-from-js .applink-authenticate")
    private PageElement jsAuthenticateLink;

    @ElementBy(cssSelector = ".created-from-js .applinks-auth-request")
    private PageElement jsAuthMessage;

    @ElementBy(linkText = "OAuth access tokens")
    private PageElement oauthAccessTokensLink;

    @ElementBy(className = "applinks-auth-confirmation")
    private PageElement authConfirmationElement;

    @ElementBy(id = "result")
    private PageElement resultElement;

    private final String queryString;

    public AuthTestPage() {
        this("");
    }

    public AuthTestPage(String queryString) {
        this.queryString = queryString;
    }

    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/auth-test" + queryString;
    }

    public boolean canAuthenticate() {
        Poller.waitUntilTrue(authenticateLink.timed().isVisible());
        return true;
    }

    public String getAuthenticateLinkUri() {
        Poller.waitUntilTrue(authenticateLink.timed().isVisible());
        return authenticateLink.getAttribute("href");
    }

    public boolean hasJSAuthenticateLink() {
        Poller.waitUntilTrue(jsAuthenticateLink.timed().isVisible());
        return true;
    }

    public String getJsAuthMessageText() {
        Poller.waitUntilTrue(jsAuthMessage.timed().isVisible());
        return jsAuthMessage.getText();
    }

    public String getJsAuthenticateLinkUri() {
        Poller.waitUntilTrue(jsAuthenticateLink.timed().isVisible());
        return jsAuthenticateLink.getAttribute("href");
    }

    public OAuthConfirmPage<AuthTestPage> authenticate() {
        authenticateLink.click();
        return pageBinder.bind(OAuthConfirmPage.class, this);
    }

    public boolean hasConfirmedAuthorization(String appName, String username) {
        List<String> confirmedAuthorizations = getConfirmedAuthorizations();
        for (String authorizedText : confirmedAuthorizations) {
            if (authorizedText.contains(appName + ": authorized (user: " + username + ")")) {
                return true;
            }
        }
        return false;
    }

    public List<String> getConfirmedAuthorizations() {
        // wait for single element
        Poller.waitUntilTrue(elementFinder.find(By.className("app-authorization")).timed().isVisible());
        return Lists.transform(elementFinder.findAll(By.className("app-authorization")), new GetTextFunction());
    }

    public boolean hasOAuthAccessTokens() {
        Poller.waitUntilTrue(oauthAccessTokensLink.timed().isVisible());
        return true;
    }

    public AuthTestPage disableRefresh() {
        disableRefreshCheckbox.click();
        return this;
    }

    public boolean hasOAuthConfirmationMessage() {
        Poller.waitUntilTrue(authConfirmationElement.timed().isVisible());
        return true;
    }

    public String getResultText() {
        Poller.waitUntilTrue(resultElement.timed().isVisible());
        return resultElement.getText();
    }
}
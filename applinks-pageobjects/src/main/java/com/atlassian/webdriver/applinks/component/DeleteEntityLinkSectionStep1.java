package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class DeleteEntityLinkSectionStep1 {
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(xpath = "//div[@id='delete-application-link-dialog']/div[1]/div[2]/button")
    private PageElement confirmButton;

    public DeleteEntityLinkSectionStep2 confirm() {
        waitUntilTrue(confirmButton.timed().isVisible());
        // Ugliness to deal with issue where confirm button was able to be clicked before a handler being attached
        waitUntilFalse(elementFinder.find(By.className("delete-applink-loading")).timed().isVisible());
        confirmButton.click();
        return pageBinder.bind(DeleteEntityLinkSectionStep2.class);
    }
}
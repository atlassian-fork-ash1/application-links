package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;

import javax.inject.Inject;
import java.util.List;

/**
 * Represents a page that contains AuthTestPage inside an iframe. Implemented by decorating AuthTestPage
 */
public class AuthIFrameTestPage extends ApplinkAbstractPage {
    @ElementBy(id = "time")
    private PageElement timeElement;

    @Inject
    private PageBinder pageBinder;

    /**
     * wrapped page
     */
    private AuthTestPage authTestPage;

    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/auth-test-iframe";
    }

    public boolean canAuthenticate() {
        intoIFrame();
        try {
            return getAuthTestPage().canAuthenticate();
        } finally {
            outOfIFrame();
        }
    }

    public OAuthConfirmPage<AuthIFrameTestPage> authenticate() {
        intoIFrame();
        try {
            getAuthTestPage().authenticate();
            return pageBinder.bind(OAuthConfirmPage.class, this);
        } finally {
            outOfIFrame();
        }
    }

    public boolean hasConfirmedAuthorization(String appName, String username) {
        intoIFrame();
        try {
            return getAuthTestPage().hasConfirmedAuthorization(appName, username);
        } finally {
            outOfIFrame();
        }
    }

    public List<String> getConfirmedAuthorizations() {
        intoIFrame();
        try {
            return getAuthTestPage().getConfirmedAuthorizations();
        } finally {
            outOfIFrame();
        }
    }

    public boolean hasOAuthAccessTokens() {
        intoIFrame();
        try {
            return getAuthTestPage().hasOAuthAccessTokens();
        } finally {
            outOfIFrame();
        }
    }

    public boolean hasOAuthConfirmationMessage() {
        intoIFrame();
        try {
            return getAuthTestPage().hasOAuthConfirmationMessage();
        } finally {
            outOfIFrame();
        }
    }

    private void intoIFrame() {
        driver.switchTo().frame("myframe");
    }

    private void outOfIFrame() {
        driver.switchTo().defaultContent();
    }

    public String getTime() {
        return timeElement.getText();
    }

    private AuthTestPage getAuthTestPage() {
        if (null == authTestPage) {
            authTestPage = pageBinder.bind(AuthTestPage.class);
        }
        return authTestPage;
    }
}
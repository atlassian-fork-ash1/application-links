package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.applinks.internal.common.lang.ApplinksStreams;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.search.PageElementQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Objects;

import static com.atlassian.applinks.internal.common.lang.FunctionalInterfaces.asSupplier;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.TroubleshootingPageUtils.clickAndCloseNewWindow;
import static com.atlassian.webdriver.applinks.util.TransformableQuery.asTransformable;
import static java.util.Objects.requireNonNull;

/**
 * Represents remote authentication status on the edit page
 *
 * @since 5.2
 */
public class RemoteAuthenticationStatus {
    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected WebDriver driver;
    @Inject
    protected Timeouts timeouts;

    private final PageElement container;
    private final String applinkId; // required for remote authentication

    public RemoteAuthenticationStatus(@Nonnull PageElement container, String applinkId) {
        this.applinkId = applinkId;
        this.container = requireNonNull(container, "container");
    }

    @Nonnull
    public TimedCondition hasApplicationIcon() {
        return container.find(By.className("remote-application-icon")).timed().isPresent();
    }

    @Nonnull
    public TimedCondition hasRemoteLink() {
        return getRemoteLinkElement().timed().isPresent();
    }

    @Nonnull
    public TimedCondition hasOAuthConfig() {
        return getOAuthConfigQuery().hasResult();
    }

    @Nonnull
    public TimedQuery<UiOAuthConfig> getOAuthConfig() {
        return asTransformable(getOAuthConfigQuery()
                .map(UiOAuthConfig::fromText)
                .filter(Objects::nonNull)
                .timed())
                .map(ApplinksStreams::firstOrNull);
    }

    @Nonnull
    public TimedCondition hasTroubleshootingLink() {
        return getTroubleshootingLinkElement().timed().isPresent();
    }

    @Nonnull
    public TimedCondition isRemoteAuthenticationRequired() {
        return getRemoteAuthenticationRequiredElement().timed().isPresent();
    }

    /**
     * Follow the link to the remote application from this remote auth section.
     */
    public void openRemoteConfig() {
        getRemoteLinkElement().click();
    }

    /**
     * Follow the link to the documentation page.
     */
    public void openTroubleshootingPage() {
        clickAndCloseNewWindow(() -> getTroubleshootingLinkElement().javascript().mouse().click(), driver, timeouts);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public OAuthConfirmPage<V3EditApplinkPage> startRemoteAuthentication() {
        waitUntilTrue(isRemoteAuthenticationRequired());
        getRemoteAuthenticationRequiredElement().click();

        return pageBinder.bind(OAuthConfirmPage.class,
                asSupplier(() -> pageBinder.bind(V3EditApplinkPage.class, applinkId)));
    }

    @Nonnull
    protected PageElement getRemoteLinkElement() {
        return container.find(By.className("remote-application-link"));
    }

    @Nonnull
    protected PageElementQuery<PageElement> getOAuthConfigQuery() {
        return container.search().by(By.className("remote-auth-level"));
    }

    @Nonnull
    protected PageElement getTroubleshootingLinkElement() {
        return container.find(By.className("troubleshoot-link"));
    }

    @Nonnull
    protected PageElement getRemoteAuthenticationRequiredElement() {
        return container.find(By.className("status-remote-auth-required"));
    }
}


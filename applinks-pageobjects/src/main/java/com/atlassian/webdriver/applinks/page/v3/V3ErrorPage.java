package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * Represents generic error page, usually resulting from user error.
 *
 * @since 5.2
 */
public class V3ErrorPage extends ApplinkAbstractPage {

    @ElementBy(id = "applink-error-title")
    protected PageElement errorTitle;

    @ElementBy(id = "applink-error-message")
    protected PageElement errorMessage;

    private final String url;

    public V3ErrorPage(@Nonnull String url) {
        this.url = requireNonNull(url, "url");
    }

    @Override
    public String getUrl() {
        return url;
    }

    // this is static page, so TimedQueries not required

    @Nullable
    public String getErrorTitle() {
        return errorTitle.getText();
    }

    public boolean isError(int httpErrorCode) {
        return getErrorTitle() != null && getErrorTitle().contains("(" + httpErrorCode + ")");
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage.getText();
    }
}

package com.atlassian.webdriver.applinks.page;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.component.AddEntityLinkSection;
import com.atlassian.webdriver.applinks.component.AddEntityLinkSectionAuthorize;
import com.atlassian.webdriver.applinks.component.AddEntityLinkSectionNonUal;
import com.atlassian.webdriver.applinks.component.DeleteEntityLinkSectionStep1;
import com.atlassian.webdriver.applinks.grid.GridFinder;
import com.atlassian.webdriver.applinks.grid.GridFinderFactory;
import com.atlassian.webdriver.applinks.grid.GridFinderRow;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.util.AuiBlanketUtil.waitUntilBlanketNotVisible;
import static org.hamcrest.Matchers.is;

public class ConfigureEntityLinksPage extends ApplinkAbstractPage {
    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected GridFinderFactory gridFinderFactory;

    @ElementBy(linkText = "Add Link")
    private PageElement addLink;

    @ElementBy(id = "entity-link-list-table")
    private PageElement linkTable;

    @ElementBy(className = "page-info")
    private PageElement pageInfoElement;

    @ElementBy(cssSelector = ".applinks-entity-header .no-app-links")
    private PageElement noAppLinksMessage;

    @ElementBy(className = "aui-blanket")
    private PageElement auiBlanket;

    protected final Class<? extends EntityType> entityType;
    protected final String key;
    protected GridFinder grid;

    public ConfigureEntityLinksPage(final Class<? extends EntityType> entityType, final String key) {
        this.entityType = entityType;
        this.key = key;
    }

    public String getUrl() {
        return "/plugins/servlet/applinks/listEntityLinks/" + entityType.getName() + "/" + key;
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(linkTable.timed().isPresent());
    }

    public AddEntityLinkSection addLink(String regEx) {
        doAddLink(regEx);
        return pageBinder.bind(AddEntityLinkSection.class);
    }

    public AddEntityLinkSectionAuthorize addLinkExpectedAuthorize(String regEx) {
        doAddLink(regEx);
        return pageBinder.bind(AddEntityLinkSectionAuthorize.class);
    }

    public AddEntityLinkSectionNonUal addLinkExpectedNonUal(String regEx) {
        doAddLink(regEx);
        return pageBinder.bind(AddEntityLinkSectionNonUal.class, this);
    }

    private void doAddLink(String regEx) {
        addLink.click();
        elementFinder.find(By.partialLinkText(regEx)).click();
    }

    public TimedQuery<List<ConfigureEntityLinksRow>> allLinks() {
        return getGrid().allRowsTimed();
    }

    public int getEntityLinkCount() {
        return getGrid().allRows().size();
    }

    public String getPageInfoText() {
        return pageInfoElement.getText();
    }

    public TimedQuery<String> getPageInfoTextTimed() {
        return pageInfoElement.timed().getText();
    }

    public TimedCondition hasRow(String columnHeader, String value) {
        return getGrid().hasRow(columnHeader, is(value));
    }

    public ConfigureEntityLinksRow inRow(String columnHeader, String value) {
        return getGrid().inRow(columnHeader, is(value));
    }

    public class ConfigureEntityLinksRow extends GridFinderRow {
        ConfigureEntityLinksRow(GridFinder gridFinder, PageElement row) {
            super(gridFinder, row);
        }

        public String getApplication() {
            return findCell("application").getText();
        }

        public String getType() {
            return findCell("type").getText();
        }

        public String getName() {
            return findCell("remote-name").getText();
        }

        public String getKey() {
            return findCell("remote-key").getText();
        }

        public DeleteEntityLinkSectionStep1 delete() {
            waitUntilBlanketNotVisible(auiBlanket);
            find(By.className("entity-delete-link")).click();
            return pageBinder.bind(DeleteEntityLinkSectionStep1.class);
        }
    }

    private GridFinder<ConfigureEntityLinksRow> getGrid() {
        if (null == grid) {
            grid = gridFinderFactory.createTable(linkTable, ConfigureEntityLinksRow::new);
        }
        return grid;
    }

    public boolean hasAddLink() {
        return addLink.isPresent();
    }

    public boolean hasNoAppLinksMessage() {
        return noAppLinksMessage.isPresent();
    }
}
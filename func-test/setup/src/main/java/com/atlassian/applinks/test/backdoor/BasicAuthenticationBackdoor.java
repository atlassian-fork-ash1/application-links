package com.atlassian.applinks.test.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.model.RestBasicStatus;
import com.atlassian.applinks.test.rest.model.RestTrustedAppsStatus;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;

/**
 * Backdoor to manage Basic Authentication configuration for applinks.
 *
 * @since 5.2
 */
public class BasicAuthenticationBackdoor {
    private static final RestUrl BASIC_REST_PATH = RestUrl.forPath("basic");

    @Nonnull
    public RestBasicStatus getStatus(TestApplink.Side side) {
        Response response = createBasicAuthTester(side.product()).get(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .expectStatus(Status.OK)
                .build());

        return response.as(RestBasicStatus.class);
    }

    @Nonnull
    public RestBasicStatus setStatus(TestApplink.Side side, TestAuthentication testAuthentication) {
        Response response = createBasicAuthTester(side.product()).put(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .body(new RestBasicStatus(testAuthentication.getUsername(), testAuthentication.getPassword()))
                .expectStatus(Status.OK)
                .build());

        return response.as(RestBasicStatus.class);
    }

    static BaseRestTester createBasicAuthTester(TestedInstance instance) {
        return new BaseRestTester(forTestRestModule(instance).root()
                .add(BASIC_REST_PATH)
                .add(ApplinksRestUrls.STATUS_PATH));
    }
}

package com.atlassian.applinks.test.rule;

import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.backdoor.RefappUserManagementBackdoor;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.model.RestUser;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * JUnit rule to create users. Automatically removes all created users at the end of the test. NOTE: currently only
 * works in Refapp.
 *
 * @since 5.0
 */
public class UserManagementRule extends TestWatcher {
    private final RefappUserManagementBackdoor backdoor;

    private final Set<String> createdUsers = new LinkedHashSet<>();

    public UserManagementRule(@Nonnull TestedInstance instance) {
        this.backdoor = new RefappUserManagementBackdoor(instance);
    }

    public void createUser(@Nonnull TestAuthentication authentication, @Nonnull PermissionLevel permissionLevel) {
        requireNonNull(authentication, "authentication");
        requireNonNull(authentication.getUsername(), "authentication.username");
        createUser(authentication.getUsername(), permissionLevel);
    }

    public void createUser(@Nonnull String username, @Nonnull PermissionLevel permissionLevel) {
        createUser(username, new RestUser.Builder()
                .permissionLevel(permissionLevel)
                .build());
    }

    public void createUser(@Nonnull String username, @Nonnull RestUser user) {
        createdUsers.add(username);
        backdoor.createUser(username, user);
    }

    @Override
    protected void finished(Description description) {
        for (String username : createdUsers) {
            backdoor.deleteUser(username);
        }
    }
}

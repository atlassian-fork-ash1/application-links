package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * {@link TestApplinkConfigurator} that updates applink URL (both RPC and display).
 */
public class UrlApplinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static TestApplinkConfigurator setUrl(@Nonnull String url) {
        return new UrlApplinkConfigurator(url);
    }

    private final String url;

    public UrlApplinkConfigurator(@Nonnull String url) {
        this.url = requireNonNull(url, "url");
    }

    public void configureSide(@Nonnull TestApplink.Side side) {
        new ApplinksBackdoor(side.product()).setUrl(side, url);
    }
}

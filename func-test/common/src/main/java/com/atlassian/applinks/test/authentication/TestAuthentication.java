package com.atlassian.applinks.test.authentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.apache.commons.lang3.Validate.notEmpty;

/**
 * @since 4.3
 */
public class TestAuthentication {
    private final String password;
    private final String username;

    private TestAuthentication() {
        password = username = null;
    }

    private TestAuthentication(String username, String password) {
        this.password = password;
        this.username = username;
    }

    @Nonnull
    public static TestAuthentication anonymous() {
        return new TestAuthentication();
    }

    /**
     * @return authentication for an "admin" user common on test installations. NOTE: this should actually correspond
     * to the built-in <i>sysadmin</i> user on all tested applications.
     */
    @Nonnull
    public static TestAuthentication admin() {
        return TestAuthentication.authenticatedWith("admin");
    }

    /**
     * @param username the username, also used as the password
     * @return an {@link TestAuthentication authentication} directive that will authenticate as the specified user
     * @see #authenticatedWith(String, String)
     */
    @Nonnull
    public static TestAuthentication authenticatedWith(@Nonnull String username) {
        return authenticatedWith(username, username);
    }

    @Nonnull
    public static TestAuthentication authenticatedWith(@Nonnull String username, @Nullable String password) {
        notEmpty(username, "A username is required for an authenticated request.");
        return new TestAuthentication(username, password);
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    @Nullable
    public String getUsername() {
        return username;
    }

    public boolean isAuthenticated() {
        return username != null;
    }
}

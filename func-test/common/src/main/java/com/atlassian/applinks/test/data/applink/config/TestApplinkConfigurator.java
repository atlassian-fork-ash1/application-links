package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

/**
 * Configures {@link TestApplink test applinks}. This involves any operation that modifies configuration and persistent
 * state of the link and will usually require a remote call to {@link TestApplink.Side#product() the product} on one, or
 * both sides of the applink (depending on whether {@link #configure(TestApplink)} or
 * {@link #configureSide(TestApplink.Side)} was called, respectively.
 * <p>
 * A typical example of configurator would be a component that adds specific authentication providers to each or any
 * side of the link, e.g. OAuth, Trusted Apps etc.
 * </p>
 *
 * @see TestApplink
 * @see TestApplink.Side
 * @since 4.3
 */
public interface TestApplinkConfigurator {
    /**
     * Configure the entire applink (both sides).
     *
     * @param applink applink to configure
     */
    void configure(@Nonnull TestApplink applink);

    /**
     * Configure one side of a test applink.
     *
     * @param side applink side to configure
     */
    void configureSide(@Nonnull TestApplink.Side side);
}

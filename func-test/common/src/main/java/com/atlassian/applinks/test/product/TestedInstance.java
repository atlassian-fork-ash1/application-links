package com.atlassian.applinks.test.product;

import javax.annotation.Nonnull;

/**
 * Represents product instance under test. Similar to {@code ProductInstnace}, but we also allows to map instance to its
 * {@link SupportedProducts product type}, as well as allows not to depend on {@code Atlassian Selenium} for all func
 * tests.
 *
 * @since 4.3
 */
public interface TestedInstance {
    @Nonnull
    String getInstanceId();

    @Nonnull
    String getBaseUrl();

    @Nonnull
    SupportedProducts getProduct();
}

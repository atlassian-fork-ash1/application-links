package com.atlassian.applinks.test.rest.migration;

import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;

public class ApplinksMigrationRestTester extends BaseRestTester {
    public ApplinksMigrationRestTester(@Nonnull final ApplinksRestUrls restUrls) {
        super(restUrls.migration());
    }

    /**
     * Perform a migration from TA/BA to OAuth
     *
     * @param request migration request
     * @return response
     */
    @Nonnull
    public Response post(@Nonnull ApplinksMigrationRequest request) {
        return super.post(request);
    }
}

package com.atlassian.applinks.test.rest.capabilities;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;

/**
 * REST tester for the "capabilities" resource.
 *
 * @since 5.0
 */
public class ApplinksCapabilitiesRestTester extends BaseRestTester {
    public ApplinksCapabilitiesRestTester(@Nonnull TestedInstance testedInstance) {
        super(forProduct(testedInstance).capabilities());
    }

    public ApplinksCapabilitiesRestTester(@Nonnull TestedInstance testedInstance,
                                          @Nullable TestAuthentication defaultAuthentication) {
        super(forProduct(testedInstance).capabilities(), defaultAuthentication);
    }

    /**
     * Get all supported capabilities, or a specific capability (depending on the request)
     *
     * @param request request
     * @return response containing a list of supported capabilities
     */
    @Nonnull
    public Response get(@Nonnull ApplinksCapilitiesRequest request) {
        return super.get(request);
    }

    /**
     * Get remote capabilities for a specific applink
     *
     * @param remoteCapilitiesRequest request
     * @return response
     */
    @Nonnull
    public Response getRemote(@Nonnull ApplinksRemoteCapilitiesRequest remoteCapilitiesRequest) {
        return super.get(remoteCapilitiesRequest);
    }
}

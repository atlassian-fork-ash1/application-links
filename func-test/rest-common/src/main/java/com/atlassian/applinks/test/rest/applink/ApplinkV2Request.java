package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

/**
 * Request to interact with a single Applinks via V2 applinks REST. This is the same as V1 plus extra methods for
 * authentication configuration.
 *
 * @since 5.2
 */
public class ApplinkV2Request extends ApplinkV1Request {
    @Nonnull
    public static ApplinkV2Request forApplink(@Nonnull TestApplink.Side applinkSide) {
        return new Builder(applinkSide).build();
    }

    private ApplinkV2Request(Builder builder) {
        super(builder);
    }

    public static final class Builder extends BaseBuilder<Builder, ApplinkV2Request> {
        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side side) {
            super(side);
        }

        @Nonnull
        @Override
        public ApplinkV2Request build() {
            return new ApplinkV2Request(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}

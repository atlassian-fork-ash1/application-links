package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink.Side;
import com.atlassian.applinks.test.data.applink.config.AbstractTestApplinkConfigurator;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapabilitiesRestTester;
import com.atlassian.applinks.test.rest.capabilities.ApplinksRemoteCapilitiesRequest;

import javax.annotation.Nonnull;

/**
 * Test Applink configurator that refreshes remote capabilities for a given Applink {@link Side}.
 *
 * @since 5.0
 */
public class RemoteCapabilitiesConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static RemoteCapabilitiesConfigurator refreshRemoteCapabilities() {
        return new RemoteCapabilitiesConfigurator();
    }

    @Override
    public void configureSide(@Nonnull Side side) {
        new ApplinksCapabilitiesRestTester(side.product())
                .getRemote(new ApplinksRemoteCapilitiesRequest.Builder(side.id())
                        .authentication(TestAuthentication.admin())
                        .maxAge(0)
                        .build());
    }
}

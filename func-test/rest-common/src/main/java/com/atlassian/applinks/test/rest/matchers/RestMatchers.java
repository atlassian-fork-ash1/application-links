package com.atlassian.applinks.test.rest.matchers;

import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

/**
 * @since 4.3
 */
public final class RestMatchers {
    private RestMatchers() {
        throw new AssertionError("Do not instantiate " + RestMatchers.class.getName());
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static Matcher<Map<String, Object>> matchesEntity(@Nonnull Map<String, Object> entity) {
        List<Matcher<?>> allMatchers = Lists.newArrayList();
        for (Map.Entry<String, Object> entry : entity.entrySet()) {
            if (entry.getValue() instanceof Map) {
                Map<String, Object> subEntity = (Map<String, Object>) entry.getValue();
                allMatchers.add(new BaseRestMatcher(entry.getKey(), matchesEntity(subEntity)));
            } else {
                allMatchers.add(new BaseRestMatcher(entry.getKey(), is(entry.getValue())));
            }
        }
        return restEntity(allMatchers);
    }

    @Nonnull
    public static Matcher<Map<String, Object>> hasPropertyThat(@Nonnull String name, Matcher<?> matcher) {
        return new BaseRestMatcher(name, matcher);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static Matcher<Map<String, Object>> restEntity(@Nonnull Matcher<?>... allMatchers) {
        return restEntity(asList(allMatchers));
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static Matcher<Map<String, Object>> restEntity(@Nonnull Iterable<Matcher<?>> allMatchers) {
        return allOf((Iterable) allMatchers);
    }

    /**
     * @param itemMatchers item matchers
     * @return similar to {@link Matchers#contains(Matcher[])} but with a distinct name to avoid common pitfall of
     * resolving to the wrong overload {@link Matchers#contains(Object[])} instead
     */
    @Nonnull
    @SuppressWarnings({"unchecked", "ConfusingArgumentToVarargsMethod"})
    public static Matcher<java.lang.Iterable<?>> restList(Matcher<?>... itemMatchers) {
        return Matchers.contains((Matcher[]) itemMatchers);
    }

    @Nonnull
    public static Matcher<Map<String, Object>> hasNoProperty(@Nonnull String propertyName) {
        return new BaseRestMatcher(propertyName, Matchers.nullValue());
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E extends Enum<E>> Matcher<Iterable<? extends String>> containsEnumValue(
            @Nonnull E value) {
        return (Matcher) hasItem(value.name());
    }

    @Nonnull
    public static <E extends Enum<E>> Matcher<Iterable<? extends String>> containsEnumValues(
            @Nonnull Set<E> expectedValues) {
        return containsInAnyOrder(toEnumNames(expectedValues));
    }

    @Nonnull
    public static <E extends Enum<E>> Matcher<Iterable<? extends String>> containsAllEnumValues(
            @Nonnull Class<E> enumType) {
        requireNonNull(enumType, "enumType");
        return containsEnumValues(EnumSet.allOf(enumType));
    }

    private static <E extends Enum<E>> String[] toEnumNames(Set<E> expectedValues) {
        return expectedValues.stream().map(Enum::name).toArray(String[]::new);
    }
}

package com.atlassian.applinks.test.rest.client;

import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.status.ApplinkOAuthStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * @since 5.1
 */
public class ApplinkOAuthStatusClient {
    @Nonnull
    public ApplinkOAuthStatus getOAuthStatus(@Nonnull TestedInstance instance, @Nonnull String applinkId) {
        requireNonNull(applinkId, "applinkId");
        Response response = new ApplinkStatusRestTester(instance).getOAuth(new ApplinkOAuthStatusRequest.Builder(applinkId)
                .authentication(TestAuthentication.admin())
                .expectStatus(OK)
                .build());

        return response.as(RestApplinkOAuthStatus.class).asDomain();
    }

    @Nonnull
    public ApplinkOAuthStatus getOAuthStatus(@Nonnull TestApplink.Side side) {
        requireNonNull(side, "side");
        return getOAuthStatus(side.product(), side.id());
    }
}

package com.atlassian.applinks.test.rest.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Feature matcher for {@code Map} entities that are common results of REST requests mapped from JSON. Matches
 * a single JSON property.
 *
 * @since 4.3
 */
public class BaseRestMatcher extends FeatureMatcher<Map<String, Object>, Object> {
    private final String propertyName;

    @SuppressWarnings("unchecked")
    public BaseRestMatcher(@Nonnull String propertyName, @Nonnull Matcher<?> valueMatcher) {
        super((Matcher) requireNonNull(valueMatcher, "valueMatcher"), propertyName, propertyName);
        this.propertyName = requireNonNull(propertyName, "propertyName");
    }

    @Override
    protected Object featureValueOf(Map<String, Object> actual) {
        return actual.get(propertyName);
    }
}

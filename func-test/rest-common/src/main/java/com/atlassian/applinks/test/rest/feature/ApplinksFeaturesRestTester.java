package com.atlassian.applinks.test.rest.feature;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * REST tester for a specific Applinks feature resource.
 *
 * @since 4.3
 */
public class ApplinksFeaturesRestTester extends BaseRestTester {
    public ApplinksFeaturesRestTester(@Nonnull ApplinksRestUrls restUrls) {
        super(restUrls.features());
    }

    public ApplinksFeaturesRestTester(@Nonnull ApplinksRestUrls restUrls,
                                      @Nullable TestAuthentication defaultAuthentication) {
        super(restUrls.features(), defaultAuthentication);
    }

    /**
     * Get status of a specific Applinks feature
     *
     * @param request feature request
     * @return response
     */
    @Nonnull
    public Response get(@Nonnull ApplinksFeatureRequest request) {
        return super.get(request);
    }

    /**
     * Enable a specific Applinks feature
     *
     * @param request feature request
     * @return response
     */
    @Nonnull
    public Response put(@Nonnull ApplinksFeatureRequest request) {
        return super.put(request);
    }

    /**
     * Disable a specific Applinks feature
     *
     * @param request feature request
     * @return response
     */
    @Nonnull
    public Response delete(@Nonnull ApplinksFeatureRequest request) {
        return super.delete(request);
    }
}

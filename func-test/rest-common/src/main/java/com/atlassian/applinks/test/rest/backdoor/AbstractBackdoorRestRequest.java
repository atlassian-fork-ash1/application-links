package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

/**
 * @since 5.1
 */
public abstract class AbstractBackdoorRestRequest extends AbstractRestRequest {
    private static final String HANDLE_ERRORS = "handleErrors";

    protected AbstractBackdoorRestRequest(AbstractBuilder<?, ?> builder) {
        super(builder);
    }

    protected abstract static class AbstractBackdoorBuilder<B extends AbstractBackdoorBuilder<B, R>,
            R extends AbstractBackdoorRestRequest> extends AbstractBuilder<B, R> {
        protected AbstractBackdoorBuilder() {
            // handle errors by default
            handleErrors(true);
        }

        @Nonnull
        public final B handleErrors(boolean handleErrors) {
            // see TestExceptionInterceptor
            return queryParam(HANDLE_ERRORS, handleErrors);
        }
    }
}

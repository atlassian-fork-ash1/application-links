package com.atlassian.applinks.test.rest.oauth;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractApplinkRequest;

import javax.annotation.Nonnull;

public class ConsumerTokenRequest extends AbstractApplinkRequest {
    /**
     * Default request with no extra expectations for given {@code applinkId}.
     *
     * @param applinkId ID of the application link to examine
     * @return request to the status resource for {@code applinkId}
     */
    @Nonnull
    public static ConsumerTokenRequest forApplinkId(@Nonnull String applinkId) {
        return new Builder(applinkId).build();
    }

    public ConsumerTokenRequest(@Nonnull Builder builder) {
        super(builder);
    }

    public static class Builder extends AbstractApplinkRequestBuilder<Builder, ConsumerTokenRequest> {
        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side applink) {
            this(applink.id());
        }

        @Nonnull
        @Override
        public ConsumerTokenRequest build() {
            return new ConsumerTokenRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}

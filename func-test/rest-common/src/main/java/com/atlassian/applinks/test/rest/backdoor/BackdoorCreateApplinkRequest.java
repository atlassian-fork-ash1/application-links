package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.DefaultTestedInstance;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.model.RestAuthenticationConfiguration;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class BackdoorCreateApplinkRequest extends AbstractBackdoorRestRequest {
    final TestedInstance to;

    private final String name;
    private final TestAuthentication remoteAuthentication;
    private final AuthenticationScenario authenticationScenario;

    private BackdoorCreateApplinkRequest(Builder builder) {
        super(builder);
        this.to = builder.to;
        this.name = builder.name;
        this.remoteAuthentication = builder.remoteAuthentication;
        this.authenticationScenario = builder.authenticationScenario;
    }

    @Nullable
    @Override
    protected Object getBody() {
        BaseRestEntity entity = new BaseRestEntity.Builder()
                .add("name", name)
                .add("baseUrl", to.getBaseUrl())
                .add(RestAuthenticationConfiguration.USERNAME, remoteAuthentication.getUsername())
                .add(RestAuthenticationConfiguration.PASSWORD, remoteAuthentication.getPassword())
                .add("twoWay", true)
                .build();
        if (authenticationScenario != null) {
            entity.put(RestAuthenticationConfiguration.TRUSTED, authenticationScenario.isTrusted());
            entity.put(RestAuthenticationConfiguration.SHARED_USERBASE, authenticationScenario.isCommonUserBase());
        }
        return entity;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.EMPTY;
    }

    public static class Builder extends AbstractBackdoorBuilder<Builder, BackdoorCreateApplinkRequest> {
        private final TestedInstance to;
        private String name;
        private TestAuthentication remoteAuthentication = TestAuthentication.admin();
        private AuthenticationScenario authenticationScenario;

        public Builder(@Nonnull String remoteBaseUrl) {
            this.to = DefaultTestedInstance.fromBaseUrl(requireNonNull(remoteBaseUrl, "remoteBaseUrl"));
            setDefaultName();
        }

        public Builder(@Nonnull TestedInstance to) {
            this.to = requireNonNull(to, "to");
            setDefaultName();
        }

        @Nonnull
        public Builder name(@Nonnull String name) {
            this.name = requireNonNull(name, "name");

            return this;
        }

        @Nonnull
        public Builder remoteAuthentication(@Nonnull TestAuthentication remoteAuthentication) {
            requireNonNull(remoteAuthentication, "remoteAuthentication");
            this.remoteAuthentication = remoteAuthentication;

            return this;
        }

        @Nonnull
        public Builder authenticationScenario(@Nullable AuthenticationScenario authenticationScenario) {
            this.authenticationScenario = authenticationScenario;
            return this;
        }

        @Nonnull
        public Builder authenticationScenario(final boolean trusted, final boolean sharedUsers) {
            return authenticationScenario(new AuthenticationScenario() {
                @Override
                public boolean isCommonUserBase() {
                    return sharedUsers;
                }

                @Override
                public boolean isTrusted() {
                    return trusted;
                }
            });
        }

        @Nonnull
        @Override
        public BackdoorCreateApplinkRequest build() {
            requireNonNull(remoteAuthentication, "remoteAuthentication");
            return new BackdoorCreateApplinkRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }

        private void setDefaultName() {
            this.name = "Test link to " + to.getInstanceId();
        }
    }
}

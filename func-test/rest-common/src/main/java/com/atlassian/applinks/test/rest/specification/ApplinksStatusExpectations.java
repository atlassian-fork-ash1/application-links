package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkStatus;
import com.atlassian.applinks.internal.rest.model.status.RestAuthorisationUriAwareApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.rest.matchers.RestMatchers.hasNoProperty;
import static com.atlassian.applinks.test.rest.specification.ApplinkErrorExpectations.expectErrorType;
import static com.atlassian.applinks.test.rest.specification.ApplinksOAuthStatusExpectations.expectOAuthStatus;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.allOf;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.jsonPath;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * Specifications for expectations of results of Applinks Status requests.
 *
 * @since 4.3
 */
public class ApplinksStatusExpectations {
    private ApplinksStatusExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * @param applinkId expected applink ID
     * @return expectation that the Status result will be mapped to {@code applinkId}
     */
    @Nonnull
    public static ResponseSpecification expectStatusWithApplink(@Nonnull String applinkId) {
        requireNonNull(applinkId, "applinkId");
        return RestAssured.expect()
                .body(RestApplinkStatus.LINK, notNullValue())
                .body(jsonPath(RestApplinkStatus.LINK, RestMinimalApplicationLink.ID), is(applinkId));
    }

    /**
     * @param applinkId expected applink ID
     * @return expectation that the Status result indicates a working status mapped to {@code applinkId}
     * @see #expectStatusWithApplink(String)
     * @see #expectStatusWorking()
     */
    @Nonnull
    public static ResponseSpecification expectStatusWorking(@Nonnull String applinkId) {
        return allOf(expectStatusWithApplink(applinkId), expectStatusWorking());
    }

    /**
     * @param applinkId expected applink ID
     * @param type      expected error type
     * @return expectation that the Status result indicates an error status of specific {@code type}, mapped to
     * {@code applinkId}
     * @see #expectStatusWithApplink(String)
     * @see #expectStatusError(ApplinkErrorType)
     */
    @Nonnull
    public static ResponseSpecification expectStatusError(@Nonnull String applinkId, @Nonnull ApplinkErrorType type) {
        return allOf(expectStatusWithApplink(applinkId), expectStatusError(type));
    }

    /**
     * @return expectation that the Status result indicates a working applink
     */
    @Nonnull
    public static ResponseSpecification expectStatusWorking() {
        return RestAssured.expect()
                .body(RestApplinkStatus.WORKING, is(true))
                .body(RestApplinkStatus.ERROR, nullValue());
    }

    /**
     * @param errorType expected error type
     * @return expectation that the Status result indicates an error of specific {@code errorType}
     */
    @Nonnull
    public static ResponseSpecification expectStatusError(@Nonnull ApplinkErrorType errorType) {
        requireNonNull(errorType, "errorType");
        ResponseSpecification errorSpec = expectBody(RestApplinkStatus.WORKING, is(false));
        return allOf(errorSpec, expectErrorType(RestApplinkStatus.ERROR, errorType));
    }

    /**
     * @param detailsMatcher matcher for error details
     * @return expectation that the Status result indicates an error with details described by {@code detailsMatcher}
     */
    @Nonnull
    public static ResponseSpecification expectErrorDetails(@Nonnull Matcher<?> detailsMatcher) {
        return ApplinkErrorExpectations.expectErrorDetails(RestApplinkStatus.ERROR, detailsMatcher);
    }

    /**
     * @return expectation that the Status contains local authentication value
     */
    @Nonnull
    public static ResponseSpecification expectLocalAuthentication() {
        return expectBody(RestApplinkStatus.LOCAL_AUTHENTICATION, notNullValue());
    }

    /**
     * @param localAuthStatus expected local OAuth status
     * @return expectation that the Status contains local authentication corresponding to {@code localAuthStatus}
     */
    @Nonnull
    public static ResponseSpecification expectLocalAuthentication(@Nonnull ApplinkOAuthStatus localAuthStatus) {
        requireNonNull(localAuthStatus, "localAuthStatus");
        return expectOAuthStatus(RestApplinkStatus.LOCAL_AUTHENTICATION, localAuthStatus);
    }

    /**
     * @return expectation that the Status contains remote authentication value
     */
    @Nonnull
    public static ResponseSpecification expectRemoteAuthentication() {
        return expectBody(RestApplinkStatus.REMOTE_AUTHENTICATION, notNullValue());
    }

    /**
     * @param remoteAuthStatus expected remote OAuth status
     * @return expectation that the Status contains local authentication corresponding to {@code localAuthStatus}
     */
    @Nonnull
    public static ResponseSpecification expectRemoteAuthentication(@Nonnull ApplinkOAuthStatus remoteAuthStatus) {
        requireNonNull(remoteAuthStatus, "remoteAuthStatus");
        return expectOAuthStatus(RestApplinkStatus.REMOTE_AUTHENTICATION, remoteAuthStatus);
    }

    /**
     * @return expectation that the Status contains local authentication corresponding to {@code localAuthStatus}
     */
    @Nonnull
    public static ResponseSpecification expectNoRemoteAuthentication() {
        return expectBody(hasNoProperty(RestApplinkStatus.REMOTE_AUTHENTICATION));
    }

    // expectations for extra fields in the status error object

    @Nonnull
    public static ResponseSpecification expectAuthorisationUri(@Nonnull Matcher<?> authorisationUriMatcher) {
        requireNonNull(authorisationUriMatcher, "authorisationUriMatcher");
        return RestAssured.expect()
                .body(jsonPath(RestApplinkStatus.ERROR, RestAuthorisationUriAwareApplinkError.AUTHORISATION_URI),
                        authorisationUriMatcher);
    }

    @Nonnull
    public static ResponseSpecification expectNoAuthorisationUri() {
        return expectBody(jsonPath(RestApplinkStatus.ERROR),
                hasNoProperty(RestAuthorisationUriAwareApplinkError.AUTHORISATION_URI));
    }
}

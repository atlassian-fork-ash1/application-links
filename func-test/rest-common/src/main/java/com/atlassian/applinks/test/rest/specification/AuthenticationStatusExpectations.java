package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.rest.model.migration.RestAuthenticationConfig;
import com.atlassian.applinks.internal.rest.model.migration.RestAuthenticationStatus;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;

import java.util.Objects;

import static org.hamcrest.Matchers.notNullValue;

public final class AuthenticationStatusExpectations {

    private AuthenticationStatusExpectations() {
        throw new UnsupportedOperationException("Do not instantiate.");
    }

    @Nonnull
    public static ResponseSpecification expectIncoming(@Nonnull final AuthenticationConfig config) {
        return expectAuthenticationConfig(RestAuthenticationStatus.INCOMING, config);
    }

    @Nonnull
    public static ResponseSpecification expectOutgoing(@Nonnull final AuthenticationConfig config) {
        return expectAuthenticationConfig(RestAuthenticationStatus.OUTGOING, config);
    }

    @Nonnull
    public static ResponseSpecification expectCapability(@Nonnull final ApplinksCapabilities capability) {
        Objects.requireNonNull(capability, "capability");
        return RestAssured.expect().body(RestAuthenticationStatus.CAPABILITIES, Matchers.hasItem(capability.name()));
    }

    private static ResponseSpecification expectAuthenticationConfig(String prefix, final AuthenticationConfig config) {
        Objects.requireNonNull(config, "config");
        return RestAssured.expect()
                .body(prefix, notNullValue())
                .body(prefix + "." + RestAuthenticationConfig.OAUTH, Matchers.is(config.isOAuthConfigured()))
                .body(prefix + "." + RestAuthenticationConfig.BASIC, Matchers.is(config.isBasicConfigured()))
                .body(prefix + "." + RestAuthenticationConfig.TRUSTED, Matchers.is(config.isTrustedConfigured()));
    }
}

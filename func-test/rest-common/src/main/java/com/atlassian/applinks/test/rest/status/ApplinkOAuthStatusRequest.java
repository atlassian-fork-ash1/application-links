package com.atlassian.applinks.test.rest.status;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractApplinkRequest;
import com.atlassian.applinks.test.rest.url.ApplinksStatusRestUrls;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;

public class ApplinkOAuthStatusRequest extends AbstractApplinkRequest {
    /**
     * Default request with no extra expectations for given {@code applinkId}.
     *
     * @param applinkId ID of the application link to examine
     * @return request to the OAuth status resource for {@code applinkId}
     */
    @Nonnull
    public static ApplinkOAuthStatusRequest forApplinkId(@Nonnull String applinkId) {
        return new Builder(applinkId).build();
    }

    /**
     * Default update request with no extra expectations for given {@code applinkId}.
     *
     * @param applinkId   ID of the application link to examine
     * @param oAuthStatus status to update to
     * @return request to the OAuth status resource to update status for {@code applinkId}
     */
    @Nonnull
    public static ApplinkOAuthStatusRequest update(@Nonnull String applinkId, @Nonnull ApplinkOAuthStatus oAuthStatus) {
        return new Builder(applinkId).oAuthStatus(oAuthStatus).expectStatus(Response.Status.NO_CONTENT).build();
    }

    private final RestApplinkOAuthStatus status;

    private ApplinkOAuthStatusRequest(@Nonnull Builder builder) {
        super(builder);
        this.status = builder.status;
    }

    @Nullable
    @Override
    protected Object getBody() {
        return status;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return super.getPath().add(ApplinksStatusRestUrls.PATH_OAUTH);
    }

    public static final class Builder extends AbstractApplinkRequestBuilder<Builder, ApplinkOAuthStatusRequest> {
        private RestApplinkOAuthStatus status;

        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side side) {
            this(side.id());
        }

        public Builder(@Nonnull TestApplink applink) {
            this(applink.from());
        }

        @Nonnull
        @Override
        public ApplinkOAuthStatusRequest build() {
            return new ApplinkOAuthStatusRequest(this);
        }

        @Nonnull
        public Builder oAuthStatus(@Nonnull ApplinkOAuthStatus status) {
            this.status = new RestApplinkOAuthStatus(status);

            return this;
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}

package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Specifications for expectations of results of Applinks features requests.
 *
 * @since 4.3
 */
public class ApplinksFeaturesExpectations {
    private ApplinksFeaturesExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * Expect {@code feature} to be in a state described by {@code status} (enabled/disabled).
     *
     * @param feature feature to examine in the response
     * @param status  expected status
     * @return expectation to use while making request to the Applinks features resource
     */
    @Nonnull
    public static ResponseSpecification expectFeatureStatus(@Nonnull ApplinksFeatures feature, boolean status) {
        return RestAssured.expect().body(requireNonNull(feature, "feature").name(), Matchers.is(status));
    }

    @Nonnull
    public static ResponseSpecification expectEnabledFeature(@Nonnull ApplinksFeatures feature) {
        return expectFeatureStatus(feature, true);
    }

    @Nonnull
    public static ResponseSpecification expectDisabledFeature(@Nonnull ApplinksFeatures feature) {
        return expectFeatureStatus(feature, false);
    }

}

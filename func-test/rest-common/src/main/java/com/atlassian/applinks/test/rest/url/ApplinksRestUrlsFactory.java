package com.atlassian.applinks.test.rest.url;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls.RestModules;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Creates {@link ApplinksRestUrls} from {@link TestedInstance}.
 *
 * @since 4.3
 */
public final class ApplinksRestUrlsFactory {
    private ApplinksRestUrlsFactory() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static ApplinksRestUrls forProduct(@Nonnull TestedInstance product) {
        return forDefaultRestModule(product);
    }

    @Nonnull
    public static ApplinksRestUrls forDefaultRestModule(@Nonnull TestedInstance product) {
        requireNonNull(product, "product");
        return ApplinksRestUrls.forDefaultRestModule(product.getBaseUrl());
    }

    @Nonnull
    public static ApplinksRestUrls forDefaultRestModule(@Nonnull TestedInstance product, @Nonnull RestVersion version) {
        requireNonNull(product, "product");
        return ApplinksRestUrls.forRestModule(product.getBaseUrl(), RestModules.DEFAULT, version);
    }

    @Nonnull
    public static ApplinksRestUrls forTestRestModule(@Nonnull TestedInstance product) {
        requireNonNull(product, "product");
        return ApplinksRestUrls.forRestModule(product.getBaseUrl(), RestModules.APPLINKS_TESTS);
    }

    @Nonnull
    public static ApplinksRestUrls forOAuthRestModule(@Nonnull TestedInstance product) {
        requireNonNull(product, "product");
        return ApplinksRestUrls.forRestModule(product.getBaseUrl(), RestModules.APPLINKS_OAUTH);
    }

    /**
     * @param instance tested instance
     * @return REST URLs for non-Applinks REST module
     */
    @Nonnull
    public static ApplinksRestUrls forExternalRestModule(@Nonnull TestedInstance instance, @Nonnull String module) {
        requireNonNull(instance, "product");
        return new ApplinksRestUrls(instance.getBaseUrl(), RestUrl.forPath(module), RestVersion.LATEST.getPath());
    }
}

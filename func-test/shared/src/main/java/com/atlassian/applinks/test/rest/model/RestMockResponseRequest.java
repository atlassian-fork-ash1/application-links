package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.test.response.MockResponseDefinition;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class RestMockResponseRequest extends BaseRestEntity {
    public static final String REQUEST_PREDICATE = "requestPredicate";
    public static final String RESPONSE_DEFINITION = "responseDefinition";

    @SuppressWarnings("unused") // for deserialization
    public RestMockResponseRequest() {
    }

    public RestMockResponseRequest(@Nonnull RestRequestPredicate requestPredicate,
                                   @Nonnull MockResponseDefinition responseDefinition) {
        requireNonNull(requestPredicate, "requestPredicate");

        put(REQUEST_PREDICATE, requestPredicate);
        put(RESPONSE_DEFINITION, new RestMockResponseDefinition(responseDefinition));
    }

    @Nonnull
    public RestRequestPredicate getRequestPredicate() {
        return new RestRequestPredicate(getRequiredJson(REQUEST_PREDICATE));
    }

    @Nonnull
    public RestMockResponseDefinition getResponseDefinition() {
        return getRequiredRestEntity(RESPONSE_DEFINITION, RestMockResponseDefinition.class);
    }
}

package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * For user management resource provided by Refapp.
 *
 * @since 5.0
 */
public class RestUser extends BaseRestEntity {
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String FULL_NAME = "fullName";
    public static final String PERMISSION_LEVEL = "permissionLevel";

    public RestUser() // for deserialization
    {
    }

    private RestUser(Map<String, Object> fields) {
        super(fields);
    }

    @Nullable
    public String getName() {
        return getString(NAME);
    }

    @Nullable
    public String getFullName() {
        return getString(FULL_NAME);
    }

    @Nullable
    public String getEmail() {
        return getString(EMAIL);
    }

    public PermissionLevel getPermissionLevel() {
        return getEnum(PERMISSION_LEVEL, PermissionLevel.class);
    }

    public static class Builder extends BaseRestEntity.Builder {

        @Nonnull
        public Builder permissionLevel(@Nullable PermissionLevel permissionLevel) {
            add(PERMISSION_LEVEL, permissionLevel);
            return this;
        }

        @Nonnull
        public Builder name(@Nullable String name) {
            add(NAME, name);
            return this;
        }

        @Nonnull
        public Builder email(@Nullable String email) {
            add(EMAIL, email);
            return this;
        }

        @Nonnull
        public Builder fullName(@Nullable String fullName) {
            add(FULL_NAME, fullName);
            return this;
        }

        @Nonnull
        public RestUser build() {
            return new RestUser(fields);
        }
    }
}

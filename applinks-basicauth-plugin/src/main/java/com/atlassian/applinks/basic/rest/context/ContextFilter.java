package com.atlassian.applinks.basic.rest.context;

/**
 * Extension of {@link com.atlassian.applinks.core.rest.context.ContextFilter} used to fors BND/OSGi Import-Package to bring in the package.
 *
 * @since 5.0.0
 */
public class ContextFilter extends com.atlassian.applinks.core.rest.context.ContextFilter {
}

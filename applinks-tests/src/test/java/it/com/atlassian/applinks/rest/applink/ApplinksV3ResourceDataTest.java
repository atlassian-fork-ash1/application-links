package it.com.atlassian.applinks.rest.applink;

import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.test.application.twitter.TwitterApplicationType;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.applink.ApplinkV3Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV3Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV3RestTester;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.backdoor.BackdoorCreateSideApplinkRequest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.rest.applink.data.AtlassianApplicationDataProvider.ATLASSIAN;
import static com.atlassian.applinks.internal.rest.applink.data.ConfigUrlDataProvider.CONFIG_URL;
import static com.atlassian.applinks.internal.rest.applink.data.EditableDataProvider.EDITABLE;
import static com.atlassian.applinks.internal.rest.applink.data.EditableDataProvider.V3_EDITABLE;
import static com.atlassian.applinks.internal.rest.applink.data.IconUriDataProvider.ICON_URI;
import static com.atlassian.applinks.internal.rest.applink.data.WebItemsDataProvider.WEB_ITEMS;
import static com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities.APPLICATION_VERSION;
import static com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities.APPLINKS_VERSION;
import static com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities.CAPABILITIES;
import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configureSide;
import static com.atlassian.applinks.test.data.applink.config.ApplinkPropertyConfigurator.setProperty;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.matcher.StringMatchers.nonEmptyString;
import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withId;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectValidVersion;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectVersion;
import static com.atlassian.applinks.test.rest.matchers.RestExtendedApplicationLinkMatchers.withDataThat;
import static com.atlassian.applinks.test.rest.matchers.RestExtendedApplicationLinkMatchers.withPropertyThat;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsAllEnumValues;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.hasPropertyThat;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.restEntity;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.rest.applink.ApplinksV3ResourceTest.containsInAnyOrder;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * Tests for the extra data attached to V3 applinks response, such as properties, capabilities etc.
 */
@Category(RefappTest.class)
public class ApplinksV3ResourceDataTest {
    @Rule
    public final RuleChain restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule testApplink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);

    private final ApplinksV3RestTester applinksRestTester = new ApplinksV3RestTester(REFAPP1);

    private final ApplinksBackdoor backdoor = new ApplinksBackdoor(REFAPP1);

    @Test
    public void getAllWithCustomProperty() {
        String randomId1 = backdoor.createRandomGeneric().id();
        String randomId2 = backdoor.createRandomGeneric().id();

        testApplink.configureFrom(
                setProperty("custom-property1", "custom-value1"),
                setProperty("custom-property2", "custom-value2")
        );

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .property("custom-property1")
                .property("custom-property2")
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        withId(testApplink.from().id()),
                        withId(randomId1),
                        withId(randomId2)
                )))
                .specification(expectBody(hasItem(restEntity(
                        withId(testApplink.from().id()),
                        withPropertyThat("custom-property1", is("custom-value1")),
                        withPropertyThat("custom-property2", is("custom-value2"))
                ))))
                .build());
    }

    @Test
    public void getAllWithRemoteCapabilitiesData() {
        String randomId1 = backdoor.createRandomGeneric().id();
        String randomId2 = backdoor.createRandomGeneric().id();

        // set version on Refapp2 and refresh remote capabilities
        refapp2Version.setApplinksVersion("5.0.5");
        testApplink.configureFrom(refreshRemoteCapabilities());

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data("applicationVersion")
                .data("capabilities")
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        withId(testApplink.from().id()),
                        withId(randomId1),
                        withId(randomId2)
                )))
                .specification(expectBody(hasItem(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat("applicationVersion", nonEmptyString()),
                        withDataThat("capabilities", restEntity(
                                hasPropertyThat(CAPABILITIES, containsAllEnumValues(ApplinksCapabilities.class)),
                                hasPropertyThat(APPLICATION_VERSION, expectValidVersion()),
                                hasPropertyThat(APPLINKS_VERSION, expectVersion(5, 0, 5))
                        ))
                ))))
                .build());
    }

    @Test
    public void getAllWithIconData() {
        String randomApplinkId = backdoor.createRandomGeneric().id();
        String twitterApplinkId = backdoor.createRandom(TwitterApplicationType.class).id();

        // set version on Refapp2 and refresh remote capabilities
        refapp2Version.setApplinksVersion("5.0.5");
        testApplink.configureFrom(refreshRemoteCapabilities());

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data(ICON_URI)
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        restEntity(
                                withId(testApplink.from().id()),
                                withDataThat(ICON_URI, endsWith("128refapp.png"))),
                        restEntity(
                                withId(randomApplinkId),
                                withDataThat(ICON_URI, endsWith("128generic.png"))),
                        restEntity(
                                withId(twitterApplinkId),
                                withDataThat(ICON_URI, nullValue()))
                )))
                .build());
    }

    @Test
    public void getAllWithConfigUrlData() {
        String randomUrl1 = createRandomUrl();
        String randomUrl2 = createRandomUrl();
        String randomId1 = backdoor.createGeneric(randomUrl1).id();
        String randomId2 = backdoor.createGeneric(randomUrl2).id();

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data(CONFIG_URL)
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        restEntity(
                                withId(testApplink.from().id()),
                                withDataThat(CONFIG_URL, is(REFAPP2.getBaseUrl() + "/plugins/servlet/applinks/listApplicationLinks"))),
                        restEntity(
                                withId(randomId1),
                                withDataThat(CONFIG_URL, is(randomUrl1))),
                        restEntity(
                                withId(randomId2),
                                withDataThat(CONFIG_URL, is(randomUrl2)))
                )))
                .build());
    }

    @Test
    public void getAllWithEditableData() {
        TestApplink.Side systemLink = backdoor.createRandomGeneric();
        TestApplink.Side genericLink = backdoor.createRandomGeneric();
        configureSide(systemLink, setSystemLink());

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data(EDITABLE)
                .data(V3_EDITABLE)
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        restEntity(
                                withId(testApplink.from().id()),
                                withDataThat(EDITABLE, is(true)),
                                withDataThat(V3_EDITABLE, is(true))),
                        restEntity(
                                withId(systemLink.id()),
                                withDataThat(EDITABLE, is(false)), // system link non-editable
                                withDataThat(V3_EDITABLE, is(false))), // generic link non-V3-editable
                        restEntity(
                                withId(genericLink.id()),
                                withDataThat(EDITABLE, is(true)),
                                withDataThat(V3_EDITABLE, is(false))) // generic link non-V3-editable
                )))
                .build());
    }

    @Test
    public void getAllWithWebItemsData() {
        // see applinks-tests plugin descriptor, "twitter-applink-action" web-item
        TestApplink.Side twitterLink = backdoor.createSide(new BackdoorCreateSideApplinkRequest.Builder(createRandomUrl())
                .type(TwitterApplicationType.class)
                .name("Twitter link")
                .build());

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data(WEB_ITEMS)
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        restEntity(
                                withId(testApplink.from().id()),
                                withDataThat(WEB_ITEMS, emptyIterable())),
                        restEntity(
                                withId(twitterLink.id()),
                                withDataThat(WEB_ITEMS, contains(
                                        // contains 1 web-item specified by "twitter-applink-action"
                                        // Refapp's web item helper relies on an i18n context param to resolve
                                        restEntity(
                                                hasPropertyThat("url", is("http://twitter.com")),
                                                hasPropertyThat("label", is("applinks-tests.actions.applink.twitter")),
                                                hasPropertyThat("tooltip", is("applinks-tests.actions.applink.twitter.tooltip"))
                                        )
                                )))
                )))
                .build());
    }

    @Test
    public void getAllWithAtlassianApplicationData() {
        String genericApplinkId = backdoor.createRandomGeneric().id();
        String twitterApplinkId = backdoor.createRandom(TwitterApplicationType.class).id();
        String jiraApplinkId = backdoor.createRandom(JiraApplicationType.class).id();

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .data(ATLASSIAN)
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        restEntity(
                                withId(testApplink.from().id()),
                                withDataThat(ATLASSIAN, is(true))),
                        restEntity(
                                withId(jiraApplinkId),
                                withDataThat(ATLASSIAN, is(true))),
                        restEntity(
                                withId(genericApplinkId),
                                withDataThat(ATLASSIAN, is(false))),
                        restEntity(
                                withId(twitterApplinkId),
                                withDataThat(ATLASSIAN, is(false)))
                )))
                .build());
    }

    @Test
    public void getAllWithCustomNonExistentPropertyAndData() {
        String randomId1 = backdoor.createRandomGeneric().id();
        String randomId2 = backdoor.createRandomGeneric().id();

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .property("nonexistent")
                .data("nonexistent")
                .expectStatus(Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        withId(testApplink.from().id()),
                        withId(randomId1),
                        withId(randomId2)
                )))
                .specification(expectBody(hasItem(restEntity(
                        withId(testApplink.from().id()),
                        withPropertyThat("nonexistent", nullValue()),
                        withDataThat("nonexistent", nullValue())
                ))))
                .build());
    }

    @Test
    public void getSingleWithCustomProperty() {
        testApplink.configureFrom(
                setProperty("custom-property1", "custom-value1"),
                setProperty("custom-property2", "custom-value2")
        );

        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .property("custom-property1")
                .property("custom-property2")
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withPropertyThat("custom-property1", is("custom-value1")),
                        withPropertyThat("custom-property2", is("custom-value2"))
                )))
                .build());
    }

    @Test
    public void getSingleWithRemoteCapabilitiesData() {
        // set version on Refapp2 and refresh remote capabilities
        refapp2Version.setApplinksVersion("5.0.5");
        testApplink.configureFrom(refreshRemoteCapabilities());

        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .data("applicationVersion")
                .data("capabilities")
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat("applicationVersion", nonEmptyString()),
                        withDataThat("capabilities", restEntity(
                                hasPropertyThat(CAPABILITIES, containsAllEnumValues(ApplinksCapabilities.class)),
                                hasPropertyThat(APPLICATION_VERSION, expectValidVersion()),
                                hasPropertyThat(APPLINKS_VERSION, expectVersion(5, 0, 5))
                        ))
                )))
                .build());
    }

    @Test
    public void getSingleWithIconData() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .data(ICON_URI)
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat(ICON_URI, endsWith("128refapp.png"))
                )))
                .build());
    }

    @Test
    public void getSingleWithConfigUrlData() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .data("configUrl")
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat("configUrl", is(REFAPP2.getBaseUrl() + "/plugins/servlet/applinks/listApplicationLinks")))))
                .build());
    }

    @Test
    public void getSingleWithConfigUrlDataConfluence() {
        String confluenceUrl = createRandomUrl();
        TestApplink.Side confluenceApplink = backdoor.createSide(new BackdoorCreateSideApplinkRequest.Builder(confluenceUrl)
                .type(ConfluenceApplicationType.class)
                .build());

        applinksRestTester.get(new ApplinkV3Request.Builder(confluenceApplink)
                .authentication(REFAPP_ADMIN_BETTY)
                .data("configUrl")
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(confluenceApplink.id()),
                        withDataThat("configUrl", is(confluenceUrl + "/admin/listapplicationlinks.action")))))
                .build());
    }

    @Test
    public void getSingleWithEditableData() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .data(EDITABLE)
                .data(V3_EDITABLE)
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat(EDITABLE, is(true)),
                        withDataThat(V3_EDITABLE, is(true)))))
                .build());
    }

    @Test
    public void getSingleWithEditableDataSystemGenericLink() {
        TestApplink.Side systemGenericLink = backdoor.createRandomGeneric();
        configureSide(systemGenericLink, setSystemLink());

        applinksRestTester.get(new ApplinkV3Request.Builder(systemGenericLink)
                .authentication(REFAPP_ADMIN_BETTY)
                .data(EDITABLE)
                .data(V3_EDITABLE)
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(systemGenericLink.id()),
                        withDataThat(EDITABLE, is(false)), // system link non-editable
                        withDataThat(V3_EDITABLE, is(false))))) // generic link non-V3-editable
                .build());
    }

    @Test
    public void getSingleWithWebItemsData() {
        // see applinks-tests plugin descriptor, "twitter-applink-action" web-item
        TestApplink.Side twitterLink = backdoor.createSide(new BackdoorCreateSideApplinkRequest.Builder(createRandomUrl())
                .type(TwitterApplicationType.class)
                .name("Twitter link")
                .build());

        applinksRestTester.get(new ApplinkV3Request.Builder(twitterLink)
                .authentication(REFAPP_ADMIN_BETTY)
                .data(WEB_ITEMS)
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(twitterLink.id()),
                        withDataThat(WEB_ITEMS, contains(
                                // contains 1 web-item specified by "twitter-applink-action"
                                // Refapp's web item helper relies on an i18n context param to resolve
                                restEntity(
                                        hasPropertyThat("url", is("http://twitter.com")),
                                        hasPropertyThat("label", is("applinks-tests.actions.applink.twitter")),
                                        hasPropertyThat("tooltip", is("applinks-tests.actions.applink.twitter.tooltip"))
                                )
                        )))
                ))
                .build());
    }

    @Test
    public void getSingleWithAtlassianApplicationData() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .data(ATLASSIAN)
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withDataThat(ATLASSIAN, is(true))
                )))
                .build());
    }

    @Test
    public void getSingleWithCustomNonExistentPropertyAndData() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .property("nonexistent")
                .data("nonexistent")
                .expectStatus(Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withPropertyThat("nonexistent", nullValue()),
                        withDataThat("nonexistent", nullValue())
                )))
                .build());
    }
}

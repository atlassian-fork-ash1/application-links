package it.com.atlassian.applinks.auth.basic;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.pageobjects.SimpleApplinksClient;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.BasicAccessAuthenticationSection;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.AbstractFullApplinksTest;
import it.com.atlassian.applinks.testing.categories.BasicAuthTest;
import it.com.atlassian.applinks.testing.categories.ConfigurationTest;
import it.com.atlassian.applinks.testing.categories.MultipleProductTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * PageObjects based test for Basic Auth edit screens.
 *
 * @since 5.0.0
 */
@Category({RefappTest.class, BasicAuthTest.class, ConfigurationTest.class, MultipleProductTest.class})
public class EditConfigurationTest extends AbstractFullApplinksTest {

    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public EditConfigurationTest() {
        applinksFeaturesRule = new ApplinksFeaturesRule(ProductInstances.fromProductInstance(getProductOne().getProductInstance()))
                .withDisabledFeatures(V3_UI);
    }

    @Before
    public void setUp() {
        LoginClient.loginAsSysadmin(productOne, productTwo);
        SimpleApplinksClient.createReciprocalAtlassianAppLinkViaRest(this.getProductOne(), this.getProductTwo());
    }

    @Test
    public void verifyOutgoingConfigurationCanBeEdited() {
        ListApplicationLinkPage listApplicationLinks = (ListApplicationLinkPage) this.getProductOne().visit(ListApplicationLinkPage.class);

        ApplicationDetailsSection applicationDetailsSection = listApplicationLinks.configureApplicationLink(this.getProductTwo().getProductInstance().getBaseUrl());
        final String username = "abcde";
        final String password = "12345";

        BasicAccessAuthenticationSection basicAccessAuthenticationSection = applicationDetailsSection.openOutgoingBasicAccess();
        basicAccessAuthenticationSection.setUsername(username).setPassword(password).enable();

        assertThat(basicAccessAuthenticationSection.isConfigured(), is(true));

        basicAccessAuthenticationSection.disable();

        assertThat(basicAccessAuthenticationSection.isNotConfigured(), is(false));
    }
}

package it.com.atlassian.applinks.refapp_refapp.v2;

import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.v2.InformationDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.net.URLEncoder;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * Test the applications response to communication problems between applications during applink creation.
 *
 * @since v4.0.0
 */
@Category({RefappTest.class})
public class CreateApplicationLinkCommunicationProblemTest extends V2AbstractApplinksTest {
    @Test
    public void verifyCreateRemoteEndOfApplicationLinkFailsGracefullyWhenOriginatingAppIsUncontactable()
            throws Exception {
        // a url that gives no response
        String originatingUrl = "http://a2a3a4a5a65a67a7a";
        verifyUalLinkCannotBeCreated(originatingUrl);
    }

    @Test
    public void verifyCreateRemoteEndOfApplicationLinkFailsGracefullyWhenOriginatingManifestIsUnobtainable()
            throws Exception {
        // a url that gives back some kind of server response, but not a manifest
        String originatingUrl = PRODUCT.getProductInstance().getBaseUrl() + "/foo";
        verifyUalLinkCannotBeCreated(originatingUrl);
    }

    private void verifyUalLinkCannotBeCreated(String originatingUrl) {
        loginAsSysadmin(PRODUCT);

        String url = PRODUCT.getProductInstance().getBaseUrl() + "/plugins/servlet/applinks/listApplicationLinks?applinkStartingUrl=" + URLEncoder.encode(originatingUrl);
        InformationDialog page = PRODUCT.visit(ListApplicationLinkPage.class).loadUrlReciprocalManifestUnobtainable(url);
        Poller.waitUntilTrue(page.isAt());
        assertThat(page.getInformation(), containsString(String.format("Click Continue to be redirected back to %s.", originatingUrl)));
    }

}

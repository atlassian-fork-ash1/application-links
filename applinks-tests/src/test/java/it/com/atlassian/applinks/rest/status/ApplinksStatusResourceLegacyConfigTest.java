package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuth;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Tests Status resource when running against applications configured with 3Lo only
 *
 * @since 5.0
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceLegacyConfigTest {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);

    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void shouldNotSupport3LoOnlyForRemoteIncoming() {
        applink.configureTo(enableOAuth(OAuthConfig.createThreeLoOnlyConfig(), OAuthConfig.createDefaultOAuthConfig()));

        assertAuthLevelUnsupported();
    }

    @Test
    public void shouldNotSupport3LoOnlyForRemote() {
        applink.configureTo(enableOAuth(OAuthConfig.createDefaultOAuthConfig(), OAuthConfig.createThreeLoOnlyConfig()));

        assertAuthLevelUnsupported();
    }

    @Test
    public void shouldNotSupport3LoOnlyForLocalIncoming() {
        applink.configureFrom(enableOAuth(OAuthConfig.createThreeLoOnlyConfig(), OAuthConfig.createDefaultOAuthConfig()));

        assertAuthLevelUnsupported();
    }

    @Test
    public void shouldNotSupport3LoOnlyForLocalOutgoing() {
        applink.configureFrom(enableOAuth(OAuthConfig.createDefaultOAuthConfig(), OAuthConfig.createThreeLoOnlyConfig()));

        assertAuthLevelUnsupported();
    }

    @Test
    public void shouldNotSupport3LoOnlyForLocalIncomingAndImpersonationForOutGoing() {
        applink.configureFrom(enableOAuth(OAuthConfig.createThreeLoOnlyConfig(), OAuthConfig.createOAuthWithImpersonationConfig()));

        assertAuthLevelUnsupported();
    }

    @Test
    public void shouldNotSupport3LoOnlyForLocalOutgoingAndImpersonationForIncoming() {
        applink.configureFrom(enableOAuth(OAuthConfig.createOAuthWithImpersonationConfig(), OAuthConfig.createThreeLoOnlyConfig()));

        assertAuthLevelUnsupported();
    }

    private void assertAuthLevelUnsupported() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .specification(expectLocalAuthentication())
                .specification(expectRemoteAuthentication())
                .build());
    }
}

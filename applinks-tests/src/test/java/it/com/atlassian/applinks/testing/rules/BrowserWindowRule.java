package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.getFirst;

/**
 * Closes all non-default windows after each test
 *
 * @since 5.2
 */
public class BrowserWindowRule extends TestWatcher {
    private final WebDriver webDriver;

    private String defaultWindowHandle;

    @Inject
    public BrowserWindowRule(@Nonnull WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public BrowserWindowRule(@Nonnull Iterable<TestedProduct<WebDriverTester>> products) {
        this(checkNotNull(getFirst(products, null)).getTester().getDriver());
    }

    @Override
    protected void starting(Description description) {
        defaultWindowHandle = webDriver.getWindowHandle();
    }

    @Override
    protected void finished(Description description) {
        checkState(defaultWindowHandle != null, "Default window handle not set, starting() has not run?");
        for (String handle : webDriver.getWindowHandles()) {
            if (!defaultWindowHandle.equals(handle)) {
                webDriver.switchTo().window(handle);
                webDriver.close();
            }
        }
        webDriver.switchTo().window(defaultWindowHandle);
    }
}

package it.com.atlassian.applinks.smoke.v3;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.page.AdminHomePage;
import com.atlassian.webdriver.applinks.externalpage.GenericAdminPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractApplinksSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.Products;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertTrue;

/**
 * Verifies that Applinks admin links are present in Confluence for both V2 and V3, depending on the NEW_UI feature.
 *
 * @since 4.3
 */
@Category(SmokeTest.class)
@Products(SupportedProducts.CONFLUENCE)
public class ConfluenceApplinksAdminLinkSmokeTest extends AbstractApplinksSmokeTest<ConfluenceTestedProduct> {
    @Rule
    public final ApplinksFeaturesRule applinksFeatures;

    public ConfluenceApplinksAdminLinkSmokeTest(@Nonnull ConfluenceTestedProduct mainProduct,
                                                @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeatures = new ApplinksFeaturesRule(mainInstance);
    }

    @Test
    public void shouldShowApplinksAdminLinkInV2() {
        applinksFeatures.disable(ApplinksFeatures.V3_UI);

        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V2", adminPage.hasConfigureApplinksLink());

        ListApplicationLinkPage v2ApplinksAdminPage = adminPage.goToConfigureApplinks(ListApplicationLinkPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v2ApplinksAdminPage.isApplinksListFullyLoaded());
    }

    @Test
    public void shouldShowApplinksAdminLinkInV3() {
        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V3", adminPage.hasConfigureApplinksLink());

        V3ListApplicationLinksPage v3ApplinksAdminPage = adminPage.goToConfigureApplinks(
                V3ListApplicationLinksPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v3ApplinksAdminPage.isLoaded());
    }

    private GenericAdminPage loginAndGoToAdmin() {
        LoginClient.loginAsSysadminAndGoTo(mainProduct, AdminHomePage.class);
        return mainProduct.getPageBinder().bind(GenericAdminPage.class);
    }
}

package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;

import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Test;

import it.com.atlassian.applinks.V2AbstractApplinksTest;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertTrue;

/**
 * 1. creates (non-ual) link to the non-existing base URL http://does.not.exist.com:12345
 * 2. asserts that the Relocate action is displayed
 * 3. clicks it and enters a non-existing URL
 * 4. asserts that a warning dialog is rendered
 * 5. clicks previous, enters localhost:5992/refapp and clicks ok
 * 6. asserts the dialog closes and the relocate action is gone
 */
@Category(RefappTest.class)
public class RelocateTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() {
        loginAsSysadmin(PRODUCT);
    }

    private static final String CLICK_HERE_TO_RELOCATE = "Click here to Relocate.";

    @Test
    public void testRelocate() throws Exception {
        createUnreachableLink();

        ListApplicationLinkPage listApplicationLink = PRODUCT.visit(ListApplicationLinkPage.class);

        // Check that we have some visible warning
        assertTrue(listApplicationLink.getPageWarning().contains("Application 'JIRA' seems to be offline."));
        // Check that the relocate link exists
        assertTrue(listApplicationLink.getPageWarning().contains(CLICK_HERE_TO_RELOCATE));

    }

    private void createUnreachableLink() throws Exception {
        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, "http://does.not.exist.com:12345", "JIRA");
        PRODUCT.visit(ListApplicationLinkPage.class);
    }
}

package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import com.atlassian.applinks.test.rule.MockResponseRule;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;
import org.osgi.framework.Version;

import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.data.util.TestUrls.withRandomPort;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthDanceConfigurator.setUp3LoAccess;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.applinks.test.rest.model.RestRequestPredicate.restRequestTo;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectErrorDetails;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectNoRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static it.com.atlassian.applinks.testing.response.MockResponses.manifestResourceRequest;
import static it.com.atlassian.applinks.testing.response.MockResponses.oAuthProblemResponse;
import static it.com.atlassian.applinks.testing.response.MockResponses.oauthStatusRequest;
import static it.com.atlassian.applinks.testing.response.MockResponses.textPlainOkResponse;
import static it.com.atlassian.applinks.testing.response.MockResponses.unexpectedStatusResponse;
import static it.com.atlassian.applinks.testing.response.MockResponses.v24xAuthenticationRequest;
import static it.com.atlassian.applinks.testing.response.MockResponses.v25xAuthenticationRequest;

/**
 * REST tests for Status resource, where the connection to the remote application fails due networking problems.
 *
 * @see com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#NETWORK_ERROR
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceNetworkErrorTest {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksCapabilitiesRule refapp2Capabilities = new ApplinksCapabilitiesRule(REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);
    @Rule
    public final MockResponseRule refapp2Responses = new MockResponseRule(REFAPP2);

    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void statusConnectionRefused() {
        // configure default OAuth then break applink URL by setting unused port - this should result in connection
        // refused
        applink.configure(enableDefaultOAuth())
                .configureFrom(setUrl(withRandomPort(REFAPP2.getLoopbackUrl())));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.CONNECTION_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusConnectionRefusedNoStatusApiCapability() {
        disableStatusApiOnRefapp2();

        // configure default OAuth then break applink URL by setting unused port - this should result in connection
        // refused
        // then refresh capabilities again to make sure they contain the network error
        applink.configure(enableDefaultOAuth())
                .configureFrom(setUrl(withRandomPort(REFAPP2.getLoopbackUrl())))
                .configureFrom(refreshRemoteCapabilities());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.CONNECTION_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnresolvedHost() {
        // configure default OAuth then break applink URL
        applink.configure(enableDefaultOAuth())
                .configureFrom(setUrl(createRandomUrl()));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNKNOWN_HOST))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthTimestampRefused() {
        applink.configure(enableDefaultOAuth());
        // mock out OAuth request to manifest to return timestamp_refused
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void statusOAuthTimestampRefusedV2DefaultOAuthNoAccess() {
        disableStatusApiOnRefapp2();

        // configure default OAuth, no access token
        applink.configure(enableDefaultOAuth());
        // mock out OAuth request to manifest to return timestamp_refused
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthTimestampRefusedV2DefaultOAuthWithAccessToken() {
        disableStatusApiOnRefapp2();

        // configure default OAuth, with established access token
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));
        // mock out request to OAuth Applink resource to return timestamp_refused
        refapp2Responses.addMockResponse(restRequestTo(RestUrl.forPath("applinks-oauth")
                        .add(".+")
                        .add("applicationlink")
                        .add(".+")
                        .add("authentication")).hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthTimestampRefusedV2OAuthWithImpersonation() {
        disableStatusApiOnRefapp2();
        // configure OAuth with impersonation
        applink.configure(enableOAuthWithImpersonation());
        // mock out request to OAuth Applink resource to return timestamp_refused
        refapp2Responses.addMockResponse(restRequestTo(RestUrl.forPath("applinks-oauth")
                        .add(".+")
                        .add("applicationlink")
                        .add(".+")
                        .add("authentication")).hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthSignatureInvalid() {
        applink.configure(enableDefaultOAuth());
        // mock out OAuth request to manifest to return signature_invalid
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_SIGNATURE_INVALID));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_SIGNATURE_INVALID))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void statusOAuthSignatureInvalidV2DefaultOAuthNoAccess() {
        disableStatusApiOnRefapp2();

        // configure default OAuth, no access token
        applink.configure(enableDefaultOAuth());
        // mock out OAuth request to manifest to return signature_invalid
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_SIGNATURE_INVALID));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_SIGNATURE_INVALID))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthSignatureInvalidV2DefaultOAuthWithAccessToken() {
        disableStatusApiOnRefapp2();

        // configure default OAuth, with established access token
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));
        // mock out request to OAuth Applink resource to return signature_invalid
        refapp2Responses.addMockResponse(restRequestTo(RestUrl.forPath("applinks-oauth")
                        .add(".+")
                        .add("applicationlink")
                        .add(".+")
                        .add("authentication")).hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_SIGNATURE_INVALID));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_SIGNATURE_INVALID))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusOAuthSignatureInvalidV2OAuthWithImpersonation() {
        disableStatusApiOnRefapp2();
        // configure OAuth with impersonation
        applink.configure(enableOAuthWithImpersonation());
        // mock out request to OAuth Applink resource to return signature_invalid
        refapp2Responses.addMockResponse(restRequestTo(RestUrl.forPath("applinks-oauth")
                        .add(".+")
                        .add("applicationlink")
                        .add(".+")
                        .add("authentication")).hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_SIGNATURE_INVALID));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_SIGNATURE_INVALID))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseStatus() {
        applink.configure(enableDefaultOAuth());
        // request to OAuth status returns 400
        refapp2Responses.addMockResponse(oauthStatusRequest().build(),
                unexpectedStatusResponse(Response.Status.BAD_REQUEST));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(Matchers.is("400: Bad Request")))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponse() {
        applink.configure(enableDefaultOAuth());
        // request to OAuth status returns unexpected contents
        refapp2Responses.addMockResponse(oauthStatusRequest().build(),
                textPlainOkResponse("Surprise!\nMore surprise!!"));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE))
                .specification(expectErrorDetails(Matchers.nullValue()))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseStatusV25xDefaultOAuthNoAccessToken() {
        disableStatusApiOnRefapp2();
        applink.configure(enableDefaultOAuth());
        // OAuth verification request to manifest returns 400
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                unexpectedStatusResponse(Response.Status.BAD_REQUEST));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(Matchers.is("400: Bad Request")))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseV25xDefaultOAuthNoAccessToken() {
        disableStatusApiOnRefapp2();
        applink.configure(enableDefaultOAuth());
        // OAuth verification request to manifest returns unexpected contents
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                textPlainOkResponse("Surprise!\nMore surprise!!"));

        // NOTE: this is "expected" behaviour as we only make HEAD request in case there's no local access token
        // at this stage it's not worth making a full blown manifest request and validating its contents
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseStatusV25xDefaultOAuthWithAccessToken() {
        disableStatusApiOnRefapp2();
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));
        // request to 5.x authentication returns 400
        refapp2Responses.addMockResponse(v25xAuthenticationRequest().hasOAuthHeader(true).build(),
                unexpectedStatusResponse(Response.Status.BAD_REQUEST));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(Matchers.is("400: Bad Request")))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseV25xDefaultOAuthWithAccessToken() {
        disableStatusApiOnRefapp2();
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));
        // request to 5.x authentication returns unexpected contents
        refapp2Responses.addMockResponse(v25xAuthenticationRequest().hasOAuthHeader(true).build(),
                textPlainOkResponse("Surprise!\nMore surprise!!"));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE))
                .specification(expectErrorDetails(Matchers.nullValue()))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseStatusV25xOAuthImpersonation() {
        disableStatusApiOnRefapp2();

        applink.configure(enableOAuthWithImpersonation());

        // request to 5.x authentication returns 400
        refapp2Responses.addMockResponse(v25xAuthenticationRequest().hasOAuthHeader(true).build(),
                unexpectedStatusResponse(Response.Status.BAD_REQUEST));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(Matchers.is("400: Bad Request")))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusUnexpectedResponseStatusV24xDefaultOAuthWithAccessToken() {
        // make Refapp2 V2 4.x
        disableStatusApiOnRefapp2(new Version(4, 3, 10));
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));
        // OAuth verification request to manifest returns 400
        refapp2Responses.addMockResponse(v24xAuthenticationRequest().hasOAuthHeader(true).build(),
                unexpectedStatusResponse(Response.Status.BAD_REQUEST));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(Matchers.is("400: Bad Request")))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    private void disableStatusApiOnRefapp2() {
        disableStatusApiOnRefapp2(new Version(5, 0, 5, "something"));
    }

    private void disableStatusApiOnRefapp2(Version version) {
        // disable STATUS_API and change version to force remote capabilities on Refapp1 to update
        refapp2Version.setApplinksVersion(version);
        refapp2Capabilities.disable(ApplinksCapabilities.STATUS_API);
        applink.configureFrom(refreshRemoteCapabilities());
    }
}

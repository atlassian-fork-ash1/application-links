package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import com.google.common.collect.Iterables;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enable3LoOnly;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openqa.selenium.net.PortProber.findFreePort;

/**
 * Tests for diagnostics dialog contents for selected error conditions.
 *
 * @since 5.0
 */
@Category(RefappTest.class)
public class V3StatusDiagnosticsDialogTest extends AbstractApplinksTest {
    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2)
            .name("Test Refapp")
            .configure(enableDefaultOAuth())
            .build();

    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);

    @Test
    public void statusConnectionRefused() {
        // trigger network error
        String invalidUrl = baseUrlWithPort(REFAPP2, findFreePort());
        testApplink.configureFrom(setUrl(invalidUrl));

        ApplinkRow applinkRow = openV3UiApplink();
        ApplinkRow.DiagnosticsDialog dialog = applinkRow.openDiagnosticsDialog();

        assertConnectionRefusedContents(dialog.getContents(), invalidUrl, applinkRow.getName());
        validateHelpLinkUrl(dialog.getTroubleshootingUrl());
        dialog.edit();
    }

    @Test
    public void statusConfigError() {
        // trigger config error
        testApplink.configureTo(enableOAuthWithImpersonation());

        ApplinkRow applinkRow = openV3UiApplink();
        ApplinkRow.DiagnosticsDialog dialog = applinkRow.openDiagnosticsDialog();

        assertMismatchContents(dialog.getContents(), applinkRow.getName());
        validateHelpLinkUrl(dialog.getTroubleshootingUrl());

        // close the dialog by clicking on status again
        applinkRow.getStatus().toggle();
        waitUntilFalse(dialog.isOpen());
    }

    @Test
    public void statusAuthLevelUnsupported() {
        testApplink.configureFrom(enable3LoOnly());

        ApplinkRow applinkRow = openV3UiApplink();
        ApplinkRow.DiagnosticsDialog dialog = applinkRow.openDiagnosticsDialog();

        assertAuthLevelUnsupportedContents(dialog.getContents());
        assertEquals("Update", dialog.getEditButtonText());
        validateHelpLinkUrl(dialog.getTroubleshootingUrl());
        dialog.edit();
    }

    @Test
    public void statusDeprecated() {
        testApplink.configure(enableTrustedApps());

        ApplinkRow applinkRow = openV3UiApplink();
        ApplinkRow.DiagnosticsDialog dialog = applinkRow.openDiagnosticsDialog();

        assertDeprecatedContents(dialog.getContents());
        assertEquals("Update", dialog.getUpdateButtonText());
        validateHelpLinkUrl(dialog.getTroubleshootingUrl());
    }

    @Test
    public void statusRemoteVersionIncompatible() {
        refapp2Version.setApplinksVersion("4.0.12");
        testApplink.configureFrom(refreshRemoteCapabilities());

        ApplinkRow applinkRow = openV3UiApplink();
        ApplinkRow.DiagnosticsDialog dialog = applinkRow.openDiagnosticsDialog();

        assertVersionIncompatibleContents(dialog);
        waitUntilFalse(dialog.hasTroubleshootingLink());

        dialog.close();
    }

    private void assertConnectionRefusedContents(Iterable<String> contents, String url, String applinkName) {
        assertThat(contents, iterableWithSize(1));

        assertThat(Iterables.get(contents, 0), containsInOrder(
                "connection to",
                url,
                "was refused",
                ". " + applinkName + " may be down" // make sure link text correct
        ));

    }

    private void assertMismatchContents(Iterable<String> contents, String applinkName) {
        assertThat(contents, iterableWithSize(3));

        assertThat(Iterables.get(contents, 0), containsInOrder(
                "local incoming connection",
                "OAuth",
                "however " + applinkName + " ", // make sure link text correct
                "OAuth with impersonation"
        ));

        assertThat(Iterables.get(contents, 1), containsInOrder(
                "local outgoing connection",
                "OAuth",
                "however Test",
                "OAuth with impersonation"
        ));
    }

    private void assertAuthLevelUnsupportedContents(Iterable<String> contents) {
        assertThat(contents, iterableWithSize(1));

        assertThat(Iterables.get(contents, 0), containsInOrder(
                "The current OAuth",
                "prevents integrations from working",
                "OAuth",
                "OAuth with impersonation"
        ));
    }

    private void assertDeprecatedContents(Iterable<String> contents) {
        assertThat(contents, iterableWithSize(2));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "As well as OAuth, this application link still ",
                "uses either the ",
                "Trusted Applications ",
                "or Basic Access authentication type",
                "both of which are deprecated"
        ));
    }

    private void assertVersionIncompatibleContents(ApplinkRow.DiagnosticsDialog dialog) {
        assertEquals("Unable to retrieve status", dialog.getTitle());
        Iterable<String> contents = dialog.getContents();
        assertThat(contents, iterableWithSize(1));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "We're unable to determine the status of this link",
                "version of Test Refapp is a little too old",
                "upgrade Test Refapp to the latest version of Reference Application"
        ));
    }

    private void validateHelpLinkUrl(String helpLinkUrl) {
        assertNotNull(helpLinkUrl);
        List<String> splitUrl = Arrays.asList(helpLinkUrl.split("/"));
        assertThat(splitUrl, not(contains("undefined")));
        assertThat(URI.create(helpLinkUrl).getHost(), is("confluence.atlassian.com"));
    }

    private ApplinkRow openV3UiApplink() {
        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(PRODUCT, V3ListApplicationLinksPage.class);
        waitUntil(page.getApplinksRows(), iterableWithSize(1));
        return page.getApplinkRow(testApplink.from().applicationId());
    }

    private static String baseUrlWithPort(ProductInstances instance, int port) {
        return instance.getLoopbackUrl().replace(Integer.toString(instance.getHttpPort()), Integer.toString(port));
    }
}

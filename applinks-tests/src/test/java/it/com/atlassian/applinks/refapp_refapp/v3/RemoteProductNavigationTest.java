package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;

import java.net.URI;
import java.util.Set;
import java.util.UUID;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.GO_TO_REMOTE_ACTION_ID;
import static java.lang.String.format;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@Category(RefappTest.class)
public class RemoteProductNavigationTest extends AbstractApplinksTest {
    private static final String APPLINKS_LIST_PATH = "/plugins/servlet/applinks/listApplicationLinks";

    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2)
            .configure(OAuthApplinkConfigurator.enableDefaultOAuth())
            .build();

    private final ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(INSTANCE1);

    private V3ListApplicationLinksPage page;


    @Before
    public void setup() {
        loginAsSysadmin(PRODUCT2);
        page = loginAsSysadminAndGoTo(PRODUCT, V3ListApplicationLinksPage.class);

        waitUntil(page.getApplinksRows(), Matchers.<ApplinkRow>iterableWithSize(1));
        waitUntilTrue(page.getApplinkRow(testApplink.from().applicationId()).hasVersion());
    }

    @Test
    public void shouldNavigateToAtlassianRemoteProductAppLinksPage() {
        final WebDriver driver = PRODUCT.getTester().getDriver();
        final URI remoteProductUri = URI.create(testApplink.to().product().getBaseUrl() + APPLINKS_LIST_PATH);

        ApplinkRow row = page.getApplinkRow(testApplink.from().applicationId());
        V3ApplinkActionDropDown actionDropDown = row.openActionsDropdown();

        waitUntil(actionDropDown.getActionIds(), hasItem(GO_TO_REMOTE_ACTION_ID));

        actionDropDown.goToRemote();

        runInNewWindowAndAssertBrowserUrl(driver, () -> {
            URI uri = URI.create(driver.getCurrentUrl());
            assertThat(uri.getHost(), is(remoteProductUri.getHost()));
            assertThat(uri.getScheme(), is(remoteProductUri.getScheme()));
            assertThat(uri.getPath(), is(remoteProductUri.getPath()));
        });
    }

    @Test
    public void shouldNavigateToGenericRemoteProductDisplayUrl() {
        final URI remoteProductUri = URI.create(format("http://www.%s.com", UUID.randomUUID().toString()));
        final WebDriver driver = PRODUCT.getTester().getDriver();
        TestApplink.Side applink = applinksBackdoor.createGeneric(remoteProductUri.toString());

        driver.navigate().refresh();

        ApplinkRow row = page.getApplinkRow(applink.applicationId());
        V3ApplinkActionDropDown actionDropDown = row.openActionsDropdown();

        waitUntil(actionDropDown.getActionIds(), hasItem(GO_TO_REMOTE_ACTION_ID));

        actionDropDown.goToRemote();

        runInNewWindowAndAssertBrowserUrl(driver, () -> {
            URI uri = URI.create(driver.getCurrentUrl());
            assertThat(uri.getHost(), is(remoteProductUri.getHost()));
            assertThat(uri.getScheme(), is(remoteProductUri.getScheme()));
            assertThat(uri.getPath(), is("/")); // make sure APPLINKS_LIST_PATH is not in the path
        });
    }

    private void runInNewWindowAndAssertBrowserUrl(WebDriver driver, Runnable assertionCallback) {
        String parentWindow = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();

        for (String windowHandle : handles) {
            if (!windowHandle.equals(parentWindow)) {
                try {
                    driver.switchTo().window(windowHandle);
                    assertionCallback.run();
                    return;
                } finally {
                    driver.close(); // close the child window
                    driver.switchTo().window(parentWindow);
                }
            }
        }
        throw new IllegalStateException("Child Window not found, assertions did not run");
    }
}

package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.backdoor.SalBackdoor;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@Category(RefappTest.class)
public class V3OnboardingDialogTest extends AbstractApplinksTest {
    private static final ProductInstances TESTED_PRODUCT = ProductInstances.REFAPP1;
    private static final TestAuthentication TEST_USER = TestAuthentication.admin();

    private final SalBackdoor backdoor = new SalBackdoor(TESTED_PRODUCT.getBaseUrl());

    @Test
    public void v3FeatureDiscoveryDialogShouldAppearWhenV3UiFirstSeen() {
        // simulates "first seen" case by deleting all user settings
        backdoor.cleanUpUserSettings(TEST_USER);

        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(V3ListApplicationLinksPage.class);

        waitUntilTrue(page.onboardingCheckHasRun());
        waitUntilTrue(page.onboardingDialogPresent());
    }

    @Test
    public void v3FeatureDiscoveryDialogShouldNotAppearWhenV3UiSeenBefore() {
        // DefaultSettingsRule in ApplinksRuleChain automatically discovers the feature, so no feature discovery needs
        // to be done here
        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(V3ListApplicationLinksPage.class);

        waitUntilTrue(page.onboardingCheckHasRun());
        waitUntilFalse(page.onboardingDialogPresent());
    }
}

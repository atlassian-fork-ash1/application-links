package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.webdriver.applinks.component.AppLinkAdminLogin;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.BasicAccessAuthenticationSection;
import com.atlassian.webdriver.applinks.component.TrustedApplicationAuthenticationSection;
import com.atlassian.webdriver.applinks.component.XsrfWarning;
import com.atlassian.webdriver.applinks.externalcomponent.WebSudoPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * <br>
 * This test creates an application link between the two refapps in {@link #setup()}.
 *
 * <br>
 * Tests are performed against the "Trusted Applications" and "Basic Access" tabs of both the "Incoming Authentication"
 * and "Outgoing Authentication" views for configuring a link. This test is <i>not</i> about testing the actual work
 * done on those screens so much as it is about testing that modifications to the atl_token on the page triggers an
 * XSRF warning. Secondarily, they also test that modifications made before that warning is triggered are not lost when
 * the "Retry Operation" button is clicked.
 */
@Category(RefappTest.class)
public class XsrfProtectionTestRefApp extends V2AbstractApplinksTest {
    private ApplicationDetailsSection detailsSection;
    private String remoteUrl;

    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);


    @Before
    public void setup() {
        applink.configure(enableTrustedApps());
        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        List<ListApplicationLinkPage.ApplicationLinkEntryRow> links = listApplicationLinkPage.getApplicationLinks();
        remoteUrl = links.get(0).getApplicationUrl();
        detailsSection = PRODUCT.visit(ListApplicationLinkPage.class).configureApplicationLink(remoteUrl);
    }

    @Test
    public void testXsrfForIncomingTrustedApplication() {
        TrustedApplicationAuthenticationSection trustedApp = detailsSection.openIncomingTrustedApplications();
        handleAdminLoginScreen();
        runTrustedApplicationTest(trustedApp);
    }

    @Test
    public void testXsrfForOutgoingTrustedApplication() {
        TrustedApplicationAuthenticationSection trustedApp = detailsSection.openOutgoingTrustedApplications();
        handleAdminLoginScreen();
        runTrustedApplicationTest(trustedApp);
    }

    @Test
    public void testXsrfForIncomingBasicAccess() {
        BasicAccessAuthenticationSection basic = detailsSection.openIncomingBasicAccess();
        handleAdminLoginScreen();
        runBasicAccessTest(basic);
    }

    @Test
    public void testXsrfForOutgoingBasicAccess() {
        BasicAccessAuthenticationSection basic = detailsSection.openOutgoingBasicAccess();
        handleAdminLoginScreen();
        runBasicAccessTest(basic);
    }

    private void runBasicAccessTest(BasicAccessAuthenticationSection basic) {
        basic.setUsername(SYSADMIN_USERNAME)
                .setPassword(SYSADMIN_PASSWORD)
                .modifyAtlToken()
                .enable();

        XsrfWarning xsrfWarning = basic.getXsrfWarning();
        assertTrue("Modifying the atl_token should have resulted in an XSRF warning", xsrfWarning.isVisible());

        xsrfWarning.retryOperation();
        assertTrue("Retrying the operation should have saved the basic configuration", basic.isConfigured());

        basic.safeClose();
    }

    private void runTrustedApplicationTest(TrustedApplicationAuthenticationSection trusted) {
        //Note: The IP pattern set here needs to be flexible enough to ensure the two refapps can still talk to each
        //      other. Otherwise, teardown() fails because the confirmation for deleting the secondary link is missing
        trusted.setIpPatterns("*.*.*.*")
                .modifyAtlToken()
                .update();

        XsrfWarning xsrfWarning = trusted.getXsrfWarning();
        assertTrue("Modifying the atl_token should have resulted in an XSRF warning", xsrfWarning.isVisible());

        xsrfWarning.retryOperation();
        handleAdminLoginScreen();
        assertEquals("Retrying the operation should have persisted the IP Patterns", "*.*.*.*", trusted.getIpPatterns());

        trusted.safeClose();
    }

    private void handleAdminLoginScreen() {
        AppLinkAdminLogin<WebSudoPage> adminLogin = PRODUCT.getPageBinder().bind(AppLinkAdminLogin.class, new WebSudoPage());
        adminLogin.handleWebLoginIfRequired(SYSADMIN_USERNAME, SYSADMIN_PASSWORD);
        WebSudoPage webSudo = PRODUCT.getPageBinder().bind(WebSudoPage.class);
        webSudo.handleIfRequired(SYSADMIN_PASSWORD);
    }
}
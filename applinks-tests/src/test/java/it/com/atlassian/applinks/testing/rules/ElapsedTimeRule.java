package it.com.atlassian.applinks.testing.rules;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Simple Rule to log to the console the time taken to run tests and/or associated rules.
 *
 * @since 4.0.15
 */
public class ElapsedTimeRule extends TestWatcher {
    private long startTime;
    private long endTime;
    private final String name;

    public ElapsedTimeRule(final String name) {
        this.name = name;
    }

    @Override
    protected void starting(Description description) {
        startTime = System.currentTimeMillis();
        System.out.println(String.format("Starting '%s - %s'", name, description));

    }

    @Override
    protected void finished(Description description) {
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Finished '%s - %s'. Elapsed Time '%d' ms", name, description, endTime - startTime));
    }
}

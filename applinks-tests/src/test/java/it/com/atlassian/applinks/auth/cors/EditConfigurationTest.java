package it.com.atlassian.applinks.auth.cors;

import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.CorsAuthenticationSection;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.AbstractFullApplinksTest;
import it.com.atlassian.applinks.testing.categories.ConfigurationTest;
import it.com.atlassian.applinks.testing.categories.CorsAuthTest;
import it.com.atlassian.applinks.testing.categories.MultipleProductTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkRule;
import it.com.atlassian.applinks.testing.rules.LoginRule;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import java.io.ByteArrayOutputStream;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static org.junit.Assert.assertTrue;

/**
 * Tests both enabling and disabling CORS requests with credentials, as well as invoking the {@code CorsTestServlet}
 * to verify the {@code CorsDefaults} implementation is functioning as expected.
 */
@Category({RefappTest.class, CorsAuthTest.class, ConfigurationTest.class, MultipleProductTest.class})
public class EditConfigurationTest extends AbstractFullApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkRule.forProducts(this.getProductOne(), this.getProductTwo()).around(LoginRule.forProducts(this.getProductOne(), this.getProductTwo()));
    
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public EditConfigurationTest() {
        applinksFeaturesRule = new ApplinksFeaturesRule(ProductInstances.fromProductInstance(getProductOne().getProductInstance()))
                .withDisabledFeatures(V3_UI);
    }

    private ApplicationDetailsSection detailsSection;

    @Before
    public void setup() {

        String url = this.getProductTwo().getProductInstance().getBaseUrl();

        ListApplicationLinkPage linkPage = (ListApplicationLinkPage) this.getProductOne().visit(ListApplicationLinkPage.class);

        detailsSection = linkPage.configureApplicationLink(url);
    }

    @Test
    public void testConfiguringCors() throws Exception {
        //http://localhost:5990/refapp/plugins/servlet/applinks/applinks-tests/cors-test?action=test&origin=http://localhost:5992
        HttpClient client = new DefaultHttpClient();
        String testUrl = new StringBuilder(this.getProductOne().getProductInstance().getBaseUrl())
                .append("/plugins/servlet/applinks/applinks-tests/cors-test?action=test&origin=")
                .append(URIUtil.utf8Encode(this.getProductTwo().getProductInstance().getBaseUrl()))
                .toString();

        CorsAuthenticationSection cors = detailsSection.openIncomingCors();
        assertTrue("CORS requests with credentials should be disabled by default", cors.isNotConfigured());
        assertTestResults(client, testUrl, false);
        cors.enable();
        assertTrue("Clicking Enable should have enabled credentialed CORS requests", cors.isConfigured());
        assertTestResults(client, testUrl, true);
        cors.disable();
        assertTrue("Clicking Disable should have disabled credentialed CORS requests", cors.isNotConfigured());
        assertTestResults(client, testUrl, false);
    }

    private static void assertTestResults(HttpClient client, String testUrl, boolean allowsCredentials) throws Exception {
        //Polls the CorsTestServlet to determine the current CORS status for PRODUCT2 in PRODUCT
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        HttpResponse response = client.execute(new HttpGet(testUrl));
        response.getEntity().writeTo(stream);
        String html = stream.toString();

        //Because the AppLink between PRODUCT and PRODUCT2 always exists, the origin should always be allowed
        assertTrue("Origin should always be allowed", html.contains("id=\"allows-origin\">true<"));
        if (allowsCredentials) {
            //If credentials should be allowed, ensure they are and that the Authorization header is also allowed
            assertTrue("Credentials should be allowed", html.contains("id=\"allows-credentials\">true<"));
            assertTrue("Authorization header should be allowed when credentials are",
                    html.contains("id=\"allowed-request-headers\">Authorization<"));
        } else {
            //Otherwise, ensure they are not allowed and that no headers are allowed
            assertTrue("Credentials should not be allowed", html.contains("id=\"allows-credentials\">false<"));
            assertTrue("No headers should be allowed when credentials are not allowed",
                    html.contains("id=\"allowed-request-headers\"><"));
        }
    }
}

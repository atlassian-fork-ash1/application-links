package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class CreateApplicationLinkFormValidationTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() {
        login();
        OAuthApplinksClient.assertNoApplicationLinksAreConfigured(PRODUCT);
    }

    @Test
    @Ignore("flaky test")
    public void testOrderAndPrimaryLinks() throws Exception {
        ListApplicationLinkPage applicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, "http://localhost:4567/jra1", "Adam JIRA");
        assertFalse(applicationLinkPage.getApplicationLinks().get(0).canMakePrimary());

        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, "http://localhost:4567/jra2", "Bob JIRA");
        assertTrue(applicationLinkPage.getApplicationLinks().get(1).canMakePrimary());

        List<ListApplicationLinkPage.ApplicationLinkEntryRow> appLinks = applicationLinkPage.getApplicationLinks();
        assertEquals(2, appLinks.size());
        assertFalse(appLinks.get(0).canMakePrimary());
        assertTrue(appLinks.get(1).canMakePrimary());

        applicationLinkPage.togglePrimaryLink("http://localhost:4567/jra2");
        assertTrue(appLinks.get(0).canMakePrimary());
        assertFalse(appLinks.get(1).canMakePrimary());

        applicationLinkPage.deleteApplicationLink("http://localhost:4567/jra1").deleteOneWayLink();
        String info = applicationLinkPage.getPageInfo();
        assertTrue(info.contains("Deleted Application Link 'Adam JIRA' successfully."));

        applicationLinkPage.deleteApplicationLink("http://localhost:4567/jra2").deleteOneWayLink();
        info = applicationLinkPage.getPageInfo();
        assertTrue(info.contains("Deleted Application Link 'Bob JIRA' successfully."));

        OAuthApplinksClient.assertNoApplicationLinksAreConfigured(PRODUCT);
    }
}

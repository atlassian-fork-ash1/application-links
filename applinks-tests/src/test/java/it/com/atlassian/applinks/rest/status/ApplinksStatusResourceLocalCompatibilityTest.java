package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectNoRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Tests Status resource running on a locally unsupported installation (containing system links, unsupported auth etc.)
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceLocalCompatibilityTest {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);


    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void notCompatibleForSystemApplink() {
        applink.configureFrom(enableDefaultOAuth(), setSystemLink());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.SYSTEM_LINK))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void notCompatibleForGenericApplink() {
        TestApplink.Side genericLink = new ApplinksBackdoor(REFAPP1).createGeneric("http://generic.com");

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(genericLink)
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(genericLink.id(), ApplinkErrorType.GENERIC_LINK))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.OFF))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void notCompatibleForApplinkOlderThan4_0_13() {
        refapp2Version.setApplinksVersion("4.0.12");
        // refresh remote capabilities to fetch the "new" version
        applink.configureFrom(enableDefaultOAuth(), refreshRemoteCapabilities());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }
}

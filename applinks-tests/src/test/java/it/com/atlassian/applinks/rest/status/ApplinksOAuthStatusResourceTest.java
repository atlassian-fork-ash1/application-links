package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.rest.status.ApplinkOAuthStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import java.util.UUID;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;
import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.data.util.TestUrls.withRandomPort;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.disableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.singleError;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withErrors;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withSummaryThat;
import static com.atlassian.applinks.test.rest.specification.ApplinksOAuthStatusExpectations.expectIncomingOAuthStatus;
import static com.atlassian.applinks.test.rest.specification.ApplinksOAuthStatusExpectations.expectOAuthStatus;
import static com.atlassian.applinks.test.rest.specification.ApplinksOAuthStatusExpectations.expectOutgoingOAuthStatus;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Category(RefappTest.class)
public class ApplinksOAuthStatusResourceTest {
    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);

    @Rule
    public final TestApplinkRule applink = new TestApplinkRule.Builder(REFAPP1, REFAPP2)
            .configure(enableDefaultOAuth()) // create 3LO+2LO by default
            .build();

    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void notFoundGivenNotExistingLinkId() {
        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(UUID.randomUUID().toString())
                .expectStatus(NOT_FOUND)
                .build());
    }

    @Test
    public void defaultOAuthStatusForAnonymousUserOnBothEnds() {
        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .specification(expectOAuthStatus(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void defaultOAuthStatusForAuthenticatedUser() {
        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_USER_BARNEY)
                .specification(expectOAuthStatus(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void defaultOAuthStatusForAdmin() {
        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .specification(expectOAuthStatus(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void defaultOAuthStatusForSysadmin() {
        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.SYSADMIN)
                .specification(expectOAuthStatus(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void threeLoOnlyOAuthStatusForAnonymous() {
        applink.configure(enableOAuth(createThreeLoOnlyConfig(), createThreeLoOnlyConfig()));

        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .specification(expectIncomingOAuthStatus(createThreeLoOnlyConfig()))
                .specification(expectOutgoingOAuthStatus(createThreeLoOnlyConfig()))
                .build());
    }

    @Test
    public void oAuthWithImpersonationStatusForAnonymous() {
        applink.configure(enableOAuthWithImpersonation());

        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .specification(expectOAuthStatus(ApplinkOAuthStatus.IMPERSONATION))
                .build());
    }

    @Test
    public void differentOAuthLevelsStatusForAnonymous() {
        applink.configure(enableOAuth(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig()));

        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .specification(expectIncomingOAuthStatus(createDefaultOAuthConfig()))
                .specification(expectOutgoingOAuthStatus(createOAuthWithImpersonationConfig()))
                .build());
    }

    @Test
    public void putNotFoundGivenNonExistingApplink() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(UUID.randomUUID().toString())
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .oAuthStatus(ApplinkOAuthStatus.DEFAULT)
                .expectStatus(Status.NOT_FOUND)
                .build());
    }

    @Test
    public void putNotAvailableToAnonymous() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .oAuthStatus(ApplinkOAuthStatus.DEFAULT)
                .expectStatus(Status.UNAUTHORIZED)
                .specification(expectBody(singleError("You need to be authenticated to change OAuth level.")))
                .build());
    }

    @Test
    public void putNotAvailableToNonAdmins() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_USER_BARNEY)
                .oAuthStatus(ApplinkOAuthStatus.DEFAULT)
                .expectStatus(Status.FORBIDDEN)
                .specification(expectBody(singleError("You need to be an administrator to change OAuth level.")))
                .build());
    }

    @Test
    public void putShouldUpdateOAuthConfig() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .oAuthStatus(ApplinkOAuthStatus.DEFAULT)
                .expectStatus(Status.NO_CONTENT)
                .build());

        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(anonymous())
                .specification(expectOAuthStatus(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void putToEnableIncomingImpersonationRequiresSysadmin() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .oAuthStatus(new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()))
                .expectStatus(Status.FORBIDDEN)
                .specification(expectBody(
                        singleError("You need to be a system administrator to configure OAuth with Impersonation.")))
                .build());
    }

    @Test
    public void putToEnableOutgoingImpersonationRequiresSysadmin() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .oAuthStatus(new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()))
                .expectStatus(Status.FORBIDDEN)
                .specification(expectBody(
                        singleError("You need to be a system administrator to configure OAuth with Impersonation.")))
                .build());
    }

    @Test
    public void putShouldEnableImpersonationForSysadmin() {
        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.SYSADMIN)
                .oAuthStatus(ApplinkOAuthStatus.IMPERSONATION)
                .expectStatus(Status.NO_CONTENT)
                .build());

        refapp1statusTester.getOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .specification(expectIncomingOAuthStatus(createOAuthWithImpersonationConfig()))
                .specification(expectOutgoingOAuthStatus(createOAuthWithImpersonationConfig()))
                .build());
    }

    @Test
    public void putToEnableIncomingOAuthShouldResultInErrorIfRemoteNotAvailable() {
        // disable OAuth and "break" remote URL to trigger connection refused
        applink.configureFrom(disableOAuth(), setUrl(withRandomPort(REFAPP2.getLoopbackUrl())));

        refapp1statusTester.putOAuth(new ApplinkOAuthStatusRequest.Builder(applink)
                .authentication(ApplinksAuthentications.REFAPP_ADMIN_BETTY)
                .oAuthStatus(ApplinkOAuthStatus.DEFAULT)
                .expectStatus(Status.CONFLICT)
                .specification(expectBody(withErrors(withSummaryThat(containsInOrder(
                        "We can't enable the incoming authentication because", "is not reachable.")))))
                .build());
    }
}

package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.internal.test.application.twitter.TwitterApplicationType;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;

import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configureSide;
import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkRow.withId;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.ERROR_SYSTEM;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.DELETE_ACTION_ID;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.GO_TO_REMOTE_ACTION_ID;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.MAKE_PRIMARY_ACTION_ID;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;

@Category(RefappTest.class)
public class V3SystemLinkTest extends AbstractApplinksTest {
    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2)
            .configureFrom(setSystemLink())
            .build();

    private final ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(INSTANCE1);

    @Test
    public void systemLinkHasCorrectStatusAndIsNotEditable() {
        TestApplink.Side anotherRefapp = applinksBackdoor.createRandom(RefAppApplicationType.class);
        configureSide(anotherRefapp, setSystemLink());

        V3ListApplicationLinksPage applinksPage = goToV3Ui();

        ApplinkRow primaryApplinkRow = applinksPage.getApplinkRow(testApplink.from());
        assertSystemLink(primaryApplinkRow);
        assertSystemLinkActions(primaryApplinkRow, GO_TO_REMOTE_ACTION_ID);

        //Reload page to close the dropdown so the next row actions are visible
        applinksPage = goToV3Ui();
        ApplinkRow anotherApplinkRow = applinksPage.getApplinkRow(anotherRefapp);
        assertSystemLink(anotherApplinkRow);
        assertSystemLinkActions(anotherApplinkRow, GO_TO_REMOTE_ACTION_ID, MAKE_PRIMARY_ACTION_ID);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void correctSortingAndFilteringGivenAtlassianAndNonAtlassianSystemLinks() {
        TestApplink.Side systemGenericLink = applinksBackdoor.createRandomGeneric();
        TestApplink.Side nonSystemGenericLink = applinksBackdoor.createRandomGeneric();
        TestApplink.Side systemJiraLink = applinksBackdoor.createRandom(JiraApplicationType.class);
        TestApplink.Side nonSystemJiraLink = applinksBackdoor.createRandom(JiraApplicationType.class);
        TestApplink.Side systemConfluenceLink = applinksBackdoor.createRandom(ConfluenceApplicationType.class);
        TestApplink.Side nonSystemConfluenceLink = applinksBackdoor.createRandom(ConfluenceApplicationType.class);
        TestApplink.Side systemTwitterLink = applinksBackdoor.createRandom(TwitterApplicationType.class);
        TestApplink.Side nonSystemTwitterLink = applinksBackdoor.createRandom(TwitterApplicationType.class);

        configureSide(systemGenericLink, setSystemLink());
        configureSide(systemJiraLink, setSystemLink());
        configureSide(systemConfluenceLink, setSystemLink());
        configureSide(systemTwitterLink, setSystemLink());

        V3ListApplicationLinksPage applinksPage = goToV3Ui();
        // non-system links first, then system Atlassian links; system non-Atlassian links are filtered out
        waitUntil("Unexpected applinks order", applinksPage.getApplinksRows(), Matchers.contains(
                withId(nonSystemGenericLink.applicationId()),
                withId(nonSystemJiraLink.applicationId()),
                withId(nonSystemConfluenceLink.applicationId()),
                withId(nonSystemTwitterLink.applicationId()),
                withId(testApplink.from().applicationId()),
                withId(systemJiraLink.applicationId()),
                withId(systemConfluenceLink.applicationId())
        ));
        assertSystemLink(applinksPage.getApplinkRow(testApplink.from()));
        assertSystemLink(applinksPage.getApplinkRow(systemJiraLink));
        assertSystemLink(applinksPage.getApplinkRow(systemConfluenceLink));
    }

    private void assertSystemLink(ApplinkRow applinkRow) {
        waitUntilEquals(ERROR_SYSTEM, applinkRow.getStatusText());
        waitUntilFalse(applinkRow.hasEdit());
        waitUntilTrue(applinkRow.hasActions());
    }

    private void assertSystemLinkActions(ApplinkRow applinkRow, String... expectedActions) {
        V3ApplinkActionDropDown actionDropDown = applinkRow.openActionsDropdown();
        waitUntil("System links should contain actions: " + Arrays.toString(expectedActions),
                actionDropDown.getActionIds(), hasItems(expectedActions));
        waitUntil("System links should not provide Delete action", actionDropDown.getActionIds(),
                not(hasItem(DELETE_ACTION_ID)));
        actionDropDown.close();
    }

    private V3ListApplicationLinksPage goToV3Ui() {
        V3ListApplicationLinksPage applinksPage = loginAsSysadminAndGoTo(PRODUCT, V3ListApplicationLinksPage.class);
        waitUntilTrue(applinksPage.getApplinkRow(testApplink.from().applicationId()).hasVersion());
        return applinksPage;
    }
}

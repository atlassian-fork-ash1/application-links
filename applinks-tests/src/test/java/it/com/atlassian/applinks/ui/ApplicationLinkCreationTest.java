package it.com.atlassian.applinks.ui;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.pageobjects.SimpleApplinksClient;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.AbstractSimpleApplinksTest;
import it.com.atlassian.applinks.testing.categories.CreationTest;
import it.com.atlassian.applinks.testing.categories.MultipleProductTest;
import it.com.atlassian.applinks.testing.categories.SimpleApplinksTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.emptyList;

/**
 * Test creation of applinks using the basic applinks plugin with no auth mechanisms installed.
 *
 * @since 5.0.0
 */
//TODO: this test is not run 
@Category({SimpleApplinksTest.class, CreationTest.class, MultipleProductTest.class})
public class ApplicationLinkCreationTest extends AbstractSimpleApplinksTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public ApplicationLinkCreationTest() {
        ProductInstances instance = ProductInstances.fromProductInstance(getProductOne().getProductInstance());
        applinksFeaturesRule = new ApplinksFeaturesRule(instance)
                .withDisabledFeatures(V3_UI);
    }

    @Test
    public void verifyCreationProcessWorks() {
        LoginClient.loginAsSysadmin(productOne, productTwo);

        ListApplicationLinkPage listApplicationLink =
                (ListApplicationLinkPage) productOne.visit(ListApplicationLinkPage.class);

        SimpleApplinksClient.createTwoWayLink(productOne, productTwo);

        waitUntil(listApplicationLink.getApplicationLinksTimed(),
                contains(hasDisplayUrl(productTwo.getProductInstance().getBaseUrl())));
    }

    @Test
    public void verifyCreateAndDeleteWorks() {
        LoginClient.loginAsSysadmin(productOne, productTwo);

        ListApplicationLinkPage listApplicationLink = (ListApplicationLinkPage) productOne.visit(ListApplicationLinkPage.class);

        SimpleApplinksClient.createTwoWayLink(productOne, productTwo);

        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(productTwo.getProductInstance().getBaseUrl())));

        listApplicationLink.deleteApplicationLink(productTwo.getProductInstance().getBaseUrl()).deleteOneWayLink();

        waitUntil(listApplicationLink.getApplicationLinksTimed(), emptyList(AbstractApplicationLinkPage.ApplicationLinkEntryRow.class));
    }
}

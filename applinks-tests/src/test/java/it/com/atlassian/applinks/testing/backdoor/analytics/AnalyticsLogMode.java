package it.com.atlassian.applinks.testing.backdoor.analytics;

/**
 * Log mode when asking for analytics logs.
 *
 * @since 4.3
 */
public enum AnalyticsLogMode {
    UNPROCESSED("unprocessed"),
    ONDEMAND("ondemand_processed"),
    BTF("btf_processed");

    public static final AnalyticsLogMode DEFAULT = BTF;

    final String modeId;

    AnalyticsLogMode(String modeId) {
        this.modeId = modeId;
    }
}

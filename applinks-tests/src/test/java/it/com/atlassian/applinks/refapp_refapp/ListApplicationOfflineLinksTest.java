package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWithTrustedAppsRuleUrlOverride;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertThat;

/**
 * Tests the listing off application links that are offline.
 */
@Category(RefappTest.class)
public class ListApplicationOfflineLinksTest extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkWithTrustedAppsRuleUrlOverride.forProducts(
            PRODUCT2.getProductInstance().getBaseUrl() + "invalid", PRODUCT, PRODUCT2);

    @Before
    public void setup() {
        loginAsSysadmin(PRODUCT, PRODUCT2);
    }

    @Test
    public void testOfflineMessageIsDisplayed() {
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        waitUntilTrue(listApplicationLinkPage.isFirstMessageShown());
        assertThat(listApplicationLinkPage.getFirstMessage(), CoreMatchers.notNullValue());
        assertThat(listApplicationLinkPage.getFirstMessage(), CoreMatchers.containsString("Application 'refapp2' seems to be offline"));
    }
}
package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.api.application.refapp.RefAppCharlieEntityType;
import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.AddEntityLinkSection;
import com.atlassian.webdriver.applinks.page.adg.entitylinks.ConfigureEntityLinksPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.emptyList;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category({TrustedAppsTest.class, RefappTest.class})
public class CreateAndDeleteUALEntityLinkTestRefApp extends V2AbstractApplinksTest {

    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    private static final String REMOTE_PROJECT_NAME = "Schultz";

    @Before
    public void setUp() {
        applink.configure(enableTrustedApps());
        loginAsSysadmin(PRODUCT, PRODUCT2);
        OAuthApplinksClient.createCharlie("sheen", "Hot Shots II");
        OAuthApplinksClient.createCharlie(PRODUCT2, "brown", REMOTE_PROJECT_NAME, ADMIN_PASSWORD);
    }

    @Test
    public void createAndDeleteProjectLink() {
        ConfigureEntityLinksPage.EntityLinkRow firstRow =
                PRODUCT.visit(ConfigureEntityLinksPage.class, RefAppCharlieEntityType.class, "sheen")
                        .clickAddLink()
                        .selectApplication("RefApp")
                        .selectProject(REMOTE_PROJECT_NAME)
                        .create()
                        .getRows()
                        .get(0);

        assertEquals(firstRow.getName(), REMOTE_PROJECT_NAME);
        assertTrue(firstRow.isPrimary());

        boolean emptyState = firstRow.delete()
                .confirm()
                .isEmptyState();
        assertTrue(emptyState);
    }
}

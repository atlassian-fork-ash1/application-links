package it.com.atlassian.applinks.refapp_applinksv3.v1integration;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.IncompatibleApplinksVersionDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import com.atlassian.webdriver.refapp.page.RefappHomePage;
import it.com.atlassian.applinks.ApplicationTestHelper;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for the interactions between v1integration and v2 applink creation workflows
 *
 * @since 4.0.0
 */
//TODO: add a category this requires REFAPP 1 and REFAPP3 so it won't work as RefappTest
public class CreateApplicationLinkTest {
    protected static final RefappTestedProduct PRODUCT = TestedProductFactory.create(RefappTestedProduct.class,
            ProductInstances.REFAPP1, null);
    protected static final RefappTestedProduct PRODUCT_APPLINKSV3 = TestedProductFactory.create(
            RefappTestedProduct.class, ProductInstances.REFAPP_APPLINKSV3, null);

    @Rule
    public final RuleChain testRules = ApplinksRuleChain.forProducts(PRODUCT);

    @BeforeClass
    public static void logInToRefappV1() {
        ApplicationTestHelper.loginAsAdmin(PRODUCT_APPLINKSV3);
    }

    @AfterClass
    public static void logOutOfRefappV1() {
        // can't use LoginClient as applinks-tests is not working on V1 any more
        PRODUCT_APPLINKSV3.visit(RefappHomePage.class);
        PRODUCT_APPLINKSV3.getTester().getDriver().manage().deleteAllCookies();
    }

    @Before
    public void logInToRefapp() {
        ApplicationTestHelper.loginAsAdmin(PRODUCT);
    }

    @Test
    public void verifyIncompatibleApplinksVersionDialogLayout() throws Exception {
        String testUrl = PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl();
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        IncompatibleApplinksVersionDialog dialog = page.getIncompatibleApplinksVersionDialog();

        OAuthApplinksClient.verifyIncompatibleApplinksVersionDialogLayout(dialog, PRODUCT_APPLINKSV3.getProductInstance());
    }

    @Test
    public void verifyIncompatibleApplinksVersionCancelReturnsToActivePage() throws Exception {
        String testUrl = PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl();
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        IncompatibleApplinksVersionDialog dialog = page.getIncompatibleApplinksVersionDialog();

        OAuthApplinksClient.verifyIncompatibleApplinksVersionDialogLayout(dialog, PRODUCT_APPLINKSV3.getProductInstance());

        dialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    @Ignore("flaky test")
    public void verifyCanCreateLocalLink() throws Exception {
        String testUrl = PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl();
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        IncompatibleApplinksVersionDialog dialog = page.getIncompatibleApplinksVersionDialog();

        OAuthApplinksClient.verifyIncompatibleApplinksVersionDialogLayout(dialog, PRODUCT_APPLINKSV3.getProductInstance());

        dialog.clickContinue();

        ListApplicationLinkPage listApplicationLink = PRODUCT.getPageBinder().bind(ListApplicationLinkPage.class);
        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl())));
        OAuthApplinksClient.verifyAtlassianLinkConfigurationOneEnded(PRODUCT, PRODUCT_APPLINKSV3, false, false, false);
    }

    @Test
    public void verifyCanRedirectDirectlyToRemoteApp() throws Exception {
        String testUrl = PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl();
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        IncompatibleApplinksVersionDialog dialog = page.getIncompatibleApplinksVersionDialog();

        OAuthApplinksClient.verifyIncompatibleApplinksVersionDialogLayout(dialog, PRODUCT_APPLINKSV3.getProductInstance());

        dialog.clickRedirectToRemoteApp();

        assertThat(PRODUCT_APPLINKSV3.getTester().getDriver().getCurrentUrl(), is(PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl() + "/plugins/servlet/applinks/listApplicationLinks"));
    }

    @Test
    public void verifyAmendedLinkIsCheckedForVersionCompatability() throws Exception {
        String badUrl = "http://badurl.invalid";
        String testUrl = PRODUCT_APPLINKSV3.getProductInstance().getBaseUrl();
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(badUrl).clickCreateNewLink();

        // amend the url to be a valid url of an instance with an old applinks version.
        ConfigureUrlDialog configureUrlDialog = page.getConfigureUrlDialog();
        configureUrlDialog.setCorrectedUrl(testUrl);
        configureUrlDialog.clickContinue();

        IncompatibleApplinksVersionDialog dialog = page.getIncompatibleApplinksVersionDialog();

        OAuthApplinksClient.verifyIncompatibleApplinksVersionDialogLayout(dialog, PRODUCT_APPLINKSV3.getProductInstance());
    }
}

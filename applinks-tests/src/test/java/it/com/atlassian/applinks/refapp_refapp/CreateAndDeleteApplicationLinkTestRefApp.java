package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage.ApplicationLinkEntryRow;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configure;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.emptyList;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

@Category(RefappTest.class)
@SuppressWarnings("unchecked")
public class CreateAndDeleteApplicationLinkTestRefApp extends V2AbstractApplinksTest {
    private static final String URL_GENERIC_APP = "http://www.google.com";

    public ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(REFAPP1);

    @Test
    public void testCreateAndDeleteTrustedAppsApplicationLink() {
        TestApplink applink = applinksBackdoor.create(REFAPP2);
        configure(applink, enableTrustedApps());

        ListApplicationLinkPage listApplicationLink = loginAsAndVerifyApplink(SYSADMIN);
        listApplicationLink.deleteApplicationLink(PRODUCT2.getProductInstance().getBaseUrl()).deleteTwoWayLink();
        waitUntil(listApplicationLink.getApplicationLinksTimed(), emptyList(ApplicationLinkEntryRow.class));
    }

    @Test
    public void testDeleteTwoWayApplinkAsSysAdmin() {
        testDeleteTwoWayLink(SYSADMIN);
    }

    @Test
    public void testDeleteTwoWayApplinkAsAdmin() {
        testDeleteTwoWayLink(REFAPP_ADMIN_BETTY);
    }

    @Test
    public void testCreateAndDeleteOneWayApplink() {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage listApplicationLink = PRODUCT.visit(ListApplicationLinkPage.class);

        OAuthApplinksClient.createOneWayLink(PRODUCT, PRODUCT2);

        waitUntil(listApplicationLink.getApplicationLinksTimed(),
                contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        listApplicationLink.deleteApplicationLink(PRODUCT2.getProductInstance().getBaseUrl()).deleteOneWayLink();
        waitUntil(listApplicationLink.getApplicationLinksTimed(), emptyList(ApplicationLinkEntryRow.class));
    }

    @Test
    public void testCreateAndDeleteThirdPartyLinkAsSysAdmin() throws Exception {
        testCreateAndDeleteThirdPartyLink(SYSADMIN);
    }


    @Test
    public void testDeleteThirdPartyLinkAsAdmin() throws Exception {
        testCreateAndDeleteThirdPartyLink(REFAPP_ADMIN_BETTY);
    }

    private static ListApplicationLinkPage loginAsAndVerifyApplink(TestAuthentication authentication) {
        LoginClient.login(authentication, PRODUCT, PRODUCT2);
        ListApplicationLinkPage listApplicationLink = PRODUCT.visit(ListApplicationLinkPage.class);

        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(
                PRODUCT2.getProductInstance().getBaseUrl())));
        return listApplicationLink;
    }

    private void testDeleteTwoWayLink(TestAuthentication authentication) {
        TestApplink applink = applinksBackdoor.create(REFAPP2);
        configure(applink, enableDefaultOAuth());

        ListApplicationLinkPage listApplicationLink = loginAsAndVerifyApplink(authentication);
        listApplicationLink.deleteApplicationLink(PRODUCT2.getProductInstance().getBaseUrl())
                .deleteTwoWayLink(true, authentication);
        waitUntil(listApplicationLink.getApplicationLinksTimed(), emptyList(ApplicationLinkEntryRow.class));
    }

    private void testCreateAndDeleteThirdPartyLink(TestAuthentication authentication) throws Exception {
        LoginClient.login(authentication, PRODUCT);

        ListApplicationLinkPage listApplicationLink = PRODUCT.visit(ListApplicationLinkPage.class);

        OAuthApplinksClient.createGenericLink(PRODUCT, URL_GENERIC_APP, "google");

        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(URL_GENERIC_APP)));

        listApplicationLink.deleteApplicationLink(URL_GENERIC_APP).deleteOneWayLink();
        waitUntil(listApplicationLink.getApplicationLinksTimed(), emptyList(ApplicationLinkEntryRow.class));
    }
}

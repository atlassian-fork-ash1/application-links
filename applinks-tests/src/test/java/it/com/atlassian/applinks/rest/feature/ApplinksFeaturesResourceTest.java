package it.com.atlassian.applinks.rest.feature;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.feature.ApplinksFeatureRequest;
import com.atlassian.applinks.test.rest.feature.ApplinksFeaturesRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.TEST_NON_SYSTEM;
import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static com.atlassian.applinks.test.rest.specification.ApplinksFeaturesExpectations.expectDisabledFeature;
import static com.atlassian.applinks.test.rest.specification.ApplinksFeaturesExpectations.expectEnabledFeature;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Category(RefappTest.class)
public class ApplinksFeaturesResourceTest {
    private static final ProductInstances TESTED_PRODUCT = ProductInstances.REFAPP1;
    private static final ApplinksRestUrls REFAPP_URL = forProduct(TESTED_PRODUCT);

    private final ApplinksFeaturesRestTester featureRestTester = new ApplinksFeaturesRestTester(REFAPP_URL);

    @Rule
    public final ApplinksFeaturesRule applinksFeatures = new ApplinksFeaturesRule(TESTED_PRODUCT);

    @Test
    public void anonymousAccessToSpecificKnownFeatureIsAllowedButAlwaysFalse() {
        applinksFeatures.enable(TEST_NON_SYSTEM);

        featureRestTester.get(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(TestAuthentication.anonymous())
                .expectStatus(OK)
                .specification(expectDisabledFeature(TEST_NON_SYSTEM))
                .build());
    }

    @Test
    public void shouldAccessGetAsUser() {
        featureRestTester.get(new ApplinksFeatureRequest.Builder(V3_UI)
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(OK)
                .build());
    }

    @Test
    public void shouldNotAccessPutUnauthenticated() {
        featureRestTester.put(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(TestAuthentication.anonymous())
                .expectStatus(UNAUTHORIZED)
                .build());
    }

    @Test
    public void shouldNotAccessPutAsUser() {
        featureRestTester.put(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(FORBIDDEN)
                .build());
    }

    @Test
    public void shouldNotAccessPutAsAdmin() {
        featureRestTester.put(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(FORBIDDEN)
                .build());
    }

    @Test
    public void shouldNotAccessDeleteUnauthenticated() {
        featureRestTester.delete(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(TestAuthentication.anonymous())
                .expectStatus(UNAUTHORIZED)
                .build());
    }

    @Test
    public void shouldNotAccessDeleteAsUser() {
        featureRestTester.delete(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(FORBIDDEN)
                .build());
    }

    @Test
    public void shouldNotAccessDeleteAsAdmin() {
        featureRestTester.delete(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(FORBIDDEN)
                .build());
    }

    @Test
    public void testEnableAndDisableNewUi() {
        // enable
        featureRestTester.put(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(SYSADMIN)
                .specification(expectEnabledFeature(TEST_NON_SYSTEM))
                .build());

        // verify
        featureRestTester.get(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(SYSADMIN)
                .specification(expectEnabledFeature(TEST_NON_SYSTEM))
                .build());

        // remove
        featureRestTester.delete(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(SYSADMIN)
                .expectStatus(Status.NO_CONTENT)
                .build());

        // verify disabled
        featureRestTester.get(new ApplinksFeatureRequest.Builder(TEST_NON_SYSTEM)
                .authentication(SYSADMIN)
                .specification(expectDisabledFeature(TEST_NON_SYSTEM))
                .build());
    }

    @Test
    public void testEnableSystemFeatureNotAllowed() {
        // using the opt-in system feature
        featureRestTester.put(new ApplinksFeatureRequest.Builder(V3_UI)
                .authentication(SYSADMIN)
                .expectStatus(Status.BAD_REQUEST)
                .build());
    }

    @Test
    public void testDisableSystemFeatureNotAllowed() {
        // using the opt-in system feature
        featureRestTester.delete(new ApplinksFeatureRequest.Builder(V3_UI)
                .authentication(SYSADMIN)
                .expectStatus(Status.BAD_REQUEST)
                .build());
    }
}

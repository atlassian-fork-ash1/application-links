package it.com.atlassian.applinks.smoke.v3;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.webdriver.applinks.page.ConfluenceConfigureEntityLinksPage;
import com.atlassian.webdriver.applinks.page.v2.LegacyConfluenceApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.LegacyConfluenceV3ApplicationLinksPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.Products;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadminAndGoTo;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Verifies that Confluence-specific admin URLs (legacy and otherwise) are supported.
 *
 * @since 4.3
 */
@Category(SmokeTest.class)
@Products(SupportedProducts.CONFLUENCE)
public class ConfluenceActionUrlsSmokeTest extends AbstractDefaultSmokeTest {
    private static final String DEMONSTRATION_SPACE_KEY = "ds";

    @Rule
    public final ApplinksFeaturesRule applinksFeatures;

    public ConfluenceActionUrlsSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                         @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        applinksFeatures = new ApplinksFeaturesRule(mainInstance);
    }

    @Test
    public void shouldGoToApplinksV2UsingLegacyUrl() {
        applinksFeatures.disable(ApplinksFeatures.V3_UI);

        ListApplicationLinkPage v2ApplinksAdminPage = loginAsSysadminAndGoTo(mainProduct,
                LegacyConfluenceApplicationLinkPage.class);
        waitUntilTrue("Applinks V2 Admin not loaded using legacy URL", v2ApplinksAdminPage.isApplinksListFullyLoaded());
    }

    @Test
    public void shouldGoToApplinksV3UsingLegacyUrl() {
        V3ListApplicationLinksPage v3ApplinksAdminPage = loginAsSysadminAndGoTo(mainProduct,
                LegacyConfluenceV3ApplicationLinksPage.class);
        waitUntilTrue("Applinks V3 Admin not loaded using legacy URL", v3ApplinksAdminPage.isLoaded());
    }

    @Test
    public void shouldGoToSpaceLinksConfigUsingActionUrl() {
        // visit the page to make sure it loads
        loginAsSysadminAndGoTo(mainProduct, ConfluenceConfigureEntityLinksPage.class, DEMONSTRATION_SPACE_KEY);
    }
}

package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.pageobjects.TestedProduct;

import org.junit.rules.RuleChain;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Creates new 3LO + 2LO links between the specified instances.
 *
 * @since 4.0.15
 */
public class CreateAppLinkWith2LOiRule extends AbstractProductLinkingRule {
    /**
     * Apply the rule to each product.
     */
    public static RuleChain forProducts(TestedProduct<?>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();

        for (final TestedProduct<?> product : products) {
            // apply the rule to each localProduct, linking to the other products.
            chain = chain.around(new CreateAppLinkWith2LOiRule(product, getOtherProducts(product, products)));
        }
        return chain;
    }

    @Inject
    public CreateAppLinkWith2LOiRule(@Nonnull TestedProduct<?> localProduct, final Iterable<TestedProduct<?>> remoteProducts) {
        super(localProduct, remoteProducts);
    }

    @Override
    protected void createLink(final TestedProduct<?> localProduct, final TestedProduct<?> remoteProduct) {
        OAuthApplinksClient.create2LOiLinkViaRest(localProduct, remoteProduct);
    }
}


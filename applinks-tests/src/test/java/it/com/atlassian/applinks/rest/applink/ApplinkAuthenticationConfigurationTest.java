package it.com.atlassian.applinks.rest.applink;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.test.backdoor.TrustedAppsBackdoor;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.backdoor.AuthenticationConfigurationBackdoor;
import com.atlassian.applinks.test.rest.backdoor.BackdoorConfigureAuthenticationRequest;
import com.atlassian.applinks.test.rest.backdoor.BackdoorCreateApplinkRequest;
import com.atlassian.applinks.test.rest.client.ApplinkOAuthStatusClient;
import com.atlassian.applinks.test.rest.model.RestTrustedAppsStatus;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.OAuthRest40CompatibilityRule;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;
import org.osgi.framework.Version;

import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusDefault;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusImpersonation;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusOff;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.testError;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withErrors;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * This test uses {@code ApplinksTestResource} in the {@code applinks-tests} plugin to test
 * {@link MutatingApplicationLinkService#configureAuthenticationForApplicationLink(ApplicationLink, AuthenticationScenario, String, String)}
 * for various authentication scenarios and product versions.
 *
 * @since 5.1
 */
@Category(RefappTest.class)
public class ApplinkAuthenticationConfigurationTest {
    @Rule
    public final RuleChain restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksCapabilitiesRule refapp2Capabilities = new ApplinksCapabilitiesRule(REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);
    @Rule
    public final OAuthRest40CompatibilityRule refapp2OAuth4x = new OAuthRest40CompatibilityRule(REFAPP2, false);

    private final ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(REFAPP1);
    private final AuthenticationConfigurationBackdoor authenticationConfigurationBackdoor =
            new AuthenticationConfigurationBackdoor(REFAPP1);
    private final ApplinkOAuthStatusClient oAuthStatusClient = new ApplinkOAuthStatusClient();
    private final TrustedAppsBackdoor trustedAppsBackdoor = new TrustedAppsBackdoor();

    @Test
    public void nonTrustedNoSharedUsers() {
        // trusted: false
        // common user base: false
        // expected: no authentication configured

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(false, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertNoOAuth(applink);
        assertNoTrustedApps(applink);
    }

    @Test
    public void nonTrustedSharedUsers() {
        // trusted: false
        // common user base: true
        // expected: no authentication configured

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(false, true)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertNoOAuth(applink);
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedNonSharedUsersWithStatusApi() {
        // trusted: true
        // common user base: false
        // remote app: Status API
        // expected: default OAuth configured (using Status API)

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusDefault());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedAndSharedUsersWithStatusApiNonSysadmin() {
        // trusted: true
        // common user base: true
        // remote app: Status API
        // user: non-sysadmin
        // expected: failed because OAuth Impersonation requires sysadmin permission

        TestApplink applink = applinksBackdoor.create(REFAPP2);
        authenticationConfigurationBackdoor.configureAuthentication(new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.INTERNAL_SERVER_ERROR)
                .specification(expectBody(withErrors(
                        testError(AuthenticationConfigurationException.class, containsString("authentication provider")))))
                .build());
    }

    @Test
    public void trustedAndSharedUsersWithStatusApi() {
        // trusted: true
        // common user base: true
        // remote app: Status API
        // expected: OAuth Impersonation configured (using Status API)

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(SYSADMIN)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusImpersonation());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedNonSharedUsers5xNoStatusApi() {
        // trusted: true
        // common user base: false
        // remote app: 5.x without Status API
        // expected: default OAuth configured (using 5.x Authentication API)

        disableStatusApi5x();

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusDefault());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedAndSharedUsers5xNoStatusApiNonSysadmin() {
        // trusted: true
        // common user base: true
        // remote app: 5.x without Status API
        // user: non-sysadmin
        // expected: failed because OAuth Impersonation requires sysadmin permission

        disableStatusApi5x();

        TestApplink applink = applinksBackdoor.create(REFAPP2);
        authenticationConfigurationBackdoor.configureAuthentication(new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.INTERNAL_SERVER_ERROR)
                .specification(expectBody(withErrors(
                        testError(AuthenticationConfigurationException.class, containsString("authentication provider")))))
                .build());
    }

    @Test
    public void trustedAndSharedUsers5xNoStatusApi() {
        // trusted: true
        // common user base: true
        // remote app: 5.x without Status API
        // expected: OAuth Impersonation configured (using 5.x Authentication API)

        disableStatusApi5x();

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(SYSADMIN)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusImpersonation());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedNonSharedUsers4xNoStatusApi() {
        // trusted: true
        // common user base: false
        // remote app: 4.x without Status API
        // expected: default OAuth configured (using 4.x Authentication API)

        disableStatusApi4x();
        refapp2OAuth4x.enable();
        // 4.x compat filter redirect will _not_ work because HTTP client does not redirect PUTs properly
        // so instead we "fake" an OK response to let the setup run
        refapp2OAuth4x.enableReturnOk();

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        // NOTE: we can only test the from() side, due to the redirect not working (see above) we cannot confirm that
        // the remote side was configured properly
        assertOAuthStatus(applink.from(), oAuthStatusDefault());
        assertNoTrustedApps(applink.from());
        assertEquals(1, refapp2OAuth4x.getInvocationCount());
    }

    @Test
    public void trustedAndSharedUsers4xNoStatusApiNonSysadmin() {
        // trusted: true
        // common user base: true
        // remote app: 4.x without Status API
        // user: non-sysadmin
        // expected: failed because OAuth Impersonation requires sysadmin permission

        disableStatusApi4x();
        refapp2OAuth4x.enable();
        // 4.x compat filter redirect will _not_ work because HTTP client does not redirect PUTs properly
        // so instead we "fake" an OK response to let the setup run
        refapp2OAuth4x.enableReturnOk();

        TestApplink applink = applinksBackdoor.create(REFAPP2);
        authenticationConfigurationBackdoor.configureAuthentication(new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.INTERNAL_SERVER_ERROR)
                .specification(expectBody(withErrors(
                        testError(AuthenticationConfigurationException.class, containsString("authentication provider")))))
                .build());
        assertEquals(1, refapp2OAuth4x.getInvocationCount());
    }

    @Test
    public void trustedAndSharedUsers4xNoStatusApi() {
        // trusted: true
        // common user base: true
        // remote app: 4.x without Status API
        // expected: OAuth Impersonation configured (using 4.x Authentication API)

        disableStatusApi4x();
        refapp2OAuth4x.enable();
        // 4.x compat filter redirect will _not_ work because HTTP client does not redirect PUTs properly
        // so instead we "fake" an OK response to let the setup run
        refapp2OAuth4x.enableReturnOk();

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, true)
                .remoteAuthentication(SYSADMIN)
                .expectStatus(Status.OK)
                .build());

        // NOTE: we can only test the from() side, due to the redirect not working (see above) we cannot confirm that
        // the remote side was configured properly
        assertOAuthStatus(applink.from(), oAuthStatusImpersonation());
        assertNoTrustedApps(applink.from());
        assertEquals(1, refapp2OAuth4x.getInvocationCount());
    }

    @Test
    public void trustedNonSharedUsersUnauthenticated() {
        // trusted: true
        // common user base: false
        // remote app: status API, should be determined without local authentication
        // expected: default OAuth configured (using Status API)

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(anonymous())
                .authenticationScenario(true, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusDefault());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedAndSharedUsersUnauthenticated() {
        // trusted: true
        // common user base: true
        // remote app: status API, should be determined without local authentication
        // expected: OAuth Impersonation configured (using Status API)

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(anonymous())
                .authenticationScenario(true, true)
                .remoteAuthentication(SYSADMIN)
                .expectStatus(Status.OK)
                .build());

        assertOAuthStatus(applink, oAuthStatusImpersonation());
        assertNoTrustedApps(applink);
    }

    @Test
    public void trustedNonSharedUsersPre4x() {
        // trusted: true
        // common user base: false
        // remote app: pre-4.x
        // expected: no config is done (remote app too old)

        disableStatusApi3x();

        TestApplink applink = applinksBackdoor.create(new BackdoorCreateApplinkRequest.Builder(REFAPP2)
                .authentication(REFAPP_ADMIN_BETTY)
                .authenticationScenario(true, false)
                .remoteAuthentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Status.OK)
                .build());

        assertThat(oAuthStatusClient.getOAuthStatus(applink.from()), oAuthStatusOff());
        assertNoTrustedApps(applink.from());
    }

    @Test
    public void disableAuthenticationWithStatusApi() {
        TestApplink applink = applinksBackdoor.create(REFAPP2);
        ApplinkConfigurators.configure(applink, enableTrustedApps(), enableDefaultOAuth());

        // requires remote SYSADMIN because TA
        authenticationConfigurationBackdoor.disableAuthentication(
                new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                        .authentication(REFAPP_ADMIN_BETTY)
                        .remoteAuthentication(SYSADMIN)
                        .expectStatus(Status.NO_CONTENT)
                        .build());

        assertNoOAuth(applink);
        assertNoTrustedApps(applink);
    }

    @Test
    public void disableAuthentication5xNoStatusApi() {
        disableStatusApi5x();

        TestApplink applink = applinksBackdoor.create(REFAPP2);
        ApplinkConfigurators.configure(applink, enableTrustedApps(), enableDefaultOAuth());

        authenticationConfigurationBackdoor.disableAuthentication(
                new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                        .authentication(REFAPP_ADMIN_BETTY)
                        .remoteAuthentication(SYSADMIN)
                        .expectStatus(Status.NO_CONTENT)
                        .build());

        assertNoOAuth(applink);
        assertNoTrustedApps(applink);
    }

    @Test
    public void disableAuthentication4xNoStatusApi() {
        disableStatusApi4x();

        TestApplink applink = applinksBackdoor.create(REFAPP2);
        ApplinkConfigurators.configure(applink, enableTrustedApps(), enableDefaultOAuth());

        authenticationConfigurationBackdoor.disableAuthentication(
                new BackdoorConfigureAuthenticationRequest.Builder(applink.from())
                        .authentication(REFAPP_ADMIN_BETTY)
                        .remoteAuthentication(SYSADMIN)
                        .expectStatus(Status.NO_CONTENT)
                        .build());

        assertNoOAuth(applink);
        assertNoTrustedApps(applink);
    }

    private void assertOAuthStatus(TestApplink applink, Matcher<ApplinkOAuthStatus> statusMatcher) {
        assertOAuthStatus(applink.from(), statusMatcher);
        assertOAuthStatus(applink.to(), statusMatcher);
    }

    private void assertOAuthStatus(TestApplink.Side side, Matcher<ApplinkOAuthStatus> statusMatcher) {
        assertThat(oAuthStatusClient.getOAuthStatus(side), statusMatcher);
    }

    private void assertNoOAuth(TestApplink applink) {
        assertOAuthStatus(applink, oAuthStatusOff());
    }

    private void assertNoTrustedApps(TestApplink applink) {
        assertNoTrustedApps(applink.from());
        assertNoTrustedApps(applink.to());
    }

    private void assertNoTrustedApps(TestApplink.Side side) {
        RestTrustedAppsStatus trustedAppsStatus = trustedAppsBackdoor.getStatus(side);
        assertFalse("Unexpected incoming trusted apps config", trustedAppsStatus.getIncoming());
        assertFalse("Unexpected outgoing trusted apps config", trustedAppsStatus.getOutgoing());
    }

    private void disableStatusApi5x() {
        refapp2Capabilities.disable(ApplinksCapabilities.STATUS_API);
        refapp2Version.setApplinksVersion(new Version(5, 0, 5));
    }

    private void disableStatusApi4x() {
        refapp2Capabilities.disable(ApplinksCapabilities.STATUS_API);
        refapp2Version.setApplinksVersion(new Version(4, 3, 10));
    }

    private void disableStatusApi3x() {
        refapp2Capabilities.disable(ApplinksCapabilities.STATUS_API);
        refapp2Version.setApplinksVersion(new Version(3, 10, 1));
    }
}

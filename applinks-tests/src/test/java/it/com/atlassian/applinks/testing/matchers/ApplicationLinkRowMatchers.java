package it.com.atlassian.applinks.testing.matchers;

import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.net.URI;
import java.net.URISyntaxException;

import static java.util.Locale.US;
import static org.hamcrest.Matchers.is;

/**
 * Matchers for {@link com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage.ApplicationLinkEntryRow}.
 *
 * @since 3.10.7
 */
public final class ApplicationLinkRowMatchers {
    private ApplicationLinkRowMatchers() {
        throw new AssertionError("Don't instantiate me");
    }

    public static Matcher<ListApplicationLinkPage.ApplicationLinkEntryRow> hasDisplayUrl(String expectedDisplayUrl) {
        return hasDisplayUrlThat(is(expectedDisplayUrl));
    }

    public static Matcher<ListApplicationLinkPage.ApplicationLinkEntryRow> hasDisplayUrlThat(Matcher<String> urlMatcher) {
        return new FeatureMatcher<ListApplicationLinkPage.ApplicationLinkEntryRow, String>(urlMatcher, "applink row with display URL that", "url") {
            @Override
            protected String featureValueOf(ListApplicationLinkPage.ApplicationLinkEntryRow actual) {
                try {
                    URI uri = new URI(actual.getDisplayUrl());
                    return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost().toLowerCase(US), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment()).toString();
                } catch (URISyntaxException e) {
                    return actual.getDisplayUrl();
                }
            }
        };
    }

    public static Matcher<ListApplicationLinkPage.ApplicationLinkEntryRow> hasApplicationUrl(String expectedApplicationUrl) {
        return hasApplicationUrlThat(is(expectedApplicationUrl));
    }

    public static Matcher<ListApplicationLinkPage.ApplicationLinkEntryRow> hasApplicationUrlThat(Matcher<String> urlMatcher) {
        return new FeatureMatcher<ListApplicationLinkPage.ApplicationLinkEntryRow, String>(urlMatcher, "applink row with application URL that", "url") {
            @Override
            protected String featureValueOf(ListApplicationLinkPage.ApplicationLinkEntryRow actual) {
                return actual.getApplicationUrl();
            }
        };
    }
}

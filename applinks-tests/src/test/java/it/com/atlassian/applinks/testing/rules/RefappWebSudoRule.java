package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import com.atlassian.jira.testkit.client.rules.AnnotatedDescription;
import com.atlassian.pageobjects.ProductInstance;
import it.com.atlassian.applinks.testing.setup.EnableWebSudo;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.lang.annotation.Annotation;
import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Allows for manipulating the WebSudo switch in Refapp. By default WebSudo is disabled, but it can be enabled for
 * specific tests for interactions requiring websudo in place.
 *
 * @since 5.0
 */
public final class RefappWebSudoRule extends TestWatcher {
    public static final String PROP_REFAPP_DISABLE_WEB_SUDO = "atlassian.refapp.websudo.disabled";

    private final SystemPropertiesBackdoor systemPropertiesBackdoor;

    private boolean wasEnabled = false;

    @Inject
    public RefappWebSudoRule(@Nonnull ProductInstance product) {
        systemPropertiesBackdoor = new SystemPropertiesBackdoor(product.getBaseUrl());
    }

    public void enable() {
        toggleRefappWebSudoDisabled(false);
        wasEnabled = true;
    }

    public void disable() {
        toggleRefappWebSudoDisabled(true);
    }

    @Override
    protected void starting(Description description) {
        if (hasAnnotation(description, EnableWebSudo.class)) {
            enable();
        }
    }

    @Override
    protected void finished(Description description) {
        // by default websudo is disabled
        if (wasEnabled) {
            disable();
        }
    }

    private static boolean hasAnnotation(Description description, Class<? extends Annotation> annotationClass) {
        return new AnnotatedDescription(description).hasAnnotation(annotationClass);
    }

    private void toggleRefappWebSudoDisabled(boolean disabled) {
        systemPropertiesBackdoor.setProperty(PROP_REFAPP_DISABLE_WEB_SUDO, Boolean.toString(disabled));
    }
}

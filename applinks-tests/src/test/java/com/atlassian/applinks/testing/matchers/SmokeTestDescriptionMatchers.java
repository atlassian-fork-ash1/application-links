package com.atlassian.applinks.testing.matchers;

import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

public final class SmokeTestDescriptionMatchers {
    private static final Pattern SMOKE_TEST_METHOD_PATTERN = Pattern.compile("(.*) \\[(.*) -> (.*)\\]");

    private static final int MATCH_GROUP_TEST_NAME = 1;
    private static final int MATCH_MAIN_PRODUCT = 2;
    private static final int MATCH_BACKGROUND_PRODUCT = 3;

    private SmokeTestDescriptionMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Description> smokeTestWith(@Nullable String expectedName,
                                                     @Nullable ProductInstances expectedMainProduct,
                                                     @Nullable ProductInstances expectedBackgroundProduct) {
        return allOf(withTestName(expectedName),
                withMainProduct(expectedMainProduct),
                withBackgroundProduct(expectedBackgroundProduct));
    }

    @Nonnull
    public static Matcher<Description> withTestName(@Nullable String expectedName) {
        return withTestNameThat(is(expectedName));
    }

    @Nonnull
    public static Matcher<Description> withTestNameThat(@Nonnull Matcher<String> smokeTestNameMatcher) {
        return new SmokeTestMatcher(checkNotNull(smokeTestNameMatcher, "smokeTestNameMatcher"), MATCH_GROUP_TEST_NAME,
                "Smoke test name");
    }

    @Nonnull
    public static Matcher<Description> withMainProduct(@Nullable ProductInstances expectedInstance) {
        return withMainProductThat(is(expectedInstance));
    }

    @Nonnull
    public static Matcher<Description> withMainProductThat(@Nonnull Matcher<ProductInstances> productInstanceMatcher) {
        return new SmokeTestProductInstanceMatcher(checkNotNull(productInstanceMatcher, "productInstanceMatcher"),
                MATCH_MAIN_PRODUCT, "Smoke test - main product instance");
    }

    @Nonnull
    public static Matcher<Description> withBackgroundProduct(@Nullable ProductInstances expectedInstance) {
        return withBackgroundProductThat(is(expectedInstance));
    }

    @Nonnull
    public static Matcher<Description> withBackgroundProductThat(@Nonnull Matcher<ProductInstances> instanceMatcher) {
        return new SmokeTestProductInstanceMatcher(checkNotNull(instanceMatcher, "instanceMatcher"),
                MATCH_BACKGROUND_PRODUCT, "Smoke test - background product instance");
    }

    private static String matchSmokeTestDescription(Description actual, int group) {
        String methodName = actual.getMethodName();
        if (methodName == null) {
            return null;
        }

        java.util.regex.Matcher matcher = SMOKE_TEST_METHOD_PATTERN.matcher(methodName);
        return matcher.matches() ? matcher.group(group) : null;
    }

    private static final class SmokeTestMatcher extends FeatureMatcher<Description, String> {
        private final int group;

        SmokeTestMatcher(Matcher<? super String> subMatcher, int group, String featureName) {
            super(subMatcher, featureName, featureName);
            this.group = group;
        }

        @Override
        protected String featureValueOf(Description actual) {
            return matchSmokeTestDescription(actual, group);
        }
    }

    private static final class SmokeTestProductInstanceMatcher extends FeatureMatcher<Description, ProductInstances> {
        private final int group;

        SmokeTestProductInstanceMatcher(Matcher<ProductInstances> productInstanceMatcher, int group, String featureName) {
            super(productInstanceMatcher, featureName, featureName);
            this.group = group;
        }

        @Override
        protected ProductInstances featureValueOf(Description description) {
            String instanceEnumName = matchSmokeTestDescription(description, group);
            return instanceEnumName != null ? getProductInstance(instanceEnumName) : null;
        }

        private ProductInstances getProductInstance(String instanceEnumName) {
            try {
                return ProductInstances.valueOf(instanceEnumName);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }
}

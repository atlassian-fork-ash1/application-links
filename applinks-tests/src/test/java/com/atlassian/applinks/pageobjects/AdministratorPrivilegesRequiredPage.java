package com.atlassian.applinks.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import org.openqa.selenium.By;

public class AdministratorPrivilegesRequiredPage extends ApplinkAbstractPage {
    @ElementBy(cssSelector = "div.aui-message.aui-message-warning")
    PageElement warning;

    public String getUrl() {
        return "/plugins/servlet/applinks/listApplicationLinks";
    }

    public boolean isWarningMessageVisible() {
        return warning.isVisible();
    }

    public String getWarningMessageTitle() {
        final PageElement title = warning.find(By.className("title"));
        return title.getText();
    }
}

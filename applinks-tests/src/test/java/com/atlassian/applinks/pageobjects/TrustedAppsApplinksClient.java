package com.atlassian.applinks.pageobjects;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.TrustedApplicationAuthenticationSection;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import it.com.atlassian.applinks.testing.backdoor.AppLinksBackdoor;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;

/**
 * A utility class that acts as a client, 'driving' pageobjects test for Trusted Apps specific UI.
 *
 * @since 5.0.0
 */
public class TrustedAppsApplinksClient {
    /**
     * Re-configure an existing link to be a Trusted link.
     */
    public static ListApplicationLinkPage configureTrustedLink(TestedProduct<WebDriverTester> hostProduct, TestedProduct<WebDriverTester> targetProduct) {
        ListApplicationLinkPage hostListApplicationLinkPage = hostProduct.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // confirm the expect link was created.
        waitUntil(hostListApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(targetProduct.getProductInstance().getBaseUrl())));

        // edit the link
        ApplicationDetailsSection applicationDetailsSection = hostListApplicationLinkPage.configureApplicationLink(targetProduct.getProductInstance().getBaseUrl());

        // edit incoming trusted
        TrustedApplicationAuthenticationSection incomingTrustedApplicationAuthenticationSection = applicationDetailsSection.openIncomingTrustedApplications();
        // turn on incoming trusted.
        incomingTrustedApplicationAuthenticationSection.enable();
        incomingTrustedApplicationAuthenticationSection.update();

        // edit outgoing trusted
        TrustedApplicationAuthenticationSection outgoingTrustedApplicationAuthenticationSection = applicationDetailsSection.openOutgoingTrustedApplications();
        // turn on outgoing trusted.
        outgoingTrustedApplicationAuthenticationSection.enable();
        outgoingTrustedApplicationAuthenticationSection.update();

        applicationDetailsSection.close();
        waitUntil(hostListApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(targetProduct.getProductInstance().getBaseUrl())));
        return hostListApplicationLinkPage;
    }

    /**
     * Create a reciprocal Trusted Apps link using the backdoors.
     *
     * @since 4.0.15
     */
    public static void createReciprocalTrustedAppsLinkViaBackDoor(final TestedProduct product1, final TestedProduct product2) {
        AppLinksBackdoor.forProduct(product1.getProductInstance())
                .createReciprocalTrustedAppsLink(product2.getProductInstance());
    }
}

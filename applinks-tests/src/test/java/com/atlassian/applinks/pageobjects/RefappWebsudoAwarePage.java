package com.atlassian.applinks.pageobjects;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.refapp.page.RefappWebSudoPage;
import javax.annotation.Nonnull;
import static java.util.Objects.requireNonNull;


/**
 * Denotes any page that should be redirected to Refapp websudo dialog
 */
public class RefappWebsudoAwarePage extends RefappWebSudoPage {

    private final Page targetPage;

    public RefappWebsudoAwarePage(@Nonnull final Page targetPage) {
        this.targetPage = requireNonNull(targetPage, "targetPage");
    }

    public String getUrl() {
        return targetPage.getUrl();
    }

    @WaitUntil
    protected void waitForPageFullyLoaded() {
        Poller.waitUntilTrue(Conditions.forSupplier(() -> this.isRequestAccessMessagePresent()));
    }
}
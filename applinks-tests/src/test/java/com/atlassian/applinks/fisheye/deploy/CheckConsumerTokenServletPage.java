package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class CheckConsumerTokenServletPage extends ApplinkAbstractPage {
    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/check-consumer-token";
    }

    public String getContent() {
        return driver.getPageSource();
    }
}

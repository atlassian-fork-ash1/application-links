package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class ClearAllServiceProviderOAuthTokensServletPage extends ApplinkAbstractPage {
    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/clear-all-service-provider-tokens";
    }

    public String getContent() {
        return driver.getPageSource();
    }
}

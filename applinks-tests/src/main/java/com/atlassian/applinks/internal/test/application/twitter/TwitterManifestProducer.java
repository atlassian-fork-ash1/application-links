package com.atlassian.applinks.internal.test.application.twitter;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestProducer;
import com.google.common.collect.ImmutableSet;
import org.osgi.framework.Version;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * @since 3.0
 */
public class TwitterManifestProducer implements ManifestProducer {
    public ApplicationStatus getStatus(URI url) {
        return ApplicationStatus.AVAILABLE;
    }

    public Manifest getManifest(final URI url) throws ManifestNotFoundException {
        return new Manifest() {
            public ApplicationId getId() {
                return ApplicationIdUtil.generate(url);
            }

            public String getName() {
                return "Twitter";
            }

            public TypeId getTypeId() {
                return TwitterApplicationType.TYPE_ID;
            }

            public String getVersion() {
                return "1.0";
            }

            public Long getBuildNumber() {
                return 0L;
            }

            public URI getUrl() {
                return URIUtil.copyOf(url);
            }

            public URI getIconUrl() {
                return null;
            }

            @Override
            public URI getIconUri() {
                return null;
            }

            public Version getAppLinksVersion() {
                return null;
            }

            public Boolean hasPublicSignup() {
                return null;
            }

            public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
                return ImmutableSet.<Class<? extends AuthenticationProvider>>of(OAuthAuthenticationProvider.class);
            }

            public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
                return Collections.emptySet();
            }
        };
    }
}

package com.atlassian.applinks.internal.test.auth;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.net.BasicHttpAuthRequestFactory;
import com.atlassian.applinks.internal.common.rest.util.RestResponses;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.test.rest.TestExceptionInterceptor;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.auth.AutoConfiguringAuthenticatorProviderPluginModule;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.test.rest.model.RestAuthenticationConfiguration;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.google.common.collect.ImmutableList;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import static com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser.parseApplicationId;

/**
 * Provides backdoor access to auto-configuring authentication per applink.
 *
 * @since 5.1
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("auth-config")
@InterceptorChain({TestExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class AuthenticationConfigurationBackdoorResource {
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final MutatingApplicationLinkService applicationLinkService;
    private final PluginAccessor pluginAccessor;
    private final RequestFactory<Request<Request<?, Response>, Response>> requestFactory;

    public AuthenticationConfigurationBackdoorResource(
            AuthenticationConfigurationManager authenticationConfigurationManager,
            MutatingApplicationLinkService applicationLinkService,
            PluginAccessor pluginAccessor,
            RequestFactory<Request<Request<?, Response>, Response>> requestFactory) {
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.applicationLinkService = applicationLinkService;
        this.pluginAccessor = pluginAccessor;
        this.requestFactory = requestFactory;
    }

    @PUT
    @WebSudoNotRequired
    @XsrfProtectionExcluded
    @Path("{applinkid}")
    public javax.ws.rs.core.Response configureAuthentication(@PathParam("applinkid") String applinkId,
                                                             RestAuthenticationConfiguration configuration)
            throws Exception {
        ApplicationLink applink = applicationLinkService.getApplicationLink(parseApplicationId(applinkId));
        applicationLinkService.configureAuthenticationForApplicationLink(applink,
                configuration.getAuthenticationScenario(), configuration.getUsername(), configuration.getPassword());
        return RestResponses.noContent();
    }

    @DELETE
    @WebSudoNotRequired
    @XsrfProtectionExcluded
    @Path("{applinkid}")
    public javax.ws.rs.core.Response disableAuthentication(@PathParam("applinkid") String applinkId,
                                                           @QueryParam(RestAuthenticationConfiguration.USERNAME) String remoteUsername,
                                                           @QueryParam(RestAuthenticationConfiguration.PASSWORD) String remotePassword)
            throws Exception {
        ApplicationLink applink = applicationLinkService.getApplicationLink(parseApplicationId(applinkId));

        for (final AutoConfiguringAuthenticatorProviderPluginModule module :
                getAutoConfiguringModules()) {
            if (authenticationConfigurationManager.isConfigured(applink.getId(), module.getAuthenticationProviderClass())) {
                module.disable(new BasicHttpAuthRequestFactory<>(requestFactory, remoteUsername, remotePassword),
                        applink);
            }
        }
        return RestResponses.noContent();
    }

    private List<AutoConfiguringAuthenticatorProviderPluginModule> getAutoConfiguringModules() {
        List<AuthenticationProviderPluginModule> providerModules =
                pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class);
        ImmutableList.Builder<AutoConfiguringAuthenticatorProviderPluginModule> autoConfigModules = ImmutableList.builder();

        for (AuthenticationProviderPluginModule providerModule : providerModules) {
            if (providerModule instanceof AutoConfiguringAuthenticatorProviderPluginModule) {
                autoConfigModules.add((AutoConfiguringAuthenticatorProviderPluginModule) providerModule);
            }
        }
        return autoConfigModules.build();
    }
}

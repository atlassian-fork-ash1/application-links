package com.atlassian.applinks.internal.test.sal;

import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Provides access to SAL APIs for testing purposes
 *
 * @since 4.3
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("sal")
@InterceptorChain({NoCacheHeaderInterceptor.class})
public class SalBackdoorResource {
    private final UserManager userManager;
    private final UserSettingsService userSettingsService;

    public SalBackdoorResource(UserManager userManager, UserSettingsService userSettingsService) {
        this.userManager = userManager;
        this.userSettingsService = userSettingsService;
    }

    /**
     * Cleans up SAL user settings for the current user.
     *
     * @return response with status {@link javax.ws.rs.core.Response.Status#NO_CONTENT}
     */
    @DELETE
    @Path("user/settings")
    public Response cleanUpUserSettings() {
        UserKey currentUser = userManager.getRemoteUserKey();
        if (currentUser == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        userSettingsService.updateUserSettings(currentUser, new Function<UserSettingsBuilder, UserSettings>() {
            @Nullable
            @Override
            public UserSettings apply(UserSettingsBuilder builder) {
                for (String key : ImmutableSet.copyOf(builder.getKeys())) {
                    builder.remove(key);
                }
                return builder.build();
            }
        });
        return Response.noContent().build();
    }
}

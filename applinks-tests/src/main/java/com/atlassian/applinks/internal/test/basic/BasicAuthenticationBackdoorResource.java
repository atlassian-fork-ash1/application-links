package com.atlassian.applinks.internal.test.basic;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.test.rest.model.RestBasicStatus;
import com.atlassian.applinks.test.rest.model.RestTrustedAppsStatus;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Use to set up and get basic status per applink
 *
 * @since 5.2
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("basic")
public class BasicAuthenticationBackdoorResource {
    private final RestApplicationIdParser restApplicationIdParser;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ApplicationLinkService applicationLinkService;

    public BasicAuthenticationBackdoorResource(ApplicationLinkService applicationLinkService,
                                               RestApplicationIdParser restApplicationIdParser,
                                               AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.restApplicationIdParser = restApplicationIdParser;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.applicationLinkService = applicationLinkService;
    }

    @GET
    @Path("status/{id}")
    public Response get(@PathParam("id") String applinkId) throws Exception {
        ApplicationLink applink = applicationLinkService.getApplicationLink(restApplicationIdParser.parse(applinkId));
        final boolean enabled = authenticationConfigurationManager.isConfigured(applink.getId(), BasicAuthenticationProvider.class);
        final Map<String, String> config = authenticationConfigurationManager.getConfiguration(applink.getId(), BasicAuthenticationProvider.class);
        if (config == null) {
            return Response.ok(new RestBasicStatus("", "", enabled)).build();
        }

        return Response.ok(new RestBasicStatus(
                config.get(RestBasicStatus.USERNAME),
                config.get(RestBasicStatus.PASSWORD),
                enabled))
                .build();
    }

    @PUT
    @Path("status/{id}")
    public Response put(@PathParam("id") String applinkId, final RestBasicStatus restBasicStatus) throws InvalidApplicationIdException, TypeNotInstalledException {
        ApplicationLink applink = applicationLinkService.getApplicationLink(restApplicationIdParser.parse(applinkId));
        final Map<String, String> config = ImmutableMap.of(
                RestBasicStatus.USERNAME, restBasicStatus.getUsername(),
                RestBasicStatus.PASSWORD, restBasicStatus.getPassword());
        authenticationConfigurationManager.registerProvider(applink.getId(), BasicAuthenticationProvider.class, config);
        return Response.ok(restBasicStatus).build();
    }
}

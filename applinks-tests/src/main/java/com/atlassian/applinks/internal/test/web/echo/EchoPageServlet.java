package com.atlassian.applinks.internal.test.web.echo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * Echo page - a simple HTML page that loads as fast as an HTML page can, useful for browser test set-up/clean-up that
 * depends on opening a specific domain in the browser.
 * </p>
 * <p>
 * Bound under:
 * /plugins/servlet/applinks-tests/echo
 * </p>
 *
 * @since 4.3
 */
public class EchoPageServlet extends HttpServlet {
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.getWriter().print(
                "<html><head><title>Applinks - echo</title></head><body id=\"applinks-echo\">Echo</body></html>");
    }
}

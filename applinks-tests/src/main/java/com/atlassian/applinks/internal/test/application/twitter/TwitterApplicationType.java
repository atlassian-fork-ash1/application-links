package com.atlassian.applinks.internal.test.application.twitter;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;
import javax.annotation.Nonnull;

public class TwitterApplicationType implements NonAppLinksApplicationType {
    static final TypeId TYPE_ID = new TypeId("twitter");

    @Nonnull
    public String getI18nKey() {
        return "applinks.test.twitter";
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    public URI getIconUrl() {
        return null;
    }

}

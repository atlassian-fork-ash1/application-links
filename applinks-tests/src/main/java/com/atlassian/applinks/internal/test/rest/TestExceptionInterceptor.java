package com.atlassian.applinks.internal.test.rest;

import com.atlassian.applinks.internal.rest.model.RestError;
import com.atlassian.applinks.internal.rest.model.RestErrors;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static org.apache.commons.lang3.BooleanUtils.toBoolean;

/**
 * Conditionally transforms exceptions into REST errors so automated tests can analyze them. By default all exceptions
 * will be propagated and result in an XML with stack trace that are easier to inspect by humans.
 *
 * @since 5.0
 */
public class TestExceptionInterceptor implements ResourceInterceptor {
    private static final Logger log = LoggerFactory.getLogger(TestExceptionInterceptor.class);

    // this query param must be true so that exceptions are transformed into JSON errors
    private static final String HANDLE_ERRORS = "handleErrors";

    @Override
    public void intercept(MethodInvocation invocation) throws IllegalAccessException, InvocationTargetException {
        try {
            invocation.invoke();
        } catch (InvocationTargetException e) {
            log.error("Error while invoking test REST resource", e.getTargetException());

            boolean handleErrors = toBoolean(invocation.getHttpContext().getRequest().getQueryParameters()
                    .getFirst(HANDLE_ERRORS));
            if (handleErrors && e.getTargetException() instanceof Exception) {
                Exception exception = (Exception) e.getTargetException();
                invocation.getHttpContext().getResponse().setResponse(createResponse(exception));
            } else {
                throw e;
            }
        }
    }

    private static Response createResponse(Exception exception) {
        // for now we always return status 500, may improve this in the future if needed along the lines of
        // ServiceExceptionInterceptor
        return Response
                .status(Status.INTERNAL_SERVER_ERROR)
                // summary == exception class, details == message
                .entity(new RestErrors(Status.INTERNAL_SERVER_ERROR,
                        new RestError(null, exception.getClass().getName(), exception.getMessage())))
                .build();
    }

}

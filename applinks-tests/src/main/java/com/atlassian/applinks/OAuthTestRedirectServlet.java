package com.atlassian.applinks;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * This servlet test that you can make a request using oauth to a resource that redirects and you get the redirected response.
 *
 * @since 3.2
 */
public class OAuthTestRedirectServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(OAuthTestRedirectServlet.class);
    private final MutatingApplicationLinkService applicationLinkService;

    public OAuthTestRedirectServlet(MutatingApplicationLinkService applicationLinkService) {
        this.applicationLinkService = applicationLinkService;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        ApplicationLink primaryApplicationLink = applicationLinkService.getPrimaryApplicationLink(RefAppApplicationType.class);
        ApplicationLinkRequestFactory requestFactory = primaryApplicationLink.createAuthenticatedRequestFactory(OAuthAuthenticationProvider.class);

        try {
            final MutableObject<String> responseBodyHolder = new MutableObject<>();
            ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET,
                    "/plugins/servlet/applinks/applinks-tests/redirecttosomwhere/abc");
            request.execute(new ApplicationLinkResponseHandler<Response>() {
                public Response credentialsRequired(final Response response) throws ResponseException {
                    responseBodyHolder.setValue(response.getResponseBodyAsString());
                    return response;
                }

                public Response handle(final Response response) throws ResponseException {
                    responseBodyHolder.setValue(response.getResponseBodyAsString());
                    return response;
                }
            });
            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();
            writer.print(firstNonNull(responseBodyHolder.getValue(), "No response recorded"));
        } catch (CredentialsRequiredException e) {
            final String contextPath = this.getServletContext().getContextPath();
            URI authorisationURI = e.getAuthorisationURI(URI.create(contextPath + "/plugins/servlet/applinks/applinks-tests/oauth-redirect"));
            resp.sendRedirect(authorisationURI.toURL().toString());
        } catch (ResponseException e) {
            LOG.error("Failed to execute request", e);
            resp.sendError(404, "An error occurred when executing a request to the remote application");
        }
    }
}

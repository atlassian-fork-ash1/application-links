package com.atlassian.applinks;

import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;

/**
 * Trusted application links servlet.
 *
 * @since 3.11.0
 */
public class TrustedTestServlet extends AbstractTrustedRequestServlet {
    public TrustedTestServlet(final MutatingApplicationLinkService applicationLinkService) {
        super(applicationLinkService);
        setUrl("/plugins/servlet/applinks/applinks-tests/redirected?requestType=");
    }
}

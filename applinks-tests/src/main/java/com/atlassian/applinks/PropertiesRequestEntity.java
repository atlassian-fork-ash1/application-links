package com.atlassian.applinks;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @since v3.3
 */
@XmlRootElement(name = "propertyRequest")
public class PropertiesRequestEntity {
    @XmlElement
    private String applicationId;

    @XmlElement
    private String entityKey;

    @XmlElement
    private String key;

    @XmlElement
    private String value;

    private PropertiesRequestEntity() {
    }

    public PropertiesRequestEntity(final String applicationId, final String entityKey, final String key, final String value) {
        this.applicationId = applicationId;
        this.entityKey = entityKey;
        this.key = key;
        this.value = value;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getEntityKey() {
        return entityKey;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}

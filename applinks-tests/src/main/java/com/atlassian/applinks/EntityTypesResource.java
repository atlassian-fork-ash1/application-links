package com.atlassian.applinks;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.applinks.internal.application.IconUriResolver.resolveIconUri;
import static javax.ws.rs.core.Response.ok;

/**
 * Backdoor resource for getting the list of all applink supported supported entity types and their icon urls and uris
 *
 * @since 4.3
 */

@AnonymousAllowed
@Path("types/entity")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EntityTypesResource {
    private final TypeAccessor typeAccessor;

    public EntityTypesResource(final TypeAccessor typeAccessor) {
        this.typeAccessor = typeAccessor;
    }

    @GET
    public Response getTypes() {
        List<BaseRestEntity> entityList = new ArrayList<BaseRestEntity>();
        for (EntityType type : typeAccessor.getEnabledEntityTypes()) {
            BaseRestEntity.Builder builder = new BaseRestEntity.Builder();
            builder.add("name", type.getI18nKey());
            builder.add("iconUrl", type.getIconUrl());
            builder.add("iconUri", resolveIconUri(type));
            entityList.add(builder.build());
        }
        return ok(entityList).build();
    }
}

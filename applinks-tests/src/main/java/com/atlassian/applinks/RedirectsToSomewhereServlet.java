package com.atlassian.applinks;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Test class to test, if OAuth can handle redirects.
 *
 * @since 3.2
 */
public class RedirectsToSomewhereServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(RedirectsToSomewhereServlet.class);

    private final UserManager userManager;

    public RedirectsToSomewhereServlet(final UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        redirect(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        redirect(req, resp);
    }

    private void redirect(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        log.debug("redirect - redirect to somewhere header signature:" + req.getHeader(TrustedApplicationUtils.Header.Request.SIGNATURE));
        if (userManager.getRemoteUsername(req) != null) {
            final String contextPath = this.getServletContext().getContextPath();
            resp.sendRedirect(contextPath + "/plugins/servlet/applinks/applinks-tests/redirected?requestType=" + req.getMethod());
        } else {
            resp.setContentType("text/html");
            final PrintWriter writer = resp.getWriter();
            writer.print("Failed to redirect, you are not logged in!");
        }
    }

}

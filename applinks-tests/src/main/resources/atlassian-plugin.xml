<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" pluginsVersion="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    </plugin-info>

    <!-- NOTE: NO MORE component-import and component entries in this file. Read comments below for details -->
    <!-- NOTE: All component-import directives are now pre-transformed in component-imports.xml. New imports should be added there, not here. -->
    <!-- NOTE: All internal component directives are now pre-transformed in components-internal.xml. New internal components should be added there, not here. -->

    <component-import key="manifestRetriever" interface="com.atlassian.applinks.spi.manifest.ManifestRetriever" />

    <component-import key="templateRenderer" interface="com.atlassian.templaterenderer.TemplateRenderer" />
    <component-import key="webResourceManager" interface="com.atlassian.plugin.webresource.WebResourceManager" />
    <component-import key="webResourceUrlProvider" interface="com.atlassian.plugin.webresource.WebResourceUrlProvider" />
    <component-import key="consumerTokenStore" interface="com.atlassian.oauth.consumer.ConsumerTokenStore" />

    <resource key="applinks-tests.i18n" name="applinks-tests.i18n" type="i18n" location="com.atlassian.applinks.applinks-tests"/>

    <rest key="applinks-tests" path="applinks-tests" version="1"/>

    <applinks-application-type name="twitter" key="twitter"
                               class="com.atlassian.applinks.internal.test.application.twitter.TwitterApplicationType">
        <manifest-producer class="com.atlassian.applinks.internal.test.application.twitter.TwitterManifestProducer"/>
    </applinks-application-type>

    <servlet-filter key="rest-40-compat"
                    class="com.atlassian.applinks.internal.test.rest.OAuthRest40CompatibilityFilter" location="after-encoding" weight="10">
        <url-pattern>/rest/applinks/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="mock-response"
                    class="com.atlassian.applinks.internal.test.response.MockResponseFilter" location="after-encoding" weight="5">
        <url-pattern>*</url-pattern>
    </servlet-filter>

    <!-- Servlet that redirects to the RedirectedServlet-->
    <servlet key="redirect-to-somewhere-servlet" class="com.atlassian.applinks.RedirectsToSomewhereServlet">
        <url-pattern>/applinks/applinks-tests/redirecttosomwhere/*</url-pattern>
    </servlet>

    <servlet key="redirect-to-servlet" class="com.atlassian.applinks.RedirectedServlet">
        <url-pattern>/applinks/applinks-tests/redirected*</url-pattern>
    </servlet>

    <servlet key="test-oauth-redirect-servlet" class="com.atlassian.applinks.OAuthTestRedirectServlet">
        <url-pattern>/applinks/applinks-tests/oauth-redirect*</url-pattern>
    </servlet>

    <servlet key="test-trusted-redirect-servlet" class="com.atlassian.applinks.TrustedTestRedirectServlet">
        <url-pattern>/applinks/tests/trusted-redirect/*</url-pattern>
    </servlet>

    <servlet key="test-trusted-servlet" class="com.atlassian.applinks.TrustedTestServlet">
        <url-pattern>/applinks/tests/trusted/*</url-pattern>
    </servlet>

    <servlet key="test-trusted-fail-servlet" class="com.atlassian.applinks.TrustedFailServlet">
        <url-pattern>/applinks/tests/trusted-fail/*</url-pattern>
    </servlet>

    <servlet key="test-failure-servlet" class="com.atlassian.applinks.FailureServlet">
        <url-pattern>/applinks/tests/failure/*</url-pattern>
    </servlet>

    <servlet key="auth-test-servlet" class="com.atlassian.applinks.AuthTestServlet">
        <url-pattern>/applinks/applinks-tests/auth-test</url-pattern>
    </servlet>

    <servlet key="auth-test-in-iframe-servlet" class="com.atlassian.applinks.AuthTestIframeServlet">
        <url-pattern>/applinks/applinks-tests/auth-test-iframe</url-pattern>
    </servlet>

    <servlet key="corsTestServlet" class="com.atlassian.applinks.CorsTestServlet">
        <url-pattern>/applinks/applinks-tests/cors-test</url-pattern>
    </servlet>

    <servlet key="twoLOTestServlet" class="com.atlassian.applinks.TwoLeggedOAuthTestServlet">
        <url-pattern>/applinks/applinks-tests/twolo-test</url-pattern>
    </servlet>

    <servlet key="twoLOImpersonationTestServlet" class="com.atlassian.applinks.TwoLeggedOAuthWithImpersonationTestServlet">
        <url-pattern>/applinks/applinks-tests/twolo-impersonation-test</url-pattern>
    </servlet>

    <servlet key="threeLOTestServlet" class="com.atlassian.applinks.ThreeLeggedOAuthRequestTestServlet">
        <url-pattern>/applinks/applinks-tests/three-lo-test</url-pattern>
    </servlet>

    <servlet key="clearAllServiceProviderOAuthTokenServlet" class="com.atlassian.applinks.ClearAllServiceProviderOAuthTokensServlet">
        <url-pattern>/applinks/applinks-tests/clear-all-service-provider-tokens</url-pattern>
    </servlet>

    <servlet key="checkConsumerTokenServlet" class="com.atlassian.applinks.CheckConsumerTokenServlet">
        <url-pattern>/applinks/applinks-tests/check-consumer-token</url-pattern>
    </servlet>

    <servlet key="echo-servlet" class="com.atlassian.applinks.internal.test.web.echo.EchoPageServlet">
        <url-pattern>/applinks-tests/echo</url-pattern>
    </servlet>

    <servlet key="maliciousjs-servlet" class="com.atlassian.applinks.internal.test.web.js.MaliciousJsPageServlet">
        <url-pattern>/applinks-tests/malicious-js</url-pattern>
    </servlet>

    <servlet key="web-resources-test-servlet" class="com.atlassian.applinks.internal.test.web.echo.WebResourcesTestServlet">
        <url-pattern>/applinks-tests/web-resources</url-pattern>
    </servlet>

    <!--Web resource that depends on all AMD libraries to test they work-->
    <web-resource key="amd-smoke-test">
        <context>applinks.list.application.links</context>
        <context>applinks.list.application.links.agent</context>

        <dependency>com.atlassian.applinks.applinks-plugin:applinks-lib</dependency>
        <dependency>com.atlassian.applinks.applinks-plugin:applinks-lib-backbone</dependency>

        <resource type="download" name="smoke-test-amd-modules.js" location="js/smoke-test-amd-modules.js"/>
    </web-resource>

    <web-resource key="test-resources">
        <dependency>com.atlassian.applinks.applinks-plugin:applinks-public</dependency>
        <resource type="download" name="auth-test.js" location="js/auth-test.js"/>
        <resource type="download" name="auth-test.css" location="css/auth-test.css"/>
    </web-resource>

    <web-item key="auth-test-web-item" section="system.admin/applinks">
        <label key="applinks-tests.auth-test.link" />
        <link>/plugins/servlet/applinks/applinks-tests/auth-test</link>
    </web-item>
    
    <web-item key="auth-test-inline-web-item" section="system.admin/applinks">
        <label key="applinks-tests.auth-test.inline.link" />
        <link>/plugins/servlet/applinks/applinks-tests/auth-test?inline</link>
    </web-item>
    
    <web-item key="auth-test-iframe-web-item" section="system.admin/applinks">
        <label key="applinks-tests.auth-test.iframe.link" />
        <link>/plugins/servlet/applinks/applinks-tests/auth-test-iframe</link>
    </web-item>

    <web-item key="twitter-applink-action" section="applinks.application.link.list.operation"
              description="Go to Twitter action on applinks list">
        <label key="applinks-tests.actions.applink.twitter" />
        <tooltip key="applinks-tests.actions.applink.twitter.tooltip" />
        <link>http://twitter.com</link>
        <condition class="com.atlassian.applinks.internal.common.web.condition.ApplicationLinkOfTypeCondition">
            <param name="type">twitter</param>
        </condition>
    </web-item>

</atlassian-plugin>

package com.atlassian.applinks.api.application.bitbucket;

import com.atlassian.applinks.api.EntityType;

/**
 * @since 4.3
 */
public interface BitbucketProjectEntityType extends EntityType {
}

package com.atlassian.applinks.api.application.stash;

import com.atlassian.applinks.api.EntityType;

/**
 * @since 3.8
 * @deprecated Stash has been renamed to Bitbucket. Please use
 * {@link com.atlassian.applinks.api.application.bitbucket.BitbucketProjectEntityType} instead.
 */
@Deprecated
public interface StashProjectEntityType extends EntityType {
}

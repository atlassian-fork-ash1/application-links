package com.atlassian.applinks.api;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ApplicationIdTest {
    private static final String ID_A = "550e8400-e29b-41d4-a716-446655440000",
            ID_B = "550e8400-e29b-41d4-a716-446655440001";

    @Test(expected = IllegalArgumentException.class)
    public void cannotBeConstructedWithNull() {
        new ApplicationId(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotBeConstructedWithIdThatIsNotAUuid() {
        new ApplicationId("not a UUID");
    }

    @Test
    public void containsSameIdAfterConstruction() {
        assertEquals(ID_A,
                new ApplicationId(ID_A).get());
    }

    @Test
    public void equalsComparesStringIds() {
        assertTrue(new ApplicationId(ID_A).equals(new ApplicationId(ID_A)));
    }

    @Test
    public void differentIdsAreNotEqual() {
        assertFalse(new ApplicationId(ID_A).equals(new ApplicationId(ID_B)));
    }
}

package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;

/**
 * Use together with {@code @Mock} annotation and {@code MockApplinksRule} to
 * automatically initialize mock Application Links in unit tests.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MockApplink {
    @Nonnull TestApplinkIds id() default TestApplinkIds.ID1;

    @Nonnull String name() default "";

    @Nonnull String url() default "";

    @Nonnull String rpcUrl() default "";

    @Nonnull Class<? extends ApplicationType> applicationType() default ApplicationType.class;
}
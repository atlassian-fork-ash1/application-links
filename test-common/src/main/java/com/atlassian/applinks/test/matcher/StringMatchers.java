package com.atlassian.applinks.test.matcher;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

/**
 * Extra Hamcrest matchers for strings, not present in the core {@link Matchers Matchers library}.
 *
 * @since 5.0
 */
public final class StringMatchers {
    private StringMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<String> containsInOrder(@Nonnull String... substrings) {
        requireNonNull(substrings, "substrings");
        return Matchers.stringContainsInOrder(asList(substrings));
    }

    @Nonnull
    public static Matcher<String> stringWithLengthThat(@Nonnull Matcher<Integer> lengthMatcher) {
        return new FeatureMatcher<String, Integer>(lengthMatcher, "string length that", "length") {
            @Override
            protected Integer featureValueOf(String actual) {
                return actual.length();
            }
        };
    }

    @Nonnull
    public static Matcher<String> stringWithLength(int expectedLength) {
        return stringWithLengthThat(is(expectedLength));
    }

    @Nonnull
    public static Matcher<String> nonEmptyString() {
        return stringWithLengthThat(greaterThan(0));
    }
}

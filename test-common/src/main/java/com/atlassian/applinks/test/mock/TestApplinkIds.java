package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationId;

import javax.annotation.Nonnull;
import java.util.UUID;

/**
 * Random Applink IDs as constants to use with {@code @MockApplink} and {@code MockApplinksRule}.
 */
public enum TestApplinkIds {
    ID1,
    ID2,
    ID3,
    ID4,
    ID5;

    /**
     * Default, implicitly used ID (for fields annotated with @MockApplink).
     */
    public static final TestApplinkIds DEFAULT_ID = ID1;

    private final UUID id = UUID.randomUUID();
    private final ApplicationId applicationId = new ApplicationId(id.toString());

    @Nonnull
    public static String createRandomId() {
        return UUID.randomUUID().toString();
    }

    @Nonnull
    public static ApplicationId createRandomApplicationId() {
        return new ApplicationId(createRandomId());
    }

    @Nonnull
    public String id() {
        return id.toString();
    }

    @Nonnull
    public UUID uuid() {
        return id;
    }

    @Nonnull
    public ApplicationId applicationId() {
        return applicationId;
    }
}

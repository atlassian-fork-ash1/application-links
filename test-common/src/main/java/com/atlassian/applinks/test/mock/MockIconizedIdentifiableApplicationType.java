package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.spi.application.IconizedType;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;


/**
 * @since 5.1
 */
public class MockIconizedIdentifiableApplicationType implements ApplicationType, IdentifiableType, IconizedType {
    private String typeId = "mock";

    @Nonnull
    public String getI18nKey() {
        return "some.dummy.i18n.key";
    }

    public URI getIconUrl() {
        return URI.create("http://localhost");
    }

    @Override
    public URI getIconUri() {
        return URI.create("http://localhost");
    }

    @Nonnull
    public TypeId getId() {
        return new TypeId(typeId);
    }

    public MockIconizedIdentifiableApplicationType setTypeId(@Nonnull String typeId) {
        this.typeId = requireNonNull(typeId, "typeId");
        return this;
    }
}

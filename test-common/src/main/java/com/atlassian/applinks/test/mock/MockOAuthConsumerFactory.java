package com.atlassian.applinks.test.mock;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.util.RSAKeys;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

public final class MockOAuthConsumerFactory {
    public static final String TEST_CONSUMER_KEY = "abc";
    public static final String TEST_CONSUMER_NAME = "Test Consumer";

    private static final KeyPair TEST_KEYS = generateKeyPair();

    private MockOAuthConsumerFactory() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    public static Consumer createConsumer(boolean threeLo, boolean twoLo, boolean twoLoi) {
        return Consumer.key(TEST_CONSUMER_KEY)
                .name(TEST_CONSUMER_NAME)
                .signatureMethod(Consumer.SignatureMethod.HMAC_SHA1)
                .threeLOAllowed(threeLo)
                .twoLOAllowed(twoLo)
                .twoLOImpersonationAllowed(twoLoi)
                .build();
    }

    public static Consumer createRsaConsumer(boolean threeLo, boolean twoLo, boolean twoLoi) {
        return Consumer.key(TEST_CONSUMER_KEY)
                .name(TEST_CONSUMER_NAME)
                .signatureMethod(Consumer.SignatureMethod.RSA_SHA1)
                .publicKey(TEST_KEYS.getPublic())
                .threeLOAllowed(threeLo)
                .twoLOAllowed(twoLo)
                .twoLOImpersonationAllowed(twoLoi)
                .build();
    }

    private static KeyPair generateKeyPair() {
        try {
            return RSAKeys.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}

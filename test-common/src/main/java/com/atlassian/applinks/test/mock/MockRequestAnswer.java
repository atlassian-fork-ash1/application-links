package com.atlassian.applinks.test.mock;

import com.atlassian.sal.api.net.Request;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;


/**
 * Mockito answer that allows to set up multiple mock request/response objects and retrieve them for validation later
 * on.
 *
 * @see MockApplicationLinkRequest
 * @see MockApplicationLinkResponse
 * @see MatchingMockRequestAnswer for an {@code Answer} that allows for response stubbing using patterns similar
 * to Mockito {@code when -> then}
 * @since 4.3
 */
public class MockRequestAnswer implements Answer<MockApplicationLinkRequest> {
    @Nonnull
    public static MockRequestAnswer create() {
        return new MockRequestAnswer(Collections.<MockApplicationLinkResponse>emptyList());
    }

    @Nonnull
    public static MockRequestAnswer withMockRequests(MockApplicationLinkResponse... responses) {
        return new MockRequestAnswer(Arrays.asList(responses));
    }

    @Nonnull
    public static MockApplicationLinkRequest newMockRequest(InvocationOnMock invocation) {
        return new MockApplicationLinkRequest((Request.MethodType) invocation.getArguments()[0],
                (String) invocation.getArguments()[1]);
    }

    private final List<MockApplicationLinkRequest> requests = Lists.newArrayList();
    private final List<MockApplicationLinkResponse> responses = Lists.newArrayList();

    private int currentResponseIndex = 0;

    private MockRequestAnswer(@Nonnull List<MockApplicationLinkResponse> responses) {
        this.responses.addAll(requireNonNull(responses, "responses"));
    }

    @Override
    public MockApplicationLinkRequest answer(InvocationOnMock invocation) throws Throwable {
        MockApplicationLinkRequest newRequest = newMockRequest(invocation).setResponse(getResponse());
        requests.add(newRequest);

        return newRequest;
    }

    @Nonnull
    public MockRequestAnswer addResponse(@Nonnull MockApplicationLinkResponse response) {
        requireNonNull(response, "response");
        responses.add(response);
        return this;
    }

    @Nonnull
    public List<MockApplicationLinkRequest> executedRequests() {
        return ImmutableList.copyOf(requests);
    }

    private MockApplicationLinkResponse getResponse() {
        if (responses.size() == currentResponseIndex) {
            responses.add(new MockApplicationLinkResponse());
        }
        return responses.get(currentResponseIndex++);
    }
}

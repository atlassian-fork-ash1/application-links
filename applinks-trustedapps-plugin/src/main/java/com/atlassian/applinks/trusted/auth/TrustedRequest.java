package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkRequest;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of a trusted application links request. This will ensure that
 * requests are signed correctly and executed with the a response handler.
 *
 * @since 3.11.0
 */
public class TrustedRequest extends AbstractApplicationLinkRequest {
    private static final Logger log = LoggerFactory.getLogger(TrustedRequest.class);

    private final CurrentApplication currentApplication;
    private final String username;

    public TrustedRequest(final String url, final Request wrappedRequest, final CurrentApplication currentApplication,
                          final String username) {
        super(url, wrappedRequest);
        this.currentApplication = currentApplication;
        this.username = username;
    }

    public <R> R execute(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) throws ResponseException {
        signRequest();
        return wrappedRequest.execute(ensureTrustedApplinksResponseHandler(applicationLinkResponseHandler));
    }

    private <R> ApplicationLinkResponseHandler<R> ensureTrustedApplinksResponseHandler(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) {
        if (applicationLinkResponseHandler instanceof TrustedApplinksResponseHandler) {
            return applicationLinkResponseHandler;
        }

        return new TrustedApplinksResponseHandler<R>(url, applicationLinkResponseHandler, this, followRedirects);
    }

    public void execute(final ResponseHandler responseHandler) throws ResponseException {
        signRequest();
        wrappedRequest.execute(ensureTrustedResponseHandler(responseHandler));
    }

    private ResponseHandler ensureTrustedResponseHandler(final ResponseHandler responseHandler) {
        if (responseHandler instanceof TrustedResponseHandler) {
            return responseHandler;
        }

        return new TrustedResponseHandler(url, responseHandler, this, followRedirects);
    }

    @Override
    public <RET> RET executeAndReturn(final ReturningResponseHandler<? super Response, RET> responseHandler)
            throws ResponseException {
        signRequest();
        return wrappedRequest.executeAndReturn(ensureTrustedApplinksReturningResponseHandler(responseHandler));
    }

    private <R> ReturningResponseHandler<? super Response, R> ensureTrustedApplinksReturningResponseHandler(final ReturningResponseHandler<? super Response, R> returningResponseHandler) {
        if (returningResponseHandler instanceof TrustedApplinksReturningResponseHandler) {
            return returningResponseHandler;
        }

        return new TrustedApplinksReturningResponseHandler<Response, R>(url, returningResponseHandler, this, followRedirects);
    }

    @Override
    public void signRequest() {
        signRequest(unsignedRequest());
    }

    public void signRequest(final com.atlassian.security.auth.trustedapps.request.TrustedRequest unsignedRequest) {
        if (log.isDebugEnabled()) {
            log.debug("signRequest - signing request for url:" + url);
        }
        TrustedApplicationUtils.addRequestParameters(currentApplication.encode(username, url), unsignedRequest);
    }

    public com.atlassian.security.auth.trustedapps.request.TrustedRequest unsignedRequest() {
        return wrappedRequest::setHeader;
    }

}
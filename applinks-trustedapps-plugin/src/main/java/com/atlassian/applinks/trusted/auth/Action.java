package com.atlassian.applinks.trusted.auth;

/**
 * @since v3.0
 */
enum Action {
    ENABLE,
    DISABLE
}

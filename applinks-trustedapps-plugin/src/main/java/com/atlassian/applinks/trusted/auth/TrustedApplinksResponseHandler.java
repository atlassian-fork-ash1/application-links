package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

/**
 * Response handler for trusted applinks requests.
 *
 * @since 3.11.0
 */
public class TrustedApplinksResponseHandler<R> extends AbstractApplicationLinkResponseHandler implements ApplicationLinkResponseHandler<R> {
    private final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler;

    public TrustedApplinksResponseHandler(
            final String url,
            final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler,
            final ApplicationLinkRequest wrappedRequest,
            final boolean followRedirects) {
        super(url, wrappedRequest, followRedirects);
        this.applicationLinkResponseHandler = applicationLinkResponseHandler;
    }

    public R credentialsRequired(final Response response) throws ResponseException {
        return applicationLinkResponseHandler.credentialsRequired(response);
    }

    public R handle(final Response response) throws ResponseException {
        return (followRedirects && redirectHelper.responseShouldRedirect(response))
                ? followRedirects(response) : handleNormally(response);
    }

    private R followRedirects(Response response) throws ResponseException {
        wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
        return wrappedRequest.execute(this);
    }

    private R handleNormally(Response response) throws ResponseException {
        return applicationLinkResponseHandler.handle(response);
    }
}
package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.ServletPathConstants;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.core.util.RendererContextBuilder;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.oauth.auth.OAuthHelper;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.auth.AuthenticationDirection;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.oauth.Consumer;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * When registering an oauth consumer this servlet allows to register a consumer that is an atlassian application
 * and has the oauth plugin installed. Because it has this plugin installed we are able to obtain all information about the consumer
 * by querying a specific endpoint provided by the oauth plugin. Thus it's enough to know the URL of the application.
 * <br>
 * When registering a consumer and this consumer has also the applinks plugin installed, then adding this consumer will also enable oauth for
 * outgoing authentication in the other application.
 *
 * @since 3.0
 */
public class AddConsumerByUrlServlet extends AbstractConsumerServlet {
    private final ManifestRetriever manifestRetriever;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final WebSudoManager webSudoManager;
    private static final String INCOMING_APPLINKS_TEMPLATE = "com/atlassian/applinks/oauth/auth/incoming_applinks.vm";
    private static final String COMMUNICATION_CONTEXT_PARAM = "communication";
    private static final String FIELD_ERROR_MESSAGES_CONTEXT_PARAM = "fieldErrorMessages";
    private static final String REMOTE_URL_CONTEXT_PARAM = "remoteURL";
    private static final String REMOTE_2LO_URL_CONTEXT_PARAM = "remote2LOURL";
    private static final String SUCCESS_MSG_CONTEXT_PARAM = "success-msg";
    public static final String UI_POSITION = "uiposition";
    private static final String TWO_LO_ENABLED = "twoLoEnabled";
    private static final String TWO_LO_EXECUTE_AS = "twoLoExecuteAs";
    private static final String TWO_LO_IMPERSONATION_ENABLED = "twoLoImpersonationEnabled";
    private static final String TWO_LO_ENABLED_ERROR_VALUE = "twoLoEnabledErrorValue";
    private static final String TWO_LO_EXECUTE_AS_ERROR_VALUE = "twoLoExecuteAsErrorValue";
    private static final String TWO_LO_IMPERSONATION_ENABLED_ERROR_VALUE = "twoLoImpersonationEnabledErrorValue";
    private static final String TWO_LO_SUCCESS_MESSAGE = "twoLoSuccessMessage";
    private static final String TWO_LO_ERROR_MESSAGE = "twoLoErrorMessage";
    private static final String ENABLE_DISABLE_OAUTH_PARAM = "ENABLE_DISABLE_OAUTH_PARAM";
    private static final String ENABLE_DISABLE_OUTGOING_TWO_LEGGED_OAUTH_PARAM = "ENABLE_DISABLE_OUTGOING_TWO_LEGGED_OAUTH_PARAM";
    private static final String ENABLE_DISABLE_OUTGOING_TWO_LEGGED_I_OAUTH_PARAM = "ENABLE_DISABLE_OUTGOING_TWO_LEGGED_I_OAUTH_PARAM";

    public static final String OUTGOING_2LO_ENABLED_CONTEXT_PARAM = "outgoing2LOEnabled";
    public static final String OUTGOING_2LOI_ENABLED_CONTEXT_PARAM = "outgoing2LOiEnabled";
    public static final String PARENT_FRAME_UNDERSTANDS_OUTGOING_2LO_PARAM = "parentFrameUnderstandsOutgoing2LO";

    private static final Logger LOG = LoggerFactory.getLogger(AddConsumerByUrlServlet.class);

    protected AddConsumerByUrlServlet(
            final I18nResolver i18nResolver,
            final MessageFactory messageFactory,
            final TemplateRenderer templateRenderer,
            final WebResourceManager webResourceManager,
            final ApplicationLinkService applicationLinkService,
            final AdminUIAuthenticator adminUIAuthenticator,
            final RequestFactory requestFactory,
            final ManifestRetriever manifestRetriever,
            final InternalHostApplication internalHostApplication,
            final ServiceProviderStoreService serviceProviderStoreService,
            final LoginUriProvider loginUriProvider,
            final DocumentationLinker documentationLinker,
            final WebSudoManager webSudoManager,
            final XsrfTokenAccessor xsrfTokenAccessor,
            final XsrfTokenValidator xsrfTokenValidator,
            final UserManager userManager) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, applicationLinkService,
                adminUIAuthenticator, requestFactory, documentationLinker, loginUriProvider, internalHostApplication,
                xsrfTokenAccessor, xsrfTokenValidator, userManager);
        this.manifestRetriever = manifestRetriever;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.webSudoManager = webSudoManager;
    }

    /**
     * Invoke with the name of the application ID as the last elements of the URL path.
     * <br>
     * /plugins/servlet/applinks/auth/conf/oauth/{application_id} /rest/
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse resp) throws IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            final ApplicationLink applicationLink = getRequiredApplicationLink(request);

            final RendererContextBuilder builder = createContextBuilder(applicationLink);

            final String parameter = request.getParameter(AddConsumerReciprocalServlet.SUCCESS_PARAM);
            if (parameter != null) {
                final boolean successfulEnabledOAuthInRemoteApp = Boolean.parseBoolean(parameter);
                if (successfulEnabledOAuthInRemoteApp) {
                    final Map<String, String> fieldErrorMessages = new HashMap<String, String>();
                    final boolean enabled = Boolean.parseBoolean(request.getParameter(OAUTH_INCOMING_ENABLED));
                    addOrRemoveConsumer(applicationLink, enabled, fieldErrorMessages);
                    final String message = getMessage(request);
                    if (!StringUtils.isEmpty(message) && fieldErrorMessages.isEmpty()) {
                        builder.put(SUCCESS_MSG_CONTEXT_PARAM, message);
                    }

                    if (!fieldErrorMessages.isEmpty()) {
                        builder.put(FIELD_ERROR_MESSAGES_CONTEXT_PARAM, fieldErrorMessages);
                    }
                } else {
                    final Map<String, String> fieldErrorMessages = new HashMap<String, String>();
                    fieldErrorMessages.put(COMMUNICATION_CONTEXT_PARAM, getMessage(request));
                    builder.put(FIELD_ERROR_MESSAGES_CONTEXT_PARAM, fieldErrorMessages);
                }
            } else {
                final String message = getMessage(request);
                if (!StringUtils.isEmpty(message)) {
                    builder.put(SUCCESS_MSG_CONTEXT_PARAM, message);
                }
            }

            final String uiPosition = request.getParameter(UI_POSITION);
            final String remoteOAuthConfigURL = getOAuthConfigRemoteURL(applicationLink, uiPosition, request);
            if (remoteOAuthConfigURL != null) {
                builder.put(REMOTE_URL_CONTEXT_PARAM, remoteOAuthConfigURL);
            }

            final String remote2LOConfigURL = get2LOConfigRemoteURL(applicationLink, uiPosition, request, getCallbackUrl(applicationLink, uiPosition, request), ENABLE_DISABLE_OUTGOING_TWO_LEGGED_OAUTH_PARAM, ENABLE_DISABLE_OUTGOING_TWO_LEGGED_I_OAUTH_PARAM);
            if (remote2LOConfigURL != null) {
                builder.put(REMOTE_2LO_URL_CONTEXT_PARAM, remote2LOConfigURL);
            }

            builder.put(UI_POSITION, uiPosition);
            builder.put(IS_SYSADMIN, userManager.isSystemAdmin(userManager.getRemoteUsername()));

            // only if OAuth is enabled.
            if (applicationLink.getProperty(OAUTH_INCOMING_CONSUMER_KEY) != null) {
                if ("local".equals(uiPosition)) {
                    populateIncoming2LOContextParams(request, applicationLink, builder);
                } else {
                    populateOutgoing2LOContextParams(request, applicationLink, builder);
                }
            }

            render(INCOMING_APPLINKS_TEMPLATE, builder.build(), request, resp, applicationLink);
        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, resp);
        }
    }

    private void populateOutgoing2LOContextParams(HttpServletRequest request, ApplicationLink applicationLink, RendererContextBuilder builder) {
        // This parameter indicates the outgoing 2lo state of the application that hosts this servlet in its {@link AuthenticatorContainerServlet}.
        String outgoing2LOEnabledParam = request.getParameter(OUTGOING_2LO_ENABLED_CONTEXT_PARAM);

        // Calculates the suggestions for user
        Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);
        boolean twoLOAllowed = (consumer != null) && consumer.getTwoLOAllowed();

        if (twoLOAllowed && !Boolean.parseBoolean(outgoing2LOEnabledParam)) {
            builder.put("outgoingTwoLoShouldBeEnabled", true);
        }
        if (!twoLOAllowed && Boolean.parseBoolean(outgoing2LOEnabledParam)) {
            builder.put("outgoingTwoLoShouldBeDisabled", true);
        }

        builder.put("incoming2LOEnabledOnRemote", twoLOAllowed);

        // If OUTGOING_2LOI_ENABLED_CONTEXT_PARAM is not present then the remote instance does not support separating config for 2LO/2LOi
        if (!StringUtils.isEmpty(request.getParameter(OUTGOING_2LOI_ENABLED_CONTEXT_PARAM))) {
            builder.put("outgoingTwoLoiShouldBeVisible", true);

            String outgoing2LOiEnabledParam = request.getParameter(OUTGOING_2LOI_ENABLED_CONTEXT_PARAM);
            boolean twoLOiAllowed = (consumer != null) && consumer.getTwoLOImpersonationAllowed();
            if (twoLOiAllowed && !Boolean.parseBoolean(outgoing2LOiEnabledParam)) {
                builder.put("outgoingTwoLoiShouldBeEnabled", true);
            }
            if (!twoLOiAllowed && Boolean.parseBoolean(outgoing2LOiEnabledParam)) {
                builder.put("outgoingTwoLoiShouldBeDisabled", true);
            }
            builder.put("incoming2LOiEnabledOnRemote", twoLOiAllowed);
        } else {
            builder.put("outgoingTwoLoiShouldBeVisible", false);
        }

        // This parameter indicates the result of state change of outgoing 2lo configuration.
        String outgoing2LOSuccessParam = request.getParameter(ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.OUTGOING_2LO_SUCCESS_PARAM);
        if (outgoing2LOSuccessParam != null) {
            if (Boolean.parseBoolean(outgoing2LOSuccessParam)) {
                builder.put("outgoingTwoLoSuccessMessage", getMessage(request));
            } else {
                builder.put("outgoingTwoLoErrorMessage", getMessage(request));
            }
        }
    }

    private void populateIncoming2LOContextParams(HttpServletRequest request, ApplicationLink applicationLink, RendererContextBuilder builder) {
        // if this is a display of the error, show the user entered parameters.
        if (request.getParameter(TWO_LO_ERROR_MESSAGE) != null) {
            builder.put(TWO_LO_ERROR_MESSAGE, request.getParameter(TWO_LO_ERROR_MESSAGE));
            builder.put(TWO_LO_ENABLED, Boolean.parseBoolean(request.getParameter(TWO_LO_ENABLED_ERROR_VALUE)));
            builder.put(TWO_LO_EXECUTE_AS, request.getParameter(TWO_LO_EXECUTE_AS_ERROR_VALUE));
            builder.put(TWO_LO_IMPERSONATION_ENABLED, Boolean.parseBoolean(request.getParameter(TWO_LO_IMPERSONATION_ENABLED_ERROR_VALUE)));
        }
        // Otherwise, show the actual parameters persisted in the backend.
        else {
            Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);
            if (consumer != null) {
                builder.put(TWO_LO_ENABLED, consumer.getTwoLOAllowed());
                builder.put(TWO_LO_EXECUTE_AS, consumer.getExecutingTwoLOUser());
                builder.put(TWO_LO_IMPERSONATION_ENABLED, consumer.getTwoLOImpersonationAllowed());

                // the success message might have to be displayed if the previous operation is a successful save.
                if (request.getParameter(TWO_LO_SUCCESS_MESSAGE) != null) {
                    builder.put(TWO_LO_SUCCESS_MESSAGE, request.getParameter(TWO_LO_SUCCESS_MESSAGE));
                }
            }
        }
    }

    /* (non-Javadoc)
     * We will only receive a POST if the remote application does not have UAL installed.
     * If UAL is installed we will redirect to the remote application and this will redirect back to us and
     * we will add this application as a consumer in our doGet method.
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            // If this is a 2lo update POST.
            if (request.getParameter("update-2lo") != null) {
                updateIncoming2LOConfig(request, response);
            } else {
                updateIncomingOAuthConfig(request, response);
            }
        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    private void updateIncomingOAuthConfig(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final ApplicationLink applicationLink = getRequiredApplicationLink(request);
        final boolean enabled = Boolean.parseBoolean(request.getParameter(OAUTH_INCOMING_ENABLED));

        final Map<String, String> fieldErrorMessages = new HashMap<String, String>();
        addOrRemoveConsumer(applicationLink, enabled, fieldErrorMessages);
        final String uiPosition = request.getParameter(UI_POSITION);
        if (fieldErrorMessages.isEmpty()) {
            final String message = enabled ? i18nResolver.getText("auth.oauth.config.serviceprovider.consumer.enabled") : i18nResolver.getText("auth.oauth.config.serviceprovider.consumer.disabled");
            response.sendRedirect("./" + applicationLink.getId() + "?" + MESSAGE_PARAM + "=" + URIUtil.utf8Encode(message) + "&uiposition=" + uiPosition);
        } else {
            final RendererContextBuilder builder = createContextBuilder(applicationLink);
            builder.put(FIELD_ERROR_MESSAGES_CONTEXT_PARAM, fieldErrorMessages);
            builder.put(UI_POSITION, uiPosition);
            render(INCOMING_APPLINKS_TEMPLATE, builder.build(), request, response, applicationLink);
        }
    }

    private void updateIncoming2LOConfig(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String uiPosition = request.getParameter(UI_POSITION);
        final ApplicationLink applicationLink = getRequiredApplicationLink(request);
        Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);
        String targetUrl = "./" + applicationLink.getId() + "?" + "uiposition=" + uiPosition;
        if (consumer == null) {
            throw new IllegalStateException("trying to edit an OAuth consumer that doesn't exist!");
        }

        boolean twoLoEnabled = Boolean.parseBoolean(request.getParameter(TWO_LO_ENABLED));

        boolean foundError = false;
        // check if the user exists if 2LO is on. if not, prepare for displaying an error
        String executeAsUser = request.getParameter(TWO_LO_EXECUTE_AS);
        if (twoLoEnabled) {
            if (!StringUtils.isEmpty(executeAsUser) && userManager.resolve(executeAsUser) == null) {
                foundError = true;
                targetUrl += "&" + TWO_LO_ERROR_MESSAGE + "=" + URIUtil.utf8Encode(i18nResolver.getText(
                        "auth.oauth.config.2lo.username.error"));
                targetUrl += "&" + TWO_LO_ENABLED_ERROR_VALUE + "=" + Boolean.parseBoolean(request.getParameter(TWO_LO_ENABLED));
                targetUrl += "&" + TWO_LO_EXECUTE_AS_ERROR_VALUE + "=" + URIUtil.utf8Encode(executeAsUser);
                targetUrl += "&" + TWO_LO_IMPERSONATION_ENABLED_ERROR_VALUE + "=" + Boolean.parseBoolean(request.getParameter(TWO_LO_IMPERSONATION_ENABLED));
            } else if (userManager.isSystemAdmin(executeAsUser)) {
                foundError = true;
                targetUrl += "&" + TWO_LO_ERROR_MESSAGE + "=" + URIUtil.utf8Encode(i18nResolver.getText("auth.oauth.config.2lo.username.cannot.be.sysadmin"));
                targetUrl += "&" + TWO_LO_ENABLED_ERROR_VALUE + "=" + Boolean.parseBoolean(request.getParameter(TWO_LO_ENABLED));
                targetUrl += "&" + TWO_LO_EXECUTE_AS_ERROR_VALUE + "=" + URIUtil.utf8Encode(executeAsUser);
                targetUrl += "&" + TWO_LO_IMPERSONATION_ENABLED_ERROR_VALUE + "=" + Boolean.parseBoolean(request.getParameter(TWO_LO_IMPERSONATION_ENABLED));
            } else if (!isSysadmin() && Boolean.parseBoolean(request.getParameter(TWO_LO_IMPERSONATION_ENABLED))) {
                throw new ForbiddenException(messageFactory.newI18nMessage("applinks.error.only.sysadmin.operation"));
            }
        }

        // if no error, update the consumer information and display an update successful message.
        if (!foundError) {
            final boolean twoLoIEnabled = twoLoEnabled ? Boolean.parseBoolean(request.getParameter(TWO_LO_IMPERSONATION_ENABLED)) : false;
            Consumer updatedConsumer = new Consumer.InstanceBuilder(consumer.getKey())
                    .name(consumer.getName())
                    .description(consumer.getDescription())
                    .publicKey(consumer.getPublicKey())
                    .signatureMethod(consumer.getSignatureMethod())
                    .callback(consumer.getCallback())
                    .twoLOAllowed(twoLoEnabled)
                    // the following two parameters only make sense if twoLoEnabled is true so we nullify them if not so.
                    .executingTwoLOUser(twoLoEnabled ? executeAsUser : null)
                    .twoLOImpersonationAllowed(twoLoIEnabled)
                    .build();
            // remove the old configuration
            serviceProviderStoreService.removeConsumer(applicationLink);
            // add the updated one
            serviceProviderStoreService.addConsumer(updatedConsumer, applicationLink);
            // tell user that it's successful
            targetUrl += "&" + TWO_LO_SUCCESS_MESSAGE + "=" + URIUtil.utf8Encode(i18nResolver.getText("auth.oauth.config.2lo.update.success"));
            // url must be absolute
            targetUrl = RequestUtil.getBaseURLFromRequest(request, internalHostApplication.getBaseUrl()) +
                    ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/add-consumer-by-url/" + targetUrl;

            // we need to call other side of the link and update it's outgoing oauth settings
            targetUrl = get2LOConfigRemoteURL(applicationLink, uiPosition, request, targetUrl, Boolean.toString(twoLoEnabled), Boolean.toString(twoLoIEnabled));
        }

        // back to our main display
        response.sendRedirect(targetUrl);
    }

    @Override
    protected List<String> getRequiredWebResources() {
        return new ImmutableList.Builder<String>()
                .addAll(super.getRequiredWebResources())
                .add(WEB_RESOURCE_KEY + "oauth-2lo-config")
                .build();
    }

    @Override
    protected void render(String template, Map<String, Object> params, HttpServletRequest request, HttpServletResponse response, ApplicationLink applicationLink) throws IOException {
        final RendererContextBuilder builder = new RendererContextBuilder(params);

        String passedEnabledParam = request.getParameter(OAUTH_OUTGOING_ENABLED);
        boolean isEnabledOnOtherSide;
        if (passedEnabledParam != null) {
            isEnabledOnOtherSide = (Boolean.parseBoolean(request.getParameter(OAUTH_OUTGOING_ENABLED)) || Boolean.parseBoolean(request.getParameter(AddConsumerReciprocalServlet.SUCCESS_PARAM)));
        } else {
            // for backward compatibility if parameter is not passed assume that it is enabled
            isEnabledOnOtherSide = true;
        }

        builder.put(ENABLED_CONTEXT_PARAM, (applicationLink.getProperty(OAUTH_INCOMING_CONSUMER_KEY) != null) && isEnabledOnOtherSide);

        final String outgoing2LOParam = request.getParameter(OUTGOING_2LO_ENABLED_CONTEXT_PARAM);
        final String outgoing2LOiParam = request.getParameter(OUTGOING_2LOI_ENABLED_CONTEXT_PARAM);
        if (outgoing2LOParam != null || outgoing2LOiParam != null) {
            // this tells if the parent frame understands the outgoing 2LO parameter.
            // for the parent frame from an older applink, this will be false thus we won't render the outgoing 2LO section.
            builder.put(PARENT_FRAME_UNDERSTANDS_OUTGOING_2LO_PARAM, true);
        }
        if (outgoing2LOParam != null) {
            // this tells the current state of outgoing 2LO config
            builder.put(OUTGOING_2LO_ENABLED_CONTEXT_PARAM, Boolean.parseBoolean(outgoing2LOParam));
        }

        if (outgoing2LOiParam != null) {
            // this tells the current state of outgoing 2LOi config
            builder.put(OUTGOING_2LOI_ENABLED_CONTEXT_PARAM, Boolean.parseBoolean(outgoing2LOiParam));
        }

        super.render(template, builder.build(), request, response);
    }

    private String getOAuthConfigRemoteURL(final ApplicationLink applicationLink, final String uiPosition, final HttpServletRequest request) {
        try {
            final Manifest manifest = manifestRetriever.getManifest(applicationLink.getRpcUrl(), applicationLink.getType());
            if (manifest.getAppLinksVersion() != null) {
                final URI remoteDisplayUrl = getRemoteDisplayUrl(applicationLink, request);
                final String encodedCallbackUrl = URIUtil.utf8Encode(getCallbackUrl(applicationLink, uiPosition, request));

                return AddConsumerReciprocalServlet.getReciprocalServletUrl(remoteDisplayUrl, internalHostApplication.getId(), encodedCallbackUrl, ENABLE_DISABLE_OAUTH_PARAM);
            }
        } catch (Exception e) {
            LOG.warn("An Error occurred when building the URL to the '" + AddConsumerReciprocalServlet.class + "' servlet of the remote application.", e);
        }
        return null;
    }

    private String get2LOConfigRemoteURL(final ApplicationLink applicationLink, final String uiPosition, final HttpServletRequest request, final String callbackUrl, final String actionParamValue, final String actionParamValue2) {
        try {
            final Manifest manifest = manifestRetriever.getManifest(applicationLink.getRpcUrl(), applicationLink.getType());
            if (manifest.getAppLinksVersion() != null) {
                final URI remoteDisplayUrl = getRemoteDisplayUrl(applicationLink, request);
                final String encodedCallbackUrl = URIUtil.utf8Encode(callbackUrl);

                return ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.getReciprocalServletUrl(remoteDisplayUrl, internalHostApplication.getId(), encodedCallbackUrl, actionParamValue, actionParamValue2);
            }
        } catch (Exception e) {
            LOG.warn("An Error occurred when building the URL to the '" + ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.class + "' servlet of the remote application.", e);
        }
        return null;
    }

    /**
     * @return the URL for the remote application to redirect to after the operation.
     */
    private String getCallbackUrl(final ApplicationLink applicationLink, final String uiPosition, final HttpServletRequest request) {
        final URI remoteDisplayUrl = getRemoteDisplayUrl(applicationLink, request);
        String callbackUrl = RequestUtil.getBaseURLFromRequest(request, internalHostApplication.getBaseUrl()) +
                ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/add-consumer-by-url/" +
                applicationLink.getId() + "/" + AuthenticationDirection.INBOUND.name() +
                "?" + OAUTH_INCOMING_ENABLED + "=" + ENABLE_DISABLE_OAUTH_PARAM +
                "&" + UI_POSITION + "=" + uiPosition +
                "&" + HOST_URL_PARAM + "=" + URIUtil.utf8Encode(remoteDisplayUrl);

        // this parameter is present only if the other side is AppLinks 3.10.0 or newer so it tells us whether
        // the outgoing 2LO option should be made visible. We have to make sure that we never introduce this new
        // parameter during this complex configuration redirection process if the original url does not contain it.
        final String outgoing2LOParam = request.getParameter(OUTGOING_2LO_ENABLED_CONTEXT_PARAM);
        if (outgoing2LOParam != null) {
            callbackUrl += "&" + OUTGOING_2LO_ENABLED_CONTEXT_PARAM + "=" + ENABLE_DISABLE_OUTGOING_TWO_LEGGED_OAUTH_PARAM;
        }

        final String outgoing2LOiParam = request.getParameter(OUTGOING_2LOI_ENABLED_CONTEXT_PARAM);
        if (outgoing2LOiParam != null) {
            callbackUrl += "&" + OUTGOING_2LOI_ENABLED_CONTEXT_PARAM + "=" + ENABLE_DISABLE_OUTGOING_TWO_LEGGED_I_OAUTH_PARAM;
        }

        return callbackUrl;
    }

    private URI getRemoteDisplayUrl(ApplicationLink applicationLink, HttpServletRequest request) {
        return (!StringUtils.isEmpty(request.getParameter(HOST_URL_PARAM))) ? URI.create(request.getParameter(HOST_URL_PARAM)) : applicationLink.getDisplayUrl();
    }

    private void addOrRemoveConsumer(final ApplicationLink applicationLink, final boolean enabled, final Map<String, String> fieldErrorMessages)
            throws IOException {
        if (enabled) {
            try {
                final Consumer consumer = OAuthHelper.fetchConsumerInformation(applicationLink);
                serviceProviderStoreService.addConsumer(consumer, applicationLink);
            } catch (ResponseException e) {
                LOG.error("Error occurred when trying to fetch the consumer information from the remote application for application link '" + applicationLink + "'", e);
                fieldErrorMessages.put(COMMUNICATION_CONTEXT_PARAM, i18nResolver.getText("auth.oauth.config.error.communication.consumer", e.getMessage()));
            } catch (Exception e) {
                LOG.error("Error occurred when trying to store consumer information for application link '" + applicationLink + "'", e);
                fieldErrorMessages.put(COMMUNICATION_CONTEXT_PARAM, i18nResolver.getText("auth.oauth.config.error.consumer.add", e.getMessage()));
            }
        } else {
            try {
                serviceProviderStoreService.removeConsumer(applicationLink);
            } catch (Exception e) {
                LOG.error("Error occurred when trying to remove consumer from application link '" + applicationLink + "'.", e);
                fieldErrorMessages.put(COMMUNICATION_CONTEXT_PARAM, i18nResolver.getText("auth.oauth.config.error.consumer.remove", e.getMessage()));
            }
        }
    }

}

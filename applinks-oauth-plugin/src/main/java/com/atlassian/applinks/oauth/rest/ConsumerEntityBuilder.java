package com.atlassian.applinks.oauth.rest;

import com.atlassian.applinks.core.rest.model.ConsumerEntity;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.plugins.rest.common.Link;

import java.net.URI;
import javax.annotation.Nonnull;

/**
 * Builder to generate a generic @{link ConsumerEntity} from an OAuth {@link com.atlassian.oauth.Consumer}
 *
 * @since 5.0.0
 */
public class ConsumerEntityBuilder {
    private Consumer consumer;
    private Link self;

    public ConsumerEntityBuilder(final Consumer consumer) {

        this.consumer = consumer;
    }

    @Nonnull
    public static ConsumerEntityBuilder consumer(final Consumer consumer) {
        return new ConsumerEntityBuilder(consumer);
    }

    @Nonnull
    public ConsumerEntityBuilder self(@Nonnull URI selfUri) {
        this.self = Link.self(selfUri);
        return this;
    }

    @Nonnull
    public ConsumerEntity build() {
        String publicKey;
        if (consumer.getPublicKey() != null) {
            publicKey = RSAKeys.toPemEncoding(consumer.getPublicKey());
        } else {
            publicKey = null;
        }

        return new ConsumerEntity(self,
                consumer.getKey(),
                consumer.getName(),
                consumer.getDescription(),
                consumer.getSignatureMethod().name(),
                publicKey,
                consumer.getCallback(),
                consumer.getTwoLOAllowed(),
                consumer.getExecutingTwoLOUser(),
                consumer.getTwoLOImpersonationAllowed()
        );
    }
}

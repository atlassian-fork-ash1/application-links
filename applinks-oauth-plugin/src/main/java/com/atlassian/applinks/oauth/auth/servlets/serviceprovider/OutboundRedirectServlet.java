package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.auth.AbstractAdminOnlyAuthServlet;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.oauth.auth.OAuthHelper;
import com.atlassian.applinks.oauth.auth.servlets.AbstractOAuthConfigServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.oauth.auth.OAuthAuthenticatorProviderPluginModule.ADD_CONSUMER_BY_URL_SERVLET_LOCATION;
import static com.atlassian.applinks.oauth.auth.OAuthAuthenticatorProviderPluginModule.OUTBOUND_ATLASSIAN_SERVLET_LOCATION;
import static com.atlassian.applinks.oauth.auth.OAuthAuthenticatorProviderPluginModule.OUTBOUND_NON_APPLINKS_SERVLET_LOCATION;


/**
 * The outgoing OAuth configuration screen is an iframe from the remote application. We need this servlet to redirect to that iframe
 * location instead of going directly so we can pass states of the local system as query string in the process.
 */
public class OutboundRedirectServlet extends AbstractOAuthConfigServlet {
    public static final String SUPPORT_APPLINK_PARAM = "supportsAppLinks";

    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final WebSudoManager webSudoManager;
    private final HostApplication hostApplication;

    protected OutboundRedirectServlet(final I18nResolver i18nResolver,
                                      final MessageFactory messageFactory,
                                      final TemplateRenderer templateRenderer,
                                      final WebResourceManager webResourceManager,
                                      final ApplicationLinkService applicationLinkService,
                                      final AdminUIAuthenticator adminUIAuthenticator,
                                      final DocumentationLinker documentationLinker,
                                      final LoginUriProvider loginUriProvider,
                                      final InternalHostApplication internalHostApplication,
                                      final XsrfTokenAccessor xsrfTokenAccessor,
                                      final XsrfTokenValidator xsrfTokenValidator,
                                      final AuthenticationConfigurationManager authenticationConfigurationManager,
                                      final WebSudoManager webSudoManager,
                                      final HostApplication hostApplication) {
        super(i18nResolver,
                messageFactory,
                templateRenderer,
                webResourceManager,
                applicationLinkService,
                adminUIAuthenticator,
                documentationLinker,
                loginUriProvider,
                internalHostApplication,
                xsrfTokenAccessor,
                xsrfTokenValidator);

        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.webSudoManager = webSudoManager;
        this.hostApplication = hostApplication;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse resp) throws ServletException, IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            final ApplicationLink link = getRequiredApplicationLink(request);
            final boolean supportsAppLinks = Boolean.parseBoolean(request.getParameter(SUPPORT_APPLINK_PARAM));

            String uri = getOutgoingConfigUrl(link, supportsAppLinks, request);
            resp.sendRedirect(uri);
        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, resp);
        }
    }

    public String getOutgoingConfigUrl(final ApplicationLink link, final boolean supportsAppLinks, final HttpServletRequest request) {
        final String configUri;

        //If the application is has the OAuth Plugin installed, we can use the same screen as for applications that have UAL installed.
        final boolean oAuthPluginInstalled = OAuthHelper.isOAuthPluginInstalled(link);

        if (supportsAppLinks) {
            // render the peer's inbound servlet
            configUri = link.getDisplayUrl() +
                    ADD_CONSUMER_BY_URL_SERVLET_LOCATION +
                    hostApplication.getId() + "?" + AddConsumerByUrlServlet.UI_POSITION + "=remote"
                    + "&" + AbstractAdminOnlyAuthServlet.HOST_URL_PARAM + "=" + URIUtil.utf8Encode(RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl())).toString()

                    // 1) this indicates the current state of local instance whether outgoing 2LO is already on.
                    // 2) this parameter also helps distinguish between AppLinks versions that support outgoing 2LO and
                    // ones that do not support since this parameter is new in 3.10.0. We check if this parameter
                    // exists in order to decide whether to show the outgoing 2LO checkbox.
                    + "&" + AddConsumerByUrlServlet.OUTGOING_2LO_ENABLED_CONTEXT_PARAM + "=" + authenticationConfigurationManager.isConfigured(link.getId(), TwoLeggedOAuthAuthenticationProvider.class)
                    // We check if this parameter exists in order to decide whether to show the outgoing 2LOi checkbox.
                    + "&" + AddConsumerByUrlServlet.OUTGOING_2LOI_ENABLED_CONTEXT_PARAM + "=" + authenticationConfigurationManager.isConfigured(link.getId(), TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
                    + "&" + AddConsumerByUrlServlet.OAUTH_OUTGOING_ENABLED + "=" + authenticationConfigurationManager.isConfigured(link.getId(), OAuthAuthenticationProvider.class);
        } else if (oAuthPluginInstalled) {
            configUri = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                    OUTBOUND_ATLASSIAN_SERVLET_LOCATION + link.getId().toString();
        } else {
            configUri = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                    OUTBOUND_NON_APPLINKS_SERVLET_LOCATION +
                    link.getId().toString();
        }

        return configUri;
    }
}

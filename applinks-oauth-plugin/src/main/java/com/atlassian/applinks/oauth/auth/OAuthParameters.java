package com.atlassian.applinks.oauth.auth;

import com.google.common.collect.ImmutableMap;
import net.oauth.OAuth;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 5.0
 */
public final class OAuthParameters {
    private OAuthParameters() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Map<String, String> asMap(@Nullable List<OAuth.Parameter> params) {
        if (params == null || params.isEmpty()) {
            return Collections.emptyMap();
        }

        ImmutableMap.Builder<String, String> paramsBuilder = ImmutableMap.builder();
        for (OAuth.Parameter parameter : params) {
            paramsBuilder.put(parameter.getKey(), parameter.getValue());
        }
        return paramsBuilder.build();
    }
}

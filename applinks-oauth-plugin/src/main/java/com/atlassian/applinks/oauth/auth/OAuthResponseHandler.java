package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;

/**
 * Response handler for oauth app links requests.
 *
 * @since 3.11.0
 */
public class OAuthResponseHandler<T extends Response>
        extends OAuthRedirectingApplicationLinkResponseHandler
        implements ResponseHandler<Response> {
    private final ResponseHandler<Response> responseHandler;

    public OAuthResponseHandler(
            final String url,
            final ResponseHandler<Response> responseHandler,
            final ConsumerTokenStoreService consumerTokenStoreService,
            final ApplicationLinkRequest wrappedRequest,
            final ApplicationId applicationId,
            final String username,
            final boolean followRedirects) {
        super(url, wrappedRequest, consumerTokenStoreService, applicationId, username, followRedirects);
        this.responseHandler = responseHandler;
    }

    public OAuthResponseHandler(
            final String url,
            final ResponseHandler<Response> responseHandler,
            final ApplicationLinkRequest wrappedRequest,
            final ApplicationId applicationId,
            final boolean followRedirects) {
        super(url, wrappedRequest, null, applicationId, null, followRedirects);
        this.responseHandler = responseHandler;
    }

    public void handle(final Response response) throws ResponseException {
        checkForOAuthProblemAndRemoveConsumerTokenIfNecessary(response);
        if (followRedirects && redirectHelper.responseShouldRedirect(response)) {
            wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
            wrappedRequest.execute(this);
        } else {
            responseHandler.handle(response);
        }
    }
}
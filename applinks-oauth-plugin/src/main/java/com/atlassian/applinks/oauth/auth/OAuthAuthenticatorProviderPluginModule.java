package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.ServletPathConstants;
import com.atlassian.applinks.core.auth.OrphanedTrustAwareAuthenticatorProviderPluginModule;
import com.atlassian.applinks.core.auth.OrphanedTrustCertificate;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AddConsumerByUrlServlet;
import com.atlassian.applinks.oauth.auth.servlets.serviceprovider.OutboundRedirectServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationDirection;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.auth.IncomingTrustAuthenticationProviderPluginModule;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.Callable;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.google.common.base.Throwables.propagateIfInstanceOf;

/**
 * The OAuthAuthenticatorProviderPluginModule returns a {@link ThreeLeggedOAuthRequestFactoryImpl} to make authenticated
 * requests using OAuth and also contains the logic to determine the correct URL for the configuration UI.
 * There are four different UIs that can be displayed:
 * Incoming:
 * 1) If the remote application has the OAuth plugin installed or the UAL plugin, we can obtain the consumer information
 * from the OAuth plugin and register this application as a consumer.
 *
 * 2) Otherwise we show a screen where the user can enter all the consumer details manually.
 *
 * Outgoing:
 * 3) If the remote application has the OAuth plugin installed or the UAL plugin, we can enable OAuth locally and the
 * remote application can read our consumer information via the OAuth plugin, thus this config UI displays only a
 * enable/disable button.
 *
 * 4) Otherwise we show a screen where the user can enter all the service provider details manually.
 *
 * @since 3.0
 */
public class OAuthAuthenticatorProviderPluginModule implements OrphanedTrustAwareAuthenticatorProviderPluginModule,
        IncomingTrustAuthenticationProviderPluginModule {
    public static final String ADD_CONSUMER_MANUALLY_SERVLET_LOCATION =
            ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/add-consumer-manually/";
    public static final String ADD_CONSUMER_BY_URL_SERVLET_LOCATION =
            ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/add-consumer-by-url/";
    public static final String OUTBOUND_NON_APPLINKS_SERVLET_LOCATION =
            ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/outbound/3rdparty/";
    public static final String OUTBOUND_ATLASSIAN_SERVLET_LOCATION =
            ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/outbound/atlassian/";
    public static final String OUTBOUND_ATLASSIAN_REDIRECT_LOCATION =
            ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/outbound/apl-redirect/";

    private static final Logger log = LoggerFactory.getLogger(OAuthAuthenticatorProviderPluginModule.class);


    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ConsumerService consumerService;
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final InternalHostApplication hostApplication;
    private final OAuthAutoConfigurator oAuthAutoConfigurator;
    private final RequestFactory requestFactory;
    private final UserManager userManager;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ElevatedPermissionsService elevatedPermissions;

    public OAuthAuthenticatorProviderPluginModule(
            AuthenticationConfigurationManager authenticationConfigurationManager,
            ConsumerService consumerService,
            ConsumerTokenStoreService consumerTokenStoreService,
            InternalHostApplication hostApplication,
            OAuthAutoConfigurator oAuthAutoConfigurator,
            RequestFactory requestFactory,
            UserManager userManager,
            ServiceProviderStoreService serviceProviderStoreService,
            ElevatedPermissionsService elevatedPermissions) {
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.consumerService = consumerService;
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.hostApplication = hostApplication;
        this.oAuthAutoConfigurator = oAuthAutoConfigurator;
        this.requestFactory = requestFactory;
        this.userManager = userManager;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.elevatedPermissions = elevatedPermissions;
    }

    public AuthenticationProvider getAuthenticationProvider(final ApplicationLink link) {
        OAuthAuthenticationProvider provider = null;
        if (authenticationConfigurationManager.isConfigured(link.getId(), OAuthAuthenticationProvider.class)) {
            provider = username -> new ThreeLeggedOAuthRequestFactoryImpl(
                    link,
                    authenticationConfigurationManager,
                    consumerService,
                    consumerTokenStoreService,
                    requestFactory,
                    userManager,
                    hostApplication);
        } else {
            log.debug("OAuthAuthenticationProvider is not configured.");
        }
        return provider;
    }

    public String getConfigUrl(final ApplicationLink link, final Version applicationLinksVersion, AuthenticationDirection direction, final HttpServletRequest request) {
        final String configUri;
        final boolean supportsAppLinks = applicationLinksVersion != null;   // TODO: maybe safer to check for < 3.0

        //If the application is has the OAuth Plugin installed, we can use the same screen as for applications that have UAL installed.
        final boolean oAuthPluginInstalled = OAuthHelper.isOAuthPluginInstalled(link);

        if (direction == AuthenticationDirection.OUTBOUND) {
            // This servlet takes care of redirecting to the appropriate url. This is required because the query string
            // passed to the remote {@link AddConsumerByUrlServlet} needs to be updated over time while this url here
            // won't be reloaded by {@link AuthenticatorContainerServlet).
            configUri = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl())
                    + OUTBOUND_ATLASSIAN_REDIRECT_LOCATION
                    + link.getId().toString()
                    + "?" + OutboundRedirectServlet.SUPPORT_APPLINK_PARAM + "=" + supportsAppLinks;
        } else {
            if (supportsAppLinks || oAuthPluginInstalled) {
                configUri = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                        ADD_CONSUMER_BY_URL_SERVLET_LOCATION +
                        link.getId().toString() + "?" + AddConsumerByUrlServlet.UI_POSITION + "=local";
            } else {
                configUri = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                        ADD_CONSUMER_MANUALLY_SERVLET_LOCATION +
                        link.getId().toString();
            }
        }
        return configUri;
    }

    public Class<? extends AuthenticationProvider> getAuthenticationProviderClass() {
        return OAuthAuthenticationProvider.class;
    }

    public boolean isApplicable(final AuthenticationScenario authenticationScenario, final ApplicationLink applicationLink) {
        return authenticationScenario.isTrusted() && !authenticationScenario.isCommonUserBase();
    }

    public boolean isApplicable(String certificateType) {
        return OrphanedTrustCertificate.Type.OAUTH.name().equals(certificateType);
    }


    @SuppressWarnings("unchecked")
    public void enable(final RequestFactory authenticatedRequestFactory, final ApplicationLink applicationLink)
            throws AuthenticationConfigurationException {
        executeAsSysAdmin(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applicationLink, authenticatedRequestFactory);
                return null;
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void disable(final RequestFactory authenticatedRequestFactory, final ApplicationLink applicationLink)
            throws AuthenticationConfigurationException {
        executeAsSysAdmin(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                oAuthAutoConfigurator.disable(applicationLink, authenticatedRequestFactory);
                return null;
            }
        });
    }

    public boolean incomingEnabled(final ApplicationLink applicationLink) {
        return serviceProviderStoreService.getConsumer(applicationLink) != null;
    }

    private void executeAsSysAdmin(Callable<Void> callable) throws AuthenticationConfigurationException {
        // Used to elevate permissions while calling {@code OAuthAutoConfigurator} as it requires admin permission level.
        try {
            elevatedPermissions.executeAs(PermissionLevel.SYSADMIN, callable);
        } catch (Exception e) {
            propagateIfInstanceOf(e, AuthenticationConfigurationException.class);
            throw new AuthenticationConfigurationException(e);
        }
    }
}

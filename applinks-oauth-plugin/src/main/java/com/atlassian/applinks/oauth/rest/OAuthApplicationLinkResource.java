package com.atlassian.applinks.oauth.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ApplicationLinkAuthenticationEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.core.rest.model.ConsumerEntity;
import com.atlassian.applinks.core.rest.model.ConsumerEntityListEntity;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.oauth.auth.OAuthHelper;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.Lists;
import com.sun.jersey.spi.resource.Singleton;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.ok;

/**
 * Applinks resource for managing OAuth config.
 *
 * @since 5.0
 */
@Path(OAuthApplicationLinkResource.CONTEXT)
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@WebSudoRequired
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class OAuthApplicationLinkResource extends com.atlassian.applinks.core.v1.rest.ApplicationLinkResource {
    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ConsumerService consumerService;

    public OAuthApplicationLinkResource(final MutatingApplicationLinkService applicationLinkService,
                                        final I18nResolver i18nResolver,
                                        final InternalTypeAccessor typeAccessor,
                                        final ManifestRetriever manifestRetriever,
                                        final RestUrlBuilder restUrlBuilder,
                                        final RequestFactory requestFactory,
                                        final UserManager userManager,
                                        final PluginAccessor pluginAccessor,
                                        final AuthenticationConfigurationManager authenticationConfigurationManager,
                                        final ServiceProviderStoreService serviceProviderStoreService,
                                        final ConsumerService consumerService) {
        super(applicationLinkService,
                i18nResolver,
                typeAccessor,
                manifestRetriever,
                restUrlBuilder,
                requestFactory,
                userManager);
        this.pluginAccessor = pluginAccessor;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.consumerService = consumerService;
    }

    /**
     * Gets a description of the Authentication configuration of the ApplicationLink defined by the unique id.
     *
     * @param id the id of the ApplicationLink
     * @return 'notfound' if the ApplicationLink cannot be found
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     * @throws URISyntaxException                                   if an invalid url is used when creating the "self" link in AuthenticationProviderEntities or ConsumerEntities.
     */
    @GET
    @Path("{id}/authentication")
    public Response getAuthentication(@PathParam("id") final String id)
            throws TypeNotInstalledException, URISyntaxException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        // get the providers configured for this link
        final List<AuthenticationProviderEntity> configuredAuthProviders = getConfiguredProviders(applicationLink);

        Iterable<Consumer> consumers = findConsumers(applicationLink, configuredAuthProviders);


        List<ConsumerEntity> consumerEntities = Lists.newArrayList();
        for (Consumer consumer : consumers) {
            consumerEntities.add(ConsumerEntityBuilder.consumer(consumer).self(new URI(OAuthApplicationLinkResource.CONTEXT + "/" + id + "/authentication/consumer")).build());
        }

        return RestUtil.ok(new ApplicationLinkAuthenticationEntity(Link.self(new URI(OAuthApplicationLinkResource.CONTEXT + "/" + id + "/authentication")),
                consumerEntities, configuredAuthProviders));
    }

    /**
     * Get the configuration of the Consumer for the specified ApplicationLink.
     *
     * @param id the unique id of the ApplicationId.
     * @return 'notfound' if the ApplicationLink cannot be found, 'badrequest' if the specified AuthenticationProvider is not recognized, 'created' otherwise.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     * @throws URISyntaxException                                   if an invalid url is used when creating the "self" link in AuthenticationProviderEntities or ConsumerEntities
     */
    @GET
    @Path("{id}/authentication/consumer")
    public Response getConsumer(@PathParam("id") final String id)
            throws TypeNotInstalledException, URISyntaxException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        // get the providers configured for this link
        final List<AuthenticationProviderEntity> configuredAuthProviders = getConfiguredProviders(applicationLink);

        Iterable<Consumer> consumers = findConsumers(applicationLink, configuredAuthProviders);

        if (!consumers.iterator().hasNext() && applicationLink.getType() instanceof GenericApplicationType
                && configuredAuthProviders.size() == 0) {
            return RestUtil.notFound(i18nResolver.getText("applinks.generic.consumer.needs.authenticationprovider", id));
        }

        if (!consumers.iterator().hasNext()) {
            return RestUtil.notFound(i18nResolver.getText("applinks.consumer.notfound", id));
        }

        List<ConsumerEntity> consumerEntities = Lists.newArrayList();
        for (Consumer consumer : consumers) {
            consumerEntities.add(ConsumerEntityBuilder.consumer(consumer).self(new URI(OAuthApplicationLinkResource.CONTEXT + "/" + id + "/authentication/consumer")).build());
        }

        return ok(new ConsumerEntityListEntity(consumerEntities));
    }

    /**
     * Add as a Consumer and its configuration to the specified ApplicationLink
     *
     * @param id             the unique id of the ApplicationLink
     * @param autoConfigure  a flag allowing a shortcut of Consumer configuration by using the existing ApplicationLink to
     *                       retrieve the Consumer information from the remote application.
     * @param consumerEntity the Consumer definition.
     * @return 'notfound' if the ApplicationLink can't be found, 'servererror' if autoconfigure has been requested but fails,
     * 'badrequest' if there is a problem with the submitted Consumer configuration and 'created' on success.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     * @throws URISyntaxException                                   if an invalid url is used when creating the "self" link in ConsumerEntities
     */
    @PUT
    @Path("{id}/authentication/consumer")
    public Response putConsumer(@PathParam("id") final String id,
                                @QueryParam("autoConfigure") final Boolean autoConfigure,
                                final ConsumerEntity consumerEntity)
            throws TypeNotInstalledException, URISyntaxException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        // auto configuration from the existing application link
        if (autoConfigure != null && autoConfigure) {
            try {
                // ask the remote app for Consumer info via the existing applink url
                Consumer consumer = OAuthHelper.fetchConsumerInformation(applicationLink);
                // update the consumer information to reflect the 2LO settings passed in.
                Consumer updatedConsumer = new Consumer.InstanceBuilder(consumer.getKey())
                        .name(consumer.getName())
                        .description(consumer.getDescription())
                        .publicKey(consumer.getPublicKey())
                        .signatureMethod(consumer.getSignatureMethod())
                        .callback(consumer.getCallback())
                        .twoLOAllowed(consumerEntity.isTwoLOAllowed())
                        .executingTwoLOUser(consumerEntity.getExecutingTwoLOUser())
                        .twoLOImpersonationAllowed(consumerEntity.isTwoLOImpersonationAllowed())
                        .build();

                // store the Consumer
                serviceProviderStoreService.addConsumer(updatedConsumer, applicationLink);
            } catch (ResponseException e) {
                return RestUtil
                        .serverError(i18nResolver.getText("applinks.consumer.autoconfigure.consumerInfo.notfound"));
            }

            return RestUtil.created(Link.self(
                    new URI(OAuthApplicationLinkResource.CONTEXT + "/" + id + "/authentication/consumer")));
        }


        // manual configuration

        Consumer consumer;
        if (applicationLink.getType() instanceof GenericApplicationType) {
            // 3rd Party Consumers

            List<String> errors = validate3rdPartyConsumer(consumerEntity);
            if (errors.size() > 0) {
                return RestUtil.badRequest(errors.toArray(new String[errors.size()]));
            }

            if (consumerEntity.isOutgoing()) {
                // create an outgoing Consumer
                this.add3rdPartyOutgoingConsumer(consumerEntity);
            } else {
                // create an incoming Consumer
                try {
                    consumer = this.createBasicConsumer(consumerEntity, applicationLink);
                    serviceProviderStoreService.addConsumer(consumer, applicationLink);
                } catch (NoSuchAlgorithmException e) {
                    return RestUtil.badRequest(i18nResolver.getText("applinks.invalid.consumer.publickey", id));
                } catch (InvalidKeySpecException e) {
                    return RestUtil.badRequest(i18nResolver.getText("applinks.invalid.consumer.publickey", id));
                }
            }
        } else {
            List<String> errors = validateAtlassianConsumer(consumerEntity);
            if (errors.size() > 0) {
                return RestUtil.badRequest(errors.toArray(new String[errors.size()]));
            }

            try {
                consumer = this.createBasicConsumer(consumerEntity, applicationLink);

                // update the consumer information to reflect the 2LO settings passed in.
                Consumer updatedConsumer = new Consumer.InstanceBuilder(consumer.getKey())
                        .name(consumer.getName())
                        .description(consumer.getDescription())
                        .publicKey(consumer.getPublicKey())
                        .signatureMethod(consumer.getSignatureMethod())
                        .callback(consumer.getCallback())
                        .twoLOAllowed(consumerEntity.isTwoLOAllowed())
                        .executingTwoLOUser(consumerEntity.getExecutingTwoLOUser())
                        .twoLOImpersonationAllowed(consumerEntity.isTwoLOImpersonationAllowed())
                        .build();

                // store the Consumer
                serviceProviderStoreService.addConsumer(updatedConsumer, applicationLink);

            } catch (NoSuchAlgorithmException e) {
                return RestUtil.badRequest(i18nResolver.getText("applinks.invalid.consumer.publickey", id));
            } catch (InvalidKeySpecException e) {
                return RestUtil.badRequest(i18nResolver.getText("applinks.invalid.consumer.publickey", id));
            }
        }

        return RestUtil
                .created(Link.self(new URI(OAuthApplicationLinkResource.CONTEXT + "/" + id + "/authentication/consumer")));
    }

    /**
     * Add an Outgoing Consumer for a 3rdParty link.
     */
    private Consumer add3rdPartyOutgoingConsumer(final ConsumerEntity consumerEntity) {
        final Consumer consumer = Consumer.key(consumerEntity.getKey())
                .name(consumerEntity.getName())
                .signatureMethod(Consumer.SignatureMethod.HMAC_SHA1)
                .description(consumerEntity.getDescription())
                .build();

        consumerService.add(consumer.getName(), consumer, consumerEntity.getSharedSecret());

        return consumer;
    }

    /**
     * Create a basic Consumer for the specified ApplicationLink
     */
    private Consumer createBasicConsumer(final ConsumerEntity consumerEntity, ApplicationLink applicationLink)
            throws InvalidKeySpecException, NoSuchAlgorithmException, URISyntaxException {
        return Consumer.key(consumerEntity.getKey())
                .name(consumerEntity.getName())
                .publicKey(RSAKeys.fromPemEncodingToPublicKey(consumerEntity.getPublicKey()))
                .description(consumerEntity.getDescription())
                .callback(consumerEntity.getCallback())
                .build();
    }

    /**
     * check the pre-requisites for creating a 3rdparty Consumer
     */
    private List<String> validate3rdPartyConsumer(ConsumerEntity consumerEntity) {
        List<String> errors = Lists.newArrayList();

        // check minimum parameters see AddServiceProviderManuallyServlet.save
        if (StringUtils.isEmpty(consumerEntity.getKey())) {
            errors.add(i18nResolver.getText("auth.oauth.config.consumer.serviceprovider.key.is.required"));
        }

        if (consumerEntity.isOutgoing()) {
            if (StringUtils.isEmpty(consumerEntity.getName())) {
                errors.add(i18nResolver.getText("auth.oauth.config.consumer.serviceprovider.name.is.required"));
            }

            if (StringUtils.isEmpty(consumerEntity.getSharedSecret())) {
                errors.add(i18nResolver.getText("auth.oauth.config.consumer.serviceprovider.shared.secret.is.required"));
            }
        } else {
            if (StringUtils.isEmpty(consumerEntity.getPublicKey())) {
                errors.add(i18nResolver.getText("auth.oauth.config.serviceprovider.missing.public.key"));
            }
        }

        return errors;
    }

    /**
     * check the pre-requisites for creating an atlassian Consumer
     */
    private List<String> validateAtlassianConsumer(ConsumerEntity consumerEntity) {
        List<String> errors = Lists.newArrayList();

        if (StringUtils.isEmpty(consumerEntity.getKey())) {
            errors.add(i18nResolver.getText("auth.oauth.config.consumer.serviceprovider.key.is.required"));
        }

        if (StringUtils.isEmpty(consumerEntity.getName())) {
            errors.add(i18nResolver.getText("auth.oauth.config.consumer.serviceprovider.name.is.required"));
        }

        if (StringUtils.isEmpty(consumerEntity.getPublicKey())) {
            errors.add(i18nResolver.getText("applinks.consumer.publickey.required"));
        }
        return errors;
    }

    /**
     * Get unfiltered Configured providers for the current ApplicationLink.
     */
    private List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink)
            throws URISyntaxException {
        return getConfiguredProviders(applicationLink, pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class));
    }

    /**
     * Get Configured providers for the current ApplicationLink.
     */
    private List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Iterable<AuthenticationProviderPluginModule> pluginModules)
            throws URISyntaxException {
        // get the providers configured for this link
        final List<AuthenticationProviderEntity> configuredAuthProviders = new ArrayList<AuthenticationProviderEntity>();
        for (AuthenticationProviderPluginModule authenticationProviderPluginModule : pluginModules) {
            final AuthenticationProvider authenticationProvider = authenticationProviderPluginModule.getAuthenticationProvider(applicationLink);
            if (authenticationProvider != null) {
                Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(), authenticationProviderPluginModule.getAuthenticationProviderClass());
                configuredAuthProviders.add(new AuthenticationProviderEntity(
                        Link.self(new URI(OAuthApplicationLinkResource.CONTEXT + "/" + applicationLink.getId().toString() + "/authentication/provider")),
                        authenticationProviderPluginModule.getClass().getName(),
                        authenticationProviderPluginModule.getAuthenticationProviderClass().getName(),
                        config));
            }
        }
        return configuredAuthProviders;
    }

    /**
     * Find the ApplicationLink by its id.
     */
    private ApplicationLink findApplicationLink(final String id) throws TypeNotInstalledException {
        ApplicationId applicationId;
        try {
            applicationId = new ApplicationId(id);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return applicationLinkService.getApplicationLink(applicationId);
    }

    /**
     * find an ApplicationLinks's Consumer
     */
    private Iterable<Consumer> findConsumers(ApplicationLink applicationLink, List<AuthenticationProviderEntity> configuredAuthProviders) {
        List<Consumer> consumers = Lists.newArrayList();
        Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);

        if (consumer != null) {
            // incoming consumer
            consumers.add(consumer);
        }

        // for generic applicationlinks the Consumer is stored in the OAuth ConsumerService using the consumerkey as the key
        // the consumerkey is stored as a property of the Authenticationprovider.
        if (applicationLink.getType() instanceof GenericApplicationType) {
            for (AuthenticationProviderEntity entity : configuredAuthProviders) {
                if (applicationLink.getType() instanceof GenericApplicationType) {
                    final String consumerKey = entity.getConfig().get("consumerKey.outbound");
                    if (StringUtils.isEmpty(consumerKey)) {
                        continue;
                    }

                    Consumer genericOutGoingConsumer = consumerService.getConsumerByKey(consumerKey);
                    if (genericOutGoingConsumer != null) {
                        // outgoing consumer
                        consumers.add(genericOutGoingConsumer);
                    }
                }
            }
        }

        return consumers;
    }
}

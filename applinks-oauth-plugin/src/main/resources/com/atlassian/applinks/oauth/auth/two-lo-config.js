function set2LoOptionsVisibility() {
    if (AJS.$('#two-lo-enabled').attr("checked")) {
        AJS.$('#2lo-impersonation-section').show();
        AJS.$('#2lo-execute-as-section').show();
    } else {
        AJS.$('#two-lo-impersonation-enabled').removeAttr("checked");
        AJS.$('#2lo-impersonation-section').hide();
        AJS.$('#2lo-execute-as-section').hide();
    }
}

function configure2LoOptionsEvents() {
    AJS.$('#two-lo-enabled').change(
        function() {
            set2LoOptionsVisibility();
        }
    );
    AJS.$('#two-loi-enabled').change(
        function() {
            set2LoiOptionsVisibility();
        }
    );
}
function setOutgoing2LoElementsVisibility() {
    var outgoing2LOEnabled = AJS.$('#outgoing-two-lo-enabled').is(":checked");
    var existingOutgoing2LOState = "true" == AJS.$('#existing-outgoing-2lo-state').val();

    if (outgoing2LOEnabled == existingOutgoing2LOState) {
        AJS.$('#outgoing-two-lo-action-update').attr('disabled','disabled');
    } else {
        AJS.$('#outgoing-two-lo-action-update').removeAttr('disabled');
    }
}

/**
 * @since 4.0.15
 */
function setOutgoing2LoiElementsVisibility() {
    var outgoing2LOiEnabled = AJS.$('#outgoing-two-loi-enabled').is(":checked");
    var existingOutgoing2LOiState = "true" == AJS.$('#existing-outgoing-2loi-state').val();

    if (outgoing2LOiEnabled == existingOutgoing2LOiState) {
        AJS.$('#outgoing-two-lo-action-update').attr('disabled','disabled');
    } else {
        AJS.$('#outgoing-two-lo-action-update').removeAttr('disabled');
    }
}

function configureOutgoing2LoEvents() {
    AJS.$('#outgoing-two-lo-enabled').change(
        function() {
            setOutgoing2LoElementsVisibility();
        }
    );

    AJS.$('#outgoing-two-loi-enabled').change(
        function() {
            setOutgoing2LoiElementsVisibility();
        }
    );
}
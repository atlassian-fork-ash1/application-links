package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.AbstractApplinksServlet.BadRequestException;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AddConsumerReciprocalServlet.CALLBACK_PARAM;
import static com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AddConsumerReciprocalServlet.ENABLE_OAUTH_AUTHENTICATION_PARAMETER;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddConsumerReciprocalServletTest {

    @Mock
    AdminUIAuthenticator adminUIAuthenticator;
    @Mock
    ApplicationLink applicationLink;
    @Mock
    ApplicationLinkService applicationLinkService;
    @Mock
    AuthenticationConfigurationManager authenticationConfigurationManager;
    @InjectMocks
    CallbackParameterValidator callbackParameterValidator;
    @Mock
    ConsumerTokenStoreService consumerTokenStoreService;
    @Mock
    DocumentationLinker documentationLinker;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    InternalHostApplication internalHostApplication;
    @Mock
    LoginUriProvider loginUriProvider;
    @Mock
    MessageFactory messageFactory;
    @Mock
    RedirectController redirectController;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    TemplateRenderer templateRenderer;
    @Mock
    WebResourceManager webResourceManager;
    @Mock
    WebSudoManager webSudoManager;
    @Mock
    XsrfTokenAccessor xsrfTokenAccessor;
    @Mock
    XsrfTokenValidator xsrfTokenValidator;
    private AddConsumerReciprocalServlet addConsumerReciprocalServlet;

    @Before
    public void setUp() {
        addConsumerReciprocalServlet =
                new AddConsumerReciprocalServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager, applicationLinkService, adminUIAuthenticator,
                        authenticationConfigurationManager, consumerTokenStoreService, internalHostApplication, loginUriProvider, documentationLinker, webSudoManager, xsrfTokenAccessor, xsrfTokenValidator, callbackParameterValidator, redirectController);
    }

    @Test(expected = BadRequestException.class)
    public void testDoGetInvalidApplicationLinkAndRedirectUrl() throws ServletException, IOException, TypeNotInstalledException, URISyntaxException {
        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(internalHostApplication.getBaseUrl()).thenReturn(new URI("something"));
        when(applicationLinkService.getApplicationLink(any())).thenReturn(null);
        when(request.getParameter(CALLBACK_PARAM)).thenReturn("//google.com");

        addConsumerReciprocalServlet.doGet(request, response);
    }

    @Test
    public void testDoGetInvalidApplicationLinkValidRedirectUrl() throws ServletException, IOException, TypeNotInstalledException, URISyntaxException {
        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(internalHostApplication.getBaseUrl()).thenReturn(new URI("something"));
        when(applicationLinkService.getApplicationLink(any())).thenReturn(null);
        when(request.getParameter(CALLBACK_PARAM)).thenReturn("google.com");
        when(internalHostApplication.getBaseUrl()).thenReturn(new URI("google"));

        addConsumerReciprocalServlet.doGet(request, response);
    }

    @Test
    public void testDoGetValidApplicationLinkAndRedirectUrl() throws ServletException, IOException, TypeNotInstalledException, URISyntaxException {
        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(applicationLinkService.getApplicationLink(any())).thenReturn(applicationLink);
        when(request.getParameter(ENABLE_OAUTH_AUTHENTICATION_PARAMETER)).thenReturn("true");
        doThrow(new RuntimeException("faking an error from the nested task")).when(authenticationConfigurationManager).registerProvider(any(), any(), any());
        when(request.getParameter(CALLBACK_PARAM)).thenReturn("google.com");
        when(internalHostApplication.getBaseUrl()).thenReturn(new URI("google"));

        addConsumerReciprocalServlet.doGet(request, response);
    }

    @Test(expected = BadRequestException.class)
    public void testDoGetValidApplicationLinkInvalidRedirectUrl() throws ServletException, IOException, TypeNotInstalledException {
        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(applicationLinkService.getApplicationLink(any())).thenReturn(applicationLink);
        when(request.getParameter(ENABLE_OAUTH_AUTHENTICATION_PARAMETER)).thenReturn("true");
        doThrow(new RuntimeException("faking an error from the nested task")).when(authenticationConfigurationManager).registerProvider(any(), any(), any());
        when(request.getParameter(CALLBACK_PARAM)).thenReturn("//google.com");

        addConsumerReciprocalServlet.doGet(request, response);
    }

    @Test
    public void testdoGetNormal() throws ServletException, IOException, TypeNotInstalledException, URISyntaxException {
        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(applicationLinkService.getApplicationLink(new ApplicationId("f81d4fae-7dec-11d0-a765-00a0c91e6bf6"))).thenReturn(applicationLink);
        when(request.getParameter(CALLBACK_PARAM)).thenReturn("google.com");

        addConsumerReciprocalServlet.doGet(request, response);
    }
}

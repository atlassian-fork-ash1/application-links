package com.atlassian.applinks.oauth.auth.servlets.consumer;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.io.PrintWriter;
import java.net.URI;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class AddServiceProviderManuallyServletTest {
    @DataPoints
    public static String[] methods = new String[]{"GET", "POST"};

    I18nResolver i18nResolver;
    MessageFactory messageFactory;
    TemplateRenderer templateRenderer;
    WebResourceManager webResourceManager;
    ApplicationLinkService applicationLinkService;
    AdminUIAuthenticator adminUIAuthenticator;
    InternalHostApplication internalHostApplication;
    DocumentationLinker documentationLinker;
    LoginUriProvider loginUriProvider;
    WebSudoManager webSudoManager;
    AuthenticationConfigurationManager configurationManager;
    ConsumerService consumerService;
    XsrfTokenAccessor xsrfTokenAccessor;
    XsrfTokenValidator xsrfTokenValidator;

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    PrintWriter writer;

    HttpServlet servlet;

    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        consumerService = mock(ConsumerService.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        writer = mock(PrintWriter.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);

        servlet = new AddServiceProviderManuallyServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager,
                applicationLinkService, adminUIAuthenticator, configurationManager, consumerService,
                internalHostApplication, loginUriProvider, documentationLinker,
                webSudoManager, xsrfTokenAccessor, xsrfTokenValidator);
    }

    @Theory
    public void verifyThatAdminAccessIsCheckedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();
        servlet.service(request, response);

        verify(adminUIAuthenticator).checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        servlet.service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    @Test
    public void verifyRelativitizeUri() {
        URI baseURI = URI.create("http://www.atlassian.com");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("http://www.atlassian.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWithPortNumber() {
        URI baseURI = URI.create("http://www.atlassian.com:1234");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("http://www.atlassian.com:1234/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriRespectingTheOriginalScheme() {
        URI baseURI = URI.create("https://www.atlassian.com");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("https://www.atlassian.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWithHttpsAndPortNumber() {
        URI baseURI = URI.create("https://www.atlassian.com:1234");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("https://www.atlassian.com:1234/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWhenNoScheme() {
        URI baseURI = URI.create("http://www.atlassian.com");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("www.atlassian.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWhenNoSchemeButWithPortNumber() {
        URI baseURI = URI.create("http://www.atlassian.com:1234");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("www.atlassian.com:1234/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriReturnsOriginalUriIfCannotRelativitize() {
        URI baseURI = URI.create("http://www.atlassian.com");
        assertEquals("http://www.google.com/blah/test", AddServiceProviderManuallyServlet.relativitize("http://www.google.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriReturnsOriginalUriWithSchemeIfCannotRelativitize() {
        URI baseURI = URI.create("http://www.atlassian.com");
        assertEquals("http://www.google.com/blah/test", AddServiceProviderManuallyServlet.relativitize("www.google.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWithTrailingSlashInBaseUri() {
        URI baseURI = URI.create("http://www.atlassian.com/");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("http://www.atlassian.com/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWithTrailingSlashInBaseUriAndPortNumber() {
        URI baseURI = URI.create("http://www.atlassian.com:1234/");
        assertEquals("/blah/test", AddServiceProviderManuallyServlet.relativitize("http://www.atlassian.com:1234/blah/test", baseURI));
    }

    @Test
    public void verifyRelativitizeUriWithTrailingSlash() {
        URI baseURI = URI.create("http://www.atlassian.com");
        assertEquals("/blah/test/", AddServiceProviderManuallyServlet.relativitize("http://www.atlassian.com/blah/test/", baseURI));
    }

    private void whenUserIsLoggedInAsAdmin() {
        when(adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }
}
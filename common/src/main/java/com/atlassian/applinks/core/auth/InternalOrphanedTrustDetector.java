package com.atlassian.applinks.core.auth;

/**
 * This is used to internally expose OrphanedTrustDetector implementations that are not supposed to be used externally.
 * They are going to become modules when the time allows. There's an issue with more detailed info - APLDEV-276
 *
 * @since 5.0
 */
public interface InternalOrphanedTrustDetector extends OrphanedTrustDetector {

}

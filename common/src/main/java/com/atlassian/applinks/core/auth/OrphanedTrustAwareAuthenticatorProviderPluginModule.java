package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.spi.auth.AutoConfiguringAuthenticatorProviderPluginModule;

/**
 * Authentication provider modules implementing this interface can be queried for support of a CertificateType.
 *
 * @since 5.0.1
 * @deprecated
 */
@Deprecated
public interface OrphanedTrustAwareAuthenticatorProviderPluginModule extends AutoConfiguringAuthenticatorProviderPluginModule {
    /**
     * @param certificateType String that represents the CertificateType name as returned by name()
     * @return true if the Provider supports the CertificateType
     */
    boolean isApplicable(String certificateType);

}

package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.ResponseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Base class for application link requests.
 *
 * @since 3.11.0
 */
public abstract class AbstractApplicationLinkRequest implements ApplicationLinkRequest {
    protected String url;
    protected final ApplicationLinkRequest wrappedRequest;
    protected final Map<String, List<String>> parameters = new HashMap<>();
    protected boolean followRedirects = true;

    public AbstractApplicationLinkRequest(final String url, final Request wrappedRequest) {
        this.url = url;
        this.wrappedRequest = new ApplicationLinkRequestAdaptor(wrappedRequest);
        this.wrappedRequest.setFollowRedirects(false);
    }

    public ApplicationLinkRequest setConnectionTimeout(final int i) {
        wrappedRequest.setConnectionTimeout(i);
        return this;
    }

    public ApplicationLinkRequest setSoTimeout(final int i) {
        wrappedRequest.setSoTimeout(i);
        return this;
    }

    public ApplicationLinkRequest setUrl(final String s) {
        this.url = s;
        wrappedRequest.setUrl(s);
        return this;
    }

    public ApplicationLinkRequest setRequestBody(final String s) {
        wrappedRequest.setRequestBody(s);
        return this;
    }

    public ApplicationLinkRequest setRequestBody(final String requestBody, final String contentType) {
        wrappedRequest.setRequestBody(requestBody, contentType);
        return this;
    }

    public ApplicationLinkRequest setFiles(final List<RequestFilePart> files) {
        wrappedRequest.setFiles(files);
        return this;
    }

    public ApplicationLinkRequest setEntity(final Object o) {
        wrappedRequest.setEntity(o);
        return this;
    }

    public ApplicationLinkRequest addRequestParameters(final String... params) {
        wrappedRequest.addRequestParameters(params);
        for (int i = 0; i < params.length; i += 2) {
            final String name = params[i];
            final String value = params[i + 1];
            List<String> list = parameters.get(name);
            if (list == null) {
                list = new ArrayList<>();
                parameters.put(name, list);
            }
            list.add(value);
        }

        return this;
    }

    public ApplicationLinkRequest addBasicAuthentication(final String hostname, final String username, final String password) {
        wrappedRequest.addBasicAuthentication(hostname, username, password);
        return this;
    }

    public ApplicationLinkRequest addHeader(final String s, final String s1) {
        wrappedRequest.addHeader(s, s1);
        return this;
    }

    public ApplicationLinkRequest setHeader(final String s, final String s1) {
        wrappedRequest.setHeader(s, s1);
        return this;
    }

    public Map<String, List<String>> getHeaders() {
        return wrappedRequest.getHeaders();
    }

    public ApplicationLinkRequest setFollowRedirects(final boolean followRedirects) {
        // NOTE: not passing it through to wrappedRequest _on purpose_; this implementation relies on subclasses
        // handle redirects "manually"
        this.followRedirects = followRedirects;
        return this;
    }

    public boolean getFollowRedirects() {
        return followRedirects;
    }

    public String execute() throws ResponseException {
        signRequest();
        return executeAndReturn(new ApplicationLinksStringReturningResponseHandler());
    }

    protected abstract void signRequest() throws ResponseException;
}
package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.core.util.RedirectHelper;

/**
 * Base class for application link response handlers.
 *
 * * @since 3.11.0
 */
public abstract class AbstractApplicationLinkResponseHandler {
    protected final ApplicationLinkRequest wrappedRequest;
    protected final boolean followRedirects;
    protected final RedirectHelper redirectHelper;

    public AbstractApplicationLinkResponseHandler(String url, ApplicationLinkRequest wrappedRequest,
                                                  boolean followRedirects) {
        this.wrappedRequest = wrappedRequest;
        this.followRedirects = followRedirects;

        this.redirectHelper = new RedirectHelper(url);
    }
}

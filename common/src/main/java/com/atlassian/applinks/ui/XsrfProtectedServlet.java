package com.atlassian.applinks.ui;

public interface XsrfProtectedServlet {
    String OVERRIDE_HEADER_NAME = "X-Atlassian-Token";
    String OVERRIDE_HEADER_VALUE = "no-check";
}

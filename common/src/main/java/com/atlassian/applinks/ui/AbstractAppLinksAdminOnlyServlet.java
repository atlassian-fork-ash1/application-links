package com.atlassian.applinks.ui;

import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.http.HttpServletRequest;

/**
 * Extend this class for servlets that are accessible to users that are able to administrate application links.
 *
 * @since v3.0
 */
public abstract class AbstractAppLinksAdminOnlyServlet extends AbstractAppLinksProtectedServlet {
    protected final AdminUIAuthenticator adminUIAuthenticator;

    public AbstractAppLinksAdminOnlyServlet(final I18nResolver i18nResolver,
                                            final MessageFactory messageFactory,
                                            final TemplateRenderer templateRenderer,
                                            final WebResourceManager webResourceManager,
                                            final AdminUIAuthenticator adminUIAuthenticator,
                                            final DocumentationLinker documentationLinker,
                                            final LoginUriProvider loginUriProvider,
                                            final InternalHostApplication internalHostApplication,
                                            final XsrfTokenAccessor xsrfTokenAccessor,
                                            final XsrfTokenValidator xsrfTokenValidator) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, documentationLinker, loginUriProvider,
                internalHostApplication, adminUIAuthenticator, xsrfTokenAccessor, xsrfTokenValidator);
        this.adminUIAuthenticator = adminUIAuthenticator;
    }

    @Override
    protected final boolean checkAccess(HttpServletRequest request) {
        return adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Override
    protected void handleUnauthorizedAccess(HttpServletRequest request) {
        if (request.getUserPrincipal() != null) {
            throw new UnauthorizedException(messageFactory.newI18nMessage("applinks.error.only.admin"));
        } else {
            throw new UnauthorizedBecauseUnauthenticatedException();
        }
    }
}

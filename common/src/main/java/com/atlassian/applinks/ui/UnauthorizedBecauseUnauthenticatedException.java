package com.atlassian.applinks.ui;

import javax.servlet.http.HttpServletResponse;

public class UnauthorizedBecauseUnauthenticatedException extends RequestException {
    public UnauthorizedBecauseUnauthenticatedException() {
        super(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
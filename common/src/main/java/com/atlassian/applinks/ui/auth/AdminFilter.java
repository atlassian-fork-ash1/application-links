package com.atlassian.applinks.ui.auth;

import java.io.IOException;
import java.net.URI;
import java.util.EnumSet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.page.PageCapability;
import com.atlassian.sal.api.user.UserRole;

/**
 * Servlet filter for urls that require admin privilege to access.
 *
 * @since 3.0
 */
public class AdminFilter implements Filter {
    protected final AdminUIAuthenticator uiAuthenticator;
    private final LoginUriProvider loginUriProvider;
    private final ApplicationProperties applicationProperties;

    public AdminFilter(final AdminUIAuthenticator uiAuthenticator, final LoginUriProvider loginUriProvider, final ApplicationProperties applicationProperties) {
        this.uiAuthenticator = uiAuthenticator;
        this.loginUriProvider = loginUriProvider;
        this.applicationProperties = applicationProperties;
    }

    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
                         final FilterChain filterChain) throws IOException, ServletException {
        if (!(servletRequest instanceof HttpServletRequest)) {
            return;
        }

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        final String username = request.getParameter(AdminUIAuthenticator.ADMIN_USERNAME);
        final String password = request.getParameter(AdminUIAuthenticator.ADMIN_PASSWORD);

        if (!checkAccess(username, password, new ServletSessionHandler(request))) {
            handleAccessDenied(request, response);
            return;
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Handles access denied as decided by this filter.
     * By default, we simply redirect to a login page. Override this method if you want it to do something else.
     *
     * @param request  request
     * @param response response
     * @throws java.io.IOException IOException
     */
    protected void handleAccessDenied(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // By default, we redirect to login page so if user actually has the privilege, he will eventually get
        // redirected to the right page after the login.
        response.sendRedirect(loginUriProvider.getLoginUriForRole(getOriginalUrl(request), getForRole(), EnumSet.of(PageCapability.IFRAME)).toASCIIString());
    }

    /***
     * Gets the required role which will be displayed in the admin login UI.
     *
     * @return required role.
     */
    UserRole getForRole() {
        return UserRole.ADMIN;
    }

    /**
     * Checks if the given username and password should be allowed to perform the operation.
     *
     * @param username       username
     * @param password       password
     * @param sessionHandler session handler
     * @return true if the user should be allowed to perform the operation.
     */
    boolean checkAccess(String username, String password, AdminUIAuthenticator.SessionHandler sessionHandler) {
        return uiAuthenticator.checkAdminUIAccessBySessionOrPasswordAndActivateAdminSession(username, password, sessionHandler);
    }

    private URI getOriginalUrl(final HttpServletRequest request) {
        final String originalUrl = new StringBuilder(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE))
                .append(request.getServletPath())
                .append(request.getPathInfo())
                .append(sanitiseQueryString(request))
                .toString();
        return URIUtil.uncheckedToUri(originalUrl);
    }

    private String sanitiseQueryString(final HttpServletRequest request) {
        String queryString = request.getQueryString();

        if (queryString == null) {
            queryString = "";
        } else {
            queryString = queryString.replaceAll("(&|^)al_(username|password)=[^&]*", "");
            if (queryString.length() > 0) {
                queryString = "?" + queryString;
            }
        }

        return queryString;
    }

    public void init(final FilterConfig filterConfig) throws ServletException {
        // sun rise
    }

    public void destroy() {
        // sun set
    }

}

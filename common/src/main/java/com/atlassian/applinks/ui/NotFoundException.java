package com.atlassian.applinks.ui;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.core.util.Message;

public class NotFoundException extends RequestException {
    public NotFoundException() {
        this(null);
    }

    public NotFoundException(final Message message) {
        super(HttpServletResponse.SC_BAD_REQUEST, message);
    }

    public NotFoundException(final Message message, final Throwable cause) {
        super(HttpServletResponse.SC_NOT_FOUND, message, cause);
    }
}


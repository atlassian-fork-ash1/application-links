package com.atlassian.applinks.ui;

import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractAppLinksProtectedServlet extends AbstractApplinksServlet implements XsrfProtectedServlet {
    public AbstractAppLinksProtectedServlet(final I18nResolver i18nResolver,
                                            final MessageFactory messageFactory,
                                            final TemplateRenderer templateRenderer,
                                            final WebResourceManager webResourceManager,
                                            final DocumentationLinker documentationLinker,
                                            final LoginUriProvider loginUriProvider,
                                            final InternalHostApplication internalHostApplication,
                                            final AdminUIAuthenticator adminUIAuthenticator,
                                            final XsrfTokenAccessor xsrfTokenAccessor,
                                            final XsrfTokenValidator xsrfTokenValidator) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, documentationLinker,
                loginUriProvider, internalHostApplication, adminUIAuthenticator, xsrfTokenAccessor,
                xsrfTokenValidator);
    }

    @Override
    protected void doService(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        if (checkAccess(request)) {
            doProtectedService(request, response);
        } else {
            handleUnauthorizedAccess(request);
        }
    }

    /**
     * Handles the situation when the request should not be allowed to perform the operation.
     * Generally this would involve displaying a useful message to user.
     *
     * @param request http request
     */
    protected abstract void handleUnauthorizedAccess(HttpServletRequest request);

    /**
     * Checks whether the request should be allowed to execute the operation.
     *
     * @param request http request
     * @return true if the operation should be allowed, false otherwise
     */
    protected abstract boolean checkAccess(final HttpServletRequest request);

    /**
     * <p>
     * Override this method for operations that need to occur before control is
     * delegated to {{doGet()}}, {{doPost()}}, etc. This method is invoked
     * after the admin permission-check is done.
     * </p>
     * <p>
     * This method may throw
     * {@link AbstractApplinksServlet.RequestException}s.
     * </p>
     */
    protected void doProtectedService(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
    }
}


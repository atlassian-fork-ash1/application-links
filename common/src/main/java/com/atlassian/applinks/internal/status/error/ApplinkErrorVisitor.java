package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * {@link ApplinkError} visitor.
 *
 * @param <T> return type parameter
 * @since 5.0
 */
public interface ApplinkErrorVisitor<T> {
    /**
     * Visit generic {@code ApplinkError}.
     *
     * @param error error to visit
     */
    @Nullable
    T visit(@Nonnull ApplinkError error);

    /**
     * Visit {@code AuthorisationUriAwareApplinkError}.
     *
     * @param error error to visit
     */
    @Nullable
    T visit(@Nonnull AuthorisationUriAwareApplinkError error);

    /**
     * Visit {@code ResponseApplinkError}.
     *
     * @param responseError response error to visit
     */
    @Nullable
    T visit(@Nonnull ResponseApplinkError responseError);
}

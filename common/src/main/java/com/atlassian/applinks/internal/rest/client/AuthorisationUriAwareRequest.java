package com.atlassian.applinks.internal.rest.client;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.internal.authentication.AuthorisationUriAware;

/**
 * Application link request that provides access to its {@link AuthorisationURIGenerator}.
 *
 * @since 4.3
 */
public interface AuthorisationUriAwareRequest extends ApplicationLinkRequest, AuthorisationUriAware {
}

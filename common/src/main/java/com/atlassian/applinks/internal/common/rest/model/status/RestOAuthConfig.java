package com.atlassian.applinks.internal.common.rest.model.status;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;
import com.atlassian.applinks.internal.rest.model.RestRepresentation;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * REST representation of {@link OAuthConfig}.
 *
 * @since 4.3
 */
public class RestOAuthConfig extends ApplinksRestRepresentation implements RestRepresentation<OAuthConfig> {
    public static final String ENABLED = "enabled";
    public static final String TWO_LO_ENABLED = "twoLoEnabled";
    public static final String TWO_LO_IMPERSONATION_ENABLED = "twoLoImpersonationEnabled";

    private boolean enabled;
    private boolean twoLoEnabled;
    private boolean twoLoImpersonationEnabled;

    //for Jackson
    public RestOAuthConfig(){
    }

    public RestOAuthConfig(@Nonnull OAuthConfig oAuthConfig) {
        requireNonNull(oAuthConfig, "oAuthConfig");
        enabled = oAuthConfig.isEnabled();
        twoLoEnabled = oAuthConfig.isTwoLoEnabled();
        twoLoImpersonationEnabled = oAuthConfig.isTwoLoImpersonationEnabled();
    }

    @Nonnull
    @Override
    public OAuthConfig asDomain() {
        if (!enabled) {
            return OAuthConfig.createDisabledConfig();
        } else if (twoLoImpersonationEnabled) {
            return OAuthConfig.createOAuthWithImpersonationConfig();
        } else if (twoLoEnabled) {
            return OAuthConfig.createDefaultOAuthConfig();
        } else {
            return OAuthConfig.createThreeLoOnlyConfig();
        }
    }
}

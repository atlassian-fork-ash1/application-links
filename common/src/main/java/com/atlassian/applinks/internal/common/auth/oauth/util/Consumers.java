package com.atlassian.applinks.internal.common.auth.oauth.util;

import com.atlassian.oauth.Consumer;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Provides (missing) "copy constructor" functionality for the {@link Consumer} object, by creating
 * {@link Consumer.InstanceBuilder} matching a {@link Consumer} instance.
 *
 * @since 4.3
 */
public final class Consumers {
    private Consumers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    public static Consumer.InstanceBuilder consumerBuilder(@Nonnull Consumer consumer) {
        requireNonNull(consumer, "consumer");

        Consumer.InstanceBuilder builder = new Consumer.InstanceBuilder(consumer.getKey())
                .name(consumer.getName())
                .description(consumer.getDescription())
                .signatureMethod(consumer.getSignatureMethod())
                .callback(consumer.getCallback())
                .twoLOAllowed(consumer.getTwoLOAllowed())
                .executingTwoLOUser(consumer.getExecutingTwoLOUser())
                .twoLOImpersonationAllowed(consumer.getTwoLOImpersonationAllowed());

        if (consumer.getPublicKey() != null) {
            builder.publicKey(consumer.getPublicKey());
        }

        return builder;
    }
}

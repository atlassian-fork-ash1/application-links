package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised specifically when an entity update fails.
 *
 * @since 4.3
 */
public class EntityUpdateException extends EntityModificationException {
    public EntityUpdateException(@Nullable String message) {
        super(message);
    }

    public EntityUpdateException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}

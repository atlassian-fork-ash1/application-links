package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * @since 5.2
 */
public class InvalidEntityStateException extends ServiceException {
    public InvalidEntityStateException(@Nullable String message) {
        super(message);
    }

    public InvalidEntityStateException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}

package com.atlassian.applinks.internal.common.rest.model.applink;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.rest.model.IllegalRestRepresentationStateException;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;

import java.net.URI;
import java.net.URISyntaxException;
import javax.annotation.Nonnull;

/**
 * Complete REST representation of {@link ApplicationLink}.
 *
 * @since 5.0
 */
public class RestApplicationLink extends RestMinimalApplicationLink {
    public static final String SYSTEM = "system";
    public static final String PRIMARY = "primary";

    public boolean isSystem() {
        return system;
    }

    public boolean isPrimary() {
        return primary;
    }

    private boolean system;
    private boolean primary;

    //for jackson
    public RestApplicationLink(){

    }

    //For testing
    public RestApplicationLink(String name, String rpcUrl, String displayUrl, boolean primary) throws URISyntaxException {
        super("", name, asURI(displayUrl), asURI(rpcUrl), "");
        this.system = false;
        this.primary = primary;
    }

    public RestApplicationLink(@Nonnull ApplicationLink link) {
        super(link);
        this.system = link.isSystem();
        this.primary = link.isPrimary();
    }

    @Nonnull
    public ApplicationLinkDetails toDetails() throws IllegalRestRepresentationStateException {
        return ApplicationLinkDetails.builder()
                .name(validateString(NAME, getName()))
                .rpcUrl(validateURI(RPC_URL, getRpcUrl()))
                .displayUrl(validateURI(DISPLAY_URL, getDisplayUrl()))
                .isPrimary(isPrimary())
                .build();
    }

    private static URI asURI(String uri) throws URISyntaxException {
        return uri == null ? null: new URI(uri);

    }
}

package com.atlassian.applinks.internal.common.net;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseStatusException;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.google.common.collect.Iterables.contains;
import static java.util.Objects.requireNonNull;

public class ResponsePreconditions {
    @Nonnull
    public static Response checkStatus(@Nonnull Response response, @Nonnull Status... expectedStatuses)
            throws ResponseStatusException {
        return checkStatus(response, ImmutableSet.copyOf(expectedStatuses));
    }

    @Nonnull
    public static Response checkStatus(@Nonnull Response response, @Nonnull Iterable<Status> expectedStatuses)
            throws ResponseStatusException {
        requireNonNull(expectedStatuses, "expectedStatuses");
        requireNonNull(response, "response");

        Status responseStatus = Status.fromStatusCode(response.getStatusCode());
        if (!contains(expectedStatuses, responseStatus)) {
            throw new ResponseStatusException("Unexpected status: " + response.getStatusCode(), response);
        }

        return response;
    }

    @Nonnull
    public static Response checkStatusOk(@Nonnull Response response)
            throws ResponseStatusException {
        return checkStatus(response, Status.OK);
    }

    @Nonnull
    public static Response fail(@Nonnull Response response)
            throws ResponseStatusException {
        throw new ResponseStatusException("Unexpected status: " + response.getStatusCode(), response);
    }
}

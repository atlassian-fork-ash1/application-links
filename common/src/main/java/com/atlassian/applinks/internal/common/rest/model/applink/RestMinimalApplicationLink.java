package com.atlassian.applinks.internal.common.rest.model.applink;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;
import javax.annotation.Nonnull;
import java.net.URI;
import static com.atlassian.applinks.internal.common.application.ApplicationTypes.resolveApplicationTypeId;
import static java.util.Objects.requireNonNull;

/**
 * Minimal JSON representation of {@code ApplicationLink}. This is read-only entity that only contains the core applink data and no
 * configuration.
 *
 * @since 4.3
 */
public class RestMinimalApplicationLink extends ApplinksRestRepresentation {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DISPLAY_URL = "displayUrl";
    public static final String RPC_URL = "rpcUrl";
    public static final String TYPE = "type";

    private String id;
    private String name;
    private URI displayUrl;
    private URI rpcUrl;
    private String type;

    RestMinimalApplicationLink(){

    }

    public RestMinimalApplicationLink(String id,
                                      String name,
                                      URI displayUrl,
                                      URI rpcUrl,
                                      String type) {
        this.id = id;
        this.name = name;
        this.displayUrl = displayUrl;
        this.rpcUrl = rpcUrl;
        this.type = type;
    }

    public RestMinimalApplicationLink(@Nonnull ApplicationLink link) {
        requireNonNull(link, "link");
        id = link.getId().get();
        name = link.getName();
        displayUrl = link.getDisplayUrl();
        rpcUrl = link.getRpcUrl();
        type = resolveApplicationTypeId(link.getType());
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public URI getDisplayUrl() {
        return displayUrl;
    }

    public URI getRpcUrl() {
        return rpcUrl;
    }

    public String getType() {
        return type;
    }
}
package com.atlassian.applinks.internal.rest.client;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.sal.api.net.Request.MethodType;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.Validate;
import org.apache.http.entity.ContentType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.ui.XsrfProtectedServlet.OVERRIDE_HEADER_NAME;
import static com.atlassian.applinks.ui.XsrfProtectedServlet.OVERRIDE_HEADER_VALUE;
import static java.util.Objects.requireNonNull;

/**
 * Utilities to create and customize Applinks {@link ApplicationLinkRequest requests} with headers,
 * parameters and bodies.
 *
 * @since 4.3
 */
public final class RestRequestBuilder {
    private static final long DEFAULT_TIMEOUT_SECONDS = 15;

    @Nonnull
    public static AuthorisationUriAwareRequest createAnonymousRequest(@Nonnull ApplicationLink link,
                                                                      @Nonnull RestUrlBuilder url,
                                                                      @Nonnull MethodType methodType) {
        return new RestRequestBuilder(link)
                .methodType(methodType)
                .url(url)
                .buildAnonymous();
    }

    @Nonnull
    public static AuthorisationUriAwareRequest createAnonymousRequest(@Nonnull ApplicationLink link,
                                                                      @Nonnull RestUrlBuilder url) {
        return new RestRequestBuilder(link)
                .url(url)
                .buildAnonymous();
    }

    private final ApplicationLink link;
    private final Map<String, String> headers = Maps.newHashMap();

    private RestUrlBuilder url = new RestUrlBuilder();
    private MethodType methodType = MethodType.GET;
    private String accept = MediaType.APPLICATION_JSON;
    private Class<? extends AuthenticationProvider> authentication = Anonymous.class;

    private Object body = null;
    private String contentType = MediaType.APPLICATION_JSON;

    private int connectionTimeoutMillis = (int) TimeUnit.SECONDS.toMillis(DEFAULT_TIMEOUT_SECONDS);
    private int socketTimeoutMillis = (int) TimeUnit.SECONDS.toMillis(DEFAULT_TIMEOUT_SECONDS);
    private boolean followRedirects = true;

    public RestRequestBuilder(@Nonnull ApplicationLink link) {
        this.link = requireNonNull(link, "link");
    }

    @Nonnull
    public RestRequestBuilder url(@Nonnull RestUrlBuilder url) {
        this.url = requireNonNull(url, "url");

        return this;
    }

    @Nonnull
    public RestRequestBuilder anonymous() {
        return authentication(Anonymous.class);
    }

    /**
     * Provide authentication provider for this request. Default value is {@link Anonymous}
     *
     * @param authentication authentication provider for this request
     * @return this builder instance
     * @see AuthenticationProvider
     */
    @Nonnull
    public RestRequestBuilder authentication(@Nonnull Class<? extends AuthenticationProvider> authentication) {
        this.authentication = requireNonNull(authentication, "authentication");

        return this;
    }

    /**
     * @param body serializable entity to use as body of the request, the entity will be serialized into the media type
     *             specified by {@link #contentType(String)}
     * @return this builder instance
     * @see #contentType(String)
     */
    @Nonnull
    public RestRequestBuilder body(@Nullable Object body) {
        this.body = body;
        return this;
    }

    /**
     * Customize content type of the request's body. The default is {@link MediaType#APPLICATION_JSON} (which also
     * implies UTF-8).
     * {@link MediaType} and {@link ContentType} classes provides handy constants compatible with this header.
     *
     * @param contentType the content type to use
     * @return this builder instance
     * @see ContentType#APPLICATION_JSON
     * @see ContentType
     * @see MediaType#APPLICATION_JSON
     * @see MediaType
     */
    @Nonnull
    public RestRequestBuilder contentType(@Nonnull String contentType) {
        this.contentType = requireNonNull(contentType, "contentType");
        return this;
    }

    /**
     * Customize the accept header of the request's body that may affect the content type of the response. The default
     * is {@link MediaType#APPLICATION_JSON}. {@link MediaType} class provides handy constants compatible with this
     * header.
     *
     * @param mediaType the media type to use
     * @return this builder instance
     * @see MediaType#APPLICATION_JSON
     * @see MediaType
     */
    @Nonnull
    public RestRequestBuilder accept(@Nonnull String mediaType) {
        this.accept = requireNonNull(mediaType, "mimeType");

        return this;
    }

    @Nonnull
    public RestRequestBuilder methodType(@Nonnull MethodType methodType) {
        this.methodType = requireNonNull(methodType, "methodType");

        return this;
    }

    /**
     * Set a custom HTTP header. Note: headers set via this method will override some built-in headers, such as
     * {@link #contentType(String)} and {@link #accept(String)}.
     *
     * @param name  header name
     * @param value header value
     * @return this builder instance
     * @see HttpHeaders
     */
    @Nonnull
    public RestRequestBuilder header(@Nonnull String name, @Nonnull String value) {
        this.headers.put(requireNonNull(name, "name"), requireNonNull(value, "value"));

        return this;
    }

    /**
     * Set a custom connection timeout. NOTE: due to limitations in the underlying API if the calculated value in
     * milliseconds exceeds {@link Integer#MAX_VALUE}, the value will be set to {@link Integer#MAX_VALUE}.
     *
     * @param value timeout value, must be greater than 0
     * @param unit  time unit
     * @return this builder instance
     */
    @Nonnull
    public RestRequestBuilder connectionTimeout(long value, @Nonnull TimeUnit unit) {
        Validate.isTrue(value > 0, "timeout value must be >0");
        this.connectionTimeoutMillis = Ints.saturatedCast(unit.toMillis(value));

        return this;
    }

    /**
     * Set a custom socket timeout. NOTE: due to limitations in the underlying API if the calculated value in
     * milliseconds exceeds {@link Integer#MAX_VALUE}, the value will be set to {@link Integer#MAX_VALUE}.
     *
     * @param value timeout value, must be greater than 0
     * @param unit  time unit
     * @return this builder instance
     */
    @Nonnull
    public RestRequestBuilder socketTimeout(long value, @Nonnull TimeUnit unit) {
        Validate.isTrue(value > 0, "timeout value must be >0");
        this.socketTimeoutMillis = Ints.saturatedCast(unit.toMillis(value));

        return this;
    }

    /**
     * @param followRedirects {@code true} if the created request should automatically follow redirects,
     *                        {@code false} otherwise; the default value of this setting is {@code true}
     * @return this builder instance
     */
    @Nonnull
    public RestRequestBuilder followRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;

        return this;
    }

    /**
     * Build the request assuming it requires authentication. In case the supplied
     * {@link #authentication(Class)} authentication provider} requires credentials that are missing, an appropriate
     * error will be raised.
     *
     * @return the request
     * @throws CredentialsRequiredException if there is no local credentials necessary to create an authenticated
     *                                      request
     * @see #buildAnonymous() if the request is {@link #anonymous() not authenticated}
     */
    @Nonnull
    public AuthorisationUriAwareRequest build() throws CredentialsRequiredException {
        return buildRequest(link.createAuthenticatedRequestFactory(authentication));
    }

    /**
     * Build the request assuming it requires authentication. In case the supplied
     * {@link #authentication(Class)} authentication provider} requires credentials that are missing, an appropriate
     * error will be raised.
     * In the case that the authentication is not configured, an empty Optional request will be returned.
     *
     * @return the Optional request
     * @throws CredentialsRequiredException if there is no local credentials necessary to create an authenticated
     *                                      request
     * @see #buildAnonymous() if the request is {@link #anonymous() not authenticated}
     */
    @Nonnull
    public Optional<AuthorisationUriAwareRequest> buildOptional() throws CredentialsRequiredException {
        ApplicationLinkRequestFactory requestFactory = link.createAuthenticatedRequestFactory(authentication);
        if (requestFactory == null) {
            return Optional.empty();
        }
        return Optional.of(buildRequest(requestFactory));
    }

    private AuthorisationUriAwareRequest buildRequest(@Nonnull ApplicationLinkRequestFactory requestFactory) throws CredentialsRequiredException {
        ApplicationLinkRequest request = requestFactory
                .createRequest(methodType, url.toString())
                .setHeader(HttpHeaders.ACCEPT, accept)
                .addHeader(OVERRIDE_HEADER_NAME, OVERRIDE_HEADER_VALUE)
                .setConnectionTimeout(connectionTimeoutMillis)
                .setSoTimeout(socketTimeoutMillis)
                .setFollowRedirects(followRedirects);

        if (body != null && isMutatingMethodType()) {
            request.setEntity(body);
            request.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
        }

        for (Map.Entry<String, String> header : headers.entrySet()) {
            request.setHeader(header.getKey(), header.getValue());
        }

        return new DefaultAuthorisationUriAwareRequest(request, requestFactory);
    }

    /**
     * Build request assuming it does not require authentication and therefore {@link CredentialsRequiredException}
     * should not occur.
     *
     * @return the request
     * @see #build()
     */
    @Nonnull
    public AuthorisationUriAwareRequest buildAnonymous() {
        try {
            return build();
        } catch (CredentialsRequiredException e) {
            throw new IllegalStateException("Unexpected credentials required", e);
        }
    }

    private boolean isMutatingMethodType() {
        return methodType == MethodType.POST || methodType == MethodType.PUT;
    }
}

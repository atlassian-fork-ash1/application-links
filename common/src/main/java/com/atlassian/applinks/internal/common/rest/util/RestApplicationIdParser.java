package com.atlassian.applinks.internal.common.rest.util;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.ws.rs.WebApplicationException;

import static com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException.invalidIdI18nKey;
import static com.atlassian.applinks.internal.common.rest.util.RestResponses.badRequest;
import static java.util.Objects.requireNonNull;


public final class RestApplicationIdParser {
    private final ServiceExceptionFactory serviceExceptionFactory;

    @Autowired
    public RestApplicationIdParser(ServiceExceptionFactory serviceExceptionFactory) {
        this.serviceExceptionFactory = serviceExceptionFactory;
    }

    /**
     * Parse {@code id} to {@code ApplicationId} object.
     *
     * @param id application ID to parse
     * @return ApplicationId if {@code id} is valid
     * @throws WebApplicationException if id is invalid
     * @deprecated use {@link #parse(String)} instead
     */
    @Deprecated
    public static ApplicationId parseApplicationId(@Nonnull String id) {
        try {
            return new ApplicationId(id);
        } catch (IllegalArgumentException e) {
            throw new WebApplicationException(badRequest(e.getMessage()));
        }
    }

    @Nonnull
    public ApplicationId parse(@Nonnull String id) throws InvalidApplicationIdException {
        try {
            return new ApplicationId(requireNonNull(id, "id"));
        } catch (IllegalArgumentException e) {
            throw serviceExceptionFactory.raise(InvalidApplicationIdException.class, invalidIdI18nKey(id), e);
        }
    }
}

package com.atlassian.applinks.internal.status.error;

/**
 * Represents an overall broad category of error that can affect Application Links.
 *
 * @since 4.3
 */
public enum ApplinkErrorCategory {
    /**
     * Unrecognized error. This would most likely be a networking problem that the system does not specifically handle.
     */
    UNKNOWN,

    /**
     * Application Link has incompatible configuration, or points to a remote application that runs an incompatible
     * version of Application Links.
     * <p>
     * Note: the affected Application Link <i>may</i> be working fine, however the system is not able to determine any
     * further details of that link's status.
     */
    INCOMPATIBLE,

    /**
     * Application Link does not point to an Atlassian application.
     * Note: the affected Application Link <i>may</i> be working fine, however the system will not attempt to determine
     * any further details of that link's status.
     */
    NON_ATLASSIAN,

    /**
     * Internal system-managed link, not editable.
     *
     * @see com.atlassian.applinks.api.ApplicationLink#isSystem()
     */
    SYSTEM,

    /**
     * Application Link has deprecated authentication configuration. Migration may be possible.
     */
    DEPRECATED,

    /**
     * Network error prevents communicating with the linked application. This most likely prevents the Application Link
     * from normal operation.
     */
    NETWORK_ERROR,

    /**
     * Authentication and/or authorization is required to retrieve further details of the Application Link status.
     * <p>
     * Note: the affected Application Link <i>may</i> be working fine, however the system is not able to determine any
     * further details of that link status.
     */
    ACCESS_ERROR,

    /**
     * Configuration error prevents the Application Link from normal operation, e.g. incoming/outgoing authentication
     * level does not match the remotely configured authentication levels.
     */
    CONFIG_ERROR,

    /**
     * Authenticated communication between the linked applications is disabled
     */
    DISABLED
}
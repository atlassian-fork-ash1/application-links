package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.1
 */
public class SimpleDetailedError implements DetailedError {
    private final String context;
    private final String summary;
    private final String details;

    public SimpleDetailedError(@Nullable String context, @Nonnull String summary, @Nullable String details) {
        this.context = context;
        this.summary = requireNonNull(summary, "summary");
        this.details = details;
    }

    public SimpleDetailedError(@Nonnull String summary) {
        this(null, summary, null);
    }

    @Nullable
    @Override
    public String getContext() {
        return context;
    }

    @Nonnull
    @Override
    public String getSummary() {
        return summary;
    }

    @Nullable
    @Override
    public String getDetails() {
        return details;
    }
}

package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Thrown when attempt is made to activate a system feature.
 *
 * @since 4.3
 */
public class SystemFeatureException extends InvalidArgumentException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.feature.system";

    public SystemFeatureException(@Nullable String message) {
        super(message);
    }

    public SystemFeatureException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}

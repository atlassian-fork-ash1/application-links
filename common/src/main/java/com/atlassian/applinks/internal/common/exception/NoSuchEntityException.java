package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised when requested entity does not exist.
 *
 * @since 4.3
 */
public abstract class NoSuchEntityException extends ServiceException {
    protected NoSuchEntityException(@Nullable String message) {
        super(message);
    }

    protected NoSuchEntityException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}

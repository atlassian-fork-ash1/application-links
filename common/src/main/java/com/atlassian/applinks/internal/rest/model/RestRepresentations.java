package com.atlassian.applinks.internal.rest.model;

import com.google.common.base.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Utilities to instantiate and process {@code RestRepresentation} implementations. This class assumes that the
 * contracts outlined in the documentation of {@link RestRepresentation}, including the mandatated constructors, are
 * followed.
 *
 * @since 4.3
 */
public final class RestRepresentations {

    private RestRepresentations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * Instantiate {@link RestRepresentation} from a {@code Map}.
     *
     * @param original   the original map entity produced by the deserializer
     * @param entityType type of the REST entity
     * @param <R>        type parameter of the REST entity
     * @return new instance of {@code E} with data copied from {@code original}
     * @see RestRepresentation
     */
    @Nonnull
    public static <R extends RestRepresentation<?>> R fromMap(@Nonnull Map<String, Object> original,
                                                              @Nonnull Class<R> entityType) {
        requireNonNull(original, "original");
        requireNonNull(entityType, "entityType");

        try {
            return entityType.getConstructor(Map.class).newInstance(original);
        } catch (Exception e) {
            throw new RuntimeException("Expected Map constructor in RestRepresentation " + entityType.getName(), e);
        }
    }

    /**
     * Instantiate {@link ReadOnlyRestRepresentation} from the corresponding domain object, allowing for {@code null}
     * values.
     *
     * @param original               the original map entity produced by the deserializer
     * @param restRepresentationType type of the REST entity
     * @param <T>                    type parameter of the domain object
     * @param <R>                    type parameter of the REST entity
     * @return new instance of {@code E} with data copied from {@code original}
     * @throws IllegalStateException if {@code R} does not provide the required constructor or if instantation fails for
     *                               any other reason
     * @see ReadOnlyRestRepresentation
     */
    @Nullable
    public static <T, R extends ReadOnlyRestRepresentation<T>> R fromDomainObject(@Nullable T original,
                                                                                  @Nonnull Class<R> restRepresentationType) {
        requireNonNull(restRepresentationType, "restRepresentationType");
        if (original == null) {
            return null;
        }

        try {
            return findConstructor(original.getClass(), restRepresentationType).newInstance(original);
        } catch (Exception e) {
            throw new IllegalStateException(format("Failed to instantiate REST representation '%s' from '%s'",
                    restRepresentationType.getName(), original.getClass().getName()), e);
        }
    }

    @Nonnull
    public static <T, R extends ReadOnlyRestRepresentation<T>> Function<T, R> fromDomainFunction(
            @Nonnull final Class<R> restRepresentationType) {
        requireNonNull(restRepresentationType, "restRepresentationType");

        return new Function<T, R>() {
            @Override
            public R apply(@Nullable T input) {
                return fromDomainObject(input, restRepresentationType);
            }
        };
    }

    @SuppressWarnings("unchecked")
    private static <T, R extends ReadOnlyRestRepresentation<T>> Constructor<R> findConstructor(Class<?> domainClass,
                                                                                               Class<R> representationClass) {
        for (Constructor<?> constructor : representationClass.getConstructors()) {
            if (constructor.getParameterTypes().length == 1
                    && constructor.getParameterTypes()[0].isAssignableFrom(domainClass)) {
                return (Constructor<R>) constructor;
            }
        }
        throw new IllegalStateException(format("REST representation class '%s' does not provide copy constructor " +
                        "accepting domain class '%s', as required by the contract in ReadOnlyRestRepresentation",
                representationClass.getName(), domainClass.getName()));
    }
}

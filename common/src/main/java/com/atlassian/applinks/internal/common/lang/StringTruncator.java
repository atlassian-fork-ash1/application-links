package com.atlassian.applinks.internal.common.lang;

import org.apache.commons.lang3.Validate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Truncates input data into strings of requested size. Whenever a line or the entire string is concatenated,
 * the characters in the truncated line, or the last line will be replaced with a predefined "ellipsis".
 *
 * @since 5.0
 */
public class StringTruncator {
    private static final int DEFAULT_MAX = 1000;

    private static final String LINE_ELLIPSIS = " (...)";
    private static final String FULL_ELLIPSIS_FORMAT = "(%d more lines ...)";

    private final Reader reader;

    private final int maxLines;
    private final int maxCharsInLine;

    private StringTruncator(Reader source, int maxLines, int maxCharsInLine) {
        requireNonNull(source, "source");

        Validate.isTrue(maxLines > 0, "maxLines must be greater than 0, was: %d", maxLines);
        Validate.isTrue(maxCharsInLine > 0, "maxCharsInLine must be greater than 0, was: %d", maxCharsInLine);

        this.reader = source;
        this.maxLines = maxLines;
        this.maxCharsInLine = maxCharsInLine;
    }

    @Nonnull
    public static StringTruncator forInput(@Nonnull Reader source) {
        return new StringTruncator(source, DEFAULT_MAX, DEFAULT_MAX);
    }

    public static StringTruncator forInput(@Nonnull String source) {
        return forInput(new StringReader(requireNonNull(source, "source")));
    }

    @Nonnull
    public StringTruncator maxLines(int maxLines) {
        return new StringTruncator(reader, maxLines, maxCharsInLine);
    }

    @Nonnull
    public StringTruncator maxCharsInLine(int maxCharsInLine) {
        return new StringTruncator(reader, maxLines, maxCharsInLine);
    }

    /**
     * Reads and truncates the source. The source will be read in its entirety (but not closed), so if the underlying
     * stream is non-resettable, it won't be suitable for any further usage.
     *
     * @return the truncated source
     */
    @Nonnull
    public String truncate() {
        try {
            BufferedReader bufferedSource = createBufferedSource();
            StringBuilder builder = new StringBuilder();
            int lines = 0;
            boolean eosReached = false;

            while (lines < maxLines - 1) {
                String line = bufferedSource.readLine();
                if (line != null) {
                    if (lines > 0) {
                        builder.append(System.lineSeparator());
                    }
                    builder.append(truncateLine(line));
                } else {
                    // end of stream reached
                    eosReached = true;
                    break;
                }
                lines++;
            }


            if (!eosReached) {
                // read the rest of the source and add ellipsis
                int extraLines = 0;
                String line;
                String lastLine = null;
                while ((line = bufferedSource.readLine()) != null) {
                    lastLine = line;
                    extraLines++;
                }

                if (extraLines == 1) {
                    builder.append(System.lineSeparator()).append(lastLine);
                } else if (extraLines > 1) {
                    builder.append(System.lineSeparator()).append(String.format(FULL_ELLIPSIS_FORMAT, extraLines));
                }
            }

            return builder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private BufferedReader createBufferedSource() {
        return reader instanceof BufferedReader ? (BufferedReader) reader : new BufferedReader(reader);
    }

    private String truncateLine(String line) {
        if (line.length() <= maxCharsInLine) {
            return line;
        } else {
            return line.substring(0, maxCharsInLine - LINE_ELLIPSIS.length()) + LINE_ELLIPSIS;
        }
    }
}


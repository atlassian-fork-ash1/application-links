package com.atlassian.applinks.internal.common.exception;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 5.1
 */
public class SimpleDetailedErrors implements DetailedErrors {
    private final List<DetailedError> errors;

    public SimpleDetailedErrors(@Nonnull Iterable<DetailedError> errors) {
        this.errors = ImmutableList.copyOf(errors);
    }

    @Nonnull
    @Override
    public Iterable<DetailedError> getErrors() {
        return errors;
    }

    @Override
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public static final class Builder {

        private final List<DetailedError> errors = new ArrayList<>();

        @Nonnull
        public Builder error(@Nullable String context, @Nonnull String summary, @Nullable String details) {
            this.errors.add(new SimpleDetailedError(context, summary, details));
            return this;
        }

        @Nonnull
        public Builder error(@Nonnull String summary) {
            this.errors.add(new SimpleDetailedError(summary));
            return this;
        }

        @Nonnull
        public SimpleDetailedErrors build() {
            return new SimpleDetailedErrors(errors);
        }

        public boolean hasErrors() {
            return !errors.isEmpty();
        }
    }

}

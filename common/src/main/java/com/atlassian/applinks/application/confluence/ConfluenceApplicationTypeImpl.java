package com.atlassian.applinks.application.confluence;

import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ConfluenceApplicationTypeImpl extends HiResIconizedIdentifiableType
        implements ConfluenceApplicationType, NonAppLinksApplicationType, BuiltinApplinksType {
    static final TypeId TYPE_ID = new TypeId("confluence");

    public ConfluenceApplicationTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
        super(pluginUtil, webResourceUrlProvider);
    }

    @Nonnull
    public String getI18nKey() {
        return "applinks.confluence";
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }
}

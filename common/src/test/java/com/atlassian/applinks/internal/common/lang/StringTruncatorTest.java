package com.atlassian.applinks.internal.common.lang;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.Reader;

import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

public class StringTruncatorTest {
    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullReaderSource() {
        StringTruncator.forInput((Reader) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullStringSource() {
        StringTruncator.forInput((String) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeMaxLines() {
        StringTruncator.forInput("something").maxLines(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void zeroMaxLines() {
        StringTruncator.forInput("something").maxLines(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeMaxCharsInLine() {
        StringTruncator.forInput("something").maxCharsInLine(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void zeroMaxCharsInLine() {
        StringTruncator.forInput("something").maxCharsInLine(0);
    }

    @Test
    public void truncateGivenSingleLineWithinLimits() {
        String input = "some text";

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(20)
                .maxLines(10)
                .truncate();

        assertEquals(input, result);
    }

    @Test
    public void truncateGivenAllLinesWithinLimits() {
        String input = asString(
                "some text",
                "some more text",
                "even more text"
        );

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(20)
                .maxLines(10)
                .truncate();

        assertThat(toLines(result), Matchers.contains(
                "some text",
                "some more text",
                "even more text"
        ));
    }

    @Test
    public void truncateGivenAllLinesMatchingLimits() {
        String input = asString(
                "some text",
                "some more text",
                "even more text"
        );

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(14) // max chars per line in input
                .maxLines(3) // lines in input
                .truncate();

        assertThat(toLines(result), Matchers.contains(
                "some text",
                "some more text",
                "even more text"
        ));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void truncateGivenTooLongLines() {
        String input = asString(
                repeat("x", 300),
                repeat("y", 200),
                repeat("z", 100)
        );

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(100)
                .maxLines(3)
                .truncate();

        assertThat(toLines(result), contains(
                containsInOrder(repeat("x", 94), " (...)"),
                containsInOrder(repeat("y", 94), " (...)"),
                equalTo(repeat("z", 100))
        ));
    }

    @Test
    public void truncateGivenTooManyLines() {
        String input = asString(
                "line 1",
                "line 2",
                "line 3",
                "line 4",
                "line 5"
        );

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(6) // max chars per line in input
                .maxLines(3)
                .truncate();

        assertThat(toLines(result), contains(
                "line 1",
                "line 2",
                "(3 more lines ...)"
        ));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void truncateGivenLastPreservedLineTooLong() {
        String input = asString(
                "line 1",
                "line 2",
                repeat("x", 101),
                "line 4",
                "line 5"
        );

        String result = StringTruncator.forInput(input)
                .maxCharsInLine(100)
                .maxLines(4)
                .truncate();

        assertThat(toLines(result), contains(
                equalTo("line 1"),
                equalTo("line 2"),
                containsInOrder(repeat("x", 94), " (...)"),
                equalTo("(2 more lines ...)")
        ));
    }

    private static String asString(String... lines) {
        return Joiner.on(System.lineSeparator()).join(lines);
    }

    private static Iterable<String> toLines(String details) {
        return Splitter.on(System.lineSeparator()).split(details);
    }
}

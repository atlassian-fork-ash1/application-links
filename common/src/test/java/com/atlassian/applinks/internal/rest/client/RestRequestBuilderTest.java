package com.atlassian.applinks.internal.rest.client;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.sal.api.net.Request.MethodType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import static com.atlassian.applinks.test.mock.MockRequestAnswer.withMockRequests;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RestRequestBuilderTest {
    @MockApplink(rpcUrl = "http://test.com")
    private ApplicationLink link;
    @Mock
    private ApplicationLinkRequestFactory applicationLinkRequestFactory;

    @Captor
    private ArgumentCaptor<Class<? extends AuthenticationProvider>> authenticationCaptor;

    @Rule
    public final MockApplinksRule mockApplinks = new MockApplinksRule(this);

    @Before
    public void setUpRequestCreation() throws Exception {
        when(link.createAuthenticatedRequestFactory(authenticationCaptor.capture()))
                .thenReturn(applicationLinkRequestFactory);
        when(applicationLinkRequestFactory.createRequest(any(MethodType.class), any()))
                .thenAnswer(withMockRequests());
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void requiresNonNullLink() {
        new RestRequestBuilder(null);
    }

    @Test
    public void defaultAnonymousRestRequest() {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link).buildAnonymous());

        assertEquals(Anonymous.class, authenticationCaptor.getValue());
        assertEquals(MethodType.GET, request.getMethodType());
        assertEquals("/rest/applinks/latest", request.getUrl());
        assertThat(request.getHeader(HttpHeaders.ACCEPT), contains(MediaType.APPLICATION_JSON));
    }

    @Test
    public void defaultAndAnonymousShouldBeEqual() throws CredentialsRequiredException {
        MockApplicationLinkRequest defaultRequest = getMockRequest(new RestRequestBuilder(link).build());
        MockApplicationLinkRequest anonymousRequest = getMockRequest(new RestRequestBuilder(link).buildAnonymous());

        assertEquals(defaultRequest.getMethodType(), anonymousRequest.getMethodType());
        assertEquals(defaultRequest.getUrl(), anonymousRequest.getUrl());
        assertEquals(defaultRequest.getRequestContentType(), anonymousRequest.getRequestContentType());
        assertEquals(defaultRequest.getHeader(HttpHeaders.ACCEPT), anonymousRequest.getHeader(HttpHeaders.ACCEPT));
    }

    @Test
    public void customAuthentication() throws CredentialsRequiredException {
        new RestRequestBuilder(link)
                .authentication(OAuthAuthenticationProvider.class)
                .build();

        assertEquals(OAuthAuthenticationProvider.class, authenticationCaptor.getValue());
    }

    @Test
    public void customUrl() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .url(new RestUrlBuilder().addPath("custom"))
                .build());

        assertEquals("/rest/applinks/latest/custom", request.getUrl());
    }

    @Test
    public void customMethodType() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .methodType(MethodType.PUT)
                .build());

        assertEquals(MethodType.PUT, request.getMethodType());
    }

    @Test
    public void customAcceptHeader() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .accept(MediaType.APPLICATION_XML)
                .build());

        assertThat(request.getHeader(HttpHeaders.ACCEPT), contains(MediaType.APPLICATION_XML));
    }

    @Test
    public void customHeader() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .header("test-key", "test-value")
                .build());

        assertThat(request.getHeader("test-key"), contains("test-value"));
    }

    @Test
    public void defaultTimeouts() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link).build());

        assertEquals(request.getConnectionTimeout(), 15000); // in millis
        assertEquals(request.getSoTimeout(), 15000); // in millis
    }

    @Test
    public void customConnectionTimeout() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .connectionTimeout(5, TimeUnit.MINUTES)
                .build());

        assertEquals(request.getConnectionTimeout(), 300000); // in millis
    }

    @Test(expected = IllegalArgumentException.class)
    public void customConnectionTimeoutNegative() throws CredentialsRequiredException {
        new RestRequestBuilder(link).connectionTimeout(-1, TimeUnit.MINUTES);
    }

    @Test
    public void customConnectionTimeoutTooLarge() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .connectionTimeout(Integer.MAX_VALUE, TimeUnit.SECONDS) // MAX_VALUE * 1000 millis
                .build());

        assertEquals(request.getConnectionTimeout(), Integer.MAX_VALUE);
    }

    @Test
    public void customSocketTimeout() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .socketTimeout(5, TimeUnit.MINUTES)
                .build());

        assertEquals(request.getSoTimeout(), 300000); // in millis
    }

    @Test(expected = IllegalArgumentException.class)
    public void customSocketTimeoutNegative() throws CredentialsRequiredException {
        new RestRequestBuilder(link).socketTimeout(-1, TimeUnit.MINUTES);
    }

    @Test
    public void customSocketTimeoutTooLarge() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .socketTimeout(Integer.MAX_VALUE, TimeUnit.SECONDS) // MAX_VALUE * 1000 millis
                .build());

        assertEquals(request.getSoTimeout(), Integer.MAX_VALUE);
    }

    @Test
    public void defaultFollowRedirects() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link).build());

        assertTrue("Default follow redirects should be true", request.isFollowRedirects());
    }

    @Test
    public void customFollowRedirects() throws CredentialsRequiredException {
        MockApplicationLinkRequest request = getMockRequest(new RestRequestBuilder(link)
                .followRedirects(false)
                .build());

        assertFalse("followRedirects should be set to false", request.isFollowRedirects());
    }

    private static MockApplicationLinkRequest getMockRequest(AuthorisationUriAwareRequest original) {
        return (MockApplicationLinkRequest) DefaultAuthorisationUriAwareRequest.class.cast(original)
                .getDelegateRequest();
    }
}

package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Marker base class for all mock service exceptions used in tests.
 *
 * @since 4.3
 */
public class MockServiceException extends ServiceException {
    protected MockServiceException(@Nullable String message) {
        super(message);
    }

    protected MockServiceException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}

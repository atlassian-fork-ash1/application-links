package com.atlassian.applinks.internal.common.web.soy;

import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * @since 5.0
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class HelpPathFunctionTest {
    @Mock
    private DocumentationLinker linker;

    @InjectMocks
    private HelpPathFunction helpPathFunction;

    private static URI DOCUMENTATION_BASE_URL = URI.create("http://example.com/wiki/");

    private static URI HELP_PAGE = URI.create(DOCUMENTATION_BASE_URL.toString() + "My+Docs");
    private static URI HELP_PAGE_WITH_SECTION = URI.create(DOCUMENTATION_BASE_URL.toString() + "My+Docs#sectionone");

    private static String HELP_LINK_KEY = "'com.atlassian.test'"; //quotes denote that this is a string literal
    private static String HELP_LINK_KEY_VAR = "helpLinkKey";//No quotes denote this is a variable that points to a string

    @Before
    public void setUp() {
        when(linker.getLink(HELP_LINK_KEY)).thenReturn(HELP_PAGE);
        when(linker.getLink(eq(HELP_LINK_KEY), any())).thenReturn(HELP_PAGE_WITH_SECTION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateHelpPathJavascriptNoArguments() {
        helpPathFunction.generate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateHelpPathJavascriptTooManyArguments() {
        helpPathFunction.generate(new JsExpression(HELP_LINK_KEY),
                new JsExpression("'sectionone'"),
                new JsExpression("'onemorearg'"));
    }

    @Test
    public void generateHelpPathJavascriptWithStringLiteralArgument() {
        String result = helpPathFunction.generate(new JsExpression(HELP_LINK_KEY)).getText();

        assertEquals("require('applinks/common/help-paths').getFullPath('com.atlassian.test',undefined)", result);
    }

    @Test
    public void generateHelpPathJavascriptWithReferenceArgument() {
        String result = helpPathFunction.generate(new JsExpression(HELP_LINK_KEY_VAR)).getText();

        assertEquals("require('applinks/common/help-paths').getFullPath(helpLinkKey,undefined)", result);
    }

    @Test
    public void generateHelpPathJavascriptWithSectionKey() {
        String result = helpPathFunction.generate(new JsExpression(HELP_LINK_KEY), new JsExpression("'sectionone'"))
                .getText();

        assertEquals("require('applinks/common/help-paths').getFullPath('com.atlassian.test','sectionone')", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateHelpPathServerSideNoArgs() {
        helpPathFunction.apply();
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateHelpPathServerSideTooManyArg() {
        helpPathFunction.apply(HELP_LINK_KEY, "sectionone", "onemorearg");
    }

    @Test
    public void generateHelpPathServerSide() {
        assertEquals(HELP_PAGE.toString(), helpPathFunction.apply(HELP_LINK_KEY));
    }

    @Test
    public void generateHelpPathWithSectionKeyServerSide() {
        assertEquals(HELP_PAGE_WITH_SECTION.toString(), helpPathFunction.apply(HELP_LINK_KEY, "sectionone"));
    }
}

package com.atlassian.applinks.internal.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.URI;
import java.util.UUID;

import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class RestUrlBuilderTest {
    @Test
    public void toStringEquivalentToUriToString() {
        assertEquals(new RestUrlBuilder().build().toString(), new RestUrlBuilder().toString());
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullBaseUrlInConstructor() {
        new RestUrlBuilder((URI) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullBaseUrlStringInConstructor() {
        new RestUrlBuilder((String) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullBaseUrlProperty() {
        new RestUrlBuilder().baseUrl((URI) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullBaseUrlStringProperty() {
        new RestUrlBuilder().baseUrl((String) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonAbsoluteBaseUrlInConstructor() {
        new RestUrlBuilder(URI.create("/not/absolute"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonAbsoluteBaseUrlStringInConstructor() {
        new RestUrlBuilder("/not/absolute");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonAbsoluteBaseUrlProperty() {
        new RestUrlBuilder(URI.create("/not/absolute"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonAbsoluteBaseUrlStringProperty() {
        new RestUrlBuilder(URI.create("/not/absolute"));
    }

    @Test
    public void defaultRestPath() {
        assertEquals("/rest/applinks/latest", new RestUrlBuilder().toString());
    }

    @Test
    public void defaultRestPathWithBaseUrl() {
        assertEquals("http://applinks.com/rest/applinks/latest", new RestUrlBuilder("http://applinks.com").toString());
    }

    @Test
    public void customRestModuleWithBaseUrl() {
        assertEquals("http://applinks.com/rest/applinks-tests/1.0", new RestUrlBuilder("http://applinks.com")
                .module("applinks-tests")
                .version(RestVersion.V1)
                .toString());
    }

    @Test
    public void customRestPathWithBaseUrl() {
        assertEquals("http://applinks.com/rest/applinks/latest/applinks/noendpoints",
                new RestUrlBuilder("http://applinks.com")
                        .addPath("applinks")
                        .addPath("noendpoints")
                        .toString());
    }

    @Test
    public void customRestPathUsingRestUrl() {
        assertEquals("http://applinks.com/rest/applinks/latest/applinks/more/evenmore/andsomemore",
                new RestUrlBuilder("http://applinks.com")
                        .addPath(RestUrl.forPath("applinks").add("more"))
                        .addPath(RestUrl.forPath("evenmore").add("andsomemore"))
                        .toString());
    }

    @Test
    public void customRestPathWithApplinkId() {
        ApplicationId id = new ApplicationId(UUID.randomUUID().toString());

        assertThat(new RestUrlBuilder("http://applinks.com")
                        .addPath("applinks")
                        .addApplicationId(id)
                        .addPath("status")
                        .toString(),
                stringContainsInOrder(ImmutableList.of(
                        "http://applinks.com/rest/applinks/latest/applinks/",
                        id.toString(),
                        "/status")));
    }

    @Test
    public void customRestPathWithApplink() {
        ApplicationId id = new ApplicationId(UUID.randomUUID().toString());
        ApplicationLink link = Mockito.mock(ApplicationLink.class);
        when(link.getId()).thenReturn(id);

        assertThat(new RestUrlBuilder("http://applinks.com")
                        .addPath("applinks")
                        .addApplink(link)
                        .addPath("status")
                        .toString(),
                stringContainsInOrder(ImmutableList.of(
                        "http://applinks.com/rest/applinks/latest/applinks/",
                        id.toString(),
                        "/status")));
    }

    @Test
    public void customRestPathWithQueryParams() {
        ApplicationId id = new ApplicationId(UUID.randomUUID().toString());

        assertThat(new RestUrlBuilder("http://applinks.com")
                        .addPath("applinks")
                        .addApplicationId(id)
                        .addPath("status")
                        .queryParam("test", "test-value11")
                        .queryParam("test", "test-value12")
                        .queryParam("test2", "test-value21")
                        .toString(),
                stringContainsInOrder(ImmutableList.of(
                        "http://applinks.com/rest/applinks/latest/applinks/",
                        id.toString(),
                        "/status",
                        "?",
                        "test=test-value11&test=test-value12&test2=test-value21")));
    }
}

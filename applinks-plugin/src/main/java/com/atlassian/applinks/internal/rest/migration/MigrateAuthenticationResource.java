package com.atlassian.applinks.internal.rest.migration;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser;
import com.atlassian.applinks.internal.migration.AuthenticationMigrationService;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.applinks.internal.rest.model.migration.RestAuthenticationStatus;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(MigrateAuthenticationResource.CONTEXT)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@AnonymousAllowed
@WebSudoRequired
@InterceptorChain({
        ContextInterceptor.class,
        ServiceExceptionInterceptor.class,
        NoCacheHeaderInterceptor.class})
public class MigrateAuthenticationResource {
    public static final String CONTEXT = "migration";
    private final AuthenticationMigrationService migrationService;
    private final RemoteCapabilitiesService remoteCapabilitiesService;
    private final RestApplicationIdParser applicationIdParser;

    public MigrateAuthenticationResource(AuthenticationMigrationService authenticationMigrationService,
                                         RemoteCapabilitiesService remoteCapabilitiesService,
                                         RestApplicationIdParser applicationIdParser) {
        this.migrationService = authenticationMigrationService;
        this.applicationIdParser = applicationIdParser;
        this.remoteCapabilitiesService = remoteCapabilitiesService;
    }

    @POST
    @Path("{id}")
    public Response migrate(@PathParam("id") final String id) throws ServiceException {
        final ApplicationId applicationId = applicationIdParser.parse(id);
        final AuthenticationStatus configs = migrationService.migrateToOAuth(applicationId);
        final RemoteApplicationCapabilities remoteCapabilities = remoteCapabilitiesService.getCapabilities(applicationId);
        return RestUtil.ok(new RestAuthenticationStatus(configs, remoteCapabilities));
    }
}
package com.atlassian.applinks.internal.web.data;

import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.JsonApplinksFeatures;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

/**
 * Injects information about Applinks context client-side.
 * For more information how context providers work check the linked documentation.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 4.3
 */
public class ApplinksFeaturesDataProvider implements WebResourceDataProvider {
    private final JsonApplinksFeatures jsonApplinksFeatures;

    public ApplinksFeaturesDataProvider(ApplinksFeatureService featureService) {
        this.jsonApplinksFeatures = new JsonApplinksFeatures(featureService);
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(jsonApplinksFeatures.allFeatures());
    }
}

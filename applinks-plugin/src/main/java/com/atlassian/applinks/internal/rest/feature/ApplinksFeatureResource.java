package com.atlassian.applinks.internal.rest.feature;


import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.common.rest.util.RestEnumParser;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.internal.feature.JsonApplinksFeatures;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.message.I18nResolver;

import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.rest.model.BaseRestEntity.createSingleFieldEntity;
import static com.google.common.base.Strings.nullToEmpty;

@Path(ApplinksFeatureResource.CONTEXT)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AnonymousAllowed
@InterceptorChain({ServiceExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplinksFeatureResource {
    public static final String CONTEXT = "features";
    public static final RestUrl FEATURES_PATH = RestUrl.forPath(CONTEXT);

    private static final String PARAM_FEATURE = "feature";
    private static final String PARAM_TEMPLATE_FEATURE = "{feature}";

    @Nonnull
    public static RestUrlBuilder featuresUrl() {
        return new RestUrlBuilder().addPath(FEATURES_PATH);
    }

    @Nonnull
    public static RestUrlBuilder featureUrl(@Nonnull ApplinksFeatures feature) {
        return featuresUrl().addPath(feature.name());
    }

    private final ApplinksFeatureService featureService;

    private final JsonApplinksFeatures jsonApplinksFeatures;
    private final RestEnumParser<ApplinksFeatures> featureParser;

    public ApplinksFeatureResource(I18nResolver i18nResolver,
                                   ApplinksFeatureService featureService) {
        this.featureService = featureService;

        this.jsonApplinksFeatures = new JsonApplinksFeatures(featureService);
        this.featureParser = new RestEnumParser<>(ApplinksFeatures.class, i18nResolver,
                "applinks.rest.feature.error.unsupported");
    }

    @GET
    @Path(PARAM_TEMPLATE_FEATURE)
    public Response isEnabled(@PathParam(PARAM_FEATURE) String featureName) {
        ApplinksFeatures feature = parseFeature(nullToEmpty(featureName));

        return RestUtil.ok(jsonApplinksFeatures.isEnabled(feature));
    }

    @PUT
    @Path(PARAM_TEMPLATE_FEATURE)
    public Response enable(@PathParam(PARAM_FEATURE) String featureName) throws ServiceException {
        ApplinksFeatures feature = parseFeature(nullToEmpty(featureName));
        featureService.enable(feature);

        return RestUtil.ok(createSingleFieldEntity(featureName, true));
    }

    @DELETE
    @Path(PARAM_TEMPLATE_FEATURE)
    public Response disable(@PathParam(PARAM_FEATURE) String featureName) throws ServiceException {
        ApplinksFeatures feature = parseFeature(nullToEmpty(featureName));
        featureService.disable(feature);

        return RestUtil.noContent();
    }

    private ApplinksFeatures parseFeature(String featureName) {
        return featureParser.parseEnumParameter(featureName, PARAM_FEATURE);
    }
}

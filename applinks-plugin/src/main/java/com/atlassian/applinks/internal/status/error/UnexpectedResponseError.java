package com.atlassian.applinks.internal.status.error;

import com.atlassian.sal.api.net.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * @see ApplinkErrorType#UNEXPECTED_RESPONSE
 * @since 5.0
 */
public class UnexpectedResponseError extends AbstractResponseApplinkError {
    public UnexpectedResponseError(@Nonnull Response response) {
        super(response);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.UNEXPECTED_RESPONSE;
    }

    @Nullable
    @Override
    public String getDetails() {
        return null;
    }
}

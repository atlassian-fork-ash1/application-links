package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.exception.DefaultServiceExceptionFactory;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.exception.SystemFeatureException;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.sal.api.features.DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.BooleanUtils.toBooleanObject;

public class DefaultApplinksFeatureService implements ApplinksFeatureService {
    private final DarkFeatureManager darkFeatureManager;
    private final PermissionValidationService permissionValidationService;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final UserManager userManager;

    @Autowired
    public DefaultApplinksFeatureService(DarkFeatureManager darkFeatureManager,
                                         DefaultServiceExceptionFactory serviceExceptionFactory,
                                         PermissionValidationService permissionValidationService,
                                         UserManager userManager) {
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.permissionValidationService = permissionValidationService;
        this.darkFeatureManager = darkFeatureManager;
        this.userManager = userManager;
    }

    @Override
    public boolean isEnabled(@Nonnull ApplinksFeatures feature) {
        requireNonNull(feature, "feature");

        if (userManager.getRemoteUserKey() == null) {
            return false;
        }
        if (feature.isSystem()) {
            // system features - read from system property, otherwise use default value
            // NOTE: can't use MoreObjects until Confluence upgrades their Guava version
            Boolean b = toBooleanObject(System.getProperty(ATLASSIAN_DARKFEATURE_PREFIX + feature.featureKey));
            return b == null ? feature.getDefaultValue() : b;
        } else {
            // standard features - use DarkFeatureManager
            return darkFeatureManager.isFeatureEnabledForCurrentUser(feature.featureKey);
        }
    }

    @Override
    public void enable(@Nonnull ApplinksFeatures feature, @Nonnull ApplinksFeatures... moreFeatures)
            throws NoAccessException, SystemFeatureException {
        requireNonNull(feature, "feature");
        requireNonNull(moreFeatures, "moreFeatures");
        validateNoSystemFeatures(feature, moreFeatures);
        permissionValidationService.validateSysadmin();

        enableFeature(feature);
        for (ApplinksFeatures more : moreFeatures) {
            enableFeature(more);
        }
    }

    @Override
    public void disable(@Nonnull ApplinksFeatures feature, @Nonnull ApplinksFeatures... moreFeatures)
            throws NoAccessException, SystemFeatureException {
        requireNonNull(feature, "feature");
        requireNonNull(moreFeatures, "moreFeatures");
        validateNoSystemFeatures(feature, moreFeatures);
        permissionValidationService.validateSysadmin();

        disableFeature(feature);
        for (ApplinksFeatures more : moreFeatures) {
            disableFeature(more);
        }
    }

    private void enableFeature(@Nonnull ApplinksFeatures feature) {
        darkFeatureManager.enableFeatureForAllUsers(feature.featureKey);
    }

    private void disableFeature(@Nonnull ApplinksFeatures feature) {
        darkFeatureManager.disableFeatureForAllUsers(feature.featureKey);
    }

    private void validateNoSystemFeatures(ApplinksFeatures feature, ApplinksFeatures... moreFeatures)
            throws SystemFeatureException {
        Set<ApplinksFeatures> systemFeatures = EnumSet.noneOf(ApplinksFeatures.class);
        if (feature.isSystem()) {
            systemFeatures.add(feature);
        }
        for (ApplinksFeatures more : moreFeatures) {
            if (more.isSystem()) {
                systemFeatures.add(more);
            }
        }
        if (!systemFeatures.isEmpty()) {
            throw serviceExceptionFactory.create(SystemFeatureException.class, systemFeatures.toString());
        }
    }
}

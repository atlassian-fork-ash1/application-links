package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseStatusException;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;

import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseOfType;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

class OAuthConfigMigrator extends TryWithAuthentication {
    private final OAuthAutoConfigurator configurator;
    private final OAuthConfig incoming;
    private final OAuthConfig outgoing;

    public OAuthConfigMigrator(@Nonnull final OAuthAutoConfigurator configurator,
                               @Nonnull final OAuthConfig incoming,
                               @Nonnull final OAuthConfig outgoing) {
        this.configurator = requireNonNull(configurator, "configurator");
        this.incoming = requireNonNull(incoming, "incoming");
        this.outgoing = requireNonNull(outgoing, "outgoing");
    }

    @Override
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public boolean execute(@Nonnull final ApplicationLink applicationLink,
                           @Nonnull final ApplicationId applicationId,
                           @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, CredentialsRequiredException, ResponseException, AuthenticationConfigurationException {
        try {
            configurator.enable(incoming, outgoing, applicationLink, factory);
            return true;
        } catch (AuthenticationConfigurationException ex) {
            ResponseStatusException cause = findCauseOfType(ex, ResponseStatusException.class);
            if (cause != null) {
                if (equalsStatus(cause.getResponse(), FORBIDDEN) || equalsStatus(cause.getResponse(), UNAUTHORIZED)) {
                    return false;
                }
            }
            throw ex;

        }
    }

    private static boolean equalsStatus(final Response response, final Status status) {
        return response.getStatusCode() == status.getStatusCode();
    }

    protected static class Factory {
        OAuthConfigMigrator getInstance(final OAuthAutoConfigurator authAutoConfigurator, final OAuthConfig incoming, final OAuthConfig outgoing) {
            return new OAuthConfigMigrator(authAutoConfigurator, incoming, outgoing);
        }
    }
}

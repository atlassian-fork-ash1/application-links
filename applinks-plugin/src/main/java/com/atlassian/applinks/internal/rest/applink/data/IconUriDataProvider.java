package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.internal.application.IconUriResolver;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.spi.application.IconizedType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides icon URL obtained from application type of the applink, under the data key {@code iconUri}. NOTE: this
 * component takes advantage of {@link IconizedType#getIconUri()} to provide hi-res icons where possible.
 *
 * @see IconUriResolver#resolveIconUri(ApplicationType)
 * @since 5.2
 */
public class IconUriDataProvider extends AbstractSingleKeyRestApplinkDataProvider {
    public static final String ICON_URI = "iconUri";

    public IconUriDataProvider() {
        super(ICON_URI);
    }

    @Nullable
    @Override
    protected Object doProvide(@Nonnull ApplicationLink applink) throws ServiceException {
        return IconUriResolver.resolveIconUri(applink.getType());
    }
}

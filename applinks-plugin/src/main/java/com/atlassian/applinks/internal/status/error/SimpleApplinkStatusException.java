package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * A generic {@link ApplinkStatusException} that can carry any {@link ApplinkErrorType}.
 *
 * @since 5.0
 */
public class SimpleApplinkStatusException extends ApplinkStatusException {
    private final ApplinkErrorType errorType;

    public SimpleApplinkStatusException(@Nonnull ApplinkErrorType errorType, @Nullable String details,
                                        @Nullable Throwable cause) {
        super(details, cause);
        this.errorType = requireNonNull(errorType, "errorType");
    }

    public SimpleApplinkStatusException(@Nonnull ApplinkErrorType errorType, @Nullable String details) {
        super(details);
        this.errorType = requireNonNull(errorType, "errorType");
    }

    public SimpleApplinkStatusException(@Nonnull ApplinkErrorType errorType) {
        this(errorType, null);
    }

    public SimpleApplinkStatusException(@Nonnull ApplinkError applinkError) {
        this(applinkError, null);
    }

    public SimpleApplinkStatusException(@Nonnull ApplinkError applinkError, @Nullable Throwable cause) {
        this(requireNonNull(applinkError, "applinkError").getType(), applinkError.getDetails(), cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return errorType;
    }

    @Nullable
    @Override
    public String getDetails() {
        return getMessage();
    }
}

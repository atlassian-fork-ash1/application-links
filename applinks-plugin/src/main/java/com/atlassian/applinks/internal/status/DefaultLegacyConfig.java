package com.atlassian.applinks.internal.status;

public final class DefaultLegacyConfig implements LegacyConfig {
    private final boolean trustedConfigured;
    private final boolean basicConfigured;

    public DefaultLegacyConfig() {
        this.trustedConfigured = false;
        this.basicConfigured = false;
    }

    private DefaultLegacyConfig(boolean trusted, boolean basic) {
        this.trustedConfigured = trusted;
        this.basicConfigured = basic;
    }

    public DefaultLegacyConfig trusted(boolean trusted) {
        return new DefaultLegacyConfig(trusted, basicConfigured);
    }

    public DefaultLegacyConfig basic(boolean basic) {
        return new DefaultLegacyConfig(trustedConfigured, basic);
    }

    @Override
    public boolean isTrustedConfigured() {
        return trustedConfigured;
    }

    @Override
    public boolean isBasicConfigured() {
        return basicConfigured;
    }
}

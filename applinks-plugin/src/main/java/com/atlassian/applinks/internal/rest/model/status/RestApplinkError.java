package com.atlassian.applinks.internal.rest.model.status;

import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorVisitor;
import com.atlassian.applinks.internal.status.error.AuthorisationUriAwareApplinkError;
import com.atlassian.applinks.internal.status.error.ResponseApplinkError;

import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * JSON representation of {@code ApplinkErrorDetails}.
 *
 * @since 4.3
 */
public class RestApplinkError extends ApplinksRestRepresentation {
    public static final String CATEGORY = "category";
    public static final String DETAILS = "details";
    public static final String TYPE = "type";

    private String category;
    private String details;
    private String type;

    public RestApplinkError(@Nonnull ApplinkError errorDetails) {
        requireNonNull(errorDetails, "errorDetails");
        category = errorDetails.getType().getCategory().name();
        type = errorDetails.getType().name();
        details = errorDetails.getDetails();
    }

    /**
     * {@link ApplinkErrorVisitor} that creates a corresponding REST error.
     */
    public static final class Visitor implements ApplinkErrorVisitor<RestApplinkError> {
        private final URI authorisationCallback;

        public Visitor() {
            this(null);
        }

        public Visitor(@Nullable URI authorisationCallback) {
            this.authorisationCallback = authorisationCallback;
        }

        @Nullable
        @Override
        public RestApplinkError visit(@Nonnull ApplinkError error) {
            return new RestApplinkError(error);
        }

        @Nullable
        @Override
        public RestApplinkError visit(@Nonnull AuthorisationUriAwareApplinkError error) {
            return new RestAuthorisationUriAwareApplinkError(error, authorisationCallback);
        }

        @Nullable
        @Override
        public RestApplinkError visit(@Nonnull ResponseApplinkError responseError) {
            // for now, we have no REST representation of this
            return new RestApplinkError(responseError);
        }
    }
}

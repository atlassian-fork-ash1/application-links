package com.atlassian.applinks.internal.permission;

import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static java.util.Objects.requireNonNull;

public class SalPermissionValidationService implements PermissionValidationService {
    private static final String ADMIN_I18N_KEY = "applinks.service.permission.admin";
    private static final String SYSADMIN_I18N_KEY = "applinks.service.permission.sysadmin";
    private static final I18nKey DEFAULT_OPERATION_KEY = newI18nKey("applinks.service.error.access.defaultoperation");

    private final I18nResolver i18nResolver;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final UserManager userManager;
    private final ElevatedPermissionsService elevatedPermissions;

    @Autowired
    public SalPermissionValidationService(I18nResolver i18nResolver,
                                          ServiceExceptionFactory serviceExceptionFactory,
                                          UserManager userManager,
                                          ElevatedPermissionsService elevatedPermissions) {
        this.i18nResolver = i18nResolver;
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.userManager = userManager;
        this.elevatedPermissions = elevatedPermissions;
    }

    @Override
    public void validateAuthenticated() throws NotAuthenticatedException {
        validateAuthenticated(DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateAuthenticated(@Nonnull I18nKey operationI18n) throws NotAuthenticatedException {
        if (!elevatedPermissions.isElevatedTo(PermissionLevel.USER)) {
            validateAuthenticated(userManager.getRemoteUserKey(), operationI18n);
        }
    }

    @Override
    public void validateAuthenticated(@Nullable UserKey user) throws NotAuthenticatedException {
        validateAuthenticated(user, DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateAuthenticated(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NotAuthenticatedException {
        requireNonNull(operationI18n, "operationI18n");
        if (user == null) {
            throw serviceExceptionFactory.create(NotAuthenticatedException.class, i18nResolver.getText(operationI18n));
        }
    }

    @Override
    public void validateAdmin() throws NoAccessException {
        validateAdmin(DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateAdmin(@Nonnull I18nKey operationI18n) throws NoAccessException {
        if (!elevatedPermissions.isElevatedTo(PermissionLevel.ADMIN)) {
            validateAdmin(userManager.getRemoteUserKey(), operationI18n);
        }
    }

    @Override
    public void validateAdmin(@Nullable UserKey user) throws NoAccessException {
        validateAdmin(user, DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateAdmin(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NoAccessException {
        validateAuthenticated(user, operationI18n);
        if (!userManager.isAdmin(user)) {
            throw serviceExceptionFactory.create(PermissionException.class, i18nResolver.getText(ADMIN_I18N_KEY),
                    i18nResolver.getText(operationI18n));
        }
    }

    @Override
    public void validateSysadmin() throws NoAccessException {
        validateSysadmin(DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateSysadmin(@Nonnull I18nKey operationI18n) throws NoAccessException {
        if (!elevatedPermissions.isElevatedTo(PermissionLevel.SYSADMIN)) {
            validateSysadmin(userManager.getRemoteUserKey(), operationI18n);
        }
    }

    @Override
    public void validateSysadmin(@Nullable UserKey user) throws NoAccessException {
        validateSysadmin(user, DEFAULT_OPERATION_KEY);
    }

    @Override
    public void validateSysadmin(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NoAccessException {
        validateAuthenticated(user, operationI18n);
        if (!userManager.isSystemAdmin(user)) {
            throw serviceExceptionFactory.create(PermissionException.class, i18nResolver.getText(SYSADMIN_I18N_KEY),
                    i18nResolver.getText(operationI18n));
        }
    }
}

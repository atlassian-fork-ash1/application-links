package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * This should not be used outside the context of migration to check for remote system admin access.
 * This is because it relies on Trusted Application module being installed remotely, which will not be the case in the future.
 */
class QuerySysAdminAccess extends TryWithAuthentication {
    public static final TryWithAuthentication INSTANCE = new QuerySysAdminAccess();
    public static final String SYS_ADMIN_ACCESS_PATH = "/plugins/servlet/applinks/auth/conf/trusted/inbound-ual/";
    private static final Logger LOGGER = LoggerFactory.getLogger(QuerySysAdminAccess.class);

    private QuerySysAdminAccess() {
    }

    @Override
    public boolean execute(@Nonnull final ApplicationLink applicationLink,
                           @Nonnull final ApplicationId localApplicationId,
                           @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, CredentialsRequiredException, ResponseException {
        final String url = SYS_ADMIN_ACCESS_PATH + localApplicationId.toString();
        final ApplicationLinkRequest request = factory.createRequest(Request.MethodType.HEAD, url);
        request.setHeader("X-Atlassian-Token", "no-check");
        RemoteActionHandler handler = new RemoteActionHandler();
        request.setConnectionTimeout((int) TimeUnit.SECONDS.toMillis(TryWithAuthentication.TIME_OUT_IN_SECONDS));
        request.setSoTimeout((int) TimeUnit.SECONDS.toMillis(TryWithAuthentication.TIME_OUT_IN_SECONDS));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(Request.MethodType.HEAD.name() + " " + applicationLink.getRpcUrl() + url);
        }
        request.execute(handler);
        return handler.isSuccessful();
    }
}

package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Raised if the system is unable to obtain remote authentication status because there is no outgoing authentication
 * configured. This only affects remote systems that do not support Status API (which allows anonymous access).
 *
 * @see ApplinkErrorType#NO_OUTGOING_AUTH
 * @since 4.3
 */
public class NoOutgoingAuthenticationException extends ApplinkStatusException implements ApplinkError {
    public NoOutgoingAuthenticationException(@Nullable String message) {
        super(message);
    }

    public NoOutgoingAuthenticationException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.NO_OUTGOING_AUTH;
    }

    @Nullable
    @Override
    public String getDetails() {
        return null;
    }
}

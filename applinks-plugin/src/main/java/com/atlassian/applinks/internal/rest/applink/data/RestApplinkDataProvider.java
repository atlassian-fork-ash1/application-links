package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.ServiceException;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides custom data to enrich REST applinks objects as requested.
 *
 * @since 5.0
 */
public interface RestApplinkDataProvider {
    /**
     * @return set of supported keys
     */
    @Nonnull
    Set<String> getSupportedKeys();

    /**
     * @param key     data key to provide
     * @param applink application link to enrich
     * @return REST data to enrich applink for given {@code key}
     * @throws ServiceException         if a service exception occurs while attempting to provide the data
     * @throws IllegalArgumentException if the {@code key} is not {@link #getSupportedKeys() supported}
     */
    @Nullable
    Object provide(@Nonnull String key, @Nonnull ApplicationLink applink)
            throws ServiceException, IllegalArgumentException;
}

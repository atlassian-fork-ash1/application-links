package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Raised if the remote application is incompatible.
 *
 * @since 4.3
 */
public class RemoteVersionIncompatibleException extends ApplinkStatusException {
    public RemoteVersionIncompatibleException() {
    }

    public RemoteVersionIncompatibleException(@Nullable String message) {
        super(message);
    }

    public RemoteVersionIncompatibleException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE;
    }

    @Nullable
    @Override
    public String getDetails() {
        return null;
    }
}

package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.internal.authentication.AuthorisationUriAware;
import com.atlassian.applinks.internal.status.error.ApplinkErrorCategory;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkErrorVisitor;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.AuthorisationUriAwareApplinkError;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Raised when remote authenticated access is required to access the Applink status, but no such access is available. Corresponds to any
 * error type of {@link ApplinkErrorCategory#ACCESS_ERROR access error category}.
 * <br>
 * All access errors also provide access to the {@link AuthorisationUriAware authorisation URI generator} that can be used to generate URIs
 * to obtain valid remote access.
 *
 * @see ApplinkErrorCategory#ACCESS_ERROR
 * @since 4.3
 */
public class ApplinkStatusAccessException extends ApplinkStatusException implements AuthorisationUriAwareApplinkError {
    private final AuthorisationURIGenerator uriGenerator;
    private final ApplinkErrorType errorType;

    public ApplinkStatusAccessException(@Nonnull ApplinkErrorType applinkErrorType,
                                        @Nonnull AuthorisationURIGenerator uriGenerator, @Nullable String message) {
        this(applinkErrorType, uriGenerator, message, null);
    }

    public ApplinkStatusAccessException(@Nonnull ApplinkErrorType applinkErrorType,
                                        @Nonnull AuthorisationURIGenerator uriGenerator, @Nullable String message,
                                        @Nullable Throwable cause) {
        super(message, cause);
        requireNonNull(applinkErrorType, "errorType");
        checkState(ApplinkErrorCategory.ACCESS_ERROR == applinkErrorType.getCategory(),
                "Only ACCESS_ERROR types allowed, was: " + applinkErrorType);
        this.errorType = applinkErrorType;
        this.uriGenerator = requireNonNull(uriGenerator, "uriGenerator");
    }

    public ApplinkStatusAccessException(@Nonnull AuthorisationUriAwareApplinkError error, @Nullable Throwable cause) {
        this(requireNonNull(error, "error").getType(), error.getAuthorisationUriGenerator(), error.getDetails(), cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return errorType;
    }

    @Nullable
    @Override
    public String getDetails() {
        return null;
    }

    @Nonnull
    public AuthorisationURIGenerator getAuthorisationUriGenerator() {
        return uriGenerator;
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor) {
        return visitor.visit(this);
    }
}

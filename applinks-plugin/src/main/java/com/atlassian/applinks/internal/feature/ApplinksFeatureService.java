package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.SystemFeatureException;

import javax.annotation.Nonnull;

/**
 * Controls feature activation within Application Links.
 *
 * @since 4.3
 */
public interface ApplinksFeatureService {
    /**
     * Check whether the {@code feature} is enabled for a specific user. This API has unrestricted access, however
     * anonymous calse will always results in {@code false}.
     *
     * @param feature feature to inspect
     * @return {@code true}, if the {@code feature} is enabled, {@code false} otherwise or if there is no user context
     */
    @Unrestricted("Anonymous access allowed for convenience of usage, however it will not produce any meaningful result")
    boolean isEnabled(@Nonnull ApplinksFeatures feature);

    /**
     * Enable the {@code feature}, and possibly {@code moreFeatures}.
     *
     * @param feature      feature to enable
     * @param moreFeatures more features to enable
     * @throws NoAccessException if there is no user context, or the user does not have system administrator permissions
     */
    @Restricted(PermissionLevel.SYSADMIN)
    void enable(@Nonnull ApplinksFeatures feature, @Nonnull ApplinksFeatures... moreFeatures)
            throws NoAccessException, SystemFeatureException;

    /**
     * Disable the {@code feature}, and possibly {@code moreFeatures}. Requires administrator permissions.
     *
     * @param feature      feature to disable
     * @param moreFeatures more features to disable
     * @throws NoAccessException if there is no user context, or the user does not have system administrator permissions
     */
    @Restricted(PermissionLevel.SYSADMIN)
    void disable(@Nonnull ApplinksFeatures feature, @Nonnull ApplinksFeatures... moreFeatures)
            throws NoAccessException, SystemFeatureException;
}

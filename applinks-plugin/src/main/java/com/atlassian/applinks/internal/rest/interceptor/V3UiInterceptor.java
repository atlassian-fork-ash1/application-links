package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.lang.reflect.InvocationTargetException;

import static java.util.Objects.requireNonNull;


/**
 * Interceptor for REST resources backing the new UI, turns those off when the new UI is disabled.
 *
 * @since 4.3
 */
public class V3UiInterceptor implements ResourceInterceptor {
    private static final Logger log = LoggerFactory.getLogger(V3UiInterceptor.class);

    private final ApplinksFeatureService applinksFeatureService;

    public V3UiInterceptor(@Nonnull ApplinksFeatureService applinksFeatureService) {
        this.applinksFeatureService = requireNonNull(applinksFeatureService, "applinksFeatureService");
    }

    @Override
    public void intercept(MethodInvocation invocation) throws IllegalAccessException, InvocationTargetException {
        if (applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI)) {
            invocation.invoke();
        } else {
            log.debug("Attempt to access resource while the New UI feature is disabled");
            invocation.getHttpContext().getResponse().setResponse(Response.status(Status.NOT_FOUND).build());
        }
    }
}

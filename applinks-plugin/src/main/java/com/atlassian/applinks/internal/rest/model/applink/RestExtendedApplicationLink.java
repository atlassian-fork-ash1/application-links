package com.atlassian.applinks.internal.rest.model.applink;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.rest.model.applink.RestApplicationLink;
import com.atlassian.applinks.internal.rest.applink.data.RestApplinkDataProvider;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Extended REST representation of {@link ApplicationLink} containing selected {@link ApplicationLink#getProperty(String) properties} and
 * custom data.
 *
 * @see RestApplinkDataProvider
 * @since 5.0
 */
public class RestExtendedApplicationLink extends RestApplicationLink {

    public static final String PROPERTIES = "properties";
    public static final String DATA = "data";

    private Map<String, Object> properties;
    private Map<String, Object> data;

    //for Jackson
    public RestExtendedApplicationLink(){
    }

    public RestExtendedApplicationLink(@Nonnull ApplicationLink link) {
        super(link);
    }

    public RestExtendedApplicationLink(@Nonnull ApplicationLink link, @Nonnull Set<String> propertyKeys,
                                       @Nonnull Map<String, Object> data) {
        super(link);
        this.properties = getProperties(link, propertyKeys);
        this.data = data;
    }


    private static Map<String, Object> getProperties(ApplicationLink link, Set<String> propertyKeys) {
        Map<String, Object> properties = new LinkedHashMap<>();
        for (String key : propertyKeys) {
            properties.put(key, link.getProperty(key));
        }
        return properties;
    }
}

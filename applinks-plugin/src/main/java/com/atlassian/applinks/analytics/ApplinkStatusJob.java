package com.atlassian.applinks.analytics;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.DAYS;

/**
 * This Job uses the DevSummaryPullService to pull dev summary updates from DataProviders which support guaranteed delivery
 *
 * At present it does this periodically even for providers where webhooks are functioning correctly
 */
public class ApplinkStatusJob implements JobRunner, LifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(ApplinkStatusJob.class);
    private static final long SCHEDULE_INTERVAL = DAYS.toMillis(1); // every day once
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(ApplinkStatusJob.class.getName());
    private static final JobId JOB_ID = JobId.of("applink-status-analytics-job");

    @Nonnull
    private final ApplinkStatusPublisher applinkStatusPublisher;
    @Nonnull
    private final SchedulerService scheduler;


    @Autowired
    public ApplinkStatusJob(
            @Nonnull final SchedulerService scheduler,
            @Nonnull final ApplinkStatusPublisher applinkStatusPublisher) {
        this.scheduler = requireNonNull(scheduler);
        this.applinkStatusPublisher = requireNonNull(applinkStatusPublisher);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(@Nonnull final JobRunnerRequest request) {
        try {

            applinkStatusPublisher.publishApplinkStatus();
            return JobRunnerResponse.success();
        } catch (final Exception e) {
            return JobRunnerResponse.failed(e);
        }
    }

    private Schedule getSchedule() {
        return Schedule.forInterval(SCHEDULE_INTERVAL, null);
    }

    @Override
    public void onStart() {
        scheduler.registerJobRunner(JOB_RUNNER_KEY, this);
        try {
            scheduler.scheduleJob(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                    .withSchedule(getSchedule()));
        } catch (final SchedulerServiceException e) {
            log.error("Unable to schedule analytics job", e);
        }
    }

    @Override
    public void onStop() {
        scheduler.unregisterJobRunner(JOB_RUNNER_KEY);
    }
}

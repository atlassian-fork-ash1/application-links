// in applinks-plugin for now so we don't have to move ApplinksFeatureService to the common module
package com.atlassian.applinks.application.bitbucket;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketProjectEntityType;
import com.atlassian.applinks.api.application.stash.StashProjectEntityType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.spi.application.NonAppLinksEntityType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.util.Assertions;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import java.net.URI;

import static java.lang.String.format;

public final class BitbucketProjectEntityTypeImpl extends HiResIconizedIdentifiableType
        implements BitbucketProjectEntityType, StashProjectEntityType, NonAppLinksEntityType, BuiltinApplinksType {
    private static final String ID_TEMPLATE = "%s.project";
    private static final String I18N_KEY_TEMPLATE = "applinks." + ID_TEMPLATE;
    private static final String I18N_PLURALIZED_KEY_TEMPLATE = I18N_KEY_TEMPLATE + ".plural";
    private static final String I18N__SHORT_KEY_TEMPLATE = I18N_KEY_TEMPLATE + ".short";

    private static final String TYPE_BITBUCKET = "bitbucket";
    private static final String TYPE_STASH = "stash";

    private final ApplinksFeatureService applinksFeatureService;

    // this needs to be kept "stash.project" for backwards compatibility - so that apps running applinks versions
    // pre-rebrand can still link to Bitbucket Server projects
    private static final TypeId TYPE_ID = new TypeId(formatId(TYPE_STASH));

    public BitbucketProjectEntityTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider,
                                          ApplinksFeatureService applinksFeatureService) {
        super(pluginUtil, webResourceUrlProvider);
        this.applinksFeatureService = applinksFeatureService;
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    @Nonnull
    public String getI18nKey() {

        return format(I18N_KEY_TEMPLATE, getType());
    }

    public String getPluralizedI18nKey() {
        return format(I18N_PLURALIZED_KEY_TEMPLATE, getType());
    }

    public String getShortenedI18nKey() {
        return format(I18N__SHORT_KEY_TEMPLATE, getType());
    }

    @Nonnull
    @Override
    protected String getIconKey() {
        return formatId(getType());
    }

    public Class<? extends ApplicationType> getApplicationType() {
        return BitbucketApplicationType.class;
    }

    public URI getDisplayUrl(final ApplicationLink link, final String project) {
        Assertions.isTrue(String.format("Application link %s is not of type %s",
                link.getId(), getApplicationType().getName()),
                link.getType() instanceof BitbucketApplicationType);

        return URIUtil.uncheckedConcatenate(link.getDisplayUrl(), "projects", project);
    }

    private static String formatId(String type) {
        return format(ID_TEMPLATE, type);
    }

    private String getType() {
        return isRebrandEnabled() ? TYPE_BITBUCKET : TYPE_STASH;
    }

    private boolean isRebrandEnabled() {
        return applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND);
    }
}

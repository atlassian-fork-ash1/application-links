// in applinks-plugin for now so we don't have to move ApplinksFeatureService to the common module
package com.atlassian.applinks.application.bitbucket;

import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.stash.StashApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public final class BitbucketApplicationTypeImpl extends HiResIconizedIdentifiableType
        implements BitbucketApplicationType, StashApplicationType, NonAppLinksApplicationType, BuiltinApplinksType {
    // this needs to be kept as "stash" for backwards compatibility - so that apps running applinks versions
    // pre-rebrand can still link to Bitbucket Servers
    static final TypeId TYPE_ID = new TypeId("stash");

    private final ApplinksFeatureService applinksFeatureService;

    public BitbucketApplicationTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider,
                                        ApplinksFeatureService applinksFeatureService) {
        super(pluginUtil, webResourceUrlProvider);
        this.applinksFeatureService = applinksFeatureService;
    }

    @Nonnull
    public String getI18nKey() {
        return isRebrandEnabled() ? "applinks.bitbucket" : "applinks.stash";
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    @Nonnull
    @Override
    protected String getIconKey() {
        return isRebrandEnabled() ? "bitbucket" : getId().get();
    }

    private boolean isRebrandEnabled() {
        return applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND);
    }
}

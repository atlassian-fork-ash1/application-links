package com.atlassian.applinks.core.property;

import com.atlassian.applinks.api.PropertySet;
import com.atlassian.sal.api.pluginsettings.PluginSettings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Default {@link PropertySet} implementation that delegates to SAL
 */
public class SalPropertySet implements PropertySet {

    private final PluginSettings pluginSettings;
    private final String keyPrefix;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock write = readWriteLock.writeLock();
    private final Lock read = readWriteLock.readLock();

    private static final Logger LOG = LoggerFactory.getLogger(SalPropertySet.class);

    public SalPropertySet(PluginSettings pluginSettings, String keyPrefix) {
        this.pluginSettings = pluginSettings;
        this.keyPrefix = keyPrefix;
    }

    public Object getProperty(String s) {
        try {
            read.lock();
            return pluginSettings.get(namespace(s));
        } finally {
            read.unlock();
        }

    }

    public Object putProperty(String s, Object o) {
        String namespace = namespace(s);

        if (LOG.isDebugEnabled()) {
            String message = String.format("Putting property [%s] as namespace [%s] with value [%s]",
                    s,
                    namespace,
                    o);
            LOG.debug(message);
        }
        try {
            write.lock();
            return pluginSettings.put(namespace(s), o);
        } finally {
            write.unlock();
        }

    }

    public Object removeProperty(String s) {
        String namespace = namespace(s);

        if (LOG.isDebugEnabled()) {
            String message = String.format("Removing property [%s] as namespace [%s] with value [%s]",
                    s,
                    namespace,
                    pluginSettings.get(namespace));
            LOG.debug(message);
        }
        try {
            write.lock();
            return pluginSettings.remove(namespace);
        } finally {
            write.unlock();
        }
    }

    private String namespace(String key) {
        return keyPrefix + "." + key;
    }

}

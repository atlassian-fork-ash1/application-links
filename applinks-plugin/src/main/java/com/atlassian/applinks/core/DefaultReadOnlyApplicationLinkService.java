package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.api.event.ApplicationLinkAddedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDetailsChangedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkMadePrimaryEvent;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.transform;
import static java.util.Objects.requireNonNull;

/**
 * Provides immutable application links based on the information returned by the underlying
 * {@link ApplicationLinkService}. The cache will be invalidated by most application link related events or after a
 * certain timeout.
 *
 * @since 4.0
 */
public class DefaultReadOnlyApplicationLinkService implements ReadOnlyApplicationLinkService, InitializingBean, DisposableBean {
    // ROTP-975 expire entries after some time to pick up applinks created by applinks 2 code
    private static final int DEFAULT_CACHE_EXPIRY_IN_SECONDS = Integer.getInteger("applinks.cache.expiry", 300);
    private static final Predicate<ReadOnlyApplicationLink> IS_PRIMARY = new Predicate<ReadOnlyApplicationLink>() {
        @Override
        public boolean apply(final ReadOnlyApplicationLink input) {
            return input.isPrimary();
        }
    };
    private static final boolean PRIMARY = true;
    private static final boolean NON_PRIMARY = false;
    private static final Ordering<ReadOnlyApplicationLink> SORT_BY_PRIMARY = Ordering.explicit(PRIMARY, NON_PRIMARY).onResultOf(Functions.forPredicate(IS_PRIMARY));

    private final ApplicationLinkService applicationLinkService;
    private final ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    private final TransactionTemplate transactionTemplate;
    private final EventPublisher eventPublisher;

    private final CachedReference<Iterable<ReadOnlyApplicationLink>> links;

    public DefaultReadOnlyApplicationLinkService(final ApplicationLinkService applicationLinkService,
                                                 final ApplicationLinkRequestFactoryFactory requestFactoryFactory,
                                                 final TransactionTemplate transactionTemplate,
                                                 final EventPublisher eventPublisher,
                                                 final CacheFactory cacheFactory) throws InvocationTargetException, IllegalAccessException {
        this.applicationLinkService = applicationLinkService;
        this.requestFactoryFactory = requestFactoryFactory;
        this.transactionTemplate = transactionTemplate;
        this.eventPublisher = eventPublisher;
        // ROTP-975 expire entries after some time to pick up applinks created by applinks 2 code
        links = cacheFactory.getCachedReference(DefaultReadOnlyApplicationLinkService.class.getName() + ".links",
                new LinkSupplier(), newCacheSettings());
    }

    private static CacheSettings newCacheSettings() throws InvocationTargetException, IllegalAccessException {
        return new CacheSettingsBuilder()
                .replicateViaInvalidation()
                .expireAfterWrite(DEFAULT_CACHE_EXPIRY_IN_SECONDS, TimeUnit.SECONDS)
                .build();
    }

    private class LinkSupplier implements Supplier<Iterable<ReadOnlyApplicationLink>> {
        @Override
        public Iterable<ReadOnlyApplicationLink> get() {
            return transactionTemplate.execute(new TransactionCallback<Iterable<ReadOnlyApplicationLink>>() {
                @Override
                public Iterable<ReadOnlyApplicationLink> doInTransaction() {
                    final Iterable<ApplicationLink> applicationLinks = applicationLinkService.getApplicationLinks();
                    return ImmutableList.copyOf(transform(applicationLinks, toImmutableApplicationLink()));
                }
            });
        }
    }

    @Override
    public Iterable<ReadOnlyApplicationLink> getApplicationLinks() {
        return links.get();
    }

    @Nullable
    @Override
    public ReadOnlyApplicationLink getApplicationLink(final ApplicationId applicationId) {
        requireNonNull(applicationId, "applicationId");
        return getOnlyElement(filter(getApplicationLinks(), hasApplicationId(applicationId)), null);
    }

    @Override
    public Iterable<ReadOnlyApplicationLink> getApplicationLinks(final Class<? extends ApplicationType> type) {
        requireNonNull(type, "type");
        return SORT_BY_PRIMARY.immutableSortedCopy(filter(links.get(), hasApplicationType(type)));
    }

    @Nullable
    @Override
    public ReadOnlyApplicationLink getPrimaryApplicationLink(final Class<? extends ApplicationType> type) {
        requireNonNull(type, "type");
        final Iterable<ReadOnlyApplicationLink> filteredByApplicationType = filter(links.get(), hasApplicationType(type));
        if (Iterables.isEmpty(filteredByApplicationType)) {
            return null;
        } else {
            final ReadOnlyApplicationLink result = getOnlyElement(filter(filteredByApplicationType, IS_PRIMARY), null);
            if (result == null) {
                throw new IllegalStateException("There are application links of type " + type + " configured, but none are " +
                        "marked as primary");
            }
            return result;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onApplicationLinkAddedEvent(final ApplicationLinkAddedEvent event) {
        links.reset();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onApplicationLinkIdChangedEvent(final ApplicationLinksIDChangedEvent event) {
        links.reset();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onApplicationLinkDetailsChangedEvent(final ApplicationLinkDetailsChangedEvent event) {
        links.reset();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onApplicationLinkMadePrimaryEvent(final ApplicationLinkMadePrimaryEvent event) {
        links.reset();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onApplicationLinkDeletedEvent(final ApplicationLinkDeletedEvent event) {
        links.reset();
    }

    private Function<ApplicationLink, ReadOnlyApplicationLink> toImmutableApplicationLink() {
        return new Function<ApplicationLink, ReadOnlyApplicationLink>() {
            @Override
            public ReadOnlyApplicationLink apply(final ApplicationLink from) {
                return new ImmutableApplicationLink(from, requestFactoryFactory);
            }
        };
    }

    private static Predicate<ReadOnlyApplicationLink> hasApplicationId(final ApplicationId applicationId) {
        return new Predicate<ReadOnlyApplicationLink>() {
            @Override
            public boolean apply(final ReadOnlyApplicationLink input) {
                return applicationId.equals(input.getId());
            }
        };
    }

    private static Predicate<ReadOnlyApplicationLink> hasApplicationType(final Class<? extends ApplicationType> type) {
        return new Predicate<ReadOnlyApplicationLink>() {
            @Override
            public boolean apply(final ReadOnlyApplicationLink input) {
                return type.isAssignableFrom(input.getType().getClass());
            }
        };
    }

}

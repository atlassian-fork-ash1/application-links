package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.AuthorisationAdminURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;
import com.atlassian.http.url.SameOrigin;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import static java.util.Objects.requireNonNull;

public class ApplicationLinkRequestFactoryFactoryImpl implements ApplicationLinkRequestFactoryFactory {
    private static final Logger log = LoggerFactory.getLogger(ApplicationLinkRequestFactoryFactoryImpl.class);

    private final AuthenticatorAccessor authenticatorAccessor;
    private final ApplicationLinkRequestFactory delegateRequestFactory;
    private final UserManager userManager;

    public ApplicationLinkRequestFactoryFactoryImpl(final RequestFactory<Request<?, ?>> requestFactory,
                                                    final UserManager userManager,
                                                    final AuthenticatorAccessor authenticatorAccessor) {
        this.authenticatorAccessor = requireNonNull(authenticatorAccessor, "authenticatorAccessor can't be null");
        this.delegateRequestFactory = new SalRequestFactoryAdapter(requireNonNull(requestFactory, "requestFactory can't be null"));
        this.userManager = requireNonNull(userManager, "userManager can't be null");
    }

    /**
     * Per the contract of {@link ApplicationLink#createAuthenticatedRequestFactory()}, if there is no context user
     * the implementation of this method must <i>always</i> return an anonymous request factory. Even if there is a
     * context user, if no authenticated providers are configured an anonymous request factory will be returned.
     *
     * @param link the {@link ApplicationLink} to create an {@link ApplicationLinkRequestFactory} for
     * @return the created factory, which is never {@code null}
     */
    public ApplicationLinkRequestFactory getApplicationLinkRequestFactory(final ApplicationLink link) {
        requireNonNull(link);

        final String username = userManager.getRemoteUsername();
        if (username == null) {
            log.debug("No current user context. Outgoing requests will be anonymous");
            return createAnonymousRequestFactory(link);
        }

        // We try to give away {@link ImpersonatingAuthenticationProvider} first. If that's not successful,
        // we fall back to {@link NonImpersonatingAuthenticationProvider}.
        ApplicationLinkRequestFactory factory = createImpersonatingRequestFactory(link, ImpersonatingAuthenticationProvider.class, username);
        if (factory == null) {
            factory = createNonImpersonatingRequestFactory(link, NonImpersonatingAuthenticationProvider.class);
            if (factory == null) {
                log.debug("No authenticator configured for link '{}', outgoing requests will be anonymous", link);
                factory = createAnonymousRequestFactory(link);
            }
        }
        return factory;
    }

    /**
     * A context user is required when requesting an {@link ImpersonatingAuthenticationProvider}, but, unlike
     * {@link #getApplicationLinkRequestFactory(ApplicationLink)}, if a {@link NonImpersonatingAuthenticationProvider}
     * is <i>explicitly requested</i>, it will be available even without a context user. This allows two-legged OAuth
     * to be used by background processing as long as it is done explicitly.
     *
     * @param link          the {@link ApplicationLink} to create an {@link ApplicationLinkRequestFactory} for
     * @param providerClass the {@link AuthenticationProvider} to bind the {@link ApplicationLinkRequestFactory} to
     * @return the created factory, which may be {@code null} if the requested provider is not configured
     */
    public ApplicationLinkRequestFactory getApplicationLinkRequestFactory(final ApplicationLink link,
                                                                          final Class<? extends AuthenticationProvider> providerClass) {
        requireNonNull(link, "link can't be null");
        requireNonNull(providerClass, "providerClass can't be null");

        if (Anonymous.class.isAssignableFrom(providerClass)) {
            return createAnonymousRequestFactory(link);
        }
        if (ImpersonatingAuthenticationProvider.class.isAssignableFrom(providerClass)) {
            return createImpersonatingRequestFactory(link, providerClass);
        }
        if (NonImpersonatingAuthenticationProvider.class.isAssignableFrom(providerClass)) {
            return createNonImpersonatingRequestFactory(link, providerClass);
        }

        throw new IllegalArgumentException(String.format(
                "Only AuthenticationProviders that are subclasses of %s, %s or %s are supported",
                ImpersonatingAuthenticationProvider.class.getSimpleName(),
                NonImpersonatingAuthenticationProvider.class.getSimpleName(),
                Anonymous.class.getSimpleName()));
    }

    private ApplicationLinkRequestFactory createAnonymousRequestFactory(final ApplicationLink link) {
        return AbsoluteURLRequestFactory.create(delegateRequestFactory, link);
    }

    private ApplicationLinkRequestFactory createImpersonatingRequestFactory(final ApplicationLink link,
                                                                            final Class<? extends AuthenticationProvider> providerClass) {
        final String username = userManager.getRemoteUsername();
        if (username == null) {
            log.debug("Cannot create request factory with authentication provider '{}' without current user context.", providerClass.getName());
            return null;
        }

        return createImpersonatingRequestFactory(link, providerClass, username);
    }

    private ApplicationLinkRequestFactory createImpersonatingRequestFactory(final ApplicationLink link,
                                                                            final Class<? extends AuthenticationProvider> providerClass,
                                                                            final String username) {
        final AuthenticationProvider authenticationProvider = authenticatorAccessor.getAuthenticationProvider(link, providerClass);
        if (authenticationProvider == null) {
            return null;
        }

        return AbsoluteURLRequestFactory.create(((ImpersonatingAuthenticationProvider) authenticationProvider).getRequestFactory(username), link);
    }

    private ApplicationLinkRequestFactory createNonImpersonatingRequestFactory(final ApplicationLink link,
                                                                               final Class<? extends AuthenticationProvider> providerClass) {
        final AuthenticationProvider authenticationProvider = authenticatorAccessor.getAuthenticationProvider(link, providerClass);
        if (authenticationProvider == null) {
            return null;
        }

        return AbsoluteURLRequestFactory.create(((NonImpersonatingAuthenticationProvider) authenticationProvider).getRequestFactory(), link);
    }

    protected static class AbsoluteURLRequestFactory implements ApplicationLinkRequestFactory {
        protected final ApplicationLinkRequestFactory requestFactory; //protected for unit tests

        private final ApplicationLink link;

        public static ApplicationLinkRequestFactory create(final ApplicationLinkRequestFactory requestFactory, final ApplicationLink link) {
            if (requestFactory instanceof AuthorisationAdminURIGenerator) {
                return new AbsoluteURLRequestFactoryWithAdminURI(requestFactory, (AuthorisationAdminURIGenerator) requestFactory, link);
            }
            return new AbsoluteURLRequestFactory(requestFactory, link);
        }

        public AbsoluteURLRequestFactory(final ApplicationLinkRequestFactory requestFactory, final ApplicationLink link) {
            this.requestFactory = requireNonNull(requestFactory, "requestFactory can't be null");
            this.link = requireNonNull(link, "link can't be null");
        }

        public ApplicationLinkRequest createRequest(final Request.MethodType methodType, final String uri) throws CredentialsRequiredException {
            requireNonNull(uri);

            boolean isAbsoluteUri = false;
            try {
                isAbsoluteUri = new URI(uri).isAbsolute();
            } catch (URISyntaxException e) {
                log.warn("Couldn't parse uri '{}' supplied to RequestFactory.createRequest(), assuming relative.", uri);
            }
            final URI updatedUri;
            try {
                if (isAbsoluteUri) {
                    updatedUri = new URI(uri);
                } else {
                    updatedUri = new URI(link.getRpcUrl() + (uri.startsWith("/") ? uri : "/" + uri));
                }
                Validate.isTrue(SameOrigin.isSameOrigin(updatedUri, link.getRpcUrl()),
                    "Request url '%s' isn't in the same origin as the rpc url '%s'", updatedUri, link.getRpcUrl());
                Validate.isTrue(updatedUri.getPath().startsWith(link.getRpcUrl().getPath()),
                    "Request url '%s' doesn't match rpc url '%s'", updatedUri, link.getRpcUrl());

            } catch(MalformedURLException | URISyntaxException e) {
                throw new IllegalArgumentException(e);
            }
            return requestFactory.createRequest(methodType, updatedUri.toString());
        }

        public URI getAuthorisationURI() {
            return requestFactory.getAuthorisationURI();
        }

        public URI getAuthorisationURI(final URI callback) {
            return requestFactory.getAuthorisationURI(callback);
        }
    }

    protected static class AbsoluteURLRequestFactoryWithAdminURI
            extends AbsoluteURLRequestFactory
            implements AuthorisationAdminURIGenerator {
        private AuthorisationAdminURIGenerator adminUriGenerator;

        public AbsoluteURLRequestFactoryWithAdminURI(final ApplicationLinkRequestFactory requestFactory,
                                                     final AuthorisationAdminURIGenerator adminUriGenerator,
                                                     final ApplicationLink link) {
            super(requestFactory, link);
            this.adminUriGenerator = adminUriGenerator;
        }

        public URI getAuthorisationAdminURI() {
            return adminUriGenerator.getAuthorisationAdminURI();
        }
    }

    protected static class SalRequestFactoryAdapter implements ApplicationLinkRequestFactory {
        private final RequestFactory<Request<?, ?>> adaptedFactory;

        public SalRequestFactoryAdapter(final RequestFactory<Request<?, ?>> requestFactory) {
            adaptedFactory = requestFactory;
        }

        public URI getAuthorisationURI() {
            return null;
        }

        public URI getAuthorisationURI(final URI callback) {
            return null;
        }

        public ApplicationLinkRequest createRequest(final Request.MethodType methodType, final String url) {
            return new ApplicationLinkRequestAdaptor(adaptedFactory.createRequest(methodType, url));
        }
    }
}

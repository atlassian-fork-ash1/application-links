package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.AuthenticationProvider;

public interface ApplicationLinkRequestFactoryFactory {
    /**
     * @param link the {@link ApplicationLink} to create an {@link ApplicationLinkRequestFactory} for
     * @return an {@link ApplicationLinkRequestFactory} configured for the specified {@link ApplicationLink}
     */
    ApplicationLinkRequestFactory getApplicationLinkRequestFactory(ApplicationLink link);

    /**
     * @param link          the {@link ApplicationLink} to create an {@link ApplicationLinkRequestFactory} for
     * @param providerClass the {@link AuthenticationProvider} to bind the {@link ApplicationLinkRequestFactory} to
     * @return an {@link ApplicationLinkRequestFactory} configured for the specified {@link ApplicationLink}, or
     * {@code null} if the requested provider is not configured for the specified {@link ApplicationLink}
     */
    ApplicationLinkRequestFactory getApplicationLinkRequestFactory(ApplicationLink link, Class<? extends AuthenticationProvider> providerClass);
}

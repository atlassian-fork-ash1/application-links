package com.atlassian.applinks.core.auth;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.applinks.api.ApplicationId;

/**
 * Aggregates authentication provider-specific {@link OrphanedTrustDetector} implementations
 *
 * @since 3.0
 */
public class DelegatingOrphanedTrustDetector implements OrphanedTrustDetector {
    private List<InternalOrphanedTrustDetector> orphanedTrustDetectors = new ArrayList<>();

    public void setOrphanedTrustDetectors(final List<InternalOrphanedTrustDetector> orphanedTrustDetectors) {
        this.orphanedTrustDetectors = orphanedTrustDetectors;
    }

    @Override
    public List<OrphanedTrustCertificate> findOrphanedTrustCertificates() {
        final List<OrphanedTrustCertificate> certificates = new ArrayList<>();
        for (OrphanedTrustDetector detector : orphanedTrustDetectors) {
            certificates.addAll(detector.findOrphanedTrustCertificates());
        }
        return certificates;
    }

    @Override
    public void deleteTrustCertificate(final String id, final OrphanedTrustCertificate.Type type) {
        for (OrphanedTrustDetector detector : orphanedTrustDetectors) {
            if (detector.canHandleCertificateType(type))
                detector.deleteTrustCertificate(id, type);
        }
    }

    @Override
    public boolean canHandleCertificateType(final OrphanedTrustCertificate.Type type) {
        for (OrphanedTrustDetector detector : orphanedTrustDetectors) {
            if (detector.canHandleCertificateType(type))
                return true;
        }
        //nobody from the known universe was able to deal with the type
        return false;
    }

    @Override
    public void addOrphanedTrustToApplicationLink(final String id, final OrphanedTrustCertificate.Type type, final ApplicationId applicationId) {
        for (OrphanedTrustDetector detector : orphanedTrustDetectors) {
            if (detector.canHandleCertificateType(type))
                detector.addOrphanedTrustToApplicationLink(id, type, applicationId);
        }
    }
}

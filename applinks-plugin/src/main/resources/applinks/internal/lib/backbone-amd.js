define('applinks/lib/backbone', function() {
    var backbone = window.Backbone;
    if (!backbone) {
        throw "window.Backbone not defined, cannot load Backbone";
    }
    return backbone;
});

define('applinks/feature/oauth-picker-model', [
    'applinks/lib/aui',
    'applinks/lib/lodash',
    'applinks/lib/jquery',
    'applinks/common/preconditions'
], function(
    AJS,
    _,
    $,
    Preconditions
) {

    function OAuthPickerModel(enabled, twoLoEnabled, twoLoImpersonationEnabled) {
        this.enabled = !!enabled; // 3LO
        this.twoLoEnabled = !!twoLoEnabled;
        this.twoLoImpersonationEnabled = !!twoLoImpersonationEnabled;
        this._adjustLevels();
        this._setId();
        this._setName();
    }

    OAuthPickerModel.OAUTH_DISABLED_ID = 0;
    OAuthPickerModel.OAUTH_ID = 1;
    OAuthPickerModel.OAUTH_IMPERSONATION_ID = 2;

    OAuthPickerModel.create = function(config) {
        Preconditions.hasValue(config, 'config');
        return new OAuthPickerModel(config.enabled, config.twoLoEnabled, config.twoLoImpersonationEnabled);
    };

    OAuthPickerModel.prototype.isOAuthEnabled = function() {
        return this.enabled === true && this.twoLoEnabled === true && this.twoLoImpersonationEnabled === false;
    };

    OAuthPickerModel.prototype.isOAuthImpersonationEnabled = function() {
        return this.enabled === true && this.twoLoEnabled === true && this.twoLoImpersonationEnabled === true;
    };

    OAuthPickerModel.prototype.isOAuthDisabled = function() {
        return !this.isOAuthImpersonationEnabled() && !this.isOAuthEnabled();
    };

    OAuthPickerModel.prototype.getName = function() {
        return this.name;
    };

    OAuthPickerModel.prototype.getId = function() {
        return this.id;
    };

    // Only enabled, twoLoEnabled, twoLoImpersonationEnabled attributes should be serialized into JSON
    OAuthPickerModel.prototype.toJSON = function() {
        return {
            enabled: this.enabled,
            twoLoEnabled: this.twoLoEnabled,
            twoLoImpersonationEnabled: this.twoLoImpersonationEnabled
        };
    };

    OAuthPickerModel.prototype.equals = function(otherConfig) {
        return this.enabled === otherConfig.enabled &&
            this.twoLoEnabled === otherConfig.twoLoEnabled &&
            this.twoLoImpersonationEnabled === otherConfig.twoLoImpersonationEnabled;
    };

    OAuthPickerModel.fromId = function(id) {
        if (id === OAuthPickerModel.OAUTH_ID) {
            return new OAuthPickerModel(true, true, false);
        } else if (id === OAuthPickerModel.OAUTH_IMPERSONATION_ID) {
            return new OAuthPickerModel(true, true, true);
        }
        return new OAuthPickerModel(false, false, false);
    };

    OAuthPickerModel.prototype._adjustLevels = function() {
        if (this.enabled && !this.twoLoEnabled) {
            // if enabled is true but 2LO false, disable all levels
            this.enabled = false;
            this.twoLoEnabled = false;
            this.twoLoImpersonationEnabled = false;
        } else if (!this.enabled) {
            // if enabled is false, none of the other levels can be true
            this.twoLoEnabled = false;
            this.twoLoImpersonationEnabled = false;
        }
    };

    OAuthPickerModel.prototype._setName = function() {
        if (this.isOAuthImpersonationEnabled()) {
            this.name = AJS.I18n.getText('applinks.component.oauth-config.oauth.impersonation');
        } else if (this.isOAuthEnabled()) {
            this.name = AJS.I18n.getText('applinks.component.oauth-config.oauth');
        } else {
            this.name = AJS.I18n.getText('applinks.component.oauth-config.disabled');
        }
    };

    OAuthPickerModel.prototype._setId = function() {
        if (this.isOAuthImpersonationEnabled()) {
            this.id = OAuthPickerModel.OAUTH_IMPERSONATION_ID;
        } else if (this.isOAuthEnabled()) {
            this.id = OAuthPickerModel.OAUTH_ID;
        } else {
            this.id = OAuthPickerModel.OAUTH_DISABLED_ID;
        }
    };

    return OAuthPickerModel;
});
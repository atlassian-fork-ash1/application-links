define('applinks/feature/v3/list', [
    'applinks/lib/lodash',
    'applinks/lib/backbone',
    'applinks/lib/jquery',
    'applinks/lib/window',
    'applinks/lib/aui',
    'applinks/shim/applinks',
    'applinks/common/events',
    'applinks/common/rest',
    'applinks/common/response-status',
    'applinks/feature/status',
    'applinks/feature/status/details',
    'applinks/feature/oauth-dance',
    'applinks/feature/v3/list-views',
    'applinks/model/applink'
], function(
    _,
    Backbone,
    $,
    window,
    AJS,
    LegacyApplinks,
    Events,
    ApplinksRest,
    ResponseStatus,
    ApplinkStatus,
    ApplinkStatusDetails,
    OAuthDance,
    ListViews,
    ApplinkModel
){
    var hasLoaded = false;

    var ACTION_DELETE = 'delete';
    var ACTION_LEGACY_EDIT = 'legacy-edit';
    var ACTION_PRIMARY = 'primary';
    var ACTION_REMOTE = 'remote';
    var BUILT_IN_ACTIONS = [ACTION_DELETE, ACTION_LEGACY_EDIT, ACTION_PRIMARY, ACTION_REMOTE];

    // extra data keys required to render the applinks config screen
    var DATA_KEYS = [
        ApplinkModel.DataKeys.APPLICATION_VERSION,
        ApplinkModel.DataKeys.ATLASSIAN,
        ApplinkModel.DataKeys.CONFIG_URL,
        ApplinkModel.DataKeys.ICON_URI,
        ApplinkModel.DataKeys.EDITABLE,
        ApplinkModel.DataKeys.V3_EDITABLE,
        ApplinkModel.DataKeys.WEB_ITEMS
    ];

    /**
     * Convert to legacy applink format for legacy editing
     *
     * @param applink applink to convert
     * @private
     */
    function toLegacyApplink(applink) {
        // self link to legacy REST URL
        applink.link = [{
            rel: 'self',
            href: ApplinksRest.V1.applink(applink.id).getUrl()
        }];

        // typeId has changed to type
        applink.typeId = applink.type;
        // mark incoming & outgoing as available
        applink.hasIncoming = true;
        applink.hasOutgoing = true;

        return applink;
    }

    function findApplinkRowId(element) {
        return $(element).parents('tr:first').attr('id');
    }

    /**
     * Main table backbone view, handles data
     * loading, remove and updating.
     */
    return Backbone.View.extend({
        template: applinks.feature.v3.list.templates,

        events: {
            'click .legacy-edit-button': 'onLegacyEditClick'
        },

        initialize: function (options) {
            // extend with options
            _.extend(this, options);

            this.applinks = options.model || new ApplinkModel.Collection(null, {dataKeys: DATA_KEYS});
            this.$el.html(this.template.table());
            this.registerEvents();

            // initial data load
            this.fetchAllApplinks();
        },

        remove: function() {
            Backbone.View.prototype.remove.call(this);
            this.unregisterEvents();
        },

        /**
         * "Manually" registered events for model and view.
         */
        registerEvents: function () {
            // model events
            // currently only status changes asynchronously, so no need to refresh the view for any other event
            this.listenTo(this.applinks, 'remove destroy', this.onModelRemove)
                .listenTo(this.applinks, 'change:status', this.onStatusChange);

            // view events
            // actions dropdown callbacks
            $(document).on('click', '.v3-applink-actions-dropdown li', _.bind(this.onActionsDropdownClick, this));
            // global OAuth dance callback for all authorise links (see applink-status)
            new OAuthDance(this.$el, ApplinkStatus.AUTHENTICATE_LINK_SELECTOR)
                .onSuccess(_.bind(this.onOAuthDanceSuccess, this))
                .initialize();
            // re-fetch all applinks on orphaned upgrade, as it likely results in a new applink
            Events.on(Events.ORPHANED_UPGRADE, this.fetchAllApplinks, this);
        },

        /**
         * Unregister "manually" registered events outside of `this.$el`.
         */
        unregisterEvents: function() {
            $(document).off('click', '.v3-applink-actions-dropdown li');
            Events.off(Events.ORPHANED_UPGRADE);
        },

        /**
         * Fetch and re-render all applinks.
         */
        fetchAllApplinks: function() {
            this.getTable().removeClass('applinks-loaded');
            this.applinks.fetch({reset: true}).always(
                _.bind(this.filterAndSort, this),
                _.bind(this.onCollectionUpdate, this, true));
        },

        /**
         * Filter the applinks collection, skipping non-Atlassian system links and moving the built-in (Atlassian)
         * system links to the bottom. System links are generally not important for the user as there's not much that
         * can be done with them.
         */
        filterAndSort: function() {
            // Backbone uses Underscore's stable sortBy so the order of applinks should otherwise be preserved
            var sorted = this.applinks.chain().filter(function(applink) {
                return !applink.get('system') || applink.getData(ApplinkModel.DataKeys.ATLASSIAN)
            }).sortBy(function(applink) {
                return applink.get('system') ? 1 : 0;
            }).value();
            this.applinks.reset(sorted);
        },


        onStatusChange: function (model) {
            var statusContainer = this.$el.find('#' + model.id + ' td.status');
            statusContainer.empty();

            // render status
            new ApplinkStatus.View({
                applink: model.toJSON(),
                container: statusContainer
            });
            // render status details dialog
            new ApplinkStatusDetails.Dialog(model);

            $(document).trigger(Events.APPLINKS_UPDATED);
        },

        onModelRemove: function (model) {
            ApplinkStatusDetails.removeDialog(model.id);
            if (this.applinks.isEmpty()) {
                this.onCollectionUpdate();
            } else {
                var $container = this.$el.find('tbody');
                this.removeTooltips($container);
                this.$el.find('#' + model.id).remove();
                $(document).trigger(Events.APPLINKS_UPDATED);
                this.renderTooltips($container);
            }
        },

        /**
         * Executed when the Applinks collection is updated.
         *
         * @param isFetched if the update is resulting from a remote fetch of the applinks list
         */
        onCollectionUpdate: function (isFetched) {
            this.populateTable();
            if (isFetched) {
                // init status fetch for all applinks
                this.fetchStatus();
            }
            this.getTable().find('.in-progress .progress-spinner').spin();
            this.getTable().addClass('applinks-loaded');

            if (hasLoaded) {
                $(document).trigger(Events.APPLINKS_UPDATED);
            } else {
                // Trigger this event once only
                hasLoaded = true;
                $(document).trigger(Events.APPLINKS_LOADED);
            }
        },

        populateTable: function () {
            var rows = document.createDocumentFragment(),
                table = this.getTable(),
                container = table.find('tbody'),
                noApplinksNotice = this.getNoApplinksNotice();

            this.removeTooltips(container);

            this.applinks.each(function (applink) {
                rows.appendChild(new ListViews.Row({model: applink}).render().get(0))
            });

            // clear container
            container.empty();

            // Update table content (in one go)
            if (rows.childNodes.length > 0) {
                // Show the table and hide the No Applinks notice
                container.append(rows);
                table.removeAttr('data-empty');
                table.removeAttr('hidden');
                noApplinksNotice.attr('hidden', '');
            } else {
                // Hide the table and show the No Applinks notice
                table.attr('data-empty','empty');
                table.attr('hidden','');
                noApplinksNotice.removeAttr('hidden');
            }

            this.renderTooltips(container);
        },

        /**
         * OAuth dance success callback: refresh status of the link corresponding to the event source.
         */
        onOAuthDanceSuccess: function(source) {
            var applink = this.applinks.get(findApplinkRowId(source));
            applink && applink.fetchStatus();
        },

        /**
         * Actions drop-down menu click handler.
         */
        onActionsDropdownClick: function (event) {
            var $element = $(event.target);
            var action = $element.data('action');

            if (action && (_.includes ? _.includes : _.contains)(BUILT_IN_ACTIONS, action)) {
                event.preventDefault();
                var id = $element.parents('.aui-dropdown2:first').data('id');
                // trigger selected action
                switch (action) {
                    case ACTION_DELETE:
                        this.confirmDelete(id);
                        break;
                    case ACTION_LEGACY_EDIT:
                        this.legacyEdit(id);
                        break;
                    case ACTION_PRIMARY:
                        this.makePrimary(id);
                        break;
                    case ACTION_REMOTE:
                        this.goToRemote(id);
                        break;
                }
            }
        },

        /**
         * @param event the event to respond to
         */
        onLegacyEditClick: function (event) {
            var id = findApplinkRowId(event.target);
            this.legacyEdit(id);
        },

        /**
         * Show delete confirmation dialog.
         */
        confirmDelete: function (id) {
            var dialog = new ListViews.ConfirmDialog({
                id: id
            });
            dialog.on('confirm', this.deleteApplink, this);

            dialog.show();
        },

        deleteApplink: function(id) {
            // REST Delete call must be synchronous otherwise the applink in the UI (model) will be deleted even if the
            // REST call fails.
            this.applinks.get(id).destroy({
                wait: true,
                expectedStatuses: [ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND]
            });
        },

        /**
         * Mark selected application link as primary.
         */
        makePrimary: function(id) {
            var self = this;
            this.applinks.get(id).makePrimary({
                success: _.bind(self.fetchAllApplinks, self)
            });
        },

        /**
         * Open the legacy edit dialog for a given applink `id`.
         *
         * @param id Applink ID
         */
        legacyEdit: function (id) {
            LegacyApplinks.UI.hideInfoBox();

            var applink = toLegacyApplink(this.applinks.get(id).toJSON());
            var refreshApplinks = _.bind(this.fetchAllApplinks, this);

            LegacyApplinks.editAppLink(applink, 'undefined', false, refreshApplinks, refreshApplinks);
        },

        /**
         * Open remote in new window.
         */
        goToRemote: function (id) {
            window.open(this.applinks.get(id).getAdminUrl());
        },

        /**
         * Init fetch Applink status for all applinks.
         */
        fetchStatus: function () {
            // init fetch status on all applinks,
            this.applinks.each(function(applink) {
                applink.fetchStatus();
            });
        },

        /**
         * Initialize tipsy handler.
         */
        renderTooltips: function ($element) {
            // first remove any existing tipsy element
            $('body > .tipsy').remove();

            // re-initialize tipsy, search only for data-title
            ($element || this.$el).find('[data-title]')
                .tooltip({
                    className: 'tipsy-left',
                    title: 'data-title',
                    html: true
                });

            ($element || this.$el).find('span.applinks-name')
                .tooltip({
                    className: 'url-tipsy',
                    gravity: 's',
                    html: true,
                    title: function () {
                        return this.getAttribute('data-displayUrl') + '<br/>' + this.getAttribute('data-rpcUrl')
                    }
                });
        },

        removeTooltips: function ($element) {
            // re-initialize tipsy, search only for data-title
            ($element || this.$el).find('[data-title]').attr('data-title', '');
        },

        getTable: function() {
            return this.$el.find('#agent-table');
        },

        getNoApplinksNotice: function() {
            return this.$el.find('#v3-no-applinks-notice');
        }
    });
});
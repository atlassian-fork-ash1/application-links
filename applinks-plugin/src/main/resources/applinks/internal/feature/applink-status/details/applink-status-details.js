define('applinks/feature/status/details', [
    'applinks/lib/aui',
    'applinks/lib/console',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/events',
    'applinks/component/inline-dialog',
    'applinks/common/preconditions',
    'applinks/common/rest',
    'applinks/feature/status',
    'applinks/feature/status/errors',
    'applinks/feature/status/oauth-matcher',
    'applinks/feature/oauth-dance',
    'applinks/common/urls',
    'applinks/feature/status/details/deprecated'
], function(
    AJS,
    console,
    $,
    _,
    ApplinksEvents,
    InlineDialog,
    Preconditions,
    ApplinksRest,
    ApplinkStatus,
    ApplinkStatusErrors,
    matchOAuthConfigs,
    OAuthDance,
    Urls,
    DeprecatedStatus
) {
    var MODE_DIALOG = 'dialog';
    var MODE_NOTIFICATION = 'notification';

    function generateStatusDetailsContents(applink, mode) {
        mode = mode || MODE_DIALOG;

        var status = applink.get('status');
        var data = {
            status: status,
            displayUrl: applink.getAdminUrl(),
            editUrl: Urls.Local.edit(applink.id)
        };
        var injectedData = {
            mode: mode,
            isDialog: mode === MODE_DIALOG
        };

        function renderTemplate(template, extraData) {
            // render with injected data
            return template(_.extend({}, data, extraData || {}), null, injectedData)
        }

        var contents = null;

        if (ApplinkStatusErrors.CONNECTION_REFUSED.matches(status)) {
            contents = renderTemplate(applinks.status.details.connection_refused);
        } else if (ApplinkStatusErrors.UNKNOWN_HOST.matches(status)) {
            contents = renderTemplate(applinks.status.details.unknown_host);
        } else if (ApplinkStatusErrors.AUTH_LEVEL_MISMATCH.matches(status)) {
            var mismatches = matchOAuthConfigs(status);
            Preconditions.hasValue(mismatches, 'mismatches', 'Unable to find mismatch description');
            contents = renderTemplate(applinks.status.details.auth_level_mismatch, {mismatches: mismatches});
        } else if (ApplinkStatusErrors.UNKNOWN.matches(status)) {
            contents = renderTemplate(applinks.status.details.unknown_error);
        } else if (ApplinkStatusErrors.AUTH_LEVEL_UNSUPPORTED.matches(status)) {
            contents = renderTemplate(applinks.status.details.auth_level_unsupported);
            contents = renderTemplate(applinks.status.details.auth_level_unsupported);
        } else if (ApplinkStatusErrors.SSL_UNTRUSTED.matches(status)) {
            contents = renderTemplate(applinks.status.details.ssl_untrusted);
        } else if (ApplinkStatusErrors.SSL_HOSTNAME_UNMATCHED.matches(status)) {
            contents = renderTemplate(applinks.status.details.ssl_hostname_unmatched);
        } else if (ApplinkStatusErrors.SSL_UNMATCHED.matches(status)) {
            contents = renderTemplate(applinks.status.details.ssl_unmatched);
        } else if (ApplinkStatusErrors.OAUTH_PROBLEM.matches(status)) {
            contents = renderTemplate(applinks.status.details.oauth_problem);
        } else if (ApplinkStatusErrors.OAUTH_TIMESTAMP_REFUSED.matches(status)) {
            contents = renderTemplate(applinks.status.details.oauth_timestamp_refused);
        } else if (ApplinkStatusErrors.OAUTH_SIGNATURE_INVALID.matches(status)) {
            contents = renderTemplate(applinks.status.details.oauth_signature_invalid);
        } else if (ApplinkStatusErrors.UNEXPECTED_RESPONSE.matches(status) ||
            ApplinkStatusErrors.UNEXPECTED_RESPONSE_STATUS.matches(status)) {
            contents = renderTemplate(applinks.status.details.unexpected_response, {
                renderDetails: ApplinkStatusErrors.UNEXPECTED_RESPONSE_STATUS.matches(status)
            });
        } else if (ApplinkStatusErrors.SYSTEM_LINK.matches(status)) {
            contents = renderTemplate(applinks.status.details.system_link);
        } else if (ApplinkStatusErrors.GENERIC_LINK.matches(status)) {
            contents = renderTemplate(applinks.status.details.generic_link);
        } else if (ApplinkStatusErrors.REMOTE_VERSION_INCOMPATIBLE.matches(status)) {
            contents = renderTemplate(applinks.status.details.remote_version_incompatible,
                {applinkType: applink.getTypeName()});
        } else if (ApplinkStatusErrors.NON_ATLASSIAN.matches(status)) {
            contents = renderTemplate(applinks.status.details.non_atlassian);
        } else if (ApplinkStatusErrors.NO_REMOTE_APPLINK.matches(status)) {
            contents = renderTemplate(applinks.status.details.no_remote_applink);
        } else if (ApplinkStatusErrors.NO_OUTGOING_AUTH.matches(status)) {
            contents = renderTemplate(applinks.status.details.no_outgoing_auth);
        } else if (ApplinkStatusErrors.INSUFFICIENT_REMOTE_PERMISSION.matches(status)) {
            contents = renderTemplate(applinks.status.details.insufficient_remote_permission);
        } else if (ApplinkStatusErrors.LEGACY_REMOVAL.matches(status)) {
            contents = renderTemplate(applinks.status.details.legacy_removal);
        } else if (ApplinkStatusErrors.LEGACY_UPDATE.matches(status)) {
            contents = renderTemplate(applinks.status.details.legacy_update);
        } else if (ApplinkStatusErrors.MANUAL_LEGACY_UPDATE.matches(status)) {
            contents = renderTemplate(applinks.status.details.manual_legacy_update);
        } else if (ApplinkStatusErrors.MANUAL_LEGACY_REMOVAL.matches(status)) {
            contents = renderTemplate(applinks.status.details.manual_legacy_removal);
        } else if (ApplinkStatusErrors.MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT.matches(status)) {
            contents = renderTemplate(applinks.status.details.manual_legacy_removal_with_old_edit);
        }
        // add more status popups here

        return contents && modeContainer(status, mode, contents);
    }

    function modeContainer(status, mode, contents) {
        if (mode == MODE_NOTIFICATION) {
            return applinks.status.details.notificationContainer({status: status, contents: contents});
        } else {
            // dialog does not need a container
            return contents;
        }
    }

    function inlineDialogId(linkId) {
        return 'applinks-status-dialog-' + linkId;
    }

    function inlineDialog(linkId) {
        return $('#' + inlineDialogId(linkId));
    }

    function initialiseOAuthDanceForInsufficientPermission($container, applink) {
        var selector = '.status-insufficient-remote-permission';
        var $oauthLink = $container.find(selector);

        // remove consumer token before redirecting user to the OAuth dance
        $oauthLink.on('click', function() {
            ApplinksRest.OAuth.V1.consumerToken(applink.id).del()
                .fail(function(req) {
                    console.warn(AJS.format('Could not remove consumer token for appLinkId {0}, {1}: {2}',
                        applink.id, req.status, req.responseText));
                    triggerOAuthDance($oauthLink);
                })
                .done(function() {
                    triggerOAuthDance($oauthLink);
                });

            return false;
        });
    }

    function onLoad(statusDialog, applink) {
        var status = statusDialog._status;
        if (status && status.error && "DEPRECATED" === status.error.category) {
            return DeprecatedStatus.onLoad(statusDialog, applink);
        }
    }

    function triggerOAuthDance($oauthLink) {
        new OAuthDance($oauthLink).start();
    }

    function fireAnalyticsForDialog(status) {
        AJS.trigger('analyticsEvent', {
            name: 'applinks.status.dialog.opened',
            data: {
                applicationId: status.link.id,
                status: status.error.type.toLowerCase(),
                category: status.error.category.toLowerCase()
            }
        });
    }

    /**
     * Remove any existing status details dialog
     *
     * @param id applink ID to remove the dialog for
     */
    function removeDialog(id) {
        inlineDialog(id).remove();
    }

    /**
     * Constructs status details in form of an inline dialog.
     *
     * @param applink applink V3 model
     * @constructor
     */
    function Dialog(applink) {
        var self = this;
        this._status = applink.get('status');

        if (this._status) {
            this._status = ApplinkStatus.processStatus(this._status);
            removeDialog(this._status.link.id);
            var contents = generateStatusDetailsContents(applink);
            if (contents) {
                var dialog = this._dialog = new InlineDialog(inlineDialogId(this._status.link.id), contents, {
                    classes: 'applinks-status-dialog',
                    alignment: 'bottom center'
                });

                onLoad(this, applink);

                var realign = _.bind(function() {
                    if (this.isVisible()) {
                        this.realign();
                    }
                }, dialog);

                dialog.onShow(function() {
                    $(document).on(ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED, realign);
                    self.onDialogShow();
                }).onHide(function() {
                    $(document).off(ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED, realign);
                });

                dialog.on('click', '.applinks-status-close, .applinks-v3-switchback', function() {
                    dialog.hide();
                });

                initialiseOAuthDanceForInsufficientPermission(dialog.contents(), applink);
            }
        }
    }

    // visible for testing
    Dialog.prototype.onDialogShow = function () {
        fireAnalyticsForDialog(this._status);
    };

    /**
     * Constructs status details in form of a static inline notification.
     *
     * @param {Object} options object to render the status with:
     *        `applink` {Object} (required) - applink model to render the status details for, with a `status` property 
     *                                        indicating applink status
     *        `container` {HTMLElement} (required) - container to attach the status notification to
     *        `refresh` {HTMLElement} (optional, true by default) - if true, remove the previous notification if the
     *                                                              container already has one
     * @constructor
     */
    function Notification(options) {
        Preconditions.hasValue(options, 'options');
        Preconditions.hasValue(options.applink, 'options.applink');
        Preconditions.hasValue(options.container, 'options.container');

        var self = this;
        this._status = options.applink.get('status');

        if (this._status) {
            this._status = ApplinkStatus.processStatus(this._status);
            var contents = generateStatusDetailsContents(options.applink, MODE_NOTIFICATION);
            if (contents) {
                if (options.refresh !== false) {
                    self.findNotification(options.container).remove();
                }
                $(options.container).append(contents);

                initialiseOAuthDanceForInsufficientPermission(self.findNotification(options.container), options.applink);
            }
        }
    }

    Notification.prototype.findNotification = function(container) {
        return findNotification(container, this._status.link.id);
    };

    function findNotification(container, applinkId) {
        return $(container).find('.applink-status-notification').filter('[data-applink-id="' + applinkId + '"]');
    }

    /**
     * Render the status details in form of a mere Troubleshooting link relevant to the provided status.
     * @param {Object} options object to render the status with:
     *        `applink` {Object} (required) - applink model to render the status details for, with a `status` property
     *                                        indicating applink status
     *        `container` {HTMLElement} (required) - container to attach the status notification to
     *        `refresh` {HTMLElement} (optional, true by default) - if true, remove the any previous help link if the
     *                                                              container already has one
     *        `style` {string} (optional, `link` by default) - controls whether the help link is rendered as a `link` 
     *                                                         or `button`
     *        `textMode` {string} (optional, `short` by default) - controls the link text, `short` or `full`
     *        `extraClasses` {string} (optional, defaults to `undefined`) - extra classes to render on the link element  
     * @constructor
     */
    function HelpLink(options) {
        Preconditions.hasValue(options, 'options');
        Preconditions.hasValue(options.applink, 'options.applink');
        Preconditions.hasValue(options.container, 'options.container');

        var status = options.applink.get('status');

        // only render for error statuses
        if (status && status.error) {
            var error = ApplinkStatusErrors.find(ApplinkStatus.processStatus(status));
            if (error) {
                var contents = applinks.status.details.helpLink({
                    linkKey: error.linkKey,
                    sectionKey: error.sectionKey,
                    style: options.style || 'link',
                    textMode: options.textMode || 'short',
                    extraClasses: options.extraClasses
                });
                // remove previous contents if refresh not explicitly == false
                if (options.refresh !== false) {
                    options.container.find('.troubleshoot-link').remove();
                }
                $(options.container).append(contents);
            }
        }
    }

    return {
        Dialog: Dialog,
        Notification: Notification,
        HelpLink: HelpLink,
        removeDialog: removeDialog
    };
});

import React, { Component } from 'react';

import Button from '@atlaskit/button/components/Button';
import DropList from '@atlaskit/droplist/components/Droplist';
import Item, { ItemGroup } from '@atlaskit/item';
import MoreIcon from '@atlaskit/icon/glyph/more';
import { ModalTransition } from '@atlaskit/modal-dialog';
import { I18n } from '@atlassian/wrm-react-i18n';
import DeleteEntityLinkDialogForm from './delete-entity-link-dialog';
import { makeEntityLinkPrimary } from './EntitylinksRest';

export default class EntityLinksMoreActions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMoreActionsOpen: false,
            isDeleteLinkModalOpen: false,
        };
    }

    onClickMakePrimaryLink(entityLink, type) {
        makeEntityLinkPrimary(entityLink, type, this.props.projectKey).then(response => {
            if (response.status === 200) {
                this.props.onMakePrimary(entityLink);
            }
        });
    }

    onClickToggleDropList() {
        this.setState({
            isMoreActionsOpen: !this.state.isMoreActionsOpen,
        });
    }

    openDeleteDialog() {
        this.setState({
            isDeleteLinkModalOpen: true,
        });
    }

    closeDeleteDialog() {
        this.setState({
            isDeleteLinkModalOpen: false,
        });
    }

    onOpenChange() {
        this.setState({
            isMoreActionsOpen: false,
        });
    }

    render() {
        const { isMoreActionsOpen, isDeleteLinkModalOpen } = this.state;
        const { type, entityLink, outgoingApplicationName } = this.props;
        return (
            <React.Fragment>
                <DropList
                    position="bottom right"
                    onClick={() => this.onClickToggleDropList()}
                    onOpenChange={() => this.onOpenChange()}
                    isOpen={isMoreActionsOpen}
                    trigger={
                        <Button className="droplist">
                            <MoreIcon size="small" />
                        </Button>
                    }
                >
                    <ItemGroup>
                        <Item onClick={() => this.openDeleteDialog()}>
                            <span className="delete-link-action">
                                {I18n.getText('entitylinks.more.actions.delete')}
                            </span>
                        </Item>
                        {!entityLink.isPrimary && (
                            <Item onClick={() => this.onClickMakePrimaryLink(entityLink, type)}>
                                {I18n.getText('entitylinks.more.actions.make.primary.link')}
                            </Item>
                        )}
                    </ItemGroup>
                </DropList>
                <ModalTransition>
                    {isDeleteLinkModalOpen && (
                        <DeleteEntityLinkDialogForm
                            currentApp={this.props.currentApp}
                            outgoingApplicationName={outgoingApplicationName}
                            type={type}
                            projectKey={this.props.projectKey}
                            entityLink={entityLink}
                            onClose={() => this.closeDeleteDialog()}
                            onDelete={this.props.onDelete}
                        />
                    )}
                </ModalTransition>
            </React.Fragment>
        );
    }
}

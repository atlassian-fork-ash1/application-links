import { initLinks } from './entity-links-manage';
import whenDomReady from 'when-dom-ready';

whenDomReady().then(() => {
    const renderElement = document.getElementById('entity-links-container');
    let data = [];
    try {
        data = JSON.parse(renderElement.getAttribute('data-links'));
    } catch (e) {
        console.log('cannot parse JSON');
    }

    initLinks(renderElement, data);
});

export function getPermissionsResponse(selectedApplink) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/permission/reciprocate-entity-create/${selectedApplink}.json`,
        {
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export function getAnonymousProjectsResponse(selectedApplink) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/entities/anonymous/${selectedApplink}.json`,
        {
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export function getProjectsResponse(selectedApplink) {
    return fetch(`${WRM.contextPath()}/rest/applinks/1.0/entities/${selectedApplink}.json`, {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'same-origin',
    });
}

export function createReciproticalLink(entityLink, projectKey, type, twoway) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/entitylink/${type}/${projectKey}.json?reciprocate=${twoway}`,
        {
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            method: 'PUT',
            body: JSON.stringify(entityLink),
            credentials: 'same-origin',
        }
    );
}

export function canDeleteEntityLinkResponse(entityLink, type, projectKey) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/permission/reciprocate-entity-delete/${
            entityLink.applicationId
        }/${type}/${projectKey}/${entityLink.typeId}/${entityLink.key}.json`,
        {
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export function deleteEntityLink(entityLink, type, projectKey, twoway) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/entitylink/${type}/${projectKey}?typeId=${
            entityLink.typeId
        }&key=${entityLink.key}&applicationId=${entityLink.applicationId}&reciprocate=${twoway}`,
        {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export function makeEntityLinkPrimary(entityLink, type, projectKey) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/entitylink/primary/${type}/${projectKey}?typeId=${
            entityLink.typeId
        }&key=${entityLink.key}&applicationId=${entityLink.applicationId}`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export function getEntityLinks(type, projectKey) {
    return fetch(
        `${WRM.contextPath()}/rest/applinks/1.0/entitylink/list/${type}/${projectKey}.json`,
        {
            headers: { 'Content-Type': 'application/json' },
            credentials: 'same-origin',
        }
    );
}

export const PermissionCode = {
    NO_AUTHENTICATION: 'NO_AUTHENTICATION',
    NO_PERMISSION: 'NO_PERMISSION',
    MISSING: 'MISSING',
    NO_AUTHENTICATION_CONFIGURED: 'NO_AUTHENTICATION_CONFIGURED',
    NO_CONNECTION: 'NO_CONNECTION',
    CREDENTIALS_REQUIRED: 'CREDENTIALS_REQUIRED',
    AUTHENTICATION_FAILED: 'AUTHENTICATION_FAILED',
    ALLOWED: 'ALLOWED',
};

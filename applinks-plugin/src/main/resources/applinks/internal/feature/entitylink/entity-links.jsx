import React, { Component, Fragment } from 'react';
import EmptyState from '@atlaskit/empty-state';
import emptyLogo from './integration.svg';
import buildingFunctionality from './building-functionality.svg';
import Button from '@atlaskit/button';
import { I18n } from '@atlassian/wrm-react-i18n';
import EntityLinkTable from './entitylinks-table';
import './entitylinks.less';
import { ModalTransition } from '@atlaskit/modal-dialog';
import { AutoDismissFlag, FlagGroup } from '@atlaskit/flag';
import CreateEntityLinkDialogForm from './create-entity-link-dialog';
import { getEntityLinks } from './EntitylinksRest';
import { colors } from '@atlaskit/theme';
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import * as helpPaths from 'applinks/common/help-paths';

const Title = <h1 className="page-title">{I18n.getText('entitylinks.page.header.title')}</h1>;

const FlagStatus = {
    HIDDEN: 'hidden',
    ENTITY_DELETED: 'entity_deleted',
    ENTITY_CREATED: 'entity_created',
};

const getSuccessFlag = description => (
    <AutoDismissFlag
        icon={
            <SuccessIcon
                primaryColor={colors.G300}
                label={I18n.getText('entitylinks.page.flag.success.label')}
            />
        }
        description={description}
        title={I18n.getText('entitylinks.page.flag.success.title')}
    />
);

export default class EntityLinks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entityLinks: this.props.entityLinks,
            isDialogOpen: false,
            shownFlag: null,
        };
    }

    addEntityLink(link) {
        this.setState(prevState => ({
            entityLinks: [...prevState.entityLinks, link],
            shownFlag: {
                status: FlagStatus.ENTITY_CREATED,
                projectName: link.name,
            },
        }));
    }

    deleteEntityLink = entityLink => {
        getEntityLinks(this.props.type, this.props.projectKey)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    entityLinks: data.entity,
                    shownFlag: {
                        status: FlagStatus.ENTITY_DELETED,
                        projectName: entityLink.name,
                    },
                });
            })
            .catch(e => {
                console.error('Unable to load the entityLinks in the background ' + e);
            });

        //Temporary delete this link until we get the updated response from the server where the link is deleted
        // and primaries are recalculated.
        const filteredEntityLinks = this.state.entityLinks.filter(link => {
            return !(
                entityLink.key === link.key && entityLink.applicationId === link.applicationId
            );
        });
        this.setState({
            entityLinks: filteredEntityLinks,
        });
    };

    makePrimaryEntityLink = entityLink => {
        const entityLinks = this.state.entityLinks;
        entityLinks.map(link => {
            if (entityLink.typeId === link.typeId) {
                link.isPrimary = entityLink === link;
            }
        });
        this.setState({ entityLinks });
    };

    createButton(appearance) {
        return (
            <div>
                <Button
                    className="add-link"
                    onClick={() => this.setState({ isDialogOpen: true })}
                    appearance={appearance}
                >
                    {I18n.getText('entitylinks.empty.state.primary.action')}
                </Button>
                <ModalTransition>
                    {this.state.isDialogOpen && (
                        <CreateEntityLinkDialogForm
                            {...this.props}
                            entityLinks={this.state.entityLinks}
                            onEntityAdded={link => this.addEntityLink(link)}
                            onClose={() => this.setState({ isDialogOpen: false })}
                        />
                    )}
                </ModalTransition>
            </div>
        );
    }

    createFlag() {
        let message;

        if (!this.state.shownFlag) {
            return;
        } else if (this.state.shownFlag.status === FlagStatus.ENTITY_CREATED) {
            message = I18n.getText(
                'entitylinks.page.flag.success.created',
                this.state.shownFlag.projectName
            );
        } else if (this.state.shownFlag.status === FlagStatus.ENTITY_DELETED) {
            message = I18n.getText(
                'entitylinks.page.flag.success.deleted',
                this.state.shownFlag.projectName
            );
        }

        return (
            <FlagGroup onDismissed={() => this.setState({ shownFlag: null })}>
                {getSuccessFlag(message)}
            </FlagGroup>
        );
    }

    onHelpButtonClicked() {
        const url = helpPaths.getFullPath('applinks.docs.adding.project.link');
        window.open(url, '_blank');
    }

    onApplinksLearnMoreClicked() {
        const url = helpPaths.getFullPath('applinks.docs.adding.application.link');
        window.open(url, '_blank');
    }

    render() {
        const { entityLinks } = this.state;
        const { type } = this.props;

        const noApplinksEmptyState = (
            <div className="no-app-links-empty-state">
                {Title}
                <EmptyState
                    header={I18n.getText('entitylinks.no.applinks.header')}
                    description={I18n.getText('entitylinks.no.applinks.description')}
                    maxImageWidth={240}
                    maxImageHeight={240}
                    imageUrl={buildingFunctionality}
                    tertiaryAction={
                        <Button appearance={'link'} onClick={() => this.onApplinksLearnMoreClicked()}>
                            {I18n.getText('entitylinks.no.applinks.tertiary.action')}
                        </Button>
                    }
                />
            </div>
        );

        const noEntityLinksEmptyState = (
            <div className="no-links-empty-state">
                {Title}
                <EmptyState
                    header={I18n.getText('entitylinks.empty.state.header')}
                    description={I18n.getText('entitylinks.empty.state.description')}
                    maxImageWidth={480}
                    maxImageHeight={480}
                    imageUrl={emptyLogo}
                    primaryAction={this.createButton('primary')}
                    tertiaryAction={
                        <Button appearance={'subtle-link'} onClick={() => this.onHelpButtonClicked()}>
                            {I18n.getText('entitylinks.empty.state.tertiary.action')}
                        </Button>
                    }
                />
            </div>
        );

        if (this.props.applicationLinks.length === 0) {
            return noApplinksEmptyState;
        } else if (entityLinks.length === 0) {
            return noEntityLinksEmptyState;
        } else {
            return (
                <Fragment>
                    {this.createFlag()}
                    <div className="top-container">
                        {Title}
                        <div className="button-container">{this.createButton('default')}</div>
                    </div>
                    <p className="explanation-container">
                        {I18n.getText('entitylinks.page.header.description')}{' '}
                        <a
                            href={helpPaths.getFullPath('applinks.docs.adding.project.link')}
                            target="_blank">
                                {I18n.getText('entitylinks.page.header.learn.more')}
                        </a>
                    </p>
                    <EntityLinkTable
                        currentApp={this.props.currentApp}
                        projectKey={this.props.projectKey}
                        type={type}
                        entityLinks={entityLinks}
                        onDelete={this.deleteEntityLink}
                        onMakePrimary={this.makePrimaryEntityLink}
                        applicationLinks={this.props.applicationLinks}
                    />
                </Fragment>
            );
        }
    }
}

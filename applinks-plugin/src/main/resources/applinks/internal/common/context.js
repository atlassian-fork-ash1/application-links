define('applinks/common/context', [
    'applinks/lib/console',
    'applinks/lib/window',
    'applinks/lib/wrm',
    'applinks/common/modules'
], function(
    console,
    window,
    WRM,
    ApplinksModules
) {
    var context = WRM.data.claim(ApplinksModules.dataFqn('applinks-common', 'applinks-context'));

    return {
        currentUser: function() {
            return context.currentUser
        },

        /**
         * @returns {object} host application containing `id` and `type` properties
         */
        hostApplication: function() {
            return context.hostApplication
        },

        validateCurrentUser: function() {
            if (!this.currentUser()) {
                console.log('No user context, reloading the page to trigger redirect to the login screen');
                window.location.reload();
            }
        }
    };
});
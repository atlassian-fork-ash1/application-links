define('applinks/common/dialog-helper', [
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/dialog-focus'
], function(
    AJS,
    $,
    _,
    DialogFocus
) {
    var globalDialogShown = false;
    var container = 'body';

    function isAnyDialogOpen() {
        return globalDialogShown;
    }

    function getContainer() {
        return container;
    }

    function _registerGlobalListeners() {
        // AJS.dialog2.on() can't be mocked in the right context in the rest-response-handlers.js, beforeEach() is executed after _registerGlobalListeners()
        if (_.isUndefined(AJS.dialog2) || _.isUndefined(AJS.dialog2.on)) {
            return;
        }

        AJS.dialog2.on("show", function() {
            DialogFocus.defaultFocus($('section.aui-dialog2'));
            globalDialogShown = true;
        });

        AJS.dialog2.on("hide", function() {
            globalDialogShown = false;
        });
    }

    _.defer(_registerGlobalListeners);

    return {
        isAnyDialogOpen: isAnyDialogOpen,
        getContainer: getContainer
    };
});
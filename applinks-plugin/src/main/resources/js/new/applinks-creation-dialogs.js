/**
 * AppLinks.Creation.Dialogs
 * Used to hold the functionality used to build and show create application link dialogs.
 */
(function ($, applinksUtil) {
    AppLinks.Creation.Dialogs = {
        /**
         * Set the enabled/disabled state of the Submit button of the main application links administration page.
         */
        setSubmitStatus: function () {
            var enabled = false;

            // if there is an entry in the application url the submit should be enabled
            if (AppLinks.Creation.userDefinedUrlField.val() !== null
                && AppLinks.Creation.userDefinedUrlField.val() !== ''
                && !AppLinks.Creation.getCreationState().creationInProgress())
            {
                enabled = true;
            }

            applinksUtil.setButtonEnabledState(
                AppLinks.Creation.createAppLinkButton,
                enabled,
                AppLinks.Creation.getCreationState().creationInProgress());
        },

        /**
         * Reset the application links creation process.
         * @param creationState the current creation process state.
         */
        resetProcess: function () {
            AppLinks.Creation.Dialogs.setSubmitStatus();
            AppLinks.Creation.userDefinedUrlField.focus();
        },
        /**
         * Display errors in a ADG error format.
         * @param data the REST response.
         */
        handleRestErrors: function (data) {
            if (data.status == 401) {
                window.location.reload();
            } else {
                var escapedMessage = AJS.escapeHtml(AppLinks.parseError(data));
                AppLinks.UI.showErrorBox(escapedMessage);
            }
        },

        /**
         * Display errors from configuring incoming authentication on Non-UAL in a ADG error format.
         * @param data the REST response.
         */
        handleConfigureIncomingNonUalLinkErrors: function (data) {
            if (data.status == 401) {
                window.location.reload();
            } else {
                // @todo extend error messaging to include a field and then catch 400 errors and highlight the problem field.
                $('.configureIncomingNonUalLinkDialogError')
                    .text(AppLinks.parseError(data)).show();
                $('aui-spinner').remove();
            }
        },

        /**
         * Display errors in a ADG error format.
         * @param data the REST response.
         */
        handleCreateNonUalLinkErrors: function (data) {
            if (data.status == 401) {
                window.location.reload();
            } else {
                var createNonUalLinkDialogError = $('.createNonUalLinkDialogError');
                createNonUalLinkDialogError.text(AppLinks.parseError(data));
                createNonUalLinkDialogError.show();
            }
        },

        /**
         * Display errors in a ADG error format.
         * @param data
         * @param creationState
         */
        handleFetchManifestRestSuccess: function (response, creationState) {
            if (AJS.$.isEmptyObject(response)) {
                //alert('empty response, code set to applinks.warning.host.does-not-support-manifest');
                response.code = "applinks.warning.host.does-not-support-manifest"
                return response;
            }

            if(typeof response.code !== 'undefined') {
                // a success but already contains an error code, so use that.
                return response;
            }

            if (typeof response.applinksVersion == 'undefined' || !applinksUtil.isVersion4OrHigher(response.applinksVersion)) {
                //alert('empty response, code set to applinks.warning.host.does-not-support-manifest');
                response.code = "applinks.warning.incompatible.applinks.version"
                return response;
            }

            // a "good" manifest
            return response;
        },
            /**
         * Display errors in a ADG error format.
         * @param response
         * @param creationState
         */
        handleFetchManifestRestFailure: function (response, creationState) {
            if (response.status == 401) {
                window.location.reload();
            }

            // CORS error. mimic the error handling behaviour of CreateApplicationLinkUIResource.tryToFetchManifest()
            // when unable to retrieve a remote manifest.
            if(response.status == 400) {
                response.warning = AJS.I18n.getText('applinks.error.badurl');
                response.code = 'applinks.error.rpcurl';
                return response;
            }

            // CORS error. mimic the error handling behaviour of CreateApplicationLinkUIResource.tryToFetchManifest()
            // when unable to retrieve a remote manifest.
            if(response.status == 404
                || response.status == 0) {
                response.warning = AJS.I18n.getText('applinks.warning.unknown.host.new');
                response.code = 'applinks.warning.unknown.host';
                return response;
            }

            if (response.status == 409) {
                response.warning = AppLinks.parseError(response);
                response.code = 'applinks.warning.host.duplicated';
                return response;
            }

            return response;
        },
        /**
         * Probably only for Dev purposes
         * Display errors in a ADG error format.
         * @param data
         */
        handleRestSuccess: function (data) {

        },
        /**
         * Binds behaviors necessary for applinks-dialogs
         */
        bindDialogBehaviors: function () {
            $('body')
                .on('change', '#applinks-dialog input:checkbox', function (e) {
                    //trigger event for checkbox changes so messages and step indicators get updated.
                    AJS.trigger('checkbox-change.applinks', e.target.id);
                })
                .on('submit', function (event) {
                    //don't submit forms inside of create dialogs.
                    var createDialog = $(event.target).closest('#create-applink-dialog');
                    if (createDialog.length) {
                        event.preventDefault();
                        createDialog.find('.aui-button-primary').click();
                    }
                })
                .on('keydown', function (event) {
                    if (event.keyCode !== 13) {
                        //not enter
                        return;
                    }
                    //don't submit forms inside of create dialogs.
                    var createDialog = $('#create-applink-dialog');
                    if (createDialog.length && createDialog.is(':visible')) {
                        event.preventDefault();
                        createDialog.find('.aui-button-primary').click();
                    }
                })
                .on('keydown', function (event) {
                    if (event.keyCode !== 27) {
                        //not escape
                        return;
                    }
                    var createDialog = $('#create-applink-dialog');
                    if (createDialog.length && createDialog.is(':visible')) {
                        createDialog.find('.applinks-cancel').click();
                    }
                });

            AJS.bind('checkbox-change.applinks', function (event, checkboxId) {
                $('#applinks-dialog').find('.' + checkboxId).toggleClass('hidden');
                $('#applinks-dialog').find('.' + checkboxId + 'Control').prop('disabled', function(idx, oldAttr) {
                    return !oldAttr;
                });
            });

            AJS.bind('remove.dialog', function() {
                AppLinks.Creation.Dialogs.resetProcess();
            });
        },

        showConfigureIncomingNonUalLinkDialog: function (creationState, deferred) {
            var dialog = AppLinks.Creation.Dialogs.buildConfigureIncomingNonUalLinkDialog(creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    $('.configureIncomingNonUalLinkDialogError').hide();
                    if (AppLinks.Creation.Dialogs.incomingNonUalFieldsAreInvalid() == false) {
                        var consumerName = $('#consumerName').val(),
                            consumerKey = $('#consumerKey').val(),
                            publicKey = $('#publicKey').val(),
                            configuringLink = AppLinks.Creation.Process.configureIncomingNonUalLink(
                                consumerName,
                                consumerKey,
                                publicKey,
                                creationState
                            );

                        applinksUtil.disableButton(dialog.popup.element.find('.aui-button'), true);
                        configuringLink
                            .done(function () {
                                console.log('non-ual incoming link created');
                                dialog.remove();
                                deferred.resolve();
                            })
                            .fail(function(){
                                applinksUtil.enableButton(dialog.popup.element.find('.aui-button'));
                            });
                    }
                }, 'aui-button aui-button-primary')
                .addLink(AJS.I18n.getText('applinks.create.cancel'), function (dialog) {
                    //cancel
                    console.log('non-ual incoming link cancel');
                    //always set the status to Success because if we are here we must have created the link previously.
                    // And for the same reason don't call AppLinks.Creation.initCreationState() as we need the state from the previous creation.
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_SUCCESS);
                    deferred.resolve();
                    dialog.remove();
                }, 'applinks-cancel');
            $('.configureIncomingNonUalLinkDialogError').hide();
            $('#applinks-configure-incoming-nonual-form .error').hide();
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        buildConfigureIncomingNonUalLinkDialog: function (creationState) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.create');

            //Create non-ual link dialog
            dialog
                .addHeader(headerMessage)
                .addPanel("Test", applinks.templates.getConfigureIncomingNonUalLinkDialog({
                    fromApp: creationState.getRemoteApplication(),
                    toApp: creationState.getLocalApplication()
                }));

            return dialog;
        },
        showCreateNonUalLinkDialog: function (typeOptions, creationState) {
            // TODO APLDEV-3 if oauth not installed then just create link from url and name
            var dialog = AppLinks.Creation.Dialogs.buildCreateNonUalLinkDialog(creationState, typeOptions)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    $('.createNonUalLinkDialogError').hide();
                    if (AppLinks.Creation.Dialogs.outgoingNonUalFieldsAreValid()) {
                        var name = $('#application-name-new').val(),
                            typeId = $('#application-types-new').find('option:selected').val(),
                            rpcUrl = AppLinks.UI.addProtocolToURL($('#non-ual-application-url').val()),
                            accessTokenUrl = $('#accessTokenUrl').val(),
                            requestTokenUrl = $('#requestTokenUrl').val(),
                            consumerKey = $('#consumerKey').val(),
                            authorizeUrl = $('#authorizeUrl').val(),
                            sharedSecret = $('#sharedSecret').val(),
                            serviceProvider = $('#serviceProvider').val(),
                            createIncoming = $('#createIncoming').is(':checked');

                        applinksUtil.disableButton(dialog.popup.element.find('.aui-button'), true);
                        var creatingOutgoing = AppLinks.Creation.Process.createOutgoingNonUalLink(
                            name,
                            typeId,
                            rpcUrl,
                            accessTokenUrl,
                            requestTokenUrl,
                            consumerKey,
                            authorizeUrl,
                            sharedSecret,
                            serviceProvider,
                            createIncoming,
                            creationState);

                        creatingOutgoing.progress(function () {
                            dialog.remove();
                        });
                        creatingOutgoing.done(function () {
                            applinksUtil.waitUntilDone(function () {
                                return AppLinks.Creation.Process.successNoRedirect(creationState);
                            })();
                        });
                        creatingOutgoing.fail(function () {
                            applinksUtil.enableButton(dialog.popup.element.find('.aui-button'));
                            //@todo: handle promise rejection, likely an ajax call failure.
                        });
                    }
                }, 'aui-button aui-button-primary');

            // if not initiated locally allow the use to stop the process
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.create.cancel'), function (dialog) {
                    //cancel
                    console.log('non-ual link cancel');
                    // cancelled so clear the state
                    AppLinks.Creation.initCreationState();
                    // the dialog.remove event will trigger a call to resetProcess to reset the UI.
                    dialog.remove();
                }, 'applinks-cancel');
            }
            $('.createNonUalLinkDialogError').hide();
            $('#applinks-create-nonual-form .error').hide();
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        buildCreateNonUalLinkDialog: function (creationState, typeOptions) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.create');

            //Create non-ual link dialog
            dialog
                .addHeader(headerMessage)
                .addPanel("Test", applinks.templates.getCreateNonUalLinkDialog({
                    fromApp: creationState.getRemoteApplication(),
                    toApp: creationState.getLocalApplication(),
                    typeOptions: typeOptions
                }));

            return dialog;
        },
        /**
         * Get the function to call when the creation process is complete.
         * This will determine if the browser is to be redirected, the local page refreshed etc.
         * @param creationState
         * @param isAdmin
         * @returns {Function}
         */
        getCreationCompleteFunction: function (creationState) {

            // if we are creating a unconfigured link then we are not going to redirect anywhere.
            if(creationState.getCreateUnconfiguredLink()) {
                if (creationState.getIncompatibleApplinksVersion()) {
                    // if the reason is because the remote app is an incompatible version then
                    // complete the process locally.
                    return function() {
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_SUCCESS);
                        return AppLinks.Creation.Process.successNoRedirect(creationState);
                    };
                } else {
                    // if the reason is because the user unchecked the admin option.
                    // the provide them with the url to go and complete the process in that application.
                    return function() {
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_SUCCESS);
                        return AppLinks.Creation.Process.successManualRedirect(creationState);
                    };
                }
            }

            // if we are simply not respecting redirects then complete the process locally.
            if(!creationState.getRespectRedirects()) {
                return function() {
                    return AppLinks.Creation.Process.successNoRedirect(creationState);
                }
            }

            // if we are creating the reciprocal then return to the originating application on completion.
            if(creationState.isCreatingReciprocal()) {
                return function () {
                    return AppLinks.Creation.Process.successRedirectToReturn(creationState);
                }
            }

            // default behaviour is to complete processing on this app and then continue on the remote app.
            return function () {
                        return AppLinks.Creation.Process.successRedirectToCreateReciprocal(creationState);
                    };
        },
        /**
         * Start the Application Link Creation process.
         * Determines which type of link to create, start that process and handles closing the parent dialog when the process is complete.
         * @param creationState
         * @param dialog
         */
        beginApplicationLinkCreation: function (creationState, dialog)
        {
            var completeCreationFunction = AppLinks.Creation.Dialogs.getCreationCompleteFunction(creationState);

            //create the local link.
            applinksUtil.disableButton(dialog.popup.element.find('.aui-button'), true);

            var ualLinkCreation;
            if (!AppLinks.Creation.Process.twoStageCreation) {
                ualLinkCreation = AppLinks.Creation.Process.createUalLinkAndConfigureIncomingAndOutgoing(creationState);
            } else {
                // 2-stage Local Creation: the following would support creation of the link and configure incoming authentication in the local app,
                // before redirecting to the remote app to create a link and configure incoming and outgoing authentication
                // before redirecting to the local app to configure the outgoing authentication.
                // likewise if creating a link with an incompatible version of Applinks confiugre as much as possible before being redirected.
                if (creationState.isCreatingReciprocal()) {
                    ualLinkCreation = AppLinks.Creation.Process.createUalLinkAndConfigureIncomingAndOutgoing(creationState);
                }
                else {
                    if (creationState.getCreateUnconfiguredLink()) {
                        ualLinkCreation = AppLinks.Creation.Process.createUrlLink(creationState);
                    } else {
                        ualLinkCreation = AppLinks.Creation.Process.createUalLinkAndConfigureIncoming(creationState);
                    }
                }
            }

            ualLinkCreation.progress(function () {
                dialog.remove();
            });
            ualLinkCreation.done(function () {
                applinksUtil.waitUntilDone(completeCreationFunction)();
            });
            ualLinkCreation.fail(function () {
                //@todo: handle promise rejection, likely an ajax call failure.
            });
        },
        /**
         * Show a dialogue to confirm the discrepancy between (entered) RPC URL and the one returned from the remote manifest.
         *
         * @param creationState
         */
        showConfirmUrlsDialog: function (creationState) {
            var dialog = AppLinks.Creation.Dialogs.buildConfirmUrlsDialog(creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {

                    var rpcUrlEdited = $('#application-url-rpc').val();

                    // clear the dialogue regardless
                    dialog.remove();

                    if (rpcUrlEdited != creationState.getCorrectedUrl()) {

                        // url has been updated by the user; restart the process with the new URL
                        creationState.setUserDefinedUrl(rpcUrlEdited);
                        creationState.setCorrectedUrl(rpcUrlEdited);
                        AppLinks.Creation.Process.fetchManifests(false, creationState);
                    } else {

                        // set the RPC url in the remote manifest then create the link
                        creationState.getRemoteApplication().rpcUrl = creationState.getCorrectedUrl();
                        AppLinks.Creation.Dialogs.showCreateApplinkDialog(creationState);
                    }

                }, 'aui-button aui-button-primary')
                .addLink(AJS.I18n.getText('applinks.create.cancel'), function (dialog) {

                    // cancelled so clear the state
                    AppLinks.Creation.initCreationState();
                    dialog.remove();
                }, 'applinks-cancel');

            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        buildConfirmUrlsDialog: function (creationState) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.confirm.urls');

            // confirm that RPC URL is different to display URL
            dialog
                .addHeader(headerMessage)
                .addPanel(headerMessage, applinks.templates.getConfirmUrlsDialog({
                        displayUrl: creationState.getRemoteApplication().url,
                        rpcUrl: creationState.getCorrectedUrl()
                    }));

            return dialog;
        },
        showCreateApplinkDialog: function (creationState) {
            var dialog = AppLinks.Creation.Dialogs.buildCreateApplinkDialog(creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    var isAdmin = $('#isAdmin').is(":checked");
                    // if not an admin then just create an unconfigured link,
                    // but only if not creating a recirocal link, as we can assume we are an admin
                    // and therefore create a full link.
                    creationState.setCreateUnconfiguredLink(!isAdmin && !creationState.isCreatingReciprocal());

                    AppLinks.Creation.Dialogs.beginApplicationLinkCreation(creationState, dialog);
                }, 'aui-button aui-button-primary');

            // if not initiated locally allow the user to cancel the process
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.create.cancel'), function (dialog) {
                    //cancel
                    console.log('ual link cancel');
                    // cancelled so clear the state
                    AppLinks.Creation.initCreationState();
                    if(creationState.isCreatingReciprocal()) {
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, "reciprocalCreationSkipped");
                        AppLinks.Creation.Process.showFinalStatus(creationState);
                    }

                    // the dialog.remove event will trigger a call to resetProcess to reset the UI.
                    dialog.remove();
                }, 'applinks-cancel');
            }
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the create applinks workflow 2.0 dialog.
         *
         * @param creationState
         */
        buildCreateApplinkDialog: function (creationState) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.create');

            console.log('Start creation process');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("Test", applinks.templates.getCreateUalLinkDialog({
                    reciprocal: creationState.isCreatingReciprocal(),
                    fromApp: creationState.getRemoteApplication(),
                    toApp: creationState.getLocalApplication(),
                    isSysAdmin: applinksUtil.isSysAdmin(),
                    sharedUserbase: creationState.getSharedUserbase(),
                    isUsingCors: creationState.getUseCors(),
                    impersonationIsSupported: (applinksUtil.applicationSupportsIncomingTwoLeggedOAuthWithImpersonation(creationState.getLocalApplication()) &&
                    applinksUtil.applicationSupportsIncomingTwoLeggedOAuthWithImpersonation(creationState.getRemoteApplication()))
                }));

            // apply any relevant defaults provided by the intitiator..
            AppLinks.ExternalInitiation.setDialogInitiatorDefaults(creationState);

            // if linking to an older version of AppLinks, hide the admin option
            // to prevent redirecting to the remote application
            if(creationState.getIncompatibleApplinksVersion()) {
                // uncheck
                $('#isAdmin').prop('checked', false);
                // hide checkbox
                $('#isAdmin').hide();
                // hide label
                $('label[for="isAdmin"]').hide();
                // hide any warning messages
                $('.isAdmin').hide();
            }

            return dialog;
        },
        showManuallyCreateReciprocalLinkDialog: function (remoteApplicationName, remoteApplicationUrl, localApplicationUrl, allowRedirectionBack, creationState) {
            var dialog = AppLinks.Creation.Dialogs.buildManuallyCreateReciprocalLinkDialog(remoteApplicationName,
                    AppLinks.Creation.generateApplinksCreationRedirectionUrl(allowRedirectionBack, creationState))
                .addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, "reciprocalCreationSkipped");
                    console.log('non-ual link close');
                    dialog.remove();
                    //cancel
                    // don't initCreationState until we've shown the final status.
                    AppLinks.Creation.Process.showFinalStatus(creationState);
                }, 'applinks-cancel');
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to display manual reciprocal applink..
         */
        buildManuallyCreateReciprocalLinkDialog: function (redirectApp, redirectUrl) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.manualRedirect.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("Test", applinks.templates.getManualRedirectDialog({
                    redirectApp: redirectApp,
                    redirectUrl: redirectUrl
                }));

            return dialog;
        },
        /**
         * Show a dialog informing the user that a duplicate link has been found, topping the process and offer them the option of redirecting back to
         * the originating application.
         * @return {*}
         */
        showReciprocalDuplicateDialog: function (remoteUrl, message, creationState) {
            // Creating a reciprocal link but can't read the manifest from the originating application
            // Probably firewall/routing problem
            // e.g. BTF instance trying to set up a reciprocal connection to a OD instance
            var warning = message;
            var information = AJS.I18n.getText("applinks.create.reciprocal.fail.redirect.information", remoteUrl);

            // if initiated locally allow the use to stop the process
            if (!creationState.doInitiatorRedirect()) {
                information = information + " " + AJS.I18n.getText("applinks.create.reciprocal.fail.redirect.information.cancel");
            }

            creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, 'reciprocalDuplicateLinkExists');

            var dialog = AppLinks.Creation.Dialogs.buildUnableToCreateReciprocalLinkDialog(warning, information)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    console.log('Duplicate AppLink. redirect back');
                    // Continue - redirect to the original application
                    applinksUtil.waitUntilDone(function () {
                            // TODO check creationState is reciprocal status == true
                            return AppLinks.Creation.redirectUserWithDialog(false, creationState);
                        }
                    )();
                    dialog.remove();
                }, 'aui-button aui-button-primary');

            // if initiated locally allow the use to stop the process
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    // Cancel - stay inthe current application
                    console.log('Duplicate Applink. cancelled redirect');
                    dialog.remove();
                    // don't initCreationState until we've shown the final status.
                    AppLinks.Creation.Process.showFinalStatus(creationState);
                }, 'applinks-cancel');
            }
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Show the dialog handling URL configuration
         * @param message
         * @param ignoreErrors assumed to be true, i.e. trust the user if they want this url.
         * @param creationState
         * @returns {*}
         */
        showConfigureUrlDialog: function (message, ignoreErrors, creationState, showSelectOptions) {
            //Display URL problems to the user.
            var dialog = AppLinks.Creation.Dialogs.buildConfigureUrlDialog(message, showSelectOptions, creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    var applicationUrlEdited = $('#application-url-corrected');

                    var checkedUrl = $('#use-application-url input[type="checkbox"]:checked');
                    if (checkedUrl.length > 0) {
                        // user has decided to ignore the corrected url and just use the user defined one.
                        creationState.setCorrectedUrl(creationState.getUserDefinedUrl())
                        ignoreErrors = true;
                    } else if (applicationUrlEdited.val() != creationState.getCorrectedUrl()) {
                        // user has manually edited the url
                        // so update the correct Url
                        var url = applicationUrlEdited.val();
                        creationState.setCorrectedUrl(url)
                        // make sure it gets rechecked
                        forceWhenHostIsOffline = false;
                        ignoreErrors = false;
                    }

                    dialog.remove();
                    // try to load amended url again...
                    AppLinks.Creation.Process.fetchManifests(ignoreErrors, creationState);
                }, 'aui-button aui-button-primary')
                .addLink(AJS.I18n.getText('applinks.create.cancel'), function (dialog) {
                    //cancel
                    // cancelled so clear the state
                    AppLinks.Creation.initCreationState();
                    // the dialog.remove event will trigger a call to resetProcess to reset the UI.
                    dialog.remove();
                }, 'applinks-cancel');
            dialog.show();
            dialog.updateHeight();
            $('#application-url-corrected').focus();
            return dialog;
        },
        /**
         * Build the dialog to handle problems with the initial application url.
         *
         * Nominal Step 0 in Create App Link workflow.
         */
        buildConfigureUrlDialog: function (urlErrorMessage, showSelectOptions, creationState) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.configureUrl.title');

            //Create dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("configure-url", applinks.templates.getConfigureUrlDialog({
                    originalUrl: creationState.getUserDefinedUrl(),
                    correctedUrl: creationState.getCorrectedUrl(),
                    urlErrorMessage: urlErrorMessage,
                    showSelectOptions: showSelectOptions
                }));

            return dialog;
        },
        /**
         * Show a dialog informing the user that an incompatible version of Applinks has been detected,
         * stop the process and offer to redirect them to the other application.
         * @param creationState the configuration of the current creation process.
         * @return {*}
         */
        showIncompatibleApplinksVersionDialog: function (creationState) {
            // Creating a reciprocal link but can't read the manifest from the originating application
            // Probably firewall/routing problem
            // e.g. BTF instance trying to set up a reciprocal connection to a OD instance
            var dialog = AppLinks.Creation.Dialogs.buildIncompatibleApplinksVersionDialog(creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    // Continue - create local end of the link
                    console.log('Incompatible AppLink Versions. Continue to create local link end');
                    creationState.setIncompatibleApplinksVersion(true);

                    // continue to create a local url only link
                    creationState.setCreateUnconfiguredLink(true);
                    AppLinks.Creation.Dialogs.beginApplicationLinkCreation(creationState, dialog);
                }, 'aui-button aui-button-primary');
            // if initiated locally allow the use to stop the process
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                        // Cancel - stay in the current application, do not create anything.
                        console.log('Incompatible AppLink Versions. cancelled creation');
                        // cancelled so clear the state
                        AppLinks.Creation.initCreationState();
                        dialog.remove();
                    }, 'applinks-cancel');
            }
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to tell the user about incompatible AppLinks versions.
         * @param creationState the current process state including the display name  and url of the remote application.
         * @return {AJS.Dialog}
         */
        buildIncompatibleApplinksVersionDialog: function (creationState) {
            var warning = AJS.I18n.getText('applinks.create.fail.incompatible-version.warning', creationState.getRemoteApplicationName());

            var informationLinkText = AJS.I18n.getText('applinks.create.fail.incompatible-version.information.transfer', AppLinks.UI.sanitiseHTML(creationState.getRemoteApplicationName()));
            var informationLink = '<a href="' + AppLinks.Creation.generateApplinksAdminUrl(creationState.getRemoteApplicationUrl(), creationState.getRemoteApplicationType()) + '">' + informationLinkText + '</a>';
            var informationHtml = informationLink + " " + AJS.I18n.getText('applinks.create.fail.incompatible-version.information.continue');
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.fail.incompatible-version.information.completion.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("applinks-dialog-panel", applinks.templates.getInformationDialog({
                    warningHtml: AppLinks.UI.sanitiseHTML(warning),
                    informationHtml: informationHtml
                }));

            return dialog;
        },
        /**
         * Show a dialog informing the user that creation has completed,
         * Offer to redirect them to the other application.
         * @param creationState the configuration of the current creation process.
         * @return {*}
         */
        showIncompatibleApplinksVersionCompletionDialog: function (creationState) {
            // Creating a reciprocal link but can't read the manifest from the originating application
            // Probably firewall/routing problem
            // e.g. BTF instance trying to set up a reciprocal connection to a OD instance
            var dialog = AppLinks.Creation.Dialogs.buildIncompatibleApplinksVersionCompletionDialog(creationState)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    console.log('Incompatible AppLink Versions. redirect to other app');
                    // Continue - redirect to the other application
                    applinksUtil.waitUntilDone(function () {
                            // TODO check creationState is reciprocal status == false
                            return AppLinks.Creation.redirectUserWithDialog(false, creationState);
                        }
                    )();
                    dialog.remove();
                }, 'aui-button aui-button-primary')
                .addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    // Cancel - stay inthe current application
                    console.log('Incompatible AppLink Versions. cancelled redirect');
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_FAILURE, 'reciprocalIncompatibleVersion');
                    // don't initCreationState until we've shown the final status.
                    AppLinks.Creation.Process.showFinalStatus(creationState);
                    dialog.remove();
                }, 'applinks-cancel');
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to allow the user to redirect to the remote application after configuring the local link
         * when there are incompatible AppLinks versions.
         * @param creationState the current process state including the display name  and url of the remote application.
         * @return {AJS.Dialog}
         */
        buildIncompatibleApplinksVersionCompletionDialog: function (creationState) {
            var information = AJS.I18n.getText('applinks.error.create.old.applinks', creationState.getRemoteApplicationName());
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.fail.incompatible-version.information.completion.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("applinks-dialog-panel", applinks.templates.getInformationDialog({
                    warning: null,
                    informationHtml: AppLinks.UI.sanitiseHTML(information)
                }));

            return dialog;
        },
        showProcessingPauseDialog: function (creationState) {
            var dialog = AppLinks.Creation.Dialogs.buildProcessingPauseDialog(creationState);
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        buildProcessingPauseDialog: function (creationState) {
            var message = AJS.I18n.getText('applinks.create.processing.message'),
                headerMessage = AJS.I18n.getText('applinks.create.processing.title');

            return AppLinks.Creation.Dialogs.buildPauseDialog(headerMessage, message);
        },
        /**
         * Show the dialog to pause user between redirects.
         * @param creatingReciprocal flag indicating if "true" that the user is currently creating a a reciprocal link
         *  or "false" the users is creating the originating link.
         * @param remoteApplicationName the name of the remote application.
         * @return {AJS.Dialog}
         */
        showRedirectPauseDialog: function (creatingReciprocal, remoteApplicationName) {
            var dialog = AppLinks.Creation.Dialogs.buildRedirectPauseDialog(creatingReciprocal, remoteApplicationName);
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to pause user between redirects.
         * @param creatingReciprocal flag indicating if "true" that the user is currently creating a a reciprocal link
         *  or "false" the users is creating the originating link.
         * @param remoteApplicationName the name of the remote application.
         * @return {AJS.Dialog}
         */
        buildRedirectPauseDialog: function (creatingReciprocal, remoteApplicationName) {
            var message,
                headerMessage = AJS.I18n.getText('applinks.create.redirect.title');

            if (creatingReciprocal == true) {
                message = AJS.I18n.getText('applinks.create.redirect.message.createReciprocal', remoteApplicationName);
            } else {
                message = AJS.I18n.getText('applinks.create.redirect.message.return', remoteApplicationName);
            }

            return AppLinks.Creation.Dialogs.buildPauseDialog(headerMessage, message);
        },
        /**
         * Show a dialog informing the user that a reciprocal link cannot be created and offer them an option to return to
         * the originating application.
         * @param creationState the configuration of the current creation process.
         * @returns {*}
         */
        showReciprocalManifestUnobtainableDialog: function (creationState) {
            var warning = AJS.I18n.getText('applinks.create.reciprocal.fail.remoteManifest.unobtainable', creationState.getCorrectedUrl())

            var information = AJS.I18n.getText("applinks.create.reciprocal.fail.redirect.information", creationState.getCorrectedUrl());
            // if initiated locally allow the use to stop the process
            if (!creationState.doInitiatorRedirect()) {
                information = information + " " + AJS.I18n.getText("applinks.create.reciprocal.fail.redirect.information.cancel");
            }

            var dialog = AppLinks.Creation.Dialogs.buildUnableToCreateReciprocalLinkDialog(warning, information)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    console.log('Remote manifest unobtainable. redirect back');
                    // Continue - redirect to the original application
                    applinksUtil.waitUntilDone(function () {
                            creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, 'reciprocalUnableToReadManifest');
                            // TODO check creationState is reciprocal status == true
                            return AppLinks.Creation.redirectUserWithDialog(false, creationState);
                        }
                    )();
                    dialog.remove();
                }, 'aui-button aui-button-primary');

            // if process was initiated externally then don't allow closing/stopping of the process.
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    // Cancel - stay inthe current application
                    console.log('Remote manifest unobtainable. cancelled redirect');
                    if(creationState.isCreatingReciprocal()) {
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, "reciprocalUnableToReadManifest");
                        AppLinks.Creation.Process.showFinalStatus(creationState);
                    }
                    dialog.remove();
                }, 'applinks-cancel');
            }
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to inform the User that we have been unable to create reciprocal link.
         * Offers them the option to redirect back to their original app.
         * @param warning a warning message specific to the current process.
         * @param information an information message explaining what happens next..
         * @return {AJS.Dialog}
         */
        buildUnableToCreateReciprocalLinkDialog: function (warning, information) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.reciprocal.fail.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("applinks-dialog-panel", applinks.templates.getInformationDialog({
                    warningHtml: AppLinks.UI.sanitiseHTML(warning),
                    informationHtml: AppLinks.UI.sanitiseHTML(information)
                }));

            return dialog;
        },
        /**
         * Show a dialog informing the user that the applinks creation process is not possible.
         * @param creationState the configuration of the current creation process.
         * @param information an information message explaining what happens next..
         * @returns {*}
         */
        showApplinkCreationWorkflowNotAvailableDialog: function (information, creationState) {
            var warning = AJS.I18n.getText("applinks.create.fail.process.unavailable");
            // if initiated remotely prevent the user stopping the process
            if (creationState.doInitiatorRedirect()) {
                information = information.replace(AJS.I18n.getText("applinks.create.fail.process.unavailable.cancel"), "");
            }

            var dialog = AppLinks.Creation.Dialogs.buildApplicationLinkWorkflowUnavailableDialog(warning, information)
                .addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    console.log('Workflow unavailable. redirect back');
                    // Continue - redirect to the original application
                    applinksUtil.waitUntilDone(function () {
                            creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, 'reciprocalWorkflowUnavailable');
                            // TODO check creationState is reciprocal status == true
                            return AppLinks.Creation.redirectUserWithDialog(false, creationState);
                        }
                    )();
                    dialog.remove();
                }, 'aui-button aui-button-primary');

            // if process was initiated externally then don't allow closing/stopping of the process.
            if (!creationState.doInitiatorRedirect()) {
                dialog.addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    // Cancel - stay inthe current application
                    console.log('Workflow unavailable. cancelled redirect');
                    dialog.remove();
                }, 'applinks-cancel');
            }
            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to inform the User the Applink creation workflow is unavailable.
         * Offers them the option to redirect back to their original app.
         * @param warning a warning message specific to the current process.
         * @param information an information message explaining what happens next..
         * @return {AJS.Dialog}
         */
        buildApplicationLinkWorkflowUnavailableDialog: function (warning, information) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.fail.process.unavailable.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("applinks-dialog-panel", applinks.templates.getInformationDialog({
                    warningHtml: AppLinks.UI.sanitiseHTML(warning),
                    informationHtml: AppLinks.UI.sanitiseHTML(information)
                }));

            return dialog;
        },
        /**
         * Show a dialog informing the user that a duplicate link has been found, stopping the process.
         * @param warning the warning message
         * @return {*}
         */
        showLocalDuplicateDialog: function (warning, creationState) {
            var dialog = AppLinks.Creation.Dialogs.buildCreateLinkErrorDialog(warning)

            // if the creation process was initiated externally then redirect back
            if (creationState.doInitiatorRedirect()) {
                dialog.addButton(AJS.I18n.getText('applinks.create.continue'), function (dialog) {
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_FAILURE, 'localDuplicateLinkExists');
                    // redirect to the initiator redirect
                    AppLinks.ExternalInitiation.redirectToInitiatorRedirect(creationState);
                    dialog.remove();
                }, 'aui-button aui-button-primary')
            } else {
                dialog.addLink(AJS.I18n.getText('applinks.close'), function (dialog) {
                    // Cancel - stay inthe current application
                    console.log('Duplicate Applink. cancelled');
                    // cancelled so clear the state
                    AppLinks.Creation.initCreationState();
                    dialog.remove();
                }, 'applinks-cancel');
            }

            dialog.show();
            dialog.updateHeight();
            return dialog;
        },
        /**
         * Build the dialog to inform the User that the url they have entered is a duplicate.
         * @param warning a warning message.
         * @return {AJS.Dialog}
         */
        buildCreateLinkErrorDialog: function (warning) {
            var dialog = new AJS.Dialog({width: 500, height: 500, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop}),
                headerMessage = AJS.I18n.getText('applinks.create.fail.title');

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("applinks-dialog-panel", applinks.templates.getInformationDialog({
                    warningHtml: AppLinks.UI.sanitiseHTML(warning),
                    informationHtml: null
                }));

            return dialog;
        },
        /**
         * Build the generic Pause dialog
         * @param headerMessage the header message.
         * @param message the mian message.
         * @return {AJS.Dialog}
         */
        buildPauseDialog: function (headerMessage, message) {
            var dialog = new AJS.Dialog({width: 400, height: 200, id: "create-applink-dialog", closeOnOutsideClick: false, keypressListener: $.noop});

            //Create application link dialog.
            dialog
                .addHeader(headerMessage)
                .addPanel("Test", applinks.templates.getPauseDialog({
                    message: AppLinks.UI.sanitiseHTML(message)
                }));

            return dialog;
        },

        // VALIDATION
        setAuthenticationFieldsAsRequired: function () {
            $('#applinks-nonual-oath-configuration').children().find("label").append("<span class='aui-icon icon-required'></span>");
        },

        /**
         * Check user defined authentication fields are valid for submission.
         * @return {boolean}
         */
        authenticationFieldsAreInValid: function () {
            //if they are all empty we don't need to validate
            if ($('#accessTokenUrl').val() == ""
                && $('#requestTokenUrl').val() == ""
                && $('#consumerKey').val() == ""
                && $('#authorizeUrl').val() == ""
                && $('#sharedSecret').val() == ""
                && $('#serviceProvider').val() == "") {
                return false;
            }

            // validate in reverse order so the error field highest up the UI has focus.
            return !AppLinks.Creation.Dialogs.validateFields(['#serviceProvider', '#consumerKey', '#sharedSecret', '#requestTokenUrl', '#accessTokenUrl', '#authorizeUrl']);
        },

        incomingNonUalFieldsAreInvalid: function () {
            var invalid = !AppLinks.Creation.Dialogs.validateFields(['#consumerKey', '#consumerName', '#publicKey']);

            return invalid;
        },

        outgoingNonUalFieldsAreValid: function () {
            if (AppLinks.Creation.Dialogs.applicationLinkFieldsAreInValid()) {
                return false;
            } else if (AppLinks.Creation.Dialogs.authenticationFieldsAreInValid()) {
                AppLinks.Creation.Dialogs.setAuthenticationFieldsAsRequired();
                return false;
            }
            return true
        },

        /**
         * Check user entered ApplicationLink fields are valid for submission.
         * @return {boolean}
         */
        applicationLinkFieldsAreInValid: function () {
            var invalid = !AppLinks.Creation.Dialogs.validateFields(['#application-name-new']);
            return invalid;
        },
        /**
         * Generic Check a field is valid and put focus on it if it isn't
         * @param id field id.
         * @returns {boolean}
         */
        validateField: function (id, setFocus) {
            if ($(id).val() == "") {
                if(setFocus) {
                    $(id).focus();
                }
                $(id).siblings('.error').show();
                return false;
            }
            $(id).siblings('.error').hide();
            return true;
        },

        /**
         * Checks an array of fields for validity. Stops after single invalid field is found.
         * @param ids
         * @returns {boolean}
         */
        validateFields: function (ids) {
            var valid = true;
            var setFocus = true;
            for (var i = 0, length = ids.length; i < length; i++) {
                valid = AppLinks.Creation.Dialogs.validateField(ids[i], setFocus);
                // once focus has been set on the first invalid field don't move it
                setFocus = valid && setFocus;
            }
            return valid;
        }

    }
})(AJS.$, require('applinks/js/util'));

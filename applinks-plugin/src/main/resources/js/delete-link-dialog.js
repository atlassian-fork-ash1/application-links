AJS.$(document).bind(AppLinks.Event.READY, function() {
    var $ = AJS.$;
    var $deleteLinkDialog = $("#delete-link-dialog");

    if (!$deleteLinkDialog.size()) {
        // this page does not have the dialog on it, therefore we don't need to initialise it.
        // pages like the permissions error page don't include this dialog.
        return;
    }

    var dialogSettings,
        showDeleteReciprocalLink,
        userHasToAuthenticate,
        authenticationUrl,
        removeLoadingIcon = function() {
            $('.delete-applink-loading').hide();
            wizard.enableSubmitBtn();
        },
        wizardSettings = {
            showButtonId: "delete-application-link",
            cancelLabel: AJS.I18n.getText('applinks.cancel'),
            oncancel: function(){
                var dialog = wizard.dialog.popup.element;
                dialog.find('h2').empty();
            },
            submitLabel: AJS.I18n.getText("applinks.dialog.delete.confirm"),
            width: 420,
            height: 330,
            id: "delete-application-link-dialog",
            aftershow: function() {

                dialogRootEl.find('.applinks-error').remove();
                var dialog = wizard.dialog.popup.element;

                var helpLink =  AppLinks.Docs.createDocLinkIcon(dialogSettings.helpKey, null, "dialog-help-link");
                dialog.find("h2").append(dialogSettings.title).append(helpLink);
                var confirmMessage = dialogSettings.confirmMessage;
                $('#delete-applink-text').html(confirmMessage);

                if (showDeleteReciprocalLink == false) {
                    var processManifest = function(manifest) {
                        var isUAL = manifest.applinksVersion && true;

                        if (isUAL) {
                            var authenticationRequired = function(authUrl) {
                                showDeleteReciprocalLink = true;
                                userHasToAuthenticate = true;
                                authenticationUrl = authUrl;
                                removeLoadingIcon();
                            };
                            var noReciprocalLinkToDelete = function(errorMsg) {
                                showDeleteReciprocalLink = false;
                                userHasToAuthenticate = false;
                                removeLoadingIcon();
                            };
                            var reciprocalLinkToDelete = function() {
                                showDeleteReciprocalLink = true;
                                userHasToAuthenticate = false;
                                removeLoadingIcon();
                            };
                            AppLinks.checkPermission(dialogSettings.doPermissionCheck, reciprocalLinkToDelete, authenticationRequired, noReciprocalLinkToDelete, AppLinks.UI.displayValidationError('delete-applink-error', dialogRootEl))
                        } else {
                            removeLoadingIcon();
                        }
                    };
                    $('.delete-applink-loading').show();
                    wizard.disableSubmitBtn(false);
                    AppLinks.SPI.getManifestFor(dialogSettings.applicationId, processManifest, AppLinks.UI.displayValidationError('delete-applink-error', dialogRootEl));
                }
            },
            onnext: function(popup) {
                $('.delete-reciprocal-applink-form').hide();
                $('.no-reciprocal-applink-form').hide();
                $('.authenticate-applink-form').hide();

                var showDeleteReciprocalLinkForm = function(){
                    $('.authenticate-applink-form').hide();
                    $('.delete-reciprocal-applink-form').show();
                    $('#delete-reciprocal-applink-text').text(dialogSettings.reciprocalLinkMessage);
                    $('.yes-delete-reciprocal-link').text(dialogSettings.deleteYesMessage);
                    $('.no-delete-reciprocal-link').text(dialogSettings.deleteNoMessage);
                    $('#yes-delete-reciprocal-link').attr('checked', true);
                    $('.delete-reciprocal-applink-form').show();
                };

                if (userHasToAuthenticate) {
                    $('.authenticate-applink-form').show();
                    $('.authenticate-message').text(dialogSettings.authenticationMessage);
                    AppLinks.SPI.addAuthenticationTrigger("#authenticate-link", authenticationUrl, {
                        before: function() {
                            popup.hide();
                        },
                        onSuccess: function() {
                            popup.show();
                            var reciprocalLinkToDelete = function() {
                                showDeleteReciprocalLinkForm();
                            }
                            var noReciprocalLinkToDelete = function(errorMsg) {
                                $('.authenticate-applink-form').hide();
                                $('.no-reciprocal-applink-form').show();
                                if (!errorMsg) {
                                    $('.no-link-message').text(dialogSettings.noReciprocalLinkMessage);
                                } else {
                                    $('.no-link-message').text(dialogSettings.failedToDetectReciprocalLinkMessage + " " + errorMsg);
                                }
                            }
                            AppLinks.checkPermission(dialogSettings.doPermissionCheck, reciprocalLinkToDelete, userHasToAuthenticate, noReciprocalLinkToDelete, AppLinks.UI.displayValidationError('delete-reciprocal-applink-error', dialogRootEl))
                        },
                        onFailure: function() {
                            popup.show();
                            $('.authenticate-applink-form').hide();
                            $('.no-reciprocal-applink-form').show();
                            $('.no-link-message').text(dialogSettings.authenticationFailedMessage);
                        }
                    });
                } else {
                  showDeleteReciprocalLinkForm();
                }
            },
            onsubmit: function(popup) {
                var success = function() {
                    wizard.enableSubmitBtn();
                    if (dialogSettings.callback()) {
                        popup.hide();
                    }
                };
                if (wizard.dialog.curpage == 1) {
                    var reciprocal = false;
                    var yesElement = $('#yes-delete-reciprocal-link');
                    var yesVisible = yesElement.is(':visible');
                    var yesChecked = yesElement.is(':checked');
                    if (yesVisible && yesChecked) {
                        reciprocal = true;
                    }
                    wizard.disableSubmitBtn();
                    dialogSettings.doDelete(reciprocal, success, AppLinks.UI.displayValidationError('delete-applink-error', dialogRootEl, function() {
                        wizard.enableSubmitBtn();
                    }));
                } else {
                    if (showDeleteReciprocalLink) {
                        wizard.nextPage();
                    } else {
                        wizard.disableSubmitBtn();
                        dialogSettings.doDelete(false, success, AppLinks.UI.displayValidationError('delete-applink-error', dialogRootEl, function() {
                            wizard.enableSubmitBtn();
                        }));
                    }
                }
                return false;
            }
        };

    var wizard = $deleteLinkDialog.wizard(wizardSettings);
    var dialogRootEl = $(wizard.dialog.popup.element);

    AppLinks.showDeleteLinkDialog = function(settings) {
        showDeleteReciprocalLink = false;
        userHasToAuthenticate = false;
        dialogSettings = settings;
        wizard.show();
    };

    AppLinks.bindDeleteLink = function(e) {
        var application = e.data.application;
        e.preventDefault();
        AppLinks.UI.hideInfoBox();
        AppLinks.SPI.getApplicationLinkInfo(application.id, function(appLinkInfo) {
            var confirmMessage = $('<div>');
            confirmMessage.append($('<div>').text(AJS.I18n.getText('applinks.dialog.delete.application.link', application.name, AppLinks.I18n.getApplicationTypeName(application.typeId))));
            if (appLinkInfo.configuredAuthProviders.length > 0){
                var authProviderMessage = AppLinks.UI.prettyJoin(appLinkInfo.configuredAuthProviders, function(value) {
                    return AppLinks.I18n.getAuthenticationTypeName(value);
                });
                confirmMessage.append($('<div>').text(AJS.I18n.getText('applinks.dialog.delete.authentication', authProviderMessage)));

                if (appLinkInfo.numConfiguredEntities > 0) {
                    var linkI18nKey;
                    if (appLinkInfo.numConfiguredEntities > 1){
                        linkI18nKey = "applinks.dialog.delete.entity.links.plural";
                    } else {
                        linkI18nKey = "applinks.dialog.delete.entity.links.singular";
                    }

                    var hostEntityTypeLabel = AppLinks.UI.prettyJoin(appLinkInfo.hostEntityTypes, function(value) {
                        if (appLinkInfo.numConfiguredEntities > 1) {
                            return AppLinks.I18n.getPluralizedEntityTypeName(value);
                        } else {
                            return AppLinks.I18n.getEntityTypeName(value);
                        }
                    });
                    var remoteEntityTypeLabel = AppLinks.UI.prettyJoin(appLinkInfo.remoteEntityTypes, function(value) {
                        return AppLinks.I18n.getPluralizedEntityTypeName(value);
                    });
                    var entityLinksMsg = AJS.I18n.getText(linkI18nKey, appLinkInfo.numConfiguredEntities, hostEntityTypeLabel, remoteEntityTypeLabel, application.name);
                    confirmMessage.append($('<div>').text(entityLinksMsg));
                }
            }
            var deleteDialogSettings = {
                title: AJS.I18n.getText("applinks.delete.long", application.name),
                applicationId: application.id,
                confirmMessage: confirmMessage,
                reciprocalLinkMessage: AJS.I18n.getText("applinks.dialog.delete.reciprocal.application.link", application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                noReciprocalLinkMessage: AJS.I18n.getText("applinks.dialog.delete.reciprocal.no.application.link", application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                deleteYesMessage: AJS.I18n.getText("applinks.dialog.delete.reciprocal.application.link.yes", application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                deleteNoMessage: AJS.I18n.getText("applinks.dialog.delete.reciprocal.application.link.no", application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                authenticationMessage: AJS.I18n.getText('applinks.dialog.authentication.required.application.link', application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                noConnectionMessage: AJS.I18n.getText('applinks.dialog.delete.link.no.connection'),
                authenticationFailedMessage: AJS.I18n.getText('applinks.dialog.application.link.authorization.failed', application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                failedToDetectReciprocalLinkMessage : AJS.I18n.getText('applinks.dialog.application.failed.to.detect.reciprocal.link', application.name, AppLinks.I18n.getApplicationTypeName(application.typeId)),
                helpKey: 'applinks.docs.delete.application.link',
                doPermissionCheck: function(success, error) {
                    AppLinks.SPI.canDeleteAppLink(application.id, success, error);
                },
                doDelete: function(reciprocate, success, error) {
                    AppLinks.SPI.deleteLink(application, reciprocate, success, error);
                },
                callback: function() {
                    AppLinks.UI.listApplicationLinks(application.name, 'delete');
                    return true;
                }
            };
            AppLinks.showDeleteLinkDialog(deleteDialogSettings);
        });
    }
});

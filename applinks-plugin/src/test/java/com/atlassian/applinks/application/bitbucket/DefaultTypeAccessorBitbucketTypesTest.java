package com.atlassian.applinks.application.bitbucket;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketProjectEntityType;
import com.atlassian.applinks.api.application.stash.StashApplicationType;
import com.atlassian.applinks.api.application.stash.StashProjectEntityType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.DefaultTypeAccessor;
import com.atlassian.applinks.core.plugin.ApplicationTypeModuleDescriptor;
import com.atlassian.applinks.core.plugin.EntityTypeModuleDescriptor;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultTypeAccessorBitbucketTypesTest {
    // for DefaultTypeAccessor
    @Mock
    private PluginAccessor pluginAccessor;
    @Spy // support @InjectMocks
    private PluginEventManager pluginEventManager = new DefaultPluginEventManager();

    // for ApplicationTypeModuleDescriptor
    @Mock
    private Plugin plugin;
    @Mock
    private ModuleFactory moduleFactory;

    // for BitbucketApplicationTypeImpl and BitbucketProjectEntityTypeImpl
    @Mock
    private AppLinkPluginUtil appLinkPluginUtil;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;
    @Mock
    private ApplinksFeatureService applinksFeatureService;

    @InjectMocks
    private DefaultTypeAccessor typeAccessor;
    @InjectMocks
    private ApplicationTypeModuleDescriptor bitbucketApplicationTypeDescriptor;
    @InjectMocks
    private EntityTypeModuleDescriptor bitbucketEntityTypeDescriptor;

    @InjectMocks
    private BitbucketApplicationTypeImpl bitbucketApplicationType;
    @InjectMocks
    private BitbucketProjectEntityTypeImpl bitbucketProjectEntityType;

    @Before
    public void setUpBitbucketApplicationTypeDescriptor() {
        bitbucketApplicationTypeDescriptor.init(plugin, createBitbucketApplicationTypeElement());
        bitbucketApplicationTypeDescriptor.enabled();

        when(moduleFactory.createModule(BitbucketApplicationTypeImpl.class.getName(),
                bitbucketApplicationTypeDescriptor)).thenReturn(bitbucketApplicationType);

        pluginEventManager.broadcast(new PluginModuleEnabledEvent(bitbucketApplicationTypeDescriptor));
    }

    @Before
    public void setUpBitbucketEntityTypeDescriptor() {
        bitbucketEntityTypeDescriptor.init(plugin, createBitbucketEntityTypeElement());
        bitbucketEntityTypeDescriptor.enabled();

        when(moduleFactory.createModule(BitbucketProjectEntityTypeImpl.class.getName(), bitbucketEntityTypeDescriptor))
                .thenReturn(bitbucketProjectEntityType);

        pluginEventManager.broadcast(new PluginModuleEnabledEvent(bitbucketEntityTypeDescriptor));
    }

    @Test
    public void shouldReturnBitbucketApplicationTypeGivenQueryForStash() {
        assertSame(bitbucketApplicationType, typeAccessor.getApplicationType(StashApplicationType.class));
    }

    @Test
    public void shouldReturnBitbucketApplicationTypeGivenQueryForBitbucket() {
        assertSame(bitbucketApplicationType, typeAccessor.getApplicationType(BitbucketApplicationType.class));
    }

    @Test
    public void shouldReturnBitbucketEntityTypeGivenQueryForStashEntityType() {
        assertSame(bitbucketProjectEntityType, typeAccessor.getEntityType(StashProjectEntityType.class));
    }

    @Test
    public void shouldReturnBitbucketEntityTypeGivenQueryForBitbucketEntityType() {
        assertSame(bitbucketProjectEntityType, typeAccessor.getEntityType(BitbucketProjectEntityType.class));
    }

    @Test
    public void shouldReturnBitbucketEntityTypeGivenQueryByApplicationType() {
        assertThat(typeAccessor.getEnabledEntityTypesForApplicationType(bitbucketApplicationType),
                Matchers.<EntityType>contains(bitbucketProjectEntityType));
    }

    /**
     * Creates a DOM4J representation of the Bitbucket Application Type XML module descriptor, equivalent to the
     * {@code applinks-application-type name="bitbucket"} element in the Applinks plugin descriptor
     * (see {@code atlassian-plugin.xml}).
     *
     * @return DOM4J {@code element} corresponding to Bitbucket Application Type module descriptor
     */
    private static Element createBitbucketApplicationTypeElement() {
        Element appElement = DocumentFactory.getInstance().createElement("applinks-application-type");
        appElement.addAttribute("key", "bitbucket");
        appElement.addAttribute("class", BitbucketApplicationTypeImpl.class.getName());

        appElement.add(createInterfaceElement(StashApplicationType.class.getName()));
        appElement.add(createInterfaceElement(BitbucketApplicationType.class.getName()));

        appElement.add(createManifestProducerElement(BitbucketManifestProducer.class.getName()));

        return appElement;
    }

    /**
     * Creates a DOM4J representation of the Bitbucket Project Type XML module descriptor, equivalent to the
     * {@code applinks-entity-type name="bitbucket-project"} element in the Applinks plugin descriptor
     * (see {@code atlassian-plugin.xml}).
     *
     * @return DOM4J {@code element} corresponding to Bitbucket Project Type module descriptor
     */
    private static Element createBitbucketEntityTypeElement() {
        Element projectElement = DocumentFactory.getInstance().createElement("applinks-entity-type");
        projectElement.addAttribute("key", "bitbucket-project");
        projectElement.addAttribute("class", BitbucketProjectEntityTypeImpl.class.getName());

        projectElement.add(createInterfaceElement(StashProjectEntityType.class.getName()));
        projectElement.add(createInterfaceElement(BitbucketProjectEntityType.class.getName()));

        return projectElement;
    }

    private static Element createInterfaceElement(String interfaceName) {
        Element interfaceElement = DocumentFactory.getInstance().createElement("interface");
        interfaceElement.setText(interfaceName);
        return interfaceElement;
    }

    private static Element createManifestProducerElement(String manifestProducerClass) {
        Element producerElement = DocumentFactory.getInstance().createElement("manifest-producer");
        producerElement.addAttribute("class", manifestProducerClass);
        return producerElement;
    }
}

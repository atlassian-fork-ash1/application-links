package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.json.jsonorg.JSONObject;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableSet;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static java.lang.String.format;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class EventWhiteListTest {
    private static final String UAL_HELP_FILE = "com/atlassian/applinks/ual-help-paths.properties";
    private static final String APPLINKS_VIEW_DOCUMENTATION = "applinks.view.documentation";

    private static JSONObject whiteList;

    @BeforeClass
    public static void beforeClass() throws IOException {
        InputStream stream = EventWhiteListTest.class.getClassLoader().getResourceAsStream("whitelist/applinks-whitelist.json");
        String data = IOUtils.toString(stream, "UTF-8");
        whiteList = new JSONObject(data);
        stream.close();
    }

    @Test
    public void whiteListTest() throws IOException, NoSuchFieldException, IntrospectionException {
        assertEventInWhiteList(ApplinksAdminViewEvent.class);
        assertEventInWhiteList(ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent.class);
        assertEventInWhiteList(ApplinkStatusEventBuilderFactory.ApplinkStatusMainEvent.class);
        assertEventInWhiteList(ApplinksCreatedEventFactory.ApplinksCreatedEvent.class);
    }

    @Test
    public void helpAnalyticKeysShouldUseTheSameKeysAsUalHelpPaths() throws IOException {
        assertTrue(whiteList.has(APPLINKS_VIEW_DOCUMENTATION));
        Properties ualHelpLinks = new Properties();
        try (final InputStream resources = EventWhiteListTest.class.getClassLoader().getResourceAsStream(UAL_HELP_FILE)) {
            ualHelpLinks.load(resources);
        }
        final Set<String> analyticsLinkKeys = getAnalyticsLinkKeys();
        for (String linkKey : analyticsLinkKeys) {
            assertThat(format("Analytics key '%s' does not exist in %s", linkKey, UAL_HELP_FILE),
                    ualHelpLinks.keySet(), hasItem(linkKey));
        }
        for (String key : ualHelpLinks.stringPropertyNames()) {
            assertThat(format("Help link key '%s' does not exist in the whitelist", key),
                    analyticsLinkKeys, hasItem(key));
        }
    }

    private static Set<String> getAnalyticsLinkKeys() {
        JSONArray linkKeysJson = ((JSONObject) (whiteList.getJSONArray(APPLINKS_VIEW_DOCUMENTATION).get(0)))
                .getJSONArray("linkKey");
        return toStream(linkKeysJson).map(Object::toString).collect(toImmutableSet());
    }

    private void assertEventInWhiteList(final Class<?> eventClass) throws NoSuchFieldException, IntrospectionException {
        String eventName = getEventName(eventClass);
        assertTrue("missing in whitelist event: " + eventName, whiteList.has(eventName));
        JSONArray jsonArray = whiteList.getJSONArray(eventName);
        matchEventFieldsWithWhiteListFields(jsonArray, eventClass);
    }

    private void matchEventFieldsWithWhiteListFields(JSONArray jsonArray, final Class<?> eventClass) throws NoSuchFieldException, IntrospectionException {
        PropertyDescriptor[] pds = Introspector.getBeanInfo(eventClass).getPropertyDescriptors();
        for (Object jsonObject : jsonArray) {
            String fieldName = "";
            if (jsonObject instanceof String) {
                fieldName = jsonObject.toString();
            } else if (jsonObject instanceof JSONObject) {
                fieldName = ((JSONObject) jsonObject).keys().next();
            }
            if (!hasGetter(pds, fieldName)) {
                fail(eventClass.getName() + " is missing getter method for field '" + fieldName + "' declared in whitelist");
            }
        }
    }

    private boolean hasGetter(PropertyDescriptor[] pds, String fieldName) throws IntrospectionException {
        for (PropertyDescriptor pd : pds) {
            if (pd.getName().equals(fieldName) && pd.getReadMethod() != null) {
                return true;
            }
        }
        return false;
    }

    private String getEventName(Class<?> eventClass) {
        EventName eventName = eventClass.getAnnotation(EventName.class);
        assertNotNull("Missing EventName annotation for class " + eventClass.getName(), eventName);
        return eventName.value();
    }
}

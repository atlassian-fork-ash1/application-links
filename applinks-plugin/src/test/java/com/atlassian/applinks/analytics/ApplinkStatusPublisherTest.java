package com.atlassian.applinks.analytics;

import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.event.api.EventPublisher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplinkStatusPublisherTest {

    @Mock
    private ReadOnlyApplicationLinkService readOnlyApplicationLinkService;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ApplinkStatusEventBuilderFactory applinkStatusEventBuilderFactory;
    @Mock
    private ApplinkStatusEventBuilderFactory.Builder builder;
    @Mock
    private ReadOnlyApplicationLink applink1;
    @Mock
    private ReadOnlyApplicationLink applink2;
    @Mock
    private ApplinkStatusEventBuilderFactory.ApplinkStatusMainEvent mainEvent;
    @Mock
    private ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent applinksEvent1;
    @Mock
    private ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent applinksEvent2;

    @InjectMocks
    private ApplinkStatusPublisher publisher;

    @Captor
    private ArgumentCaptor<ReadOnlyApplicationLink> captor;

    @Before
    public void setUp() {
        when(applinkStatusEventBuilderFactory.createBuilder()).thenReturn(builder);
        when(readOnlyApplicationLinkService.getApplicationLinks()).thenReturn(Arrays.asList(applink1, applink2));
        when(builder.buildMainEvent()).thenReturn(mainEvent);
        when(builder.buildApplinkEvents()).thenReturn(Arrays.asList(applinksEvent1, applinksEvent2));
    }


    @Test
    public void shouldCreateABuilder() {
        // invoke
        publisher.publishApplinkStatus();

        // check
        verify(applinkStatusEventBuilderFactory, times(1)).createBuilder();
    }

    @Test
    public void shouldCallBuilderWithEveryApplink() {
        // invoke
        publisher.publishApplinkStatus();

        // check
        verify(builder, times(2)).addApplink(captor.capture());
        assertSame(captor.getAllValues().get(0), applink1);
        assertSame(captor.getAllValues().get(1), applink2);
    }

    @Test
    public void shouldCallEventPublisherWithEvents() {
        // invoke
        publisher.publishApplinkStatus();

        // check
        verify(eventPublisher, times(3)).publish(captor.capture());
        assertSame(captor.getAllValues().get(0), mainEvent);
        assertSame(captor.getAllValues().get(1), applinksEvent1);
        assertSame(captor.getAllValues().get(2), applinksEvent2);
    }
}
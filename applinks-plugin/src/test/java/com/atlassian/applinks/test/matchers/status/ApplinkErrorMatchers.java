package com.atlassian.applinks.test.matchers.status;

import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ResponseApplinkError;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * @since 5.0
 */
public final class ApplinkErrorMatchers {
    private ApplinkErrorMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<ApplinkError> withTypeThat(@Nonnull Matcher<ApplinkErrorType> errorTypeMatcher) {
        return new FeatureMatcher<ApplinkError, ApplinkErrorType>(errorTypeMatcher, "error type that", "errorType") {
            @Override
            protected ApplinkErrorType featureValueOf(ApplinkError error) {
                return error.getType();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplinkError> withType(@Nullable ApplinkErrorType expectedErrorType) {
        return withTypeThat(is(expectedErrorType));
    }

    @Nonnull
    public static Matcher<ApplinkError> withDetailsThat(@Nonnull Matcher<String> errorDetailsMatcher) {
        return new FeatureMatcher<ApplinkError, String>(errorDetailsMatcher, "error details that", "errorDetails") {
            @Override
            protected String featureValueOf(ApplinkError error) {
                return error.getDetails();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplinkError> withDetails(@Nullable String expectedErrorDetails) {
        return withDetailsThat(is(expectedErrorDetails));
    }

    public static Matcher<ResponseApplinkError> responseErrorWith(@Nullable Response.Status status,
                                                                  @Nullable String body, @Nullable String contentType) {
        return allOf(withStatus(status), withBody(body), withContentType(contentType));
    }

    public static Matcher<ResponseApplinkError> responseErrorWithNoBody(@Nullable Response.Status status) {
        return responseErrorWith(status, null, null);
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withStatusThat(@Nonnull Matcher<Response.Status> statusMatcher) {
        return new FeatureMatcher<ResponseApplinkError, Response.Status>(statusMatcher, "status that", "status") {
            @Override
            protected Response.Status featureValueOf(ResponseApplinkError error) {
                return Response.Status.fromStatusCode(error.getStatusCode());
            }
        };
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withStatus(@Nullable Response.Status expectedStatus) {
        return withStatusThat(is(expectedStatus));
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withBodyThat(@Nonnull Matcher<String> bodyMatcher) {
        return new FeatureMatcher<ResponseApplinkError, String>(bodyMatcher, "body that", "body") {
            @Override
            protected String featureValueOf(ResponseApplinkError error) {
                return error.getBody();
            }
        };
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withBody(@Nullable String expectedBody) {
        return withBodyThat(is(expectedBody));
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withContentTypeThat(@Nonnull Matcher<String> contentTypeMatcher) {
        return new FeatureMatcher<ResponseApplinkError, String>(contentTypeMatcher, "content type that", "contentType") {
            @Override
            protected String featureValueOf(ResponseApplinkError error) {
                return error.getContentType();
            }
        };
    }

    @Nonnull
    public static Matcher<ResponseApplinkError> withContentType(@Nullable String expectedContentType) {
        return withContentTypeThat(is(expectedContentType));
    }
}

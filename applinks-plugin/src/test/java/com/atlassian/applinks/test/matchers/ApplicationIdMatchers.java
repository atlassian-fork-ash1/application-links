package com.atlassian.applinks.test.matchers;

import java.util.UUID;

import com.atlassian.applinks.api.ApplicationId;

import static com.atlassian.applinks.test.matchers.MatchByMethodReturnValue.hasMethodWithReturnValue;

/**
 * {@link com.atlassian.applinks.api.ApplicationId} specific matchers.
 */
public class ApplicationIdMatchers {
    /**
     * Match using the return value of {@link com.atlassian.applinks.api.ApplicationId.get()}
     *
     * @param id the id value to match to.
     */
    public static ApplicationId matchApplicationIdById(UUID id) {
        return matchApplicationIdById(id.toString());
    }

    /**
     * Match using the return value of {@link com.atlassian.applinks.api.ApplicationId.get()}
     *
     * @param id the id value to match to.
     */
    public static ApplicationId matchApplicationIdById(String id) {
        return hasMethodWithReturnValue("get", id, ApplicationId.class);
    }

    /**
     * Match using the return value of {@link com.atlassian.applinks.api.ApplicationId.get()}
     *
     * @param id the id value to match to.
     */
    public static ApplicationId matchApplicationIdById(ApplicationId id) {
        return hasMethodWithReturnValue("get", id.get(), ApplicationId.class);
    }
}

package com.atlassian.applinks.test.matchers;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.security.PublicKey;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

public final class ConsumerMatchers {
    private ConsumerMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Consumer> withName(@Nullable String expectedName) {
        return withNameThat(is(expectedName));
    }

    @Nonnull
    public static Matcher<Consumer> withNameThat(@Nonnull Matcher<String> nameMatcher) {
        return new FeatureMatcher<Consumer, String>(nameMatcher, "name", "name") {
            @Override
            protected String featureValueOf(Consumer consumer) {
                return consumer.getName();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withKey(@Nullable String expectedKey) {
        return withKeyThat(is(expectedKey));
    }

    @Nonnull
    public static Matcher<Consumer> withKeyThat(@Nonnull Matcher<String> keyMatcher) {
        return new FeatureMatcher<Consumer, String>(keyMatcher, "key", "key") {
            @Override
            protected String featureValueOf(Consumer consumer) {
                return consumer.getKey();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withSignatureMethod(@Nullable SignatureMethod signatureMethod) {
        return new FeatureMatcher<Consumer, SignatureMethod>(is(signatureMethod), "signatureMethod", "signatureMethod") {
            @Override
            protected SignatureMethod featureValueOf(Consumer consumer) {
                return consumer.getSignatureMethod();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withPublicKeyThat(@Nonnull Matcher<PublicKey> publicKeyMatcher) {
        return new FeatureMatcher<Consumer, PublicKey>(publicKeyMatcher, "publicKey", "publicKey") {
            @Override
            protected PublicKey featureValueOf(Consumer consumer) {
                return consumer.getPublicKey();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withConfig(boolean threLoAllowed, boolean twoLoAllowed,
                                               boolean twoLoImpersonationAllowed) {
        return allOf(
                withThreeLoAllowed(threLoAllowed),
                withTwoLoAllowed(twoLoAllowed),
                withTwoLoImpersonationAllowed(twoLoImpersonationAllowed)
        );
    }

    @Nonnull
    public static Matcher<Consumer> withThreeLoAllowed(boolean allowed) {
        return new FeatureMatcher<Consumer, Boolean>(is(allowed), "threeLOAllowed", "threeLOAllowed") {
            @Override
            protected Boolean featureValueOf(Consumer consumer) {
                return consumer.getThreeLOAllowed();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withTwoLoAllowed(boolean allowed) {
        return new FeatureMatcher<Consumer, Boolean>(is(allowed), "twoLoAllowed", "twoLoAllowed") {
            @Override
            protected Boolean featureValueOf(Consumer consumer) {
                return consumer.getTwoLOAllowed();
            }
        };
    }

    @Nonnull
    public static Matcher<Consumer> withTwoLoImpersonationAllowed(boolean allowed) {
        return new FeatureMatcher<Consumer, Boolean>(is(allowed), "twoLoImpersonationAllowed",
                "twoLoImpersonationAllowed") {
            @Override
            protected Boolean featureValueOf(Consumer consumer) {
                return consumer.getTwoLOImpersonationAllowed();
            }
        };
    }
}

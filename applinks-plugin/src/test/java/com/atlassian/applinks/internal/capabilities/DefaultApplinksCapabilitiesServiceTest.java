package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.rule.SystemPropertyRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class DefaultApplinksCapabilitiesServiceTest {
    @Rule
    public final SystemPropertyRule systemPropertyRule = new SystemPropertyRule();

    private DefaultApplinksCapabilitiesService capabilitiesService = new DefaultApplinksCapabilitiesService();

    @Test
    public void allCapabilitiesEnabledByDefault() {
        Set<ApplinksCapabilities> capabilities = capabilitiesService.getCapabilities();

        assertEquals(capabilities, allCapabilities());
    }

    @Test
    public void supportsDisablingCapabilitiesUsingSystemProperties() {
        systemPropertyRule.setProperty(ApplinksCapabilities.STATUS_API.key, "false");

        Set<ApplinksCapabilities> capabilities = capabilitiesService.getCapabilities();

        assertEquals(capabilities, allCapabilitiesWithout(ApplinksCapabilities.STATUS_API));
    }

    @Test
    public void supportsDisablingCapabilitiesUsingSystemPropertiesCaseInsensitive() {
        systemPropertyRule.setProperty(ApplinksCapabilities.STATUS_API.key, "fAlSe");

        Set<ApplinksCapabilities> capabilities = capabilitiesService.getCapabilities();

        assertEquals(capabilities, allCapabilitiesWithout(ApplinksCapabilities.STATUS_API));
    }

    @Test
    public void doesNotDisableCapabilityForSystemPropertyValueDifferentThanFalse() {
        systemPropertyRule.setProperty(ApplinksCapabilities.STATUS_API.key, "falsy");

        Set<ApplinksCapabilities> capabilities = capabilitiesService.getCapabilities();

        assertEquals(capabilities, allCapabilities());
    }

    private static EnumSet<ApplinksCapabilities> allCapabilities() {
        return EnumSet.allOf(ApplinksCapabilities.class);
    }

    private static EnumSet<ApplinksCapabilities> allCapabilitiesWithout(ApplinksCapabilities excludedCapability) {
        EnumSet<ApplinksCapabilities> answer = allCapabilities();
        answer.remove(excludedCapability);
        return answer;
    }
}

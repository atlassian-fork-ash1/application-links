package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseStatusException;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OAuthConfigMigratorTest {
    @Mock
    private OAuthAutoConfigurator autoConfigurator;
    @Mock
    private ApplicationLinkRequestFactory factory;
    @Mock
    private ApplicationLink link;
    @Mock
    private Response response;
    private OAuthConfigMigrator authConfigurator;
    private OAuthConfig incoming;
    private OAuthConfig ougoing;

    @Before
    public void setUp() {
        incoming = OAuthConfig.createDefaultOAuthConfig();
        ougoing = OAuthConfig.createOAuthWithImpersonationConfig();

        authConfigurator = new OAuthConfigMigrator(autoConfigurator, incoming, ougoing);
    }

    @Test
    public void execute() throws Exception {
        when(link.createAuthenticatedRequestFactory(BasicAuthenticationProvider.class)).thenReturn(factory);

        assertTrue(authConfigurator.execute(link, new ApplicationId(UUID.randomUUID().toString()), BasicAuthenticationProvider.class));
        verify(autoConfigurator).enable(incoming, ougoing, link, factory);
    }

    @Test
    public void executeResultsInForbiddenExceptionShouldReturnsFalse() throws Exception {
        when(response.getStatusCode()).thenReturn(HttpStatus.SC_FORBIDDEN);
        when(link.createAuthenticatedRequestFactory(BasicAuthenticationProvider.class)).thenReturn(factory);
        ResponseStatusException responseStatusException = new ResponseStatusException("", response);
        doThrow(new AuthenticationConfigurationException("", responseStatusException)).when(autoConfigurator).enable(incoming, ougoing, link, factory);

        assertFalse(authConfigurator.execute(link, new ApplicationId(UUID.randomUUID().toString()), BasicAuthenticationProvider.class));
    }

    @Test
    public void executeResultsInUnauthorizedExceptionShouldReturnsFalse() throws Exception {
        when(response.getStatusCode()).thenReturn(HttpStatus.SC_UNAUTHORIZED);
        when(link.createAuthenticatedRequestFactory(BasicAuthenticationProvider.class)).thenReturn(factory);
        ResponseStatusException responseStatusException = new ResponseStatusException("", response);
        doThrow(new AuthenticationConfigurationException("", responseStatusException)).when(autoConfigurator).enable(incoming, ougoing, link, factory);

        assertFalse(authConfigurator.execute(link, new ApplicationId(UUID.randomUUID().toString()), BasicAuthenticationProvider.class));
    }

    @Test(expected = AuthenticationConfigurationException.class)
    public void executeResultsInNotFoundExceptionShouldThrowException() throws Exception {
        when(response.getStatusCode()).thenReturn(HttpStatus.SC_NOT_FOUND);
        when(link.createAuthenticatedRequestFactory(BasicAuthenticationProvider.class)).thenReturn(factory);
        ResponseStatusException responseStatusException = new ResponseStatusException("", response);
        doThrow(new AuthenticationConfigurationException("", responseStatusException)).when(autoConfigurator).enable(incoming, ougoing, link, factory);

        authConfigurator.execute(link, new ApplicationId(UUID.randomUUID().toString()), BasicAuthenticationProvider.class);
    }

    @Test
    public void executeWithInvalidFactory() throws Exception {
        when(link.createAuthenticatedRequestFactory(BasicAuthenticationProvider.class)).thenReturn(null);
        assertFalse(authConfigurator.execute(link, new ApplicationId(UUID.randomUUID().toString()), BasicAuthenticationProvider.class));
    }
}
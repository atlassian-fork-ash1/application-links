package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseTransportException;
import org.junit.Test;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RemoteNetworkExceptionTest {
    @Test
    public void withUnderlyingConnectException() {
        SocketException underlyingCause = new ConnectException("Connection refused");
        RemoteNetworkException exception = new RemoteNetworkException(ApplinkErrorType.CONNECTION_REFUSED,
                ConnectException.class, "Test", new ResponseTransportException(underlyingCause));

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, exception.getType());
        assertEquals("java.net.ConnectException: Connection refused", exception.getDetails());
    }

    @Test
    public void withUnderlyingUnknownHostException() {
        UnknownHostException underlyingCause = new UnknownHostException("Unknown host");
        RemoteNetworkException exception = new RemoteNetworkException(ApplinkErrorType.UNKNOWN_HOST,
                UnknownHostException.class, "Test", new ResponseException(underlyingCause));

        assertEquals(ApplinkErrorType.UNKNOWN_HOST, exception.getType());
        assertEquals("java.net.UnknownHostException: Unknown host", exception.getDetails());
    }

    @Test
    public void withUnderlyingErrorThatDoesNotMatch() {
        RuntimeException underlyingCause = new RuntimeException("Surprise!");
        RemoteNetworkException exception = new RemoteNetworkException(ApplinkErrorType.CONNECTION_REFUSED,
                ConnectException.class, "Test", new ResponseTransportException("Connection refused", underlyingCause));

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, exception.getType());
        assertNull(null, exception.getDetails());
    }

    @Test
    public void withNoCausalChain() {
        RemoteNetworkException exception = new RemoteNetworkException(ApplinkErrorType.CONNECTION_REFUSED,
                ConnectException.class, "Test", new RuntimeException("Test"));

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, exception.getType());
        assertNull(null, exception.getDetails());
    }
}

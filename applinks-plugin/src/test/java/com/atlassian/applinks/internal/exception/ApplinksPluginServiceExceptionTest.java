package com.atlassian.applinks.internal.exception;

import com.atlassian.applinks.internal.common.exception.DefaultServiceExceptionFactoryTest;
import org.junit.Test;

public class ApplinksPluginServiceExceptionTest {
    /**
     * Re-run the service exception test in the applinks plugin to cover any service exception classes defined here
     *
     * @throws Exception because it's a test
     */
    @Test
    public void testAllServiceExceptionsInApplinkPluginHaveMatchingI18n() throws Exception {
        DefaultServiceExceptionFactoryTest.testAllServiceExceptionsHaveMatchingI18n(
                "/com/atlassian/applinks/internal/common/applinks-service-common.properties",
                "/com/atlassian/applinks/applinks-service.properties"
        );
    }
}

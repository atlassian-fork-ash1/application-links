package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import com.atlassian.sal.api.net.Response;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RemoteActionHandlerTest {
    @Mock
    private Response response;

    @Test
    public void handle() throws Exception {
        assertTrue(handle(HttpStatus.SC_CREATED).isSuccessful());
        assertTrue(handle(HttpStatus.SC_ACCEPTED).isSuccessful());
        assertTrue(handle(HttpStatus.SC_CREATED).isSuccessful());
        assertFalse(handle(HttpStatus.SC_MOVED_TEMPORARILY).isSuccessful());
        assertFalse(handle(HttpStatus.SC_MOVED_PERMANENTLY).isSuccessful());
        assertFalse(handle(HttpStatus.SC_NOT_FOUND).isSuccessful());
        assertFalse(handle(HttpStatus.SC_CONTINUE).isSuccessful());
    }

    private RemoteActionHandler handle(int statusCode) throws ResponseException {
        final RemoteActionHandler handler = new RemoteActionHandler();
        when(response.getResponseBodyAsString()).thenReturn("");
        when(response.getStatusCode()).thenReturn(statusCode);
        handler.handle(response);
        return handler;
    }
}
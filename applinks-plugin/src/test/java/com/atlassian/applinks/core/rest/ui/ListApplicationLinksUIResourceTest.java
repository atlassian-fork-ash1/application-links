package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.core.concurrent.ConcurrentExecutor;
import com.atlassian.applinks.core.rest.RestResourceTestUtils;
import com.atlassian.applinks.core.rest.model.LinkAndAuthProviderEntity;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import javax.annotation.Nullable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ListApplicationLinksUIResourceTest {
    @Mock
    MutableApplicationLink applicationLink1;
    @Mock
    MutableApplicationLink applicationLink2;
    @Mock
    LinkAndAuthProviderEntity linkAndAuthProviderEntity;
    @Mock
    MutatingApplicationLinkService applicationLinkService;
    @Mock
    ConcurrentExecutor executor;
    @Mock
    Future future1;
    @Mock
    Future future2;
    private ApplicationType dummyType;
    private ListApplicationLinksUIResource resource;

    @Before
    public void setUp() throws Exception {
        dummyType = RestResourceTestUtils.getDummyApplicationType();
        URI uri = URI.create("http://localhost");

        when(applicationLink1.getId()).thenReturn(ApplicationIdUtil.generate(uri));
        when(applicationLink1.getType()).thenReturn(dummyType);
        when(applicationLink1.getName()).thenReturn("name1");
        when(applicationLink1.getDisplayUrl()).thenReturn(uri);
        when(applicationLink1.getRpcUrl()).thenReturn(uri);

        when(applicationLink2.getId()).thenReturn(ApplicationIdUtil.generate(uri));
        when(applicationLink2.getType()).thenReturn(dummyType);
        when(applicationLink2.getName()).thenReturn("name2");
        when(applicationLink2.getDisplayUrl()).thenReturn(uri);
        when(applicationLink2.getRpcUrl()).thenReturn(uri);

        when(future1.get()).thenReturn(null);
        when(future2.get()).thenReturn(linkAndAuthProviderEntity);

        when(executor.invokeAll(any(Collection.class))).thenAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<Callable<LinkAndAuthProviderEntity>> list = (List<Callable<LinkAndAuthProviderEntity>>) invocationOnMock.getArguments()[0];
                return Lists.transform(list, new Function<Callable<LinkAndAuthProviderEntity>, Object>() {
                    public Object apply(@Nullable Callable<LinkAndAuthProviderEntity> from) {
                        try {
                            if (from.call() != null) {
                                return future2;
                            }
                            return future1;
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            }
        });

        resource = new ListApplicationLinksUIResource(applicationLinkService, null, null, null, null, null, null, null, null, executor) {
            @Override
            protected LinkAndAuthProviderEntity getLinkAndAuthProviderEntity(ApplicationLink applicationLink) {
                if (applicationLink.getName().equals("name1")) {
                    throw new RuntimeException("failed getting the manifest");
                }
                return linkAndAuthProviderEntity;
            }
        };
    }

    @Test
    public void createJobsDoesntLeakExceptionIfRetrievingManifestHasProblems() throws Exception {
        List<Callable<LinkAndAuthProviderEntity>> list = resource.createJobs(ImmutableList.<ApplicationLink>of(applicationLink1, applicationLink2));

        assertEquals(2, list.size());
        assertNotNull(list.get(0));
        assertNotNull(list.get(1));
    }

    @Test
    public void retrieveLinkAndAuthProviderEntityListNeverReturnsNullElements() throws Exception {
        when(applicationLinkService.getApplicationLinks()).thenReturn(ImmutableList.<ApplicationLink>of(applicationLink1, applicationLink2));
        List<LinkAndAuthProviderEntity> list = resource.retrieveLinkAndAuthProviderEntityList();
        assertEquals(1, list.size());
        assertSame(linkAndAuthProviderEntity, list.get(0));
    }
}
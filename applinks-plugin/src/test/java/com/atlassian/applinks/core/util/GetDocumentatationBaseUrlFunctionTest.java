package com.atlassian.applinks.core.util;


import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GetDocumentatationBaseUrlFunctionTest {
    @Mock
    DocumentationLinker documentationLinker;

    @Test
    public void testGenerate() throws URISyntaxException {
        when(documentationLinker.getDocumentationBaseUrl()).thenReturn(new URI("http://test"));
        JsExpression generatedExpression = new GetDocumentationBaseUrlFunction(documentationLinker).generate();
        assertEquals(generatedExpression.getText(), "'http://test'");
    }
}

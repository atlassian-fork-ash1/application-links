package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.auth.AuthenticationConfigurator;
import com.atlassian.applinks.core.rest.client.EntityLinkClient;
import com.atlassian.applinks.core.rest.model.ConfigurationFormValuesEntity;
import com.atlassian.applinks.core.rest.model.ErrorListEntity;
import com.atlassian.applinks.core.rest.model.UpgradeApplicationLinkRequestEntity;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AutoConfiguringAuthenticatorProviderPluginModule;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UpgradeApplicationLinkUIResourceTest {
    @Mock
    MutatingApplicationLinkService applicationLinkService;
    @Mock
    RequestFactory requestFactory;
    @Mock
    InternalHostApplication internalHostApplication;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    InternalTypeAccessor typeAccessor;
    @Mock
    ManifestRetriever manifestRetriever;
    @Mock
    RestUrlBuilder restUrlBuilder;
    @Mock
    PluginAccessor pluginAccessor;
    @Mock
    UserManager userManager;
    @Mock
    MutatingEntityLinkService entityLinkService;
    @Mock
    AuthenticationConfigurator authenticationConfigurator;
    @Mock
    AuthenticationConfigurationManager authenticationConfigurationManager;
    @Mock
    EventPublisher eventPublisher;
    @Mock
    EntityLinkClient entityLinkClient;

    @Mock
    MutableApplicationLink applicationLink;

    @InjectMocks
    UpgradeApplicationLinkUIResource resource;

    @Before
    public void createService() {
        when(pluginAccessor.getEnabledModulesByClass(AutoConfiguringAuthenticatorProviderPluginModule.class)).thenReturn(ImmutableList.<AutoConfiguringAuthenticatorProviderPluginModule>of());
        when(userManager.getRemoteUserKey()).thenReturn(new UserKey("username"));
        when(userManager.isSystemAdmin(any(UserKey.class))).thenReturn(false);
        when(i18nResolver.getText("applinks.error.only.sysadmin.operation")).thenReturn("error text");
    }

    @Test
    public void verifyNonSysadminCannotUpgradeToTrustedAppLink() throws Exception {
        UpgradeApplicationLinkRequestEntity upgradeRequest = createRequestEntity(true);

        Response result = resource.upgradeAuthentication(upgradeRequest, new ArrayList<String>(), requestFactory, applicationLink);
        assertNotNull(result);

        ErrorListEntity error = ((ErrorListEntity) result.getEntity());
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, result.getStatus());
        assertEquals("error text", error.getErrors().get(0));
        assertEquals("same-userbase", error.getFields().get(0));
    }

    @Test
    public void verifySysadminCanUpgradeToTrustedAppLink() throws Exception {
        when(userManager.isSystemAdmin(eq(new UserKey("username")))).thenReturn(true);
        UpgradeApplicationLinkRequestEntity upgradeRequest = createRequestEntity(true);

        Response result = resource.upgradeAuthentication(upgradeRequest, new ArrayList<String>(), requestFactory, applicationLink);
        assertNull(result);
    }

    @Test
    public void verifyNonSysadminCanUpgradeToOAuthAppLink() throws Exception {
        UpgradeApplicationLinkRequestEntity upgradeRequest = createRequestEntity(false);

        Response result = resource.upgradeAuthentication(upgradeRequest, new ArrayList<String>(), requestFactory, applicationLink);
        assertNull(result);
    }

    private UpgradeApplicationLinkRequestEntity createRequestEntity(boolean shareUserBase) {
        URI uri = URI.create("http://localhost");
        ConfigurationFormValuesEntity configForm = new ConfigurationFormValuesEntity(true, shareUserBase);
        return new UpgradeApplicationLinkRequestEntity(configForm, true, "password", true, "username", uri);
    }
}
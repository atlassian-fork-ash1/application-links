package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.PropertySet;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.event.ApplicationLinkAddedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkMadePrimaryEvent;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.core.DefaultApplicationLinkService.EntityLinkServiceApi;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactoryImpl;
import com.atlassian.applinks.core.auth.AuthenticationConfigurator;
import com.atlassian.applinks.core.auth.AuthenticatorAccessor;
import com.atlassian.applinks.core.link.InternalApplicationLink;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.plugin.ApplicationTypeModuleDescriptor;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.MockPluginSettingsPropertySet;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.ui.AuthenticationResource;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.net.BasicHttpAuthRequestFactory;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultApplicationLinkServiceTest {
    private static final String JIRA_I18N = "applinks.jira";
    private static final String CONF_I18N = "applinks.confluence";

    private static JiraApplicationType JIRA_TYPE;

    private static ConfluenceApplicationType CONF_TYPE;

    private static final URI JAC_RPC;
    private static final URI JAC_DISPLAY;
    private static final URI JDAC_RPC;
    private static final URI JDAC_DISPLAY;
    private static final URI CAC_DISPLAY;
    private static final URI CAC_RPC;
    private static final URI CAC_RPC_CLEANED;
    private static final URI EAC_DISPLAY;
    private static final URI EAC_RPC;
    private static final URI GOOGLE_RPC;

    private static final String ID_1 = "11111111-1111-1111-1111-111111111111";
    private static final String ID_2 = "22222222-2222-2222-2222-222222222222";
    private static final String ID_3 = "33333333-3333-3333-3333-333333333333";
    private static int applicationIdCount = 5;

    private static final String USERNAME = "john";
    private static final String PASSWORD = "secret";

    static {
        try {
            JAC_RPC = new URI("http://tinyurl.com/2g9mqh");
            JAC_DISPLAY = new URI("https://jira.atlassian.com");
            JDAC_RPC = new URI("http://developer.atlassian.com/jira");
            JDAC_DISPLAY = new URI("https://developer.atlassian.com/jira");
            CAC_DISPLAY = new URI("https://confluence.atlassian.com");
            CAC_RPC = new URI("http://localhost:8080/");
            CAC_RPC_CLEANED = new URI("http://localhost:8080");
            EAC_DISPLAY = new URI("https://extranet.atlassian.com");
            EAC_RPC = new URI("http://localhost:8077/eac");
            GOOGLE_RPC = new URI("http://www.google.com");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static final ApplicationId JAC_ID = new ApplicationId("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");
    private static final String JAC_NAME = "jac";
    private static final ApplicationId JDAC_ID = new ApplicationId("bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb");
    private static final String JDAC_NAME = "JDAC";
    private static final ApplicationId CAC_ID = new ApplicationId("cccccccc-cccc-cccc-cccc-cccccccccccc");
    private static final String CAC_NAME = "CAC";
    private static final ApplicationId EAC_ID = new ApplicationId("eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee");
    private static final String EAC_NAME = "EAC";
    private static final String GOOGLE_NAME = "Google";

    private static final ApplicationLinkDetails JAC_DETAILS = ApplicationLinkDetails.builder().name(JAC_NAME)
            .displayUrl(JAC_DISPLAY).rpcUrl(JAC_RPC).build();
    private static final ApplicationLinkDetails JDAC_DETAILS = ApplicationLinkDetails.builder().name(JDAC_NAME)
            .displayUrl(JDAC_DISPLAY).rpcUrl(JDAC_RPC).build();
    private static final ApplicationLinkDetails CAC_DETAILS = ApplicationLinkDetails.builder().name(CAC_NAME)
            .displayUrl(CAC_DISPLAY).rpcUrl(CAC_RPC).build();
    private static final ApplicationLinkDetails EAC_DETAILS = ApplicationLinkDetails.builder().name(EAC_NAME)
            .displayUrl(EAC_DISPLAY).rpcUrl(EAC_RPC).build();
    private static final ApplicationLinkDetails PRIMARY_LINK_DETAILS = ApplicationLinkDetails.builder().name(GOOGLE_NAME)
            .displayUrl(GOOGLE_RPC).rpcUrl(GOOGLE_RPC).isPrimary(true).build();

    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    @Mock
    private PropertyService propertyService;
    @Mock
    private ApplicationTypeModuleDescriptor jiraTypeModuleDescriptor;
    @Mock
    private ApplicationTypeModuleDescriptor confluenceApplicationTypeModuleDescriptor;
    @Mock
    private ManifestRetriever manifestRetriever;
    @Mock
    private ApplicationLinkProperties mockApplicationLinkProperties;
    @Mock
    private InternalHostApplication mockInternalHostApplication;
    @Mock
    private RestUrlBuilder mockRestUrlBuilder;
    @Mock
    private RequestFactory<Request<Request<?, Response>, Response>> mockRequestFactory;
    @Mock
    private AppLinkPluginUtil mockAppLinkPluginUtil;
    @Mock
    private WebResourceUrlProvider mockWebResourceUrlProvider;
    @Mock
    private RequestFactory delegateFactory;
    @Mock
    private UserManager userManager;
    @Mock
    private AuthenticatorAccessor authenticatorAccessor;
    @Mock
    private InternalTypeAccessor typeAccessor;
    @Mock
    private EntityLinkServiceApi entityLinkService;
    @Mock
    private ApplicationLink mockApplicationLink;

    @Spy
    private MockEventPublisher eventPublisher = new MockEventPublisher();
    @Spy
    private MockPluginSettingsPropertySet globalAdminProperties = new MockPluginSettingsPropertySet();
    @Spy
    private AuthenticationConfigurator mockAuthenticationConfigurator = new MockAuthenticationConfigurator();

    private DefaultApplicationLinkService service;

    @Before
    public void setUp() {
        when(typeAccessor.loadApplicationType((TypeId) null)).thenThrow(new NullPointerException("Null TypeIds are not supported"));

        JIRA_TYPE = new JiraApplicationTypeImpl(mockAppLinkPluginUtil, mockWebResourceUrlProvider);
        CONF_TYPE = new ConfluenceApplicationTypeImpl(mockAppLinkPluginUtil, mockWebResourceUrlProvider);

        when(jiraTypeModuleDescriptor.getI18nNameKey()).thenReturn(JIRA_I18N);

        when(jiraTypeModuleDescriptor.getI18nNameKey()).thenReturn(JIRA_I18N);
        when(confluenceApplicationTypeModuleDescriptor.getI18nNameKey()).thenReturn(CONF_I18N);

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ApplicationTypeModuleDescriptor.class)).thenReturn(
                ImmutableList.of(
                        jiraTypeModuleDescriptor,
                        confluenceApplicationTypeModuleDescriptor)
        );


        when(propertyService.getGlobalAdminProperties()).thenReturn(globalAdminProperties);

        when(mockInternalHostApplication.getId()).thenReturn(CAC_ID);
        when(mockInternalHostApplication.getType()).thenReturn(CONF_TYPE);
        when(mockInternalHostApplication.getBaseUrl()).thenReturn(CAC_RPC);

        setUpType(JIRA_TYPE);
        setUpType(CONF_TYPE);

        service = new DefaultApplicationLinkService(
                propertyService,
                requestFactoryFactory,
                typeAccessor,
                null,
                eventPublisher,
                mockInternalHostApplication,
                mockRequestFactory,
                mockRestUrlBuilder,
                manifestRetriever,
                mockAuthenticationConfigurator,
                entityLinkService);
    }

    @SuppressWarnings("unchecked")
    private <T extends ApplicationType> void setUpType(final T type) {
        when(typeAccessor.loadApplicationType(type.getClass().getName())).thenReturn(type);
        when(typeAccessor.getApplicationType((Class) type.getClass())).thenReturn(type);
        TypeId id = ((IdentifiableType) type).getId();
        assertNotNull(id);
        when(typeAccessor.loadApplicationType(id)).thenReturn(type);
    }

    private <T extends ApplicationType> void removeType(final T type) {
        when(typeAccessor.loadApplicationType(type.getClass().getName())).thenReturn(null);
        when(typeAccessor.getApplicationType(type.getClass())).thenReturn(null);
        TypeId id = ((IdentifiableType) type).getId();
        assertNotNull(id);
        when(typeAccessor.loadApplicationType(id)).thenReturn(null);
    }

    @Test
    public void testApplicationLinkCRUD() throws Exception {
        expectAdminPropertySet(JAC_ID);
        service.addApplicationLink(JAC_ID, JIRA_TYPE, JAC_DETAILS);

        assertJac(service.getApplicationLink(JAC_ID));
        assertJac(service.getPrimaryApplicationLink(JIRA_TYPE.getClass()));

        Iterator<ApplicationLink> it = service.getApplicationLinks().iterator();
        assertJac(it.next());
        assertFalse(it.hasNext());

        it = service.getApplicationLinks(JIRA_TYPE.getClass()).iterator();
        assertJac(it.next());
        assertFalse(it.hasNext());

        expectAdminPropertySet(CAC_ID);
        service.addApplicationLink(CAC_ID, CONF_TYPE, CAC_DETAILS);

        try {
            final ApplicationType typeX = new ApplicationType() {
                public String getI18nKey() {
                    return "some.other.i18n.key";
                }

                public Class getTypeClass() {
                    return getClass();
                }

                @Override
                public URI getIconUrl() {
                    return null;
                }

            };
            service.addApplicationLink(new ApplicationId("dddddddd-dddd-dddd-dddd-dddddddddddd"), typeX, CAC_DETAILS);
            fail("Should have thrown TypeNotInstalledException");
        } catch (Exception expected) {
        }

        assertCac(service.getApplicationLink(CAC_ID));
        assertCac(service.getPrimaryApplicationLink(CONF_TYPE.getClass()));

        assertJac(service.getApplicationLink(JAC_ID));
        assertJac(service.getPrimaryApplicationLink(JIRA_TYPE.getClass()));

        it = service.getApplicationLinks().iterator(); // names are stored in a list, insertion order is maintained
        assertJac(it.next());
        assertCac(it.next());
        assertFalse(it.hasNext());

        it = service.getApplicationLinks(JIRA_TYPE.getClass()).iterator();
        assertJac(it.next());
        assertFalse(it.hasNext());

        it = service.getApplicationLinks(CONF_TYPE.getClass()).iterator();
        assertCac(it.next());
        assertFalse(it.hasNext());

        expectAdminPropertySet(JDAC_ID);
        service.addApplicationLink(JDAC_ID, JIRA_TYPE, JDAC_DETAILS);

        assertJdac(service.getApplicationLink(JDAC_ID));

        assertJac(service.getApplicationLink(JAC_ID));
        assertJac(service.getPrimaryApplicationLink(JIRA_TYPE.getClass()));

        it = service.getApplicationLinks(JIRA_TYPE.getClass()).iterator();
        assertJac(it.next());
        assertJdac(it.next());
        assertFalse(it.hasNext());

        it = service.getApplicationLinks().iterator(); // names are stored in a list, insertion order is maintained
        assertJac(it.next());
        assertCac(it.next());
        assertJdac(it.next());
        assertFalse(it.hasNext());
    }

    @Test
    public void testApplicationLinkAddedEvent() throws Exception {
        expectAdminPropertySet(JAC_ID);
        final ApplicationLink link = service.addApplicationLink(JAC_ID, JIRA_TYPE, JAC_DETAILS);
        assertEquals(link, eventPublisher.getLastFired(ApplicationLinkAddedEvent.class).getApplicationLink());
    }

    @Test
    public void testApplicationLinkDeletedEvent() throws Exception {
        expectAdminPropertySet(JAC_ID);
        final ApplicationLink link = service.addApplicationLink(JAC_ID, JIRA_TYPE, JAC_DETAILS);
        service.deleteApplicationLink(link);
        assertEquals(link.getId(), eventPublisher.getLastFired(ApplicationLinkDeletedEvent.class).getApplicationId());
    }

    @Test
    public void testEmpty() throws Exception {
        assertNull(service.getApplicationLink(new ApplicationId("eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee")));
        assertNull(service.getPrimaryApplicationLink(JIRA_TYPE.getClass()));
        assertFalse(service.getApplicationLinks().iterator().hasNext());
        assertFalse(service.getApplicationLinks(JIRA_TYPE.getClass()).iterator().hasNext());
    }

    @Test
    public void testTypeNotInstalled() throws Exception {
        expectAdminPropertySet(JAC_ID, JDAC_ID, CAC_ID);
        service.addApplicationLink(JAC_ID, JIRA_TYPE, JAC_DETAILS);
        service.addApplicationLink(JDAC_ID, JIRA_TYPE, JDAC_DETAILS);
        service.addApplicationLink(CAC_ID, CONF_TYPE, CAC_DETAILS);

        Iterator<ApplicationLink> it = service.getApplicationLinks().iterator();
        assertJac(it.next());
        assertJdac(it.next());
        assertCac(it.next());
        assertFalse(it.hasNext());

        removeType(CONF_TYPE);

        assertJac(service.getApplicationLink(JAC_ID));
        assertJdac(service.getApplicationLink(JDAC_ID));
        try {
            service.getApplicationLink(CAC_ID);
            fail("getApplicationLink() for a disabled type should throw a " + TypeNotPresentException.class.getSimpleName());
        } catch (TypeNotInstalledException e) {
            // expected
        }

        it = service.getApplicationLinks().iterator();
        assertJac(it.next());
        assertJdac(it.next());
        assertFalse(it.hasNext());

        service.makePrimary(JAC_ID);
        service.makePrimary(JDAC_ID);

        try {
            service.makePrimary(CAC_ID);
            fail("makePrimary() for a disabled type should throw a " + TypeNotPresentException.class.getSimpleName());
        } catch (TypeNotInstalledException e) {
            // expected
        }

        removeType(JIRA_TYPE);

        try {
            service.getApplicationLink(JAC_ID);
            fail("getApplicationLink() for a disabled type should throw a " + TypeNotPresentException.class.getSimpleName());
        } catch (TypeNotInstalledException e) {
            // expected
        }

        try {
            service.makePrimary(JAC_ID);
            fail("makePrimary() for a disabled type should throw a " + TypeNotPresentException.class.getSimpleName());
        } catch (TypeNotInstalledException e) {
            // expected
        }

        assertFalse("getApplicationLinks() should return an empty iterable when all types are disabled", service.getApplicationLinks().iterator().hasNext());
    }

    @Test
    public void testAddApplicationLinks() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        final MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertEquals(JAC_NAME, link1.getName());
        assertEquals(JIRA_TYPE, link1.getType());
        assertEquals(JAC_DETAILS.getRpcUrl(), link1.getRpcUrl());
        assertEquals(JAC_DETAILS.getDisplayUrl(), link1.getDisplayUrl());
        assertEquals(new ApplicationId(ID_1), link1.getId());
        assertTrue(link1.isPrimary());
    }

    @Test
    public void testApplicationLinkWithTrailingSlash() throws Exception {
        expectAdminPropertySet(CAC_ID);
        final MutableApplicationLink link1 = service.addApplicationLink(CAC_ID, CONF_TYPE, CAC_DETAILS);
        assertEquals(CAC_NAME, link1.getName());
        assertEquals(CONF_TYPE, link1.getType());
        assertEquals(CAC_ID, link1.getId());
        assertTrue(link1.isPrimary());
        assertEquals(CAC_DETAILS.getRpcUrl(), link1.getRpcUrl());
        assertEquals(CAC_DETAILS.getDisplayUrl(), link1.getDisplayUrl());
        assertEquals(CAC_RPC_CLEANED, link1.getRpcUrl());
    }

    @Test
    public void testTrailingSlashHandled() throws Exception {
        String uri = "/com/atlassian/applinks/core/rest/1.0/entities";
        String expected = CAC_RPC_CLEANED + uri;
        expectAdminPropertySet(CAC_ID);
        final MutableApplicationLink link = service.addApplicationLink(CAC_ID, CONF_TYPE, CAC_DETAILS);
        ApplicationLinkRequest mockRequest = mock(ApplicationLinkRequest.class);
        when(delegateFactory.createRequest(Request.MethodType.GET, expected)).thenReturn(mockRequest);
        ApplicationLinkRequestFactoryFactoryImpl factoryFactory = new ApplicationLinkRequestFactoryFactoryImpl(delegateFactory, userManager, authenticatorAccessor);
        ApplicationLinkRequestFactory innerFactory = factoryFactory.getApplicationLinkRequestFactory(link);
        innerFactory.createRequest(Request.MethodType.GET, uri);
        verify(delegateFactory).createRequest(Request.MethodType.GET, expected);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addApplicationLinkWithDuplicateIdShouldFail() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);

        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JDAC_DETAILS);
    }

    @Test
    public void testAddApplicationLinkWithDuplicateName() throws Exception {
        // Create a first application link
        expectAdminPropertySet(new ApplicationId(ID_1));
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);

        // It is important to create 11 duplicates, to check the " - 10" is replaced with " - 11" even though it's 2 chars long.
        for (int counter = 2; counter < 11; counter++) {
            ApplicationId id = generateNewApplicationId();
            expectAdminPropertySet(id);
            InternalApplicationLink duplicateApplicationLink = service.addApplicationLink(id, JIRA_TYPE, JAC_DETAILS);

            String alternateName = String.format("%s - %d", JAC_DETAILS.getName(), counter);
            assertEquals("DefaultApplicationLinkService#addApplicationLink() should find an alternate name for the second application link", alternateName,
                    duplicateApplicationLink.getName());
        }
    }

    @Test
    public void testNameInUseReturnsTrueWhenAnotherLinkWithSameNameExists() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertTrue(service.isNameInUse(JAC_DETAILS.getName(), null));
    }

    @Test
    public void testNameInUseReturnsFalseWhenNoOtherLinkWithSameNameExists() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        assertFalse(service.isNameInUse(JAC_DETAILS.getName(), null));

        final MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertFalse(service.isNameInUse(JAC_DETAILS.getName(), link1.getId()));
    }

    @Test
    public void testKeepPrimaryWhenNewLinkOfSameTypeAdded() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_2));
        service.addApplicationLink(new ApplicationId(ID_2), JIRA_TYPE, PRIMARY_LINK_DETAILS);
        expectAdminPropertySet(new ApplicationId(ID_1));
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        final Iterable<ApplicationLink> iterable = service.getApplicationLinks(JiraApplicationType.class);
        final Iterator<ApplicationLink> linkIterator = iterable.iterator();
        final ApplicationLink applicationLink2 = linkIterator.next();
        assertEquals(new ApplicationId(ID_2), applicationLink2.getId());
        assertTrue(applicationLink2.isPrimary());
        linkIterator.next();
        assertFalse(linkIterator.hasNext());
    }

    @Test
    public void testDeletePrimaryApplicationLink() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertTrue(link1.isPrimary());
        expectAdminPropertySet(new ApplicationId(ID_2));
        final MutableApplicationLink link2 = service.addApplicationLink(new ApplicationId(ID_2), JIRA_TYPE, JDAC_DETAILS);
        assertFalse(link2.isPrimary());
        service.makePrimary(link2.getId());
        assertTrue(link2.isPrimary());

        link1 = service.getApplicationLink(new ApplicationId(ID_1));
        assertFalse(link1.isPrimary());

        service.deleteApplicationLink(link2);

        assertNull(service.getApplicationLink(new ApplicationId(ID_2)));
        link1 = service.getApplicationLink(new ApplicationId(ID_1));
        assertTrue(link1.isPrimary());
    }

    @Test
    public void testDeleteCorruptedPrimaryApplicationLink() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertTrue(link1.isPrimary());

        ApplicationLinkProperties properties = propertyService.getApplicationLinkProperties(new ApplicationId(ID_1));
        properties.remove();

        service.deleteApplicationLink(link1);
        assertNull(service.getApplicationLink(new ApplicationId(ID_1)));
    }

    @Test
    public void testDontChangePrimaryAfterDeletingNonPrimaryApplicationLink() throws Exception {
        final ApplicationId id1 = new ApplicationId(ID_1);
        expectAdminPropertySet(id1);
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        final ApplicationId id2 = new ApplicationId(ID_2);
        expectAdminPropertySet(id2);
        service.addApplicationLink(new ApplicationId(ID_2), JIRA_TYPE, JDAC_DETAILS);
        final ApplicationId id3 = new ApplicationId(ID_3);
        expectAdminPropertySet(id3);
        //Primary is the second application link
        service.makePrimary(id2);
        final InternalApplicationLink nonPrimaryApplicationLink = service.addApplicationLink(new ApplicationId(ID_3), JIRA_TYPE, CAC_DETAILS);
        service.deleteApplicationLink(nonPrimaryApplicationLink);
        final InternalApplicationLink link1 = service.getApplicationLink(id1);
        assertFalse("Link2 should be the primary", link1.isPrimary());
        final InternalApplicationLink link2 = service.getApplicationLink(id2);
        assertTrue(link2.isPrimary());
    }

    @Test
    public void testSimpleDeleteApplicationLink() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        final MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertTrue(link1.isPrimary());

        service.deleteApplicationLink(link1);
        assertNull(service.getApplicationLink(new ApplicationId(ID_1)));
    }

    @Test
    public void testGetCorruptedApplicationLink() {
        expectAdminPropertySet(new ApplicationId(ID_1));
        MutableApplicationLink link1 = service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);
        assertTrue(link1.isPrimary());

        ApplicationLinkProperties properties = propertyService.getApplicationLinkProperties(new ApplicationId(ID_1));
        properties.remove();

        try {
            service.getApplicationLink(new ApplicationId(ID_1));
            fail("Corrupted applinks must never be returned");
        } catch (TypeNotInstalledException expected) {
            assertTrue(expected.getType().contains("unknown"));
        }
    }

    @Test
    public void testSetPrimary() throws Exception {
        expectAdminPropertySet(new ApplicationId(ID_1));
        service.addApplicationLink(new ApplicationId(ID_1), JIRA_TYPE, JAC_DETAILS);

        expectAdminPropertySet(new ApplicationId(ID_2));
        service.addApplicationLink(new ApplicationId(ID_2), JIRA_TYPE, CAC_DETAILS);

        expectAdminPropertySet(new ApplicationId(ID_3));
        service.addApplicationLink(new ApplicationId(ID_3), JIRA_TYPE, JDAC_DETAILS);

        service.makePrimary(new ApplicationId(ID_1));

        final MutableApplicationLink link1 = service.getApplicationLink(new ApplicationId(ID_1));
        assertTrue(link1.isPrimary());

        final MutableApplicationLink link2 = service.getApplicationLink(new ApplicationId(ID_2));
        assertFalse(link2.isPrimary());

        final MutableApplicationLink link3 = service.getApplicationLink(new ApplicationId(ID_3));
        assertFalse(link3.isPrimary());

        assertEquals(new ApplicationId(ID_1),
                eventPublisher.getLastFired(ApplicationLinkMadePrimaryEvent.class).getApplicationId());
    }

    @Test(expected = IllegalStateException.class)
    public void testInconsistentPrimaryState() {
        expectAdminPropertySet(JAC_ID);
        service.addApplicationLink(JAC_ID, JIRA_TYPE, JAC_DETAILS);

        propertyService.getApplicationLinkProperties(JAC_ID).setIsPrimary(false);

        service.getPrimaryApplicationLink(JIRA_TYPE.getClass());
    }

    @Test
    public void testTwoConfluences() {
        expectAdminPropertySet(CAC_ID, EAC_ID);
        service.addApplicationLink(CAC_ID, CONF_TYPE, CAC_DETAILS);
        service.addApplicationLink(EAC_ID, CONF_TYPE, EAC_DETAILS);

        final Iterator<ApplicationLink> it = service.getApplicationLinks(CONF_TYPE.getClass()).iterator();

        assertCac(it.next());
        assertEac(it.next());
        assertFalse(it.hasNext());
    }

    @Test
    public void testCreateApplicationLink() throws Exception {
        final Manifest mockManifest = mock(Manifest.class);
        final PropertySet mockApplinksAdminProperties = new MockPropertySet();
        final PropertySet mockApplinksProperties = new MockPropertySet();

        when(manifestRetriever.getManifest(Mockito.eq(JAC_RPC), Mockito.any(JiraApplicationType.class))).thenReturn(mockManifest);
        when(mockManifest.getId()).thenReturn(JAC_ID);
        when(propertyService.getApplicationLinkProperties(JAC_ID)).thenReturn(new ApplicationLinkProperties(mockApplinksAdminProperties, mockApplinksProperties));

        final ApplicationLink applicationLink = service.createApplicationLink(JIRA_TYPE, JAC_DETAILS);

        assertTrue(((Collection<String>) globalAdminProperties.get(DefaultApplicationLinkService.APPLICATION_IDS)).contains(JAC_ID.get()));
        assertEquals(((IdentifiableType) JIRA_TYPE).getId().get(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.TYPE.key()));
        assertEquals(JAC_NAME, mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.NAME.key()));
        assertEquals(JAC_DISPLAY.toString(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.DISPLAY_URL.key()));
        assertEquals(JAC_RPC.toString(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.RPC_URL.key()));
        assertEquals(String.valueOf(true), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.PRIMARY.key()));
        final LinkedList<Object> events = eventPublisher.getEvents();
        assertEquals(2, events.size());
        assertEquals(applicationLink, ((ApplicationLinkMadePrimaryEvent) events.get(0)).getApplicationLink());
        assertSame(applicationLink, ((ApplicationLinkAddedEvent) events.get(1)).getApplicationLink());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateApplicationLinkWithExistingIDFails() throws Exception {
        testCreateApplicationLink();

        service.createApplicationLink(JIRA_TYPE, JAC_DETAILS);
    }

    @Test
    public void testCreateSecondApplicationLinkOfType() throws Exception {
        testCreateApplicationLink();

        final Manifest mockManifest = mock(Manifest.class);
        final PropertySet mockApplinksAdminProperties = new MockPropertySet();
        final PropertySet mockApplinksProperties = new MockPropertySet();

        when(manifestRetriever.getManifest(Mockito.eq(JDAC_RPC), Mockito.any(JiraApplicationType.class))).thenReturn(mockManifest);
        when(mockManifest.getId()).thenReturn(JDAC_ID);
        when(propertyService.getApplicationLinkProperties(JDAC_ID)).thenReturn(new ApplicationLinkProperties(mockApplinksAdminProperties, mockApplinksProperties));

        final ApplicationLink applicationLink = service.createApplicationLink(JIRA_TYPE, JDAC_DETAILS);

        final Collection<String> ids = (Collection<String>) globalAdminProperties.get(DefaultApplicationLinkService.APPLICATION_IDS);
        assertTrue(ids.contains(JAC_ID.get()));
        assertTrue(ids.contains(JDAC_ID.get()));
        assertEquals(((IdentifiableType) JIRA_TYPE).getId().get(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.TYPE.key()));
        assertEquals(JDAC_NAME, mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.NAME.key()));
        assertEquals(JDAC_DISPLAY.toString(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.DISPLAY_URL.key()));
        assertEquals(JDAC_RPC.toString(), mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.RPC_URL.key()));
        assertNull(mockApplinksAdminProperties.getProperty(ApplicationLinkProperties.Property.PRIMARY.key()));
        final LinkedList<Object> events = eventPublisher.getEvents();
        assertEquals(3, events.size());
        assertSame(applicationLink, ((ApplicationLinkAddedEvent) events.get(2)).getApplicationLink());
    }

    @Test
    public void testIsAdminUserInRemoteApplicationTrue() throws Exception {
        testIsAdminUserInRemoteApplication(true);
    }

    @Test
    public void testIsAdminUserInRemoteApplicationFalse() throws Exception {
        testIsAdminUserInRemoteApplication(false);
    }

    private void testIsAdminUserInRemoteApplication(final boolean result) throws ResponseException {
        expectIsAdminUserInRemoteApplication(result);
        assertEquals(result, service.isAdminUserInRemoteApplication(JAC_RPC, USERNAME, PASSWORD));
    }

    @SuppressWarnings("unchecked")
    private void expectIsAdminUserInRemoteApplication(final boolean result) throws ResponseException {
        final AppLinksManifestDownloader mockManifestDownloader = mock(AppLinksManifestDownloader.class);
        final Request<Request<?, Response>, Response> mockRequest = mock(Request.class);
        final Response mockResponse = mock(Response.class);

        when(mockRestUrlBuilder.getUrlFor(Mockito.<URI>any(), Mockito.eq(AuthenticationResource.class)))
                .thenReturn(new AuthenticationResource(mockManifestDownloader));
        when(mockRequestFactory.createRequest(Mockito.eq(Request.MethodType.GET), Mockito.<String>any()))
                .thenReturn(mockRequest);
        when(mockRequest.addBasicAuthentication(JAC_RPC.getHost(), USERNAME, PASSWORD))
                .thenReturn((Request) mockRequest);
        when(mockResponse.isSuccessful()).thenReturn(result);
        when(mockRequest.executeAndReturn(any(ReturningResponseHandler.class))).thenAnswer(new Answer<Boolean>() {

            public final Boolean answer(final InvocationOnMock invocationOnMock) throws Throwable {
                return (Boolean) ReturningResponseHandler.class.cast(invocationOnMock.getArguments()[0])
                        .handle(mockResponse);
            }

        });
    }

    private void expectAdminPropertySet(final ApplicationId... ids) {
        for (final ApplicationId id : ids) {
            final MockPluginSettingsPropertySet propertySet = new MockPluginSettingsPropertySet();
            when(propertyService.getApplicationLinkProperties(id)).thenReturn(new ApplicationLinkProperties(propertySet, propertySet));
        }
    }

    private void assertCac(final ApplicationLink link) {
        assertEquals(CAC_ID, link.getId());
        assertEquals(CAC_NAME, link.getName());
        assertEquals(CONF_TYPE, link.getType());
        assertEquals(CONF_I18N, link.getType().getI18nKey());
        assertEquals(CAC_DISPLAY, link.getDisplayUrl());
        assertEquals(CAC_RPC_CLEANED, link.getRpcUrl());
    }

    private void assertEac(final ApplicationLink link) {
        assertEquals(EAC_ID, link.getId());
        assertEquals(EAC_NAME, link.getName());
        assertEquals(CONF_TYPE, link.getType());
        assertEquals(CONF_I18N, link.getType().getI18nKey());
        assertEquals(EAC_DISPLAY, link.getDisplayUrl());
        assertEquals(EAC_RPC, link.getRpcUrl());
    }

    private void assertJac(final ApplicationLink link) {
        assertEquals(JAC_ID, link.getId());
        assertEquals(JAC_NAME, link.getName());
        assertEquals(JIRA_TYPE, link.getType());
        assertEquals(JIRA_I18N, link.getType().getI18nKey());
        assertEquals(JAC_DISPLAY, link.getDisplayUrl());
        assertEquals(JAC_RPC, link.getRpcUrl());
    }

    private void assertJdac(final ApplicationLink link) {
        assertEquals(JDAC_ID, link.getId());
        assertEquals(JDAC_NAME, link.getName());
        assertEquals(JIRA_TYPE, link.getType());
        assertEquals(JIRA_I18N, link.getType().getI18nKey());
        assertEquals(JDAC_DISPLAY, link.getDisplayUrl());
        assertEquals(JDAC_RPC, link.getRpcUrl());
    }

    private ApplicationId generateNewApplicationId() {
        applicationIdCount++;
        // This formats the applicationIdCount on 10 characters
        String fullFigure = String.format("%010d", applicationIdCount);
        String lastFigure = String.valueOf(applicationIdCount % 10);

        // Takes ID_1 as an example
        String id = ID_1.substring(0, ID_1.length() - fullFigure.length());
        id = id.replaceAll("1", lastFigure);
        id += fullFigure;

        return new ApplicationId(id);
    }

    @Test
    public void getInternalApplicationLinksIteratorCanBeReused() throws Exception {
        final PropertySet mockApplinksAdminProperties = new MockPropertySet();
        final PropertySet mockApplinksProperties = new MockPropertySet();

        Manifest mockManifest = mock(Manifest.class);
        when(manifestRetriever.getManifest(Mockito.eq(JAC_RPC), Mockito.any(JiraApplicationType.class))).thenReturn(mockManifest);
        when(mockManifest.getId()).thenReturn(JAC_ID);
        when(propertyService.getApplicationLinkProperties(JAC_ID)).thenReturn(new ApplicationLinkProperties(mockApplinksAdminProperties, mockApplinksProperties));

        service.createApplicationLink(JIRA_TYPE, JAC_DETAILS);

        mockManifest = mock(Manifest.class);
        when(manifestRetriever.getManifest(Mockito.eq(JDAC_RPC), Mockito.any(JiraApplicationType.class))).thenReturn(mockManifest);
        when(mockManifest.getId()).thenReturn(JDAC_ID);
        when(propertyService.getApplicationLinkProperties(JDAC_ID)).thenReturn(new ApplicationLinkProperties(mockApplinksAdminProperties, mockApplinksProperties));

        service.createApplicationLink(JIRA_TYPE, JDAC_DETAILS);


        Iterable<InternalApplicationLink> links = service.getInternalApplicationLinks();
        assertEquals("There are two links", 2, Iterables.size(links));
        assertEquals("There are two links according to the second iterator", 2, Iterables.size(links));
    }

    @Test
    public void getInternalApplicationLinksMasksTypeNotInstalledExceptions() throws Exception {
        final PropertySet mockApplinksAdminProperties = new MockPropertySet();
        final PropertySet mockApplinksProperties = new MockPropertySet();

        Manifest mockManifest = mock(Manifest.class);
        when(manifestRetriever.getManifest(Mockito.eq(JAC_RPC), Mockito.any(JiraApplicationType.class))).thenReturn(mockManifest);
        when(mockManifest.getId()).thenReturn(JAC_ID);
        when(propertyService.getApplicationLinkProperties(JAC_ID)).thenReturn(new ApplicationLinkProperties(mockApplinksAdminProperties, mockApplinksProperties));

        service.createApplicationLink(JIRA_TYPE, JAC_DETAILS);

        removeType(JIRA_TYPE);
        Iterable<InternalApplicationLink> links = service.getInternalApplicationLinks();
        assertEquals("There are no valid links", 0, Iterables.size(links));
    }

    @Test
    public void getApplicationLinkWithNoTypeId() throws Exception {
        final PropertySet mockGlobalAdminProperties = new MockPropertySet();
        List<String> appIds = new ArrayList<String>();
        mockGlobalAdminProperties.putProperty(DefaultApplicationLinkService.APPLICATION_IDS, appIds);
        appIds.add("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");
        when(propertyService.getGlobalAdminProperties()).thenReturn(mockGlobalAdminProperties);
        when(propertyService.getApplicationLinkProperties(JAC_ID)).thenReturn(mockApplicationLinkProperties);
        // use a try catch instead of ExpectedException Rule in order to test exception contents
        try {
            service.getApplicationLink(JAC_ID);
            fail("Expecting TypeNotInstalledException");
        } catch (TypeNotInstalledException e) {
            assertEquals("unknown", e.getType());
            assertEquals(null, e.getName());
            assertEquals(null, e.getRpcUrl());
        }
    }

    @Test
    public void getApplicationLinkWithNoApplicationType() throws Exception {
        final PropertySet mockGlobalAdminProperties = new MockPropertySet();
        List<String> appIds = new ArrayList<String>();
        mockGlobalAdminProperties.putProperty(DefaultApplicationLinkService.APPLICATION_IDS, appIds);
        appIds.add("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");
        when(propertyService.getGlobalAdminProperties()).thenReturn(mockGlobalAdminProperties);
        when(propertyService.getApplicationLinkProperties(JAC_ID)).thenReturn(mockApplicationLinkProperties);
        when(mockApplicationLinkProperties.getType()).thenReturn(new TypeId("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"));
        when(mockApplicationLinkProperties.getName()).thenReturn("test-app-name");
        when(mockApplicationLinkProperties.getRpcUrl()).thenReturn(new URI("/test-uri"));
        // use a try catch instead of ExpectedException Rule in order to test exception contents
        try {
            service.getApplicationLink(JAC_ID);
            fail("Expecting TypeNotInstalledException");
        } catch (TypeNotInstalledException e) {
            assertEquals("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", e.getType());
            assertEquals("test-app-name", e.getName());
            assertEquals(new URI("/test-uri"), e.getRpcUrl());
        }
    }

    @Test
    public void testConfigureAuthenticationForApplicationLink() throws Exception {
        // just verify that the call is forwared to AuthenticationConfigurator, it has its own suite of unit tests
        AuthenticationScenario scenario = mock(AuthenticationScenario.class);
        service.configureAuthenticationForApplicationLink(mockApplicationLink, scenario, USERNAME, PASSWORD);

        verify(mockAuthenticationConfigurator).configureAuthenticationForApplicationLink(eq(mockApplicationLink),
                eq(scenario), isA(BasicHttpAuthRequestFactory.class));
    }

    @Test
    public void testCreateReciprocalLink() throws Exception {
        expectIsAdminUserInRemoteApplication(true);
        final Response mockRestResponse = setUpApplinksResponse();
        service.createReciprocalLink(JAC_RPC, CAC_RPC, USERNAME, PASSWORD);
        //once agin this doesn't really return anything, so I'm relying on verification rather than assertion
        verify(mockRestResponse).isSuccessful();
    }

    private Response setUpApplinksResponse() throws ResponseException, TypeNotInstalledException {
        final ApplicationLinkResource mockLocalApplicationLinkResource = mock(ApplicationLinkResource.class);
        final ApplicationLinkResource mockRemoteApplicationLinkResource = mock(ApplicationLinkResource.class);
        final Request<Request<?, Response>, Response> mockRequest = mock(Request.class);
        final javax.ws.rs.core.Response mockLocalResponse = mock(javax.ws.rs.core.Response.class);
        final javax.ws.rs.core.Response mockRemoteResponse = mock(javax.ws.rs.core.Response.class);
        final Response mockRestResponse = mock(Response.class);
        when(mockRestUrlBuilder.getUrlFor(eq(CAC_RPC), eq(ApplicationLinkResource.class)))
                .thenReturn(mockLocalApplicationLinkResource);
        when(mockRestUrlBuilder.getUrlFor(eq(RestUtil.getBaseRestUri(JAC_RPC)), eq(ApplicationLinkResource.class)))
                .thenReturn(mockRemoteApplicationLinkResource);
        when(mockRestUrlBuilder.getURI(mockLocalResponse))
                .thenReturn(CAC_RPC);
        when(mockLocalApplicationLinkResource.getApplicationLink(CAC_ID.get())).thenReturn(mockLocalResponse);
        when(mockRemoteApplicationLinkResource.updateApplicationLink(CAC_ID.get(), null)).thenReturn(mockRemoteResponse);
        when(mockRemoteResponse.toString()).thenReturn(JAC_RPC.getHost());
        when(mockRequestFactory.createRequest(Mockito.eq(Request.MethodType.PUT), Mockito.<String>any())).thenReturn(mockRequest);
        when(mockRequest.addBasicAuthentication(JAC_RPC.getHost(), USERNAME, PASSWORD)).thenReturn((Request) mockRequest);
        when(mockRequest.setEntity(any(ApplicationLinkEntity.class))).thenReturn((Request) mockRequest);
        final ArgumentCaptor<ReturningResponseHandler> responseHandlerCaptor
                = ArgumentCaptor.forClass(ReturningResponseHandler.class);
        when(mockRequest.executeAndReturn(responseHandlerCaptor.capture())).thenAnswer(new Answer<Boolean>() {

            public final Boolean answer(final InvocationOnMock invocationOnMock) throws Throwable {
                return (Boolean) responseHandlerCaptor.getValue().handle(mockRestResponse);
            }

        });
        when(mockRestResponse.isSuccessful()).thenReturn(true);
        return mockRestResponse;
    }

    private static class MockAuthenticationConfigurator extends AuthenticationConfigurator {

        public MockAuthenticationConfigurator() {
            super(null);
        }

        @Override
        public boolean configureAuthenticationForApplicationLink(ApplicationLink applicationLink,
                                                                 AuthenticationScenario scenario, RequestFactory requestFactory)
                throws AuthenticationConfigurationException {
            return true;
        }
    }
}

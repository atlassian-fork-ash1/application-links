package com.atlassian.applinks.core.v1.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.MockAuthenticationProviderPluginModule;
import com.atlassian.applinks.core.rest.RestResourceTestUtils;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ErrorListEntity;
import com.atlassian.applinks.internal.application.IconUriResolver;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.Status;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplicationLinkResourceTest {
    private static final String APPLICATION_LINK_ID = UUID.randomUUID().toString();
    private static URI URI1 = URI.create("http://localhost");
    private static URI URI2 = URI.create("http://localhost/anotherurl");

    @Mock
    MutatingApplicationLinkService applicationLinkService;
    @Mock
    RequestFactory requestFactory;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    InternalTypeAccessor typeAccessor;
    @Mock
    ManifestRetriever manifestRetriever;
    @Mock
    RestUrlBuilder restUrlBuilder;
    @Mock
    UserManager userManager;
    @Mock
    PluginAccessor pluginAccessor;

    @Mock
    MutableApplicationLink applicationLink;
    @Mock
    AuthenticationProvider authenticationProvider;
    @Mock
    AuthenticationConfigurationManager authenticationConfigurationManager;

    ApplicationType dummyType;
    ApplicationLinkResource resource;

    @Before
    public void createService() throws Exception {
        resource = new ApplicationLinkResource(applicationLinkService, i18nResolver, typeAccessor, manifestRetriever,
                restUrlBuilder, requestFactory, userManager);

        dummyType = RestResourceTestUtils.getDummyApplicationType();

        when(applicationLink.getId()).thenReturn(ApplicationIdUtil.generate(URI1));
        when(applicationLink.getType()).thenReturn(dummyType);
        when(applicationLink.getName()).thenReturn("name");
        when(applicationLink.getDisplayUrl()).thenReturn(URI1);
        when(applicationLink.getRpcUrl()).thenReturn(URI1);

        when(typeAccessor.loadApplicationType(new TypeId("dummy"))).thenReturn(dummyType);
        when(applicationLinkService.getApplicationLink(new ApplicationId(APPLICATION_LINK_ID))).thenReturn(applicationLink);
        when(applicationLinkService.isNameInUse(any(), any())).thenReturn(false);
        when(applicationLinkService.createSelfLinkFor(any())).thenReturn(URI1);

        when(userManager.getRemoteUsername()).thenReturn("username");
        when(userManager.isSystemAdmin("username")).thenReturn(true);
        when(i18nResolver.getText("applinks.error.cannot.update.rpcurl")).thenReturn("cannot update rpcurl");
        when(i18nResolver.getText("applinks.error.only.sysadmin.operation")).thenReturn("sysadmin only");

        MockAuthenticationProviderPluginModule authenticationProviderPluginModule = new MockAuthenticationProviderPluginModule(authenticationProvider);
        when(pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class)).thenReturn(Lists.newArrayList((AuthenticationProviderPluginModule) authenticationProviderPluginModule));
    }

    @Test
    public void verifyRpcUrlCannotBeUpdated() throws Exception {
        Link link = Link.link(URI1, "rel", "type");
        ApplicationLinkEntity linkEntity = createLinkEntity(applicationLink, link, URI2);

        Response result = resource.updateApplicationLink(APPLICATION_LINK_ID, linkEntity);

        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, result.getStatus());
        List<String> errors = ((ErrorListEntity) result.getEntity()).getErrors();
        assertEquals(1, errors.size());
        assertEquals("cannot update rpcurl", errors.get(0));
    }

    @Test
    public void verifyAdminCannotUpdateSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(true);
        when(userManager.isSystemAdmin("username")).thenReturn(false);
        Link link = Link.link(URI1, "rel", "type");
        ApplicationLinkEntity linkEntity = createLinkEntity(applicationLink, link, URI1);

        Response result = resource.updateApplicationLink(APPLICATION_LINK_ID, linkEntity);

        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_FORBIDDEN, result.getStatus());
        assertEquals("sysadmin only", ((Status) result.getEntity()).getMessage());
    }

    @Test
    public void verifySysadminCanUpdateSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(true);
        when(userManager.isSystemAdmin("username")).thenReturn(true);
        Link link = Link.link(URI1, "rel", "type");
        ApplicationLinkEntity linkEntity = createLinkEntity(applicationLink, link, URI1);

        Response result = resource.updateApplicationLink(APPLICATION_LINK_ID, linkEntity);
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    @Test
    public void verifyAdminCanUpdateNonSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(false);
        when(userManager.isSystemAdmin("username")).thenReturn(false);
        Link link = Link.link(URI1, "rel", "type");
        ApplicationLinkEntity linkEntity = createLinkEntity(applicationLink, link, URI1);

        Response result = resource.updateApplicationLink(APPLICATION_LINK_ID, linkEntity);
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    @Test
    public void verifyAdminCannotDeleteSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(true);
        when(userManager.isSystemAdmin("username")).thenReturn(false);

        Response result = resource.deleteApplicationLink(APPLICATION_LINK_ID, true);

        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_FORBIDDEN, result.getStatus());
        assertEquals("sysadmin only", ((Status) result.getEntity()).getMessage());
    }

    @Test
    public void verifySysadminCanDeleteSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(true);
        when(userManager.isSystemAdmin("username")).thenReturn(true);

        Response result = resource.deleteApplicationLink(APPLICATION_LINK_ID, true);
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    @Test
    public void verifyAdminCanDeleteNonSystemLink() throws Exception {
        when(applicationLink.isSystem()).thenReturn(false);
        when(userManager.isSystemAdmin("username")).thenReturn(true);

        Response result = resource.deleteApplicationLink(APPLICATION_LINK_ID, true);
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    private ApplicationLinkEntity createLinkEntity(final ApplicationLink applicationLink, final Link self, URI rpcUrl) {
        return new ApplicationLinkEntity(applicationLink.getId(), TypeId.getTypeId(applicationLink.getType()), applicationLink.getName(),
                applicationLink.getDisplayUrl(), applicationLink.getType().getIconUrl(), IconUriResolver.resolveIconUri(applicationLink.getType()), rpcUrl,
                applicationLink.isPrimary(), false, self);
    }
}

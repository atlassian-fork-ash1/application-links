package com.atlassian.applinks.ui.validators;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.ui.AbstractApplinksServlet;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CallbackParameterValidatorTest {

    private static final String APPLICATION_BASE_DOMAIN = "http://www.atlassian.test";
    private static final int APPLICATION_BASE_PORT = 8080;
    private static final String APPLICATION_BASE_URL = APPLICATION_BASE_DOMAIN + ":" + APPLICATION_BASE_PORT;

    @Mock
    private MessageFactory mockMessageFactory;
    @Mock
    private InternalHostApplication mockInternalHostApplication;
    @Mock
    private ApplicationLinkService mockApplicationLinkService;

    private CallbackParameterValidator callbackParameterValidator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.callbackParameterValidator = new CallbackParameterValidator(mockMessageFactory,
                mockInternalHostApplication,
                mockApplicationLinkService);

        when(mockMessageFactory.newI18nMessage("auth.config.parameter.missing", String.class)).thenReturn(null);
        when(mockInternalHostApplication.getBaseUrl()).thenReturn(new URI(APPLICATION_BASE_URL));
        when(mockApplicationLinkService.getApplicationLinks()).thenReturn(Lists.<ApplicationLink>newArrayList());
    }


    @Test
    public void testGetCallbackParameterInvalidAbsolute() throws Exception {
        String callbackUrl = "http://www.atlassian.invalid";

        boolean isValid = callbackParameterValidator.isCallbackUrlValid(callbackUrl);
        assertThat(isValid, equalTo(false));
    }

    @Test(expected = AbstractApplinksServlet.BadRequestException.class)
    public void testGetCallbackParameterInvalidSchemaless() throws Exception {
        String callbackUrl = "//www.atlassian.invalid";

        callbackParameterValidator.validate(callbackUrl);
    }

    @Test
    public void testCallbackParameterValidBecauseEqualToAppBaseUrl() throws Exception {
        String callbackUrl = APPLICATION_BASE_URL;

        boolean isValid = callbackParameterValidator.isCallbackUrlValid(callbackUrl);
        assertThat("CallbackUrl is recognized as invalid, but should be valid", isValid, equalTo(true));
    }

    @Test
    public void testCallbackParameterInvalidBecauseOfWrongPort() throws Exception {
        String callbackUrl = APPLICATION_BASE_DOMAIN + ":" + 1212;

        boolean isValid = callbackParameterValidator.isCallbackUrlValid(callbackUrl);
        assertThat("CallbackUrl is recognized as valid, but should be invalid", isValid, equalTo(false));
    }

    @Test
    public void testGetCallbackParameterValidRelative() throws Exception {
        String callbackUrl = "/home.page";

        callbackParameterValidator.validate(callbackUrl);
    }

    @Test(expected = AbstractApplinksServlet.BadRequestException.class)
    public void testGetCallbackParameterInvalidURLWithAuth() throws Exception {
        String callbackUrl = "https://www.atlassian.net@bugzilla.invalid";

        callbackParameterValidator.validate(callbackUrl);
    }

    @Test(expected = AbstractApplinksServlet.BadRequestException.class)
    public void testGetCallbackParameterInvalidInternalLink() {
        String callbackUrl = "http://www.bitbucket.invalid/";
        String linkUrl = "http://www.github.text/";

        ApplicationLink applicationLink = mock(ApplicationLink.class);
        when(applicationLink.getDisplayUrl()).thenReturn(URI.create(linkUrl));
        when(applicationLink.getRpcUrl()).thenReturn(URI.create(linkUrl));
        when(mockApplicationLinkService.getApplicationLinks()).thenReturn(Lists.newArrayList(applicationLink));

        callbackParameterValidator.validate(callbackUrl);
    }

    @Test
    public void testCallbackParameterValidDisplayApplicationLink() {
        String callbackUrl = "http://www.bitbucket.test/";
        String linkUrl = "http://www.github.test/";

        ApplicationLink applicationLink = mock(ApplicationLink.class);
        when(applicationLink.getDisplayUrl()).thenReturn(URI.create(callbackUrl));
        when(applicationLink.getRpcUrl()).thenReturn(URI.create(linkUrl));
        when(mockApplicationLinkService.getApplicationLinks()).thenReturn(Lists.newArrayList(applicationLink));

        callbackParameterValidator.validate(callbackUrl);
    }

    @Test
    public void testCallbackParameterValidRpcApplicationLink() {
        String callbackUrl = "http://www.bitbucket.test/";
        String linkUrl = "http://www.github.test/";

        ApplicationLink applicationLink = mock(ApplicationLink.class);
        when(applicationLink.getDisplayUrl()).thenReturn(URI.create(linkUrl));
        when(applicationLink.getRpcUrl()).thenReturn(URI.create(callbackUrl));
        when(mockApplicationLinkService.getApplicationLinks()).thenReturn(Lists.newArrayList(applicationLink));

        callbackParameterValidator.validate(callbackUrl);
    }

}

define('applinks/test/template-params', [
    'applinks/lib/jquery'
], function(
    $
) {
    function parseText(assert, text) {
        try {
            return JSON.parse(text);
        } catch(error) {
            assert.fail('Failed to parse JSON text "' + text + '": ' + error);
        }
    }

    /**
     * Utility functions to store and retrieve template params passed to a template from JS code
     */
    return {
        storeParamsIn: function(html) {
            // the actual mock template function
            return function(params, out, injectedData) {
                var result = $(html);
                // use .template-params-container to add template params if present, otherwise add to result
                var container = result.find('.template-params-container');
                if (container.length == 0) {
                    container = result;
                }
                
                if (params) {
                    var paramsElement = $('<span class="template-params"></span>');
                    paramsElement[0].textContent = JSON.stringify(params);
                    container.append(paramsElement);
                }
                if (injectedData) {
                    var ijDataElement = $('<span class="template-ij"></span>');
                    ijDataElement[0].textContent = JSON.stringify(injectedData);
                    container.append(ijDataElement);
                }
                return result.prop('outerHTML');
            }
        },
                    
        getTemplateParams: function(assert, templateContainer) {
            return parseText(assert, templateContainer.find('.template-params').text());
        },

        getInjectedData: function(assert, templateContainer) {
            return parseText(assert, templateContainer.find('.template-ij').text());
        }
    }
});

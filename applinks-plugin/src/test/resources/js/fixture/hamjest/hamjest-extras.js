define('applinks/test/hamjest', [
    'applinks/lib/lodash',
    'hamjest',
    'applinks/test/assertion-error',
    'applinks/test/hamjest-aggregate-matchers',
    'applinks/test/hamjest-sinon',
    'applinks/test/hamjest-string'
], function(
    _,
    __,
    AssertionError,
    applyForMatcherArray,
    SinonMatchers,
    StringMatchers
) {
    function error(type, message) {
        if (arguments.length == 1) {
            message = type;
            type = Error;
        }
        if (!type.prototype instanceof Error) {
            throw new AssertionError(type + ' should extend Error');
        }

        return __.allOf(
            __.instanceOf(type),
            __.hasProperty('message', message)
        );
    }

    /**
     * Extended version of Hamjest containing our extra matchers.
     */
    return _.extend({}, __, SinonMatchers, StringMatchers, {
        error: error,

        allOf: applyForMatcherArray(__.allOf),
        anyOf: applyForMatcherArray(__.anyOf),
        contains: applyForMatcherArray(__.contains),
        hasItems: applyForMatcherArray(__.hasItems)
    });
});
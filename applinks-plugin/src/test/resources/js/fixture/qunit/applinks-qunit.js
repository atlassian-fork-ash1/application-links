define('applinks/test/qunit', [
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'squire',
    'object-path',
    'applinks/test/qunit-lib',
    'applinks/test/hamjest-assert',
    'applinks/test/mock-declaration'
], function(
    $,
    _,
    Squire,
    objectPath,
    QUnit,
    HamjestAssert,
    MockDeclaration
) {
    var originalModule = QUnit.module;
    var originalTest = QUnit.test;


    return _.extend({}, QUnit, {
        
        /**
         * QUnit.module implementation that takes additional properties properties to allow easy testing of AMD modules.
         *
         * `require`: contains mapping from field name to AMD module IDs of all real AMD modules required for the test.
         * This will most commonly be the actually tested module. Those will be set as properties of the test module
         * itself. If the `require` map contains a `main` key, that module will also be injected as the second parameter
         * to the test callback.
         *
         * `mocks`: mappings from keys to AMD module IDs that need mocking out. Those will be provided as a `mocks`
         * property of the test modules.
         *
         * `templates`: map of keys to functions or string values that will be set on the `window` object for the
         * duration of the test to act as Soy templates.
         *
         * @param name
         * @param {{mocks: function|object,
         * require:function|object,
         * templates:function|string,
         * beforeEach: function,
         * afterEach: function}} hooks
         */
        module: function(name, hooks) {
            originalModule(name, _.extend({}, hooks, {
                beforeEach: function(assert) {
                    this.sandbox = sinon.sandbox.create();
                    this.context = new Squire();
                    this.fixture = $('#qunit-fixture');
                    this.mocks = createMocks(this, this.context);
                    createTemplates(this.templates);

                    /** @type {{string: String}} a hash of property name to AMD module id */
                    var testRequires = this.require || {};
                    var testRequiredModuleIds = _.values(testRequires);


                    // Squire will call our async callback after it has called our setup function
                    this.context.run(testRequiredModuleIds, doSetup.bind(this))(assert.async());

                    function doSetup() {
                        // assign each required module to a test property
                        _.extend(this, _.object(_.zip(_.keys(testRequires), _.toArray(arguments))));

                        // call wrapped setup
                        hooks && hooks.beforeEach && hooks.beforeEach.apply(this, enhanceCallbackArgs(this, assert));
                    }
                },

                afterEach: function(assert) {
                    // call wrapped teardown
                    hooks && hooks.afterEach && hooks.afterEach.apply(this, enhanceCallbackArgs(this, assert));

                    destroyTemplates(this.templates);
                    this.sandbox.restore();
                    this.context.remove();
                }
            }));
        },

        /**
         * Customize QUnit.test to pass in improved Assert, tested module and optionally main mock/all mocks map
         *
         * @param name test name
         * @param callback test callback
         */
        test: function(name, callback) {
            originalTest(name, function(assert) {
                callback.apply(this, enhanceCallbackArgs(this, assert));
            });
        },

        mock: function(mockDeclaration) {
            return new SimpleMock(mockDeclaration);
        },

        moduleMock: function(moduleId, mockDeclaration) {
            return new ModuleMock(moduleId, mockDeclaration);
        }
    });

    function enhanceCallbackArgs(that, assert) {
        var callbackArgs = [HamjestAssert(assert), that.main];
        if (that.mocks && that.mocks.main) {
            callbackArgs.push(that.mocks.main);
        }
        if (that.mocks) {
            callbackArgs.push(that.mocks)
        }
        return callbackArgs;
    }

    function ModuleMock(id, mockDeclaration) {
        this.moduleId = id;
        this.mockDeclaration = mockDeclaration;
    }

    function SimpleMock(mockDeclaration) {
        this.mockDeclaration = mockDeclaration;
    }

    function createMocks(testEnv, context) {
        var testMocks = _.isFunction(testEnv.mocks) ? testEnv.mocks : _.constant(testEnv.mocks);
        var mocksDefinition = _.defaults({}, testMocks());

        return _(mocksDefinition).chain().map(function (value, key) {
            var mocked = createMock(testEnv, value);
            if (mocked.moduleId) {
                context.mock(mocked.moduleId, mocked.mock);
            }
            return [ key, mocked.mock ];
        }).object().value();
    }

    function createMock(testEnv, mockDefinition) {
        if (mockDefinition instanceof ModuleMock) {
            return {
                moduleId: mockDefinition.moduleId,
                mock: createMockInstance(testEnv, mockDefinition.mockDeclaration)
            };
        } else if (mockDefinition instanceof SimpleMock) {
            return { mock: createMockInstance(testEnv, mockDefinition.mockDeclaration) };
        } else {
            throw new Error("invalid mock value: " + JSON.stringify(mockDefinition));
        }
    }

    function createMockInstance(testEnv, mockDeclaration) {
        if (_.isUndefined(mockDeclaration)) {
            return testEnv.sandbox.stub();
        } else if (mockDeclaration instanceof MockDeclaration) {
            return mockDeclaration.createMock(testEnv.sandbox);
        } else if (_.isFunction(mockDeclaration)) {
            return mockDeclaration();
        } else {
            return mockDeclaration;
        }
    }

    function createTemplates(templates) {
        if (_.isObject(templates)) {
            _.forOwn(templates, function (func, key) {
                objectPath.set(window, key, _.isString(func) ? _.constant(func) : func);
            });
        }
    }

    function destroyTemplates(templates) {
        if (_.isObject(templates)) {
            _.forOwn(templates, function (func, key) {
                objectPath.del(window, key);
            });
        }
    }
});
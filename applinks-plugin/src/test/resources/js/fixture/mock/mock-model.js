define('applinks/test/mock-model', function() {

    function setUpCollection(currentTest, collectionInstance) {
        var sandbox = currentTest.sandbox;
        stubRemoteMethod(sandbox, collectionInstance, 'fetch');
        return collectionInstance;
    }

    function setUpModel(currentTest, modelInstance) {
        var sandbox = currentTest.sandbox;
        stubRemoteMethod(sandbox, modelInstance, 'fetch');
        stubRemoteMethod(sandbox, modelInstance, 'save');
        stubRemoteMethod(sandbox, modelInstance, 'destroy');

        return modelInstance;
    }

    function stubRemoteMethod(sandbox, model, methodName) {
        sandbox.stub(model, methodName, function() {
            var response = model[methodName].response || new ResponseFail(0, methodName + ': response not provided');
            var promise = {
                done: _.bind(response.runDone, response),
                fail: _.bind(response.runFail, response),
                always: _.bind(response.runAlways, response)
            };
            // set the data on model on done
            if (methodName == 'fetch') {
                promise.done(function(data) {
                    model.set(data);
                });
            }

            sandbox.spy(promise, 'done');
            sandbox.spy(promise, 'fail');
            sandbox.spy(promise, 'always');

            model[methodName].lastAnswer = promise;
            return promise;
        });
    }

    function ResponseSuccess(data, status, responseText) {
        this.data = data;
        this.request = {
            status: status,
            responseText: responseText
        }
    }

    ResponseSuccess.prototype.runDone = function() {
        _.each(arguments, _.bind(function(callback) {
            callback(this.data, 'test', this.request);
        }, this));
    };
    ResponseSuccess.prototype.runAlways = function() {
        _.each(arguments, function(callback) {callback()});
    };
    ResponseSuccess.prototype.runFail = function() {};


    function ResponseFail(status, responseText) {
        this.request = {
            status: status,
            responseText: responseText
        }
    }

    ResponseFail.prototype.runFail = function() {
        _.each(arguments, _.bind(function(callback) {
            callback(this.request, 'test', 'test');
        }, this));
    };
    ResponseFail.prototype.runAlways = function() {
        _.each(arguments, function(callback) {callback()});
    };
    ResponseFail.prototype.runDone = function() {};


    /**
     * Provides utilities to set up Backbone model and collection mocks. Main functions here are `setUpModel` and
     * `setUpCollection`, to mock out a Backbone model and collection, respectively. Refer to the docs of the particular
     * functions for details on how those differ.
     *
     * The mocking consists in replacing some original functions that would ordinarily attempt a request to a remote
     * server with simple Sinon stubs that read a `response` property on the stubbed method and construct a mock promise
     * based on that. The `response` can be either `responseSuccess` or `responseFail`, depending on whether we want
     * the particular method's imaginary call to the remote server to succeed or fail. Refer to `responseSuccess` or
     * `responseFail` for details about arguments that can be provided to those.
     *
     * Additionally for all mock `fetch` methods if the expected response is successful, Backbone `set` will be invoked
     * on the model with the data provided in the corresponding `responseSuccess`.
     *
     * The returned promise is stored as `lastAnswer` on the stubbed method. All methods are spied using Sinon and
     * therefore can be verified for whether they have been interacted with in the expected way.
     *
     * Example usage in a test:
     *
     * ```
     * var mockModel = MockModels.setUpModel(this, new MyModel({id: '123'}));
     * mockModel.fetch.response = MockModels.responseSuccess({foo: 'bar', baz: 'boom'});
     *
     * mockModel.fetch().done(function(data) {console.log(data)});
     *
     * assert.assertThat(mockModel.get('foo'), __.equalTo('bar'));
     * assert.assertThat(mockModel.get('baz'), __.equalTo('boom'));
     * assert.assertThat(mockModel.fetch.lastAnswer.done, __.calledOnce());
     * ```
     *
     */
    return {
        /**
         * Set up mock Backbone model out of real model instance. Note that this needs the real model implementation
         * rather than a mock to start with. This will stub all default Backbone methods that would normally result in
         * remote request to the server: `save()`, `fetch()` and `destroy`. Refer to the main docs for how this is done.
         *
         * @param currentTest - current QUnit test case with a `sandbox` field to use for stubbing
         * @param modelInstance - real model instance to set up
         */
        setUpModel: setUpModel,
        /**
         * Set up mock Backbone collection out of real collection instance. Note that this needs the real collection
         * implementation rather than a mock to start with. This will stub the default Backbone `fetch` method that
         * would normally result in a remote request to the server. Refer to the main docs for how this is done.
         *
         * @param currentTest - current QUnit test case with a `sandbox` field to use for stubbing
         * @param collectionInstance - real collection instance to set up
         */
        setUpCollection: setUpCollection,

        /**
         * Create a successful response definition for a mocked model method.
         *
         * @param data data
         * @param status (optional) response status
         * @param responseText (optional) response text
         * @returns {ResponseSuccess}
         */
        responseSuccess: function(data, status, responseText) {return new ResponseSuccess(data, status, responseText)},
        /**
         * Create a failed response definition for a mocked model method.
         *
         * @param status failed response status
         * @param responseText failed response text
         * @returns {ResponseFail}
         */
        responseFail: function(status, responseText) {return new ResponseFail(status, responseText)}
    };
});
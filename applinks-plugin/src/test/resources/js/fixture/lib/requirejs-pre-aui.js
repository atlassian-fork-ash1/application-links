/**
 * Rationale: aui-prototyping needs to create AJS global instead of AMD module.
 * "define.amd" is saved before the removal for a revert later.
 */
if (window.define) {
    window.__defineamd = window.define.amd;
    delete window.define.amd;
}
define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/feature/oauth-callback', {
        require: {
            main: 'applinks/feature/oauth-callback'
        },
        mocks: {
            main: QUnit.moduleMock('applinks/lib/window', new MockDeclaration(['open'])),
            newWindow: QUnit.mock(new MockDeclaration(['close']))
        },
        beforeEach: function(assert, OAuthCallback, windowMock, mocks) {
            windowMock.open.returns(mocks.newWindow);
        }
    });

    QUnit.test('OAuthCallback requires defined URL', function(assert, OAuthCallback) {
        function invalidConstructor() {
            new OAuthCallback()
        }
        assert.assertThat(invalidConstructor, __.throws(__.error('url: expected a non-empty string, was: <undefined>')));
    });

    QUnit.test('Success callback must be a function', function(assert, OAuthCallback) {
        function wrongSuccessCallback() {
            new OAuthCallback('http://test.com').onSuccess({foo: 'bar'});
        }
        assert.assertThat(wrongSuccessCallback,
            __.throws(__.error('onSuccess: expected a function, was: [object Object]')));
    });

    QUnit.test('Failure callback must be a function', function(assert, OAuthCallback) {
        function wrongFailureCallback() {
            new OAuthCallback('http://test.com').onFailure({foo: 'bar'});
        }
        assert.assertThat(wrongFailureCallback,
            __.throws(__.error('onFailure: expected a function, was: [object Object]')));
    });

    QUnit.test('OAuth dance with no callback ending with success', function(assert, OAuthCallback, windowMock, mocks) {
        var oAuthCallback = new OAuthCallback('http://test.com');
        oAuthCallback.open();

        assert.assertThat(windowMock.open.calledOnce, __.truthy());
        assert.assertThat(windowMock.open.firstCall.args,
            __.contains('http://test.com', 'com_atlassian_applinks_authentication'));
        assert.assertThat(windowMock.oauthCallback, __.is(oAuthCallback));

        oAuthCallback.success();

        assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
        assert.assertThat(windowMock.oauthCallback, __.undefined());
    });

    QUnit.test('OAuth dance with no callback ending with failure', function(assert, OAuthCallback, windowMock, mocks) {
        var oAuthCallback = new OAuthCallback('http://test.com');
        oAuthCallback.open();

        assert.assertThat(windowMock.open.calledOnce, __.truthy());
        assert.assertThat(windowMock.open.firstCall.args,
            __.contains('http://test.com', 'com_atlassian_applinks_authentication'));
        assert.assertThat(windowMock.oauthCallback, __.is(oAuthCallback));

        oAuthCallback.failure();

        assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
        assert.assertThat(windowMock.oauthCallback, __.undefined());
    });

    QUnit.test('OAuth dance with success callback ending with success',
        function(assert, OAuthCallback, windowMock, mocks) {

            var oAuthCallback = new OAuthCallback('http://test.com');
            var successCallback = this.sandbox.stub();
            oAuthCallback.onSuccess(successCallback);
            oAuthCallback.open();

            assert.assertThat(windowMock.open.calledOnce, __.truthy());

            oAuthCallback.success();

            assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
            assert.assertThat(successCallback, __.calledOnce());
        }
    );

    QUnit.test('OAuth dance with success callback ending with failure',
        function(assert, OAuthCallback, windowMock, mocks) {

            var oAuthCallback = new OAuthCallback('http://test.com');
            var successCallback = this.sandbox.stub();
            oAuthCallback.onSuccess(successCallback);
            oAuthCallback.open();

            assert.assertThat(windowMock.open.calledOnce, __.truthy());

            oAuthCallback.failure();

            assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
            assert.assertThat(successCallback.called, __.falsy());
        }
    );

    QUnit.test('OAuth dance with failure callback ending with failure',
        function(assert, OAuthCallback, windowMock, mocks) {

            var oAuthCallback = new OAuthCallback('http://test.com');
            var failureCallback = this.sandbox.stub();
            oAuthCallback.onFailure(failureCallback);
            oAuthCallback.open();

            assert.assertThat(windowMock.open.calledOnce, __.truthy());

            oAuthCallback.failure();

            assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
            assert.assertThat(failureCallback.calledOnce, __.truthy());
        }
    );

    QUnit.test('OAuth dance with failure callback ending with success',
        function(assert, OAuthCallback, windowMock, mocks) {

            var oAuthCallback = new OAuthCallback('http://test.com');
            var failureCallback = this.sandbox.stub();
            oAuthCallback.onFailure(failureCallback);
            oAuthCallback.open();

            assert.assertThat(windowMock.open.calledOnce, __.truthy());

            oAuthCallback.success();

            assert.assertThat(mocks.newWindow.close.calledOnce, __.truthy());
            assert.assertThat(failureCallback.called, __.falsy());
        });

    QUnit.test('OAuth dance with undefined source', function(assert, OAuthCallback) {
        var invalidCall = function() {
            new OAuthCallback('http://test.com').source(undefined);
        };

        assert.assertThat(invalidCall, __.throws(__.error('source: expected a value')));
    });

    QUnit.test('OAuth dance success callback with source', function(assert, OAuthCallback) {
        var successCallback = this.sandbox.stub();

        var oAuthCallback = new OAuthCallback('http://test.com')
            .source('test source')
            .onSuccess(successCallback);

        oAuthCallback.open();
        oAuthCallback.success();

        assert.assertThat(successCallback, __.calledOnceWith('test source'));
    });

    QUnit.test('OAuth dance failure callback with source', function(assert, OAuthCallback) {
        var failureCallback = this.sandbox.stub();

        var oAuthCallback = new OAuthCallback('http://test.com')
            .source('test source')
            .onFailure(failureCallback);

        oAuthCallback.open();
        oAuthCallback.failure();

        assert.assertThat(failureCallback, __.calledOnceWith('test source'));
    });
});
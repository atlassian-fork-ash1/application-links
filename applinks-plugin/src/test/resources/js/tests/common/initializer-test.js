define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    _,
    QUnit,
    __,
    MockDeclaration
) {

    var mockReady;
    var mockJquery = QUnit.moduleMock('applinks/lib/jquery', function() {
        return function fakejQuery() {
            mockReady(arguments);
        }
    });

    QUnit.module('applinks/common/initializer', {
        require: {
            main: 'applinks/common/initializer'
        },
        mocks: {
            jquery: mockJquery,
            noninit: QUnit.mock(new MockDeclaration(['noninit', 'notinitialize'])),
            withinit: QUnit.mock(new MockDeclaration(['init'])),
            withinitialize: QUnit.mock(new MockDeclaration(['initialize']))
        },
        beforeEach: function() {
            mockReady = this.sandbox.stub();
        }
    });

    QUnit.test('init with null module', function(assert, Initializer) {
        function test() {
            Initializer.init(null);
        }

        assert.assertThat(test, __.not(__.throws()));
    });

    QUnit.test('init with non-null module without init callback', function(assert, Initializer, mocks) {
        Initializer.init(mocks.noninit);

        assertNonInitMethodsNotCalled(assert, mocks.noninit);
    });

    QUnit.test('init with non-null module with init non-function', function(assert, Initializer, mocks) {
        mocks.noninit.init = 'not a function';

        Initializer.init(mocks.noninit);

        assertNonInitMethodsNotCalled(assert, mocks.noninit);
    });

    QUnit.test('init with non-null module with init function', function(assert, Initializer, mocks) {
        Initializer.init(mocks.withinit);

        assert.assertThat(mocks.withinit.init, __.calledOnce());

    });

    QUnit.test('init with non-null module with initialize function', function(assert, Initializer, mocks) {
        Initializer.init(mocks.withinitialize);

        assert.assertThat(mocks.withinitialize.initialize, __.calledOnce());

    });

    QUnit.test('init with non-null module with both init and initialize function', function(assert, Initializer, mocks) {
        var mockModule = _.extend({}, mocks.withinit, mocks.withinitialize);

        Initializer.init(mockModule);

        assert.assertThat(mockModule.init, __.calledOnce());
        assert.assertThat(mockModule.initialize, __.calledOnce());

    });

    QUnit.test('init with multiple modules', function(assert, Initializer, mocks) {
        Initializer.init(mocks.withinit, mocks.withinitialize);

        assert.assertThat(mocks.withinit.init, __.calledOnce());
        assert.assertThat(mocks.withinitialize.initialize, __.calledOnce());

    });

    QUnit.test('initOnDomReady with null module', function(assert, Initializer, mocks) {
        Initializer.initOnDomReady(null);
        runDomContentLoaded(assert);
    });

    QUnit.test('initOnDomReady with non-null module without init callback', function(assert, Initializer, mocks) {
        Initializer.initOnDomReady(mocks.noninit);

        runDomContentLoaded(assert);
        assertNonInitMethodsNotCalled(assert, mocks.noninit);
    });

    QUnit.test('initOnDomReady with non-null module with init non-function', function(assert, Initializer, mocks) {
        mocks.noninit.init = 'not a function';

        Initializer.initOnDomReady(mocks.noninit);

        runDomContentLoaded(assert);
        assertNonInitMethodsNotCalled(assert, mocks.noninit);
    });

    QUnit.test('initOnDomReady with non-null module with init function', function(assert, Initializer, mocks) {
        Initializer.initOnDomReady(mocks.withinit);

        // init should not have been called just yet
        assert.assertThat(mocks.withinit.init, __.notCalled());
        // run ready callback, which should call init
        runDomContentLoaded(assert);
        assert.assertThat(mocks.withinit.init, __.calledOnce());

    });

    QUnit.test('initOnDomReady with non-null module with initialize function', function(assert, Initializer, mocks) {
        Initializer.initOnDomReady(mocks.withinitialize);

        // initialize should not have been called just yet
        assert.assertThat(mocks.withinitialize.initialize, __.notCalled());
        // run ready callback, which should call initialize
        runDomContentLoaded(assert);
        assert.assertThat(mocks.withinitialize.initialize, __.calledOnce());

    });

    QUnit.test('initOnDomReady with non-null module with both init and initialize function', function(assert, Initializer, mocks) {
        var mockModule = _.extend({}, mocks.withinit, mocks.withinitialize);

        Initializer.initOnDomReady(mockModule);

        // module init callbacks should not have been called just yet
        assert.assertThat(mockModule.init, __.notCalled());
        assert.assertThat(mockModule.initialize, __.notCalled());
        // run ready callback, which should call both init and initialize
        runDomContentLoaded(assert);
        assert.assertThat(mockModule.init, __.calledOnce());
        assert.assertThat(mockModule.initialize, __.calledOnce());

    });

    QUnit.test('initOnDomReady with multiple modules', function(assert, Initializer, mocks) {
        Initializer.initOnDomReady(mocks.withinit, mocks.withinitialize);

        // run both callbacks added by initOnDomReady for each module
        runDomContentLoaded(assert, 2);
        assert.assertThat(mocks.withinit.init, __.calledOnce());
        assert.assertThat(mocks.withinitialize.initialize, __.calledOnce());
    });

    function runDomContentLoaded(assert, times) {
        var timeAssertion = times ? __.equalTo(times) : __.greaterThan(0);
        assert.assertThat(mockReady, __.calledTimes(timeAssertion));
        for (var i = 0; i < mockReady.callCount; i++) {
            var fn = mockReady.getCall(i).args[0][0];
            fn.call();
        }
    }

    function assertNonInitMethodsNotCalled(assert, nonInitMock) {
        assert.assertThat(nonInitMock.noninit, __.notCalled());
        assert.assertThat(nonInitMock.notinitialize, __.notCalled());
    }
});

define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/hamjest-applink-status',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks',
    'applinks/test/mock-common'
], function(
    _,
    QUnit,
    __,
    ApplinkStatusMatchers,
    MockDeclaration,
    MockCallbacks,
    ApplinksCommonMocks
) {
    var ApplinkModelConstants = {
        DataKeys: {
            CONFIG_URL: 'configUrl',
            ICON_URI: 'iconUri',
            EDITABLE: 'editable',
            V3_EDITABLE: 'v3editable'
        }
    };

    var Arrows = {
        RIGHT_MATCH : 'right-match',
        RIGHT_MISMATCH : 'right-mismatch',
        RIGHT_DISABLED : 'right-disabled',
        LEFT_MATCH: 'left-match',
        LEFT_MISMATCH: 'left-mismatch',
        LEFT_DISABLED: 'left-disabled'
    };

    QUnit.module('applinks/page/applink-edit', {
        require: {
            main: 'applinks/page/applink-edit',
            jQuery: 'applinks/lib/jquery',
            ApplinkStatusErrors: 'applinks/feature/status/errors',
            OAuthPickerModel: 'applinks/feature/oauth-picker-model'
        },
        mocks: {
            // module mocks
            formNotification: QUnit.moduleMock('aui/form-notification', new MockDeclaration([])),
            formValidation: QUnit.moduleMock('aui/form-validation', new MockDeclaration([])),
            AJS: QUnit.moduleMock('applinks/lib/aui',
                new MockDeclaration(['messages.error', 'dialog2.on', 'I18n.getText'])),
            Browser: QUnit.moduleMock('applinks/common/browser', new MockDeclaration(['isMicrosoft'])),
            ApplinksProducts: ApplinksCommonMocks.mockProductsModule(QUnit),
            ApplinksRest: ApplinksCommonMocks.mockRestModule(QUnit),
            ApplinkStatus: QUnit.moduleMock('applinks/feature/status',
                new MockDeclaration(['View'], {AUTHENTICATE_LINK_SELECTOR: '.auth-link'})),
            ApplinkStatusDetails: QUnit.moduleMock('applinks/feature/status/details',
                new MockDeclaration(['Notification', 'HelpLink'])),
            ApplinksUrls: QUnit.moduleMock('applinks/common/urls', new MockDeclaration(['Local.admin'])),
            InlineDialog: QUnit.moduleMock('applinks/component/inline-dialog',
                new MockDeclaration(['contents', 'show', 'hide', 'isVisible', 'onShow', 'onHide']).withConstructor()),
            InlineDialogFindContents: QUnit.mock(new MockDeclaration(['find', 'remove', 'on'])),
            InlineDialogHandler: QUnit.mock(new MockDeclaration(['on'])),
            HelpLinkAnalytics: QUnit.moduleMock('applinks/feature/help-link/analytics',
                new MockDeclaration(['init'])),
            main: QUnit.moduleMock('applinks/model/applink', new MockDeclaration([], ApplinkModelConstants)),
            OAuthDance: QUnit.moduleMock('applinks/feature/oauth-dance', new MockDeclaration(
                ['initialize', 'onSuccess']).withConstructor()),
            OAuthPicker: QUnit.moduleMock('applinks/feature/oauth-picker', new MockDeclaration(['View'])),
            window: QUnit.moduleMock('applinks/lib/window', function() { return {location: {href: 'applinks_edit'}}}),
            // non-module mocks
            ApplinkModel: QUnit.mock(new MockDeclaration(['attributes', 'fetch', 'fetchStatus', 'get', 'getData', 'on',
                'save', 'toJSON', 'getAdminUrl']).withConstructor()),
            fetchPromise: ApplinksCommonMocks.mockPromiseModule(QUnit),
            savePromise: ApplinksCommonMocks.mockPromiseModule(QUnit),
            OAuthPickerView: QUnit.mock(new MockDeclaration(['getModel', 'onChangeHandler']))
        },
        templates: {
            "applinks.feature.oauth.dropdown": function (params) {
                return '<span id="@name" data-oauth-id="@oauthId" data-oauth-name="@oauthName"></span>'
                    .replace(/@name/g, params.name)
                    .replace(/@oauthId/g, params.currentAuthType.id)
                    .replace(/@oauthName/g, params.currentAuthType.name);
            },
            "applinks.page.applink.edit.status": function (params) {
                return JSON.stringify(params);
            },
            "applinks.page.applink.edit.connectivityArrow" : function(params) {
                return '<span data-src="@direction-@status" class="v3-connectivity-arrow" />'
                    .replace(/@direction/g, params.direction)
                    .replace(/@status/g, params.status);
            },
            "applinks.page.applink.edit.oauthMismatchInlineDialogContents": function(params) {
                return '<span id="@id">@remoteAppUrl</span>'
                    .replace(/@remoteAppUrl/g, params.remoteAppUrl)
                    .replace(/@id/g, params.id);
            }
        },

        beforeEach: function(assert, ApplinksEdit, ApplinkModel, mocks) {
            this.jQuery.fn.spin = this.sandbox.spy();
            this.jQuery.fn.spinStop = this.sandbox.spy();

            new ApplinksCommonMocks.Stubber(this)
                .setUpPromise(mocks.savePromise)
                .setUpPromise(mocks.fetchPromise)
                .setUpRest(mocks.ApplinksRest);
            
            ApplinkModel.Applink = mocks.ApplinkModel;
            mocks.ApplinkObject = createApplink();
            ApplinkModel.Applink.answer.save.returns(mocks.savePromise);
            ApplinkModel.Applink.answer.fetch.returns(mocks.fetchPromise);
            ApplinkModel.Applink.answer.toJSON.returns(mocks.ApplinkObject);
            ApplinkModel.Applink.answer.getAdminUrl.returns('applinks_admin');

            mocks.ApplinksUrls.Local.admin.returns('applinks_admin');
            mocks.OAuthDance.answer.onSuccess.returns(mocks.OAuthDance.answer);

            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.oauth').returns('OAuth');
            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.oauth.impersonation').returns('OAuth (impersonation)');
            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.disabled').returns('Disabled');
            mocks.AJS.escapeHtml = function(summary){ return AJS.escapeHtml(summary); };

            // AUI 5.8 and non-IE by default
            mocks.AJS.versionDetails = { is58: true, is59: false};
            mocks.Browser.isMicrosoft.returns(false);

            mocks.InlineDialog.answer.contents.returns(mocks.InlineDialogFindContents);
            mocks.InlineDialogFindContents.find.returns(mocks.InlineDialogHandler);

            this.fixture.append('<div>' +
                '<div id="applink-edit-header"><span class="status-lozenge"></span></div>' +
                '<form data-applink-id="1234abcd">' +
                '<input id="display-url"><input id="application-url"><input id="application-name">' +
                '<span class="button-spinner"></span>' +
                '<span id="applink-outgoing-auth-arrow" />' +
                '<span id="applink-incoming-auth-arrow" />' +
                '<a href="" id="button-cancel"></a><button id="button-submit"></button>' +
                '</form>' +
                '</div>');
            this.fixture.append('<div id="aui-message-bar">FAIL</div>');
            this.fixture.append('<div id="applink-outgoing-remote-auth-section"></div><div id="applink-outgoing-auth-arrow" /><div id="applink-incoming-remote-auth-section"></div>');
        },
        afterEach: function() {
            delete this.jQuery.fn.spin;
            delete this.jQuery.fn.spinStop;
        },
        setupOAuthDropdown: function(mocks) {
            this.oauthConfigIncoming = this.OAuthPickerModel.create(mocks.ApplinkObject.status.localAuthentication.incoming);
            this.oauthConfigOutgoing = this.OAuthPickerModel.create(mocks.ApplinkObject.status.localAuthentication.outgoing);
            mocks.OAuthPickerView.getModel.onFirstCall().returns(this.oauthConfigOutgoing);
            mocks.OAuthPickerView.getModel.onSecondCall().returns(this.oauthConfigIncoming);
            mocks.OAuthPicker.View.returns(mocks.OAuthPickerView);
        },
        /**
         * Set up mock status on both mock Applink Model and Applink Object
         * @param status status to set up
         * @param mocks mocks map
         */
        setUpStatus: function(status, mocks) {
            mocks.ApplinkModel.answer.get.withArgs('statusLoaded').returns(true);
            mocks.ApplinkModel.answer.get.withArgs('status').returns(status);
            mocks.ApplinkObject.status = status;
            mocks.ApplinkObject.statusLoaded = true;

            this.setupOAuthDropdown(mocks);
        }
    });

    QUnit.test('Initializes help link analytics', function(assert, ApplinksEdit, mockApplink, mocks) {
        ApplinksEdit.init();
        assert.assertThat(mocks.HelpLinkAnalytics.init, __.calledOnce());
    });

    QUnit.test('Initial features state', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        assert.assertThat(ApplinkModel.Applink, __.calledWithNew());

        MockCallbacks.runEventCallback(assert, ApplinkModel.Applink.answer, 'change', [{
            attributes: {
                name: 'PASS',
                rpcUrl: 'PASS',
                displayUrl: 'PASS'
            }
        }]);

        assert.assertThat(this.fixture.find('#application-name').val(), __.is('PASS'));
        assert.assertThat(this.fixture.find('#application-url').val(), __.is('PASS'));
        assert.assertThat(this.fixture.find('#display-url').val(), __.is('PASS'));

        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.notCalled());
    });

    QUnit.test('Should wire up OAuth Dance for authenticate links', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        assert.assertThat(ApplinkModel.Applink.answer.fetchStatus, __.calledOnce());
        assert.assertThat(mocks.OAuthDance, __.calledWithNew());
        assert.assertThat(mocks.OAuthDance, __.calledOnceWith(
            __.object(), __.equalTo(mocks.ApplinkStatus.AUTHENTICATE_LINK_SELECTOR)));
        assert.assertThat(mocks.OAuthDance.answer.onSuccess, __.calledOnce());
        assert.assertThat(mocks.OAuthDance.answer.initialize, __.calledOnce());

        // simulate successful Dance
        MockCallbacks.runCallback(assert, mocks.OAuthDance.answer.onSuccess);

        // make sure fetchStatus called again
        assert.assertThat(ApplinkModel.Applink.answer.fetchStatus, __.calledTwice());
    });

    QUnit.test('Status rendering when applink already loaded ', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        // status not loaded to start with
        ApplinkModel.Applink.answer.get.withArgs('statusLoaded').returns(false);

        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledTwiceWith(__.allOf(
            __.hasProperty('applink'),
            __.hasProperty('clickable', false)
        )));

        // simulate successful status fetch
        this.setUpStatus(createStatusWorking(), mocks);
        MockCallbacks.runEventCallback(assert, ApplinkModel.Applink.answer, 'change:status');

        // make sure status view is refreshed with the fetched status
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink',
                __.hasProperty('statusLoaded', true),
                __.hasProperty('status', __.hasProperty('working', true))),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWith(__.allOf(
            __.hasProperty('applink', ApplinkModel.Applink.answer),
            __.hasProperty('container')
        )));
    });

    QUnit.test('Render status details for OAuth mismatch', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusMismatchError(this.ApplinkStatusErrors), mocks);
        initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError(this.ApplinkStatusErrors.AUTH_LEVEL_MISMATCH)),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);
        assertArrowsMismatchShouldHaveInlineDialogRendered(assert, mocks);
    });

    QUnit.test('Render status details for OAuth mismatch with AUI 5.9 and IE', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        mocks.Browser.isMicrosoft.returns(true);
        mocks.AJS.versionDetails = { is58: false, is59: true};

        this.setUpStatus(createStatusMismatchError(this.ApplinkStatusErrors), mocks);
        ApplinksEdit.init();

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError(this.ApplinkStatusErrors.AUTH_LEVEL_MISMATCH)),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        // ApplinksEdit.updateOutgoingOAuthArrow()/ApplinksEdit.updateIncomingOAuthArrow() should not be called for IE
        // during ApplinksEdit.init() - when single select is rendered, a change event is raised in IE or under AUI 5.8.
        // Since single select is mocked here, arrows won't be rendered
        assert.assertThat(mocks.OAuthPickerView.getModel, __.notCalled());
        assert.assertThat(mocks.InlineDialog, __.notCalled());
    });

    QUnit.test('Render status details for OAuth mismatch with AUI 5.9 and non-IE browser', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        mocks.Browser.isMicrosoft.returns(false);
        mocks.AJS.versionDetails = { is58: false, is59: true};

        this.setUpStatus(createStatusMismatchError(this.ApplinkStatusErrors), mocks);
        ApplinksEdit.init();

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError(this.ApplinkStatusErrors.AUTH_LEVEL_MISMATCH)),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);
        assertArrowsMismatchShouldHaveInlineDialogRendered(assert, mocks);
    });

    QUnit.test('Render status details for OAuth mismatch with AUI 6.0 and non-IE browser', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        mocks.Browser.isMicrosoft.returns(false);
        mocks.AJS.versionDetails = { is58: false, is59: false};

        this.setUpStatus(createStatusMismatchError(this.ApplinkStatusErrors), mocks);
        ApplinksEdit.init();

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError(this.ApplinkStatusErrors.AUTH_LEVEL_MISMATCH)),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);
        assertArrowsMismatchShouldHaveInlineDialogRendered(assert, mocks);
    });

    QUnit.test('Render status details for OAuth mismatch disabled', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusMismatchErrorForDisabledOAuth(), mocks);
        initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError({category: 'DISABLED', type: 'DISABLED'})),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        assertArrows(assert, this.fixture, Arrows.RIGHT_DISABLED, Arrows.LEFT_DISABLED);
        assert.assertThat(mocks.InlineDialog, __.notCalled());
    });

    QUnit.test('Render status details for OAuth Network Error that has remote oauth', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(_createStatus(this.ApplinkStatusErrors.OAUTH_TIMESTAMP_REFUSED, _createDefaultAuth(), _createDefaultAuth()), mocks);
        initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError({category: 'NETWORK_ERROR', type: 'OAUTH_TIMESTAMP_REFUSED'})),
            __.hasProperty('clickable', false)
        )));
        assert.assertThat(mocks.ApplinkStatusDetails.Notification, __.calledWithNew());

        assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);
        assert.assertThat(mocks.InlineDialog, __.notCalled());
    });


    QUnit.test('Render local oauth authentication', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusWorking(), mocks);
        initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

        assert.assertThat(mocks.OAuthPicker.View.calledWithNew(), __.truthy());

        var expectedOAuthConfig = new this.OAuthPickerModel(true, true, false);
        assert.assertThat(mocks.OAuthPicker.View.firstCall.args[0], __.is('#applink-outgoing-local-auth-section'));
        assert.assertThat(mocks.OAuthPicker.View.firstCall.args[1], __.is('applink-outgoing-local-auth'));
        assert.assertThat(expectedOAuthConfig.equals(mocks.OAuthPicker.View.firstCall.args[2]), __.truthy());

        assert.assertThat(mocks.OAuthPicker.View.secondCall.args[0], __.is('#applink-incoming-local-auth-section'));
        assert.assertThat(mocks.OAuthPicker.View.secondCall.args[1], __.is('applink-incoming-local-auth'));
        assert.assertThat(expectedOAuthConfig.equals(mocks.OAuthPicker.View.secondCall.args[2]), __.truthy());

        assertArrows(assert, this.fixture, Arrows.RIGHT_MATCH, Arrows.LEFT_MATCH);
        assert.assertThat(mocks.InlineDialog, __.notCalled());
    });

    QUnit.test('Render remote oauth authentication status: incoming enabled, outgoing disabled',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            var OAuthPickerModel = this.OAuthPickerModel;

            // set up status
            var status = createStatusWorking();
            status.remoteAuthentication.incoming = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_ID).toJSON();
            status.remoteAuthentication.outgoing = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_DISABLED_ID).toJSON();
            this.setUpStatus(status, mocks);
            // set up extra data required to render remote authentication
            mocks.ApplinkObject.data = {
                iconUri: 'jira-icon',
                configUrl: mocks.ApplinkObject.rpcUrl
            };

            initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

            var remoteIncoming = parseDataFromHtml(assert, this.fixture.find("#applink-incoming-remote-auth-section"));
            var remoteOutgoing = parseDataFromHtml(assert, this.fixture.find("#applink-outgoing-remote-auth-section"));

            assertRemoteAuthentication(assert, remoteIncoming, 'OAuth');
            assertRemoteAuthentication(assert, remoteOutgoing, 'Disabled');

            assertArrows(assert, this.fixture, Arrows.RIGHT_MATCH, Arrows.LEFT_MISMATCH);
            assert.assertThat(mocks.InlineDialog, __.calledOnce());
            assert.assertThat(mocks.InlineDialog.firstCall.args[1], __.is(getInlineDialogExpectedContent('left')));
        });

    QUnit.test('Render remote oauth authentication status: incoming enabled with impersonation, outgoing enabled',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            var OAuthPickerModel = this.OAuthPickerModel;

            // set up status
            var status = createStatusWorking();
            status.remoteAuthentication.incoming = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_IMPERSONATION_ID).toJSON();
            status.remoteAuthentication.outgoing = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_ID).toJSON();
            this.setUpStatus(status, mocks);
            // set up extra data required to render remote authentication
            mocks.ApplinkObject.data = {
                iconUri: 'jira-icon',
                configUrl: mocks.ApplinkObject.rpcUrl
            };

            initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

            var remoteIncoming = parseDataFromHtml(assert, this.fixture.find("#applink-incoming-remote-auth-section"));
            var remoteOutgoing = parseDataFromHtml(assert, this.fixture.find("#applink-outgoing-remote-auth-section"));

            assertRemoteAuthentication(assert, remoteIncoming, 'OAuth (impersonation)');
            assertRemoteAuthentication(assert, remoteOutgoing, 'OAuth');

            assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MATCH);
            assert.assertThat(mocks.InlineDialog, __.calledOnce());
            assert.assertThat(mocks.InlineDialog.firstCall.args[1], __.is(getInlineDialogExpectedContent('right')));
        });

    QUnit.test('Render remote oauth authentication status: incoming disabled, outgoing enabled with impersonation',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            var OAuthPickerModel = this.OAuthPickerModel;

            // set up status
            var status = createStatusWorking();
            status.remoteAuthentication.incoming = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_DISABLED_ID).toJSON();
            status.remoteAuthentication.outgoing = OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_IMPERSONATION_ID).toJSON();
            this.setUpStatus(status, mocks);
            // set up extra data required to render remote authentication
            mocks.ApplinkObject.data = {
                iconUri: 'jira-icon',
                configUrl: mocks.ApplinkObject.rpcUrl
            };

            initApplinksEditAndOAuthArrows(assert,mocks, ApplinksEdit, mocks.ApplinkObject.status);

            var remoteIncoming = parseDataFromHtml(assert, this.fixture.find("#applink-incoming-remote-auth-section"));
            var remoteOutgoing = parseDataFromHtml(assert, this.fixture.find("#applink-outgoing-remote-auth-section"));

            assertRemoteAuthentication(assert, remoteIncoming, 'Disabled');
            assertRemoteAuthentication(assert, remoteOutgoing, 'OAuth (impersonation)');

            assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);
            assertArrowsMismatchShouldHaveInlineDialogRendered(assert, mocks);
        });

    QUnit.test('Render remote oauth authentication status: network error with no remote auth',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            // set up status
            var status = createStatusError(this.ApplinkStatusErrors.CONNECTION_REFUSED);
            delete status.remoteAuthentication;
            this.setUpStatus(status, mocks);
            // set up extra data required to render remote authentication
            mocks.ApplinkObject.data = {
                iconUri: 'jira-icon',
                configUrl: mocks.ApplinkObject.rpcUrl
            };

            initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

            var remoteIncoming = parseDataFromHtml(assert, this.fixture.find("#applink-incoming-remote-auth-section"));
            var remoteOutgoing = parseDataFromHtml(assert, this.fixture.find("#applink-outgoing-remote-auth-section"));
            assertRemoteAuthentication(assert, remoteIncoming, '');
            assertRemoteAuthentication(assert, remoteOutgoing, '');

            assertArrows(assert, this.fixture, Arrows.RIGHT_MISMATCH, Arrows.LEFT_MISMATCH);

            // Network error so there is no remote oauth info - inline dialog is not rendered in this scenario
            assert.assertThat(mocks.InlineDialog, __.notCalled());

            // render troubleshooting link
            assert.assertThat(mocks.ApplinkStatusDetails.HelpLink, __.calledWith(__.allOf(
                __.hasProperty('applink', ApplinkModel.Applink.answer),
                __.hasProperty('container'),
                __.hasProperty('extraClasses')
            )));
        });

    QUnit.test('Render remote oauth authentication status: access error - no auth token ',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            testOAuthTokenMissing(this, assert, ApplinksEdit, mocks, this.ApplinkStatusErrors.REMOTE_AUTH_TOKEN_REQUIRED);
        });

    QUnit.test('Render local oauth authentication status: access error - no auth token ',
        function(assert, ApplinksEdit, ApplinkModel, mocks) {
            testOAuthTokenMissing(this, assert, ApplinksEdit, mocks, this.ApplinkStatusErrors.LOCAL_AUTH_TOKEN_REQUIRED);
        });

    QUnit.test('Save with no error', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusWorking(), mocks);

        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);

        var applicationName = this.fixture.find('#application-name');
        var applicationUrl = this.fixture.find('#application-url');
        var displayUrl = this.fixture.find('#display-url');

        // Add these attributes to test that submitting removes them
        applicationName.attr('data-aui-notification-error', 'Test');
        applicationUrl.attr('data-aui-notification-error', 'Test');
        displayUrl.attr('data-aui-notification-error', 'Test');

        // Test values should be saved in the model data
        applicationName[0].value = 'TEST';
        applicationUrl[0].value = 'TEST';
        displayUrl[0].value = 'TEST';

        // reset spinner spies called in ApplinksEdit.init()
        this.jQuery.fn.spin.reset();
        this.jQuery.fn.spinStop.reset();

        this.fixture.find('#button-submit').click();

        // Verify data from text fields was correctly saved
        assert.assertThat(ApplinkModel.Applink.answer.save, __.calledOnceWith(
            {displayUrl: 'TEST', rpcUrl: 'TEST', name: 'TEST'}, defaultSaveOptions()));

        // spinners started
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.notCalled());

        // Invoke callbacks that indicate success
        var oAuthStatusPromise = mocks.ApplinksRest.V3.oAuthStatus.answer.put.answer;
        MockCallbacks.runCallbacks(assert, mocks.savePromise.done);
        MockCallbacks.runCallbacks(assert, oAuthStatusPromise.always);
        MockCallbacks.runCallbacks(assert, oAuthStatusPromise.done);

        // assert no form errors
        assert.assertThat(this.fixture.find('#aui-message-bar').html(), __.is(''));
        assert.assertThat(mocks.AJS.messages.error, __.notCalled());
        assert.assertThat(applicationName.attr('data-aui-notification-error'), __.is(__.undefined()));
        assert.assertThat(applicationUrl.attr('data-aui-notification-error'), __.is(__.undefined()));
        assert.assertThat(displayUrl.attr('data-aui-notification-error'),  __.is(__.undefined()));

        // spinners stopped
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.calledOnce());

        // verify REST PUT payload for Local OAuth Authentication
        var jsonPayload = JSON.parse(mocks.ApplinksRest.V3.oAuthStatus.answer.put.args[0]);
        var incomingAuthArg = this.OAuthPickerModel.create(jsonPayload.incoming);
        var outgoingAuthArg = this.OAuthPickerModel.create(jsonPayload.outgoing);

        assert.assertThat(this.oauthConfigIncoming, __.equalTo(incomingAuthArg));
        assert.assertThat(this.oauthConfigOutgoing, __.equalTo(outgoingAuthArg));

        // navigated back to Applinks admin
        assert.assertThat(mocks.window.location.href, __.equalTo('applinks_admin'));
    });

    QUnit.test('Save with error', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusMismatchError(this.ApplinkStatusErrors), mocks);

        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);

        var applicationName = this.fixture.find('#application-name');
        var applicationUrl = this.fixture.find('#application-url');
        var displayUrl = this.fixture.find('#display-url');

        // Add these attributes to test that submitting removes them
        applicationName.attr('data-aui-notification-error', 'Test');
        applicationUrl.attr('data-aui-notification-error', 'Test');
        displayUrl.attr('data-aui-notification-error', 'Test');

        // Test values should be saved in the model data
        applicationName[0].value = 'TEST';
        applicationUrl[0].value = 'TEST';
        displayUrl[0].value = 'TEST';

        // reset spinner spies called in ApplinksEdit.init()
        this.jQuery.fn.spin.reset();
        this.jQuery.fn.spinStop.reset();

        this.fixture.find('#button-submit').click();

        // Verify data from text fields was correctly saved
        assert.assertThat(ApplinkModel.Applink.answer.save,
            __.calledOnceWith({displayUrl: 'TEST', rpcUrl: 'TEST', name: 'TEST'}, defaultSaveOptions()));

        // spinners started
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.notCalled());

        // Invoke callbacks that indicate error
        MockCallbacks.runCallbacks(assert, mocks.savePromise.fail, [{
            status: 400,
            responseText: '{"status":400,"errors":[{"summary":"Missing name.<script>alert()</script>","context":"name"},{"summary":"Malformed URL.","context":"rpcUrl"},{"summary":"Malformed URL.","context":"displayUrl"}, {"summary":"Example Error"}]}'
        }]);

        // assert form errors
        assert.assertThat(applicationName.attr('data-aui-notification-error'), __.is('Missing name.&lt;script&gt;alert()&lt;/script&gt;'));
        assert.assertThat(applicationUrl.attr('data-aui-notification-error'), __.is('Malformed URL.'));
        assert.assertThat(displayUrl.attr('data-aui-notification-error'),  __.is('Malformed URL.'));
        assert.assertThat(mocks.AJS.messages.error, __.calledOnceWith({ body: 'Example Error'}));

        // spinners stopped
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.calledOnce());

        // stay on current page
        assert.assertThat(mocks.window.location.href, __.equalTo('applinks_edit'));
    });

    QUnit.test('Save with OAuth error', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        this.setUpStatus(createStatusWorking(), mocks);

        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);

        var applicationName = this.fixture.find('#application-name');
        var applicationUrl = this.fixture.find('#application-url');
        var displayUrl = this.fixture.find('#display-url');

        // Add these attributes to test that submitting removes them
        applicationName.attr('data-aui-notification-error', 'Test');
        applicationUrl.attr('data-aui-notification-error', 'Test');
        displayUrl.attr('data-aui-notification-error', 'Test');

        // Test values should be saved in the model data
        applicationName[0].value = 'TEST';
        applicationUrl[0].value = 'TEST';
        displayUrl[0].value = 'TEST';

        // reset spinner spies called in ApplinksEdit.init()
        this.jQuery.fn.spin.reset();
        this.jQuery.fn.spinStop.reset();

        this.fixture.find('#button-submit').click();

        // Verify data from text fields was correctly saved
        assert.assertThat(ApplinkModel.Applink.answer.save,
            __.calledOnceWith({displayUrl: 'TEST', rpcUrl: 'TEST', name: 'TEST'}, defaultSaveOptions()));

        // spinners started
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.notCalled());

        // Invoke callbacks that indicate save success, but OAuth save failure (x2)
        var oAuthStatusPromise = mocks.ApplinksRest.V3.oAuthStatus.answer.put.answer;
        MockCallbacks.runCallbacks(assert, mocks.savePromise.done);
        MockCallbacks.runCallbacks(assert, oAuthStatusPromise.always);
        MockCallbacks.runCallbacks(assert, oAuthStatusPromise.fail, [{
            status: 403,
            responseText: '{"status":403,"errors":[{"summary":"Example Error 403"}]}'
        }]);
        MockCallbacks.runCallbacks(assert, oAuthStatusPromise.fail, [{
            status: 409,
            responseText: '{"status":409,"errors":[{"summary":"Example Error 409"}]}'
        }]);

        // assert form errors
        assert.assertThat(mocks.AJS.messages.error, __.calledTwice());
        assert.assertThat(mocks.AJS.messages.error, __.calledWithAt(0, {body: 'Example Error 403'}));
        assert.assertThat(mocks.AJS.messages.error, __.calledWithAt(1, {body: 'Example Error 409'}));

        // spinners started and stopped
        assert.assertThat(this.jQuery.fn.spin, __.calledOnce());
        assert.assertThat(this.jQuery.fn.spinStop, __.calledOnce()); // invoked only once in the "always" callback

        // stay on current page
        assert.assertThat(mocks.window.location.href, __.equalTo('applinks_edit'));
    });

    QUnit.test('Cancel changes', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);

        var applicationName = this.fixture.find('#application-name');
        var applicationUrl = this.fixture.find('#application-url');
        var displayUrl = this.fixture.find('#display-url');

        // Add these attributes to test that cancelling removes them
        applicationName.attr('data-aui-notification-error', 'Test');
        applicationUrl.attr('data-aui-notification-error', 'Test');
        displayUrl.attr('data-aui-notification-error', 'Test');

        ApplinkModel.Applink.answer.attributes = {
            name: 'PASS',
            rpcUrl: 'PASS',
            displayUrl: 'PASS'
        };

        ApplinksEdit.cancel();

        assert.assertThat(applicationName[0].value, __.is('PASS'));
        assert.assertThat(applicationUrl[0].value, __.is('PASS'));
        assert.assertThat(displayUrl[0].value, __.is('PASS'));

        assert.assertThat(applicationName.attr('data-aui-notification-error'), __.is(__.undefined()));
        assert.assertThat(applicationUrl.attr('data-aui-notification-error'), __.is(__.undefined()));
        assert.assertThat(displayUrl.attr('data-aui-notification-error'),  __.is(__.undefined()));
    });

    QUnit.test('Status not rendered if applink data is not loaded', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        mocks.ApplinkObject.name = undefined;

        ApplinksEdit.init();

        this.setUpStatus(createStatusWorking(), mocks);

        //should be empty (templates not called because neither applink status data, nor applink metadata(name, url etc)
        // has been retrieved)
        assert.assertThat(this.fixture.find('#applink-outgoing-remote-auth-section').text(),__.isEmpty());

        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());

        //should remain empty (templates not called, because applink metadata not available)
        assert.assertThat(this.fixture.find('#applink-outgoing-remote-auth-section').text(), __.isEmpty());
    });

    QUnit.test('Status rendered if applink data is loaded', function(assert, ApplinksEdit, ApplinkModel, mocks) {
        ApplinksEdit.init();

        this.setUpStatus(createStatusWorking(), mocks);

        //should be empty (templates not called because neither applink status data, nor applink metadata (name, url etc)
        //  has been retrieved)
        assert.assertThat(this.fixture.find('#applink-outgoing-remote-auth-section').text(), __.isEmpty());

        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());

        //should be populated, because applink metadata is available and applink status has been retrieved
        assert.assertThat(this.fixture.find('#applink-outgoing-remote-auth-section').text(), __.not(__.isEmpty()));
    });

    function testOAuthTokenMissing(self, assert, ApplinksEdit, mocks, applinkStatusErrors) {
        // set up status
        var status = createStatusError(applinkStatusErrors);
        delete status.remoteAuthentication;
        self.setUpStatus(status, mocks);
        // set up extra data required to render remote authentication
        mocks.ApplinkObject.data = {
            iconUri: 'jira-icon',
            configUrl: mocks.ApplinkObject.rpcUrl
        };

        initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, mocks.ApplinkObject.status);

        var remoteIncoming = parseDataFromHtml(assert, self.fixture.find("#applink-incoming-remote-auth-section"));
        var remoteOutgoing = parseDataFromHtml(assert, self.fixture.find("#applink-outgoing-remote-auth-section"));
        assertRemoteAuthentication(assert, remoteIncoming, '');
        assertRemoteAuthentication(assert, remoteOutgoing, '');

        assertArrows(assert, self.fixture, Arrows.RIGHT_DISABLED, Arrows.LEFT_DISABLED);

        // There is no remote oauth info - inline dialog is not rendered in this scenario
        assert.assertThat(mocks.InlineDialog, __.notCalled());

        assert.assertThat(mocks.ApplinkStatus.View, __.calledWithNew());
        assert.assertThat(mocks.ApplinkStatus.View, __.calledWith(__.allOf(
            __.hasProperty('applink', applinkWithStatusError(applinkStatusErrors)),
            __.hasProperty('clickable', __.undefined()) // should not be specified so it defaults to true
        )));
    }

    function assertRemoteAuthentication(assert, authentication, expectedAuthLevel) {
        assert.assertThat(authentication.applink.name, __.is('Test applink'));
        assert.assertThat(authentication.applicationType, __.is('JIRA')); // from mocks.ApplinkProducts.getTypeName
        assert.assertThat(authentication.authLevelName, __.is(expectedAuthLevel));
        // extra data required to render
        assert.assertThat(authentication.applink.data.configUrl, __.is('http://test-jira.com'));
        assert.assertThat(authentication.applink.data.iconUri, __.is('jira-icon'));
    }

    function assertArrows(assert, fixture, expectedOutgoingArrow, expectedIncomingArrow) {
        assert.assertThat(fixture.find('#applink-outgoing-auth-arrow .v3-connectivity-arrow').attr('data-src'), __.is(expectedOutgoingArrow));
        assert.assertThat(fixture.find('#applink-incoming-auth-arrow .v3-connectivity-arrow').attr('data-src'), __.is(expectedIncomingArrow));
    }

    function assertArrowsMismatchShouldHaveInlineDialogRendered(assert, mocks) {
        assert.assertThat(mocks.InlineDialog, __.calledTwice());
        assert.assertThat(mocks.InlineDialog.firstCall.args[1], __.is(getInlineDialogExpectedContent('right')));
        assert.assertThat(mocks.InlineDialog.secondCall.args[1], __.is(getInlineDialogExpectedContent('left')));
    }

    function getInlineDialogExpectedContent(direction) {
        return '<span id="oauth-mismatch-'+direction+'-btn">applinks_admin</span>';
    }

    function createStatusWorking() {
        return _createStatus(undefined, _createDefaultAuth(), _createDefaultAuth());
    }

    function createStatusError(error) {
        return _createStatus(error, _createDefaultAuth(), null);
    }

    function createStatusMismatchError(ApplinkStatusErrors) {
        var defaultOAuth = _createOAuthConfig(true, true, false);
        var allEnabled = _createOAuthConfig(true, true, true);

        return _createStatus(ApplinkStatusErrors.AUTH_LEVEL_MISMATCH,
                                _createAuth(defaultOAuth, defaultOAuth),
                                _createAuth(allEnabled, allEnabled));
    }

    function createStatusMismatchErrorForDisabledOAuth() {
        var disabledOAuth = _createOAuthConfig(false, false, false);
        var disabledAuth = _createAuth(disabledOAuth, disabledOAuth);
        return _createStatus({category: 'DISABLED', type: 'DISABLED'}, disabledAuth, disabledAuth);
    }

    function createApplink() {
        return  {
            id: '1e477f15-564d-4b04-a3ca-542e59c7964e',
            name: 'Test applink',
            displayUrl: 'http://test-jira.com',
            rpcUrl: 'http://test-jira.com',
            type: 'jira'
        }
    }

    function setUpPromise(mockPromise) {
        mockPromise.done.returns(mockPromise);
        mockPromise.fail.returns(mockPromise);
        mockPromise.always.returns(mockPromise);
    }

    function parseDataFromHtml(assert, element) {
        var dataText = element.text();
        try {
            return JSON.parse(dataText);
        } catch(error) {
            assert.fail('Failed to parse JSON text "' + dataText + '": ' + error);
        }
    }

    function applinkWithStatusError(expectedError) {
        return ApplinkStatusMatchers.applinkWithStatus(ApplinkStatusMatchers.statusError(expectedError))
    }

    function updateOutgoingOAuthArrow(ApplinksEdit, ApplinkStatus) {
        ApplinksEdit.updateOutgoingOAuthArrow({
            data : {
                statusLoaded: true,
                status: ApplinkStatus
            }
        });
    }

    function updateIncomingOAuthArrow(ApplinksEdit, ApplinkStatus) {
        ApplinksEdit.updateIncomingOAuthArrow({
            data : {
                statusLoaded: true,
                status: ApplinkStatus
            }
        });
    }

    function initApplinksEditAndOAuthArrows(assert, mocks, ApplinksEdit, ApplinkStatus) {
        ApplinksEdit.init();
        MockCallbacks.runCallbacks(assert, mocks.fetchPromise.done);
        updateOutgoingOAuthArrow(ApplinksEdit, ApplinkStatus);
        updateIncomingOAuthArrow(ApplinksEdit, ApplinkStatus);
    }

    function defaultSaveOptions() {
        // default save options for the applink model: wait == true and expecting status 20x / 400
        return __.allOf(
            __.hasProperty('wait', true),
            __.hasProperty('expectedStatuses', __.contains(__.hasProperty('familyCode', 2), __.hasProperty('code', 400)))
        );
    }

    function _createStatus(error, localAuth, remoteAuth) {
        var status = {
            link: createApplink(),
            working: !error,
            localAuthentication: localAuth,
            remoteAuthentication: remoteAuth
        };
        if (error) {
            status.error = error;
        }
        return status;
    }

    function _createAuth(incoming, outgoing) {
        return {
            incoming: incoming,
            outgoing: outgoing
        }
    }

    function _createDefaultAuth() {
        return {
            incoming: _createDefaultOAuthConfig(),
            outgoing: _createDefaultOAuthConfig()
        }
    }

    function _createDefaultOAuthConfig() {
        return _createOAuthConfig(true, true, false);
    }

    function _createOAuthConfig(enabled, twoLoEnabled, twoLoImpersonationEnabled) {
        return {
            enabled:enabled,
            twoLoEnabled:twoLoEnabled,
            twoLoImpersonationEnabled:twoLoImpersonationEnabled
        };
    }

});

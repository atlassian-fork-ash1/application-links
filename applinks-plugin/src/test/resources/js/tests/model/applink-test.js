define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks',
    'applinks/test/mock-common'
], function(
    $,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks,
    ApplinksCommonMocks
) {

    var MOCK_DONE_HANDLER = 'done',
        MOCK_FAIL_HANDLER = 'fail';

    QUnit.module('applinks/model/applink', {
        require: {
            main: 'applinks/model/applink',
            Backbone: 'applinks/lib/backbone',
            ResponseStatus: 'applinks/common/response-status'
        },
        mocks: {
            main: ApplinksCommonMocks.mockRestModule(QUnit),
            ApplinksProducts: ApplinksCommonMocks.mockProductsModule(QUnit),
            ResponseHandlers: QUnit.moduleMock('applinks/common/response-handlers',
                new MockDeclaration(['done', 'fail'])),
            promise: ApplinksCommonMocks.mockPromiseModule(QUnit)
        },
        beforeEach: function(assert, ApplinkModel, ApplinksRest, mocks) {
            new ApplinksCommonMocks.Stubber(this)
                .setUpRest(ApplinksRest)
                .setUpPromise(mocks.promise);

            mocks.ResponseHandlers.done.returns(MOCK_DONE_HANDLER);
            mocks.ResponseHandlers.fail.returns(MOCK_FAIL_HANDLER);
        }
    });

    QUnit.test('DataKeys should match REST ApplinkData', function(assert, ApplinkModel, ApplinksRest) {
        assert.assertThat(ApplinkModel.DataKeys, __.equalTo(ApplinksRest.V3.ApplinkData))
    });



    // Applink tests
    QUnit.test('Applink should use correct URL without data', function(assert, ApplinkModel, ApplinksRest) {
        ApplinksRest.V3.applink.answer.getUrl.returns('applink_url');

        var url = new ApplinkModel.Applink({id: 'abcdef'}).url();

        assert.assertThat(url, __.is('applink_url'));
        assert.assertThat(ApplinksRest.V3.applink, __.calledOnceWith('abcdef', __.undefined()));
    });

    QUnit.test('Applink should use correct URL with data', function(assert, ApplinkModel, ApplinksRest) {
        ApplinksRest.V3.applink.answer.getUrl.returns('applink_url');

        var url = new ApplinkModel.Applink({id: 'abcdef'}, {dataKeys: [ApplinkModel.DataKeys.CONFIG_URL]}).url();

        assert.assertThat(url, __.is('applink_url'));
        assert.assertThat(ApplinksRest.V3.applink, __.calledOnceWith('abcdef', [ApplinkModel.DataKeys.CONFIG_URL]));
    });

    QUnit.test('Applink.getAdminUrl should use data.configUrl', function(assert, ApplinkModel) {
        var applink = new ApplinkModel.Applink({id: 'abcdef'});
        applink.set('data', {configUrl: 'config_url'});

        var url = applink.getAdminUrl();

        assert.assertThat(url, __.is('config_url'));
    });

    QUnit.test('Applink.getAdminUrl should handle missing data', function(assert, ApplinkModel) {
        var applink = new ApplinkModel.Applink({id: 'abcdef'});

        var url = applink.getAdminUrl();

        assert.assertThat(url, __.falsy());
    });

    QUnit.test('Applink.getTypeName', function(assert, ApplinkModel) {
        var applink = new ApplinkModel.Applink({id: 'abcdef', type: 'jira'});

        var typeName = applink.getTypeName();

        assert.assertThat(typeName, __.equalTo('JIRA'));
    });

    QUnit.test('Applink.fetch should add default response handlers given undefined options',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Model.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            new ApplinkModel.Applink({id: 'abcdef'}).fetch();

            assert.assertThat(parentFetch, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.fetch should add default response handlers given options without expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Model.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            // no expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).fetch({wait: true});

            assert.assertThat(parentFetch, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.fetch should add custom response handlers given options with expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Model.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            // options contain expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).fetch({
                wait: true,
                expectedStatuses: [this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]
            });

            assert.assertThat(parentFetch, __.calledOnce());
            // custom handlers called
            assert.assertThat(mocks.ResponseHandlers.done,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.ResponseHandlers.fail,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.save should add default response handlers given undefined options',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentSave = this.sandbox.stub(this.Backbone.Model.prototype, 'save');
            parentSave.returns(mocks.promise);

            new ApplinkModel.Applink({id: 'abcdef'}).save({name: 'New name'});

            assert.assertThat(parentSave, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.save should add default response handlers given options without expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentSave = this.sandbox.stub(this.Backbone.Model.prototype, 'save');
            parentSave.returns(mocks.promise);

            // no expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).save({name: 'New name'}, {wait: true});

            assert.assertThat(parentSave, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.save should add custom response handlers given options with expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentSave = this.sandbox.stub(this.Backbone.Model.prototype, 'save');
            parentSave.returns(mocks.promise);

            // options contain expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).save({name: 'New name'}, {
                wait: true,
                expectedStatuses: [this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]
            });

            assert.assertThat(parentSave, __.calledOnce());
            // custom handlers called
            assert.assertThat(mocks.ResponseHandlers.done,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.ResponseHandlers.fail,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });


    QUnit.test('Applink.destroy should add default response handlers given undefined options',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentDestroy = this.sandbox.stub(this.Backbone.Model.prototype, 'destroy');
            parentDestroy.returns(mocks.promise);

            new ApplinkModel.Applink({id: 'abcdef'}).destroy();

            assert.assertThat(parentDestroy, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.destroy should add default response handlers given options without expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentDestroy = this.sandbox.stub(this.Backbone.Model.prototype, 'destroy');
            parentDestroy.returns(mocks.promise);

            // no expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).destroy({wait: true});

            assert.assertThat(parentDestroy, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.destroy should add custom response handlers given options with expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentDestroy = this.sandbox.stub(this.Backbone.Model.prototype, 'destroy');
            parentDestroy.returns(mocks.promise);

            // options contain expectedStatuses
            new ApplinkModel.Applink({id: 'abcdef'}).destroy({
                wait: true,
                expectedStatuses: [this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]
            });

            assert.assertThat(parentDestroy, __.calledOnce());
            // custom handlers called
            assert.assertThat(mocks.ResponseHandlers.done,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.ResponseHandlers.fail,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Applink.makePrimary should save with primary flag set to true',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentSave = this.sandbox.stub(this.Backbone.Model.prototype, 'save');
            parentSave.returns(mocks.promise);

            var applink = new ApplinkModel.Applink({id: 'abcdef', primary: false, name: 'Test applink'});
            var promise = applink.makePrimary();

            assert.assertThat(promise, __.equalTo(mocks.promise));
            assert.assertThat(parentSave,
                __.calledOnceWith(__.hasProperty('primary', true), __.hasProperty('wait', true)));
        });

    QUnit.test('Applink.makePrimary with custom options',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentSave = this.sandbox.stub(this.Backbone.Model.prototype, 'save');
            parentSave.returns(mocks.promise);

            var applink = new ApplinkModel.Applink({id: 'abcdef', primary: false, name: 'Test applink'});
            var promise = applink.makePrimary({foo: 'bar'});

            assert.assertThat(promise, __.equalTo(mocks.promise));
            assert.assertThat(parentSave, __.calledOnceWith(__.hasProperty('primary', true),
                __.hasProperties({wait: true, foo: 'bar'})));
        });

    QUnit.test('Applink.fetchStatus given successful status fetch from server',
        function(assert, ApplinkModel, ApplinksRest) {
            var statusGetPromise = ApplinksRest.V3.status.answer.get.answer;

            var applink = new ApplinkModel.Applink({id: 'abcdef', statusLoaded: true, status: 'Original status'});
            var promise = applink.fetchStatus();

            assert.assertThat(promise, __.equalTo(statusGetPromise));
            // status should be reset at the beginning of the request
            assert.assertThat(applink.get('statusLoaded'), __.falsy());
            assert.assertThat(applink.get('status'), __.is(null));

            // run success callback and make sure status is updated
            MockCallbacks.runCallback(assert, promise.done, ['New status']);
            assert.assertThat(applink.get('statusLoaded'), __.truthy());
            assert.assertThat(applink.get('status'), __.is('New status'));
        });




    // Collection tests

    QUnit.test('Collection should use correct URL without data', function(assert, ApplinkModel, ApplinksRest) {
        ApplinksRest.V3.applinks.answer.getUrl.returns('applinks_url');

        var url = new ApplinkModel.Collection().url();

        assert.assertThat(url, __.is('applinks_url'));
        assert.assertThat(ApplinksRest.V3.applinks, __.calledOnceWith(__.undefined()));
    });

    QUnit.test('Collection should use correct URL with data', function(assert, ApplinkModel, ApplinksRest) {
        ApplinksRest.V3.applinks.answer.getUrl.returns('applinks_url');

        var url = new ApplinkModel.Collection(null, {dataKeys: [ApplinkModel.DataKeys.CONFIG_URL]}).url();

        assert.assertThat(url, __.is('applinks_url'));
        assert.assertThat(ApplinksRest.V3.applinks, __.calledOnceWith([ApplinkModel.DataKeys.CONFIG_URL]));
    });

    QUnit.test('Collection.fetch should add default response handlers given undefined options',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Collection.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            new ApplinkModel.Collection().fetch();

            assert.assertThat(parentFetch, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Collection.fetch should add default response handlers given options without expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Collection.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            // no expectedStatuses
            new ApplinkModel.Collection({id: 'abcdef'}).fetch({wait: true});

            assert.assertThat(parentFetch, __.calledOnce());
            // default handlers called
            assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith([this.ResponseStatus.Family.SUCCESSFUL]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });

    QUnit.test('Collection.fetch should add custom response handlers given options with expectedStatuses',
        function(assert, ApplinkModel, ApplinksRest, mocks) {
            var parentFetch = this.sandbox.stub(this.Backbone.Collection.prototype, 'fetch');
            parentFetch.returns(mocks.promise);

            // options contain expectedStatuses
            new ApplinkModel.Collection({id: 'abcdef'}).fetch({
                wait: true,
                expectedStatuses: [this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]
            });

            assert.assertThat(parentFetch, __.calledOnce());
            // custom handlers called
            assert.assertThat(mocks.ResponseHandlers.done,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.ResponseHandlers.fail,
                __.calledOnceWith([this.ResponseStatus.OK, this.ResponseStatus.NOT_FOUND]));
            assert.assertThat(mocks.promise.done, __.calledOnceWith(MOCK_DONE_HANDLER));
            assert.assertThat(mocks.promise.fail, __.calledOnceWith(MOCK_FAIL_HANDLER));
        });
});
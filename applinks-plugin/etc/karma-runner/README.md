Karma server configuration for Applinks
---------------------------------------
---------------------------------------

Prerequisites
-------------

npm

Commands
--------

`npm install` to install (one-off)

`npm test` to run all tests

`npm start` to start the Karma server for interactive debugging and fixing tests


Troubleshooting
---------------

If `npm test` fails on first try after pull, it's likely that either node dependencies changed, or your `target` 
contents are out of date. First try removing contents of `target` and re-running the tests, if that does not help, run
`npm install` again.